<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class error
{
    private static $instance;

    protected $maintenance_file;
    protected $path;

    public function __construct($path, $maintenance_file = "maintenance.php")
    {
        $this->maintenance_file = $maintenance_file;
        $this->path             = $path;
    }

    public static function singleton($path = "", $maintenance_file = "maintenance.php")
    {
        if(!isset(self::$instance))
        { // Wenn es noch keine Instanz der Klasse gibt
            $className      = __CLASS__; // Hole Klassennamen
            self::$instance = new $className($path, $maintenance_file); // Erzeuge Klasse und speicher Instanz
        }
        return self::$instance; // Gebe Instanz zurück, wenn bereits existiert
    }

    public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public function throw_error($error_type)
    {
        // Die Variable $error_type wird im maintenance_file verarbeitet
        include $this->path.$this->maintenance_file;
        die(); // Beende Programmausführung
    }
}
