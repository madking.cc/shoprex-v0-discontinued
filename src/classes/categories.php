<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class categories
{
    protected $Cdb;
    protected $Cpage;
    protected $Carticles;
    protected $Atrail;
    protected $Alastentries;
    public $Acategories;

    public function __construct($Cdb, $Cpage, $Carticles)
    {
        $this->Cdb         = $Cdb;
        $this->Cpage       = $Cpage;
        $this->Carticles   = $Carticles;
        $this->Acategories = array();
        $this->Atrail      = array();
    }

    // Hole alle Kategorien in array()
    public function get_all($start_id)
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$start_id;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $cat_field  = $cat_result->fetch_assoc();

        if(!empty($cat_field['dir']))
        {
            $dir = unserialize($cat_field['dir']);
            foreach($dir as $key => $value)
            {
                if($value['type'] == "C")
                {
                    $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =".$value['id']." AND `status` =1 AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0);";
                    $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $cat_field  = $cat_result->fetch_assoc();
                    $this->Acategories[$start_id][$value['id']] = $cat_field;
                    $this->get_all($value['id']);
                }
            }
        }
        return;
    }

    public function check_status($category)
    {
        if(sizeof($this->Acategories) == 0)
        {
            $start_id = $this->Carticles->get_dir_root();
            $this->get_all($start_id);
        }
        if(!isset($this->Acategories[$category]))
        {
            return FALSE;
        }
        return TRUE;
    }

    public function get_trail($category, $start_id, $i = 0)
    {
        if($category != 0)
        {
            $dir        = array();
            $tmp        = array();
            $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = $category;";
            $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Adb        = $cat_result->fetch_assoc();

            $tmp['id']   = $category;
            $tmp['type'] = "C";
            if($Adb['status'] == 0)
            {
                return;
            }
            array_push($this->Atrail, $tmp);

            $dir = unserialize($Adb['dir']);
            if((!empty($dir)) && ($i == 0))
            {
                foreach($dir as $key => $value)
                {
                    if(($value['type'] == "A") || ($value['type'] == "Ac"))
                    {
                        $tmp['id']   = $value['id'];
                        $tmp['type'] = "A";
                        array_push($this->Atrail, $tmp);
                        continue;
                    }
                    if($value['type'] == "C")
                    {
                        $tmp['id']   = $value['id'];
                        $tmp['type'] = "C";
                        array_push($this->Atrail, $tmp);
                    }
                }
            }
            $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
            $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            while($Adb = $cat_result->fetch_assoc())
            {
                if($Adb['id'] == $start_id)
                {
                    continue;
                }
                $dir = unserialize($Adb['dir']);
                if(!empty($dir))
                {
                    foreach($dir as $key => $value)
                    {
                        if(($value['id'] == $category) && ($value['type'] == "C"))
                        {
                            $sql         = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =".$Adb['id'].";";
                            $cat_result2 = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                            $Adb2        = $cat_result2->fetch_assoc();
                            $dir2        = unserialize($Adb['dir']);
                            if(!empty($dir2))
                            {
                                foreach($dir2 as $key2 => $value2)
                                {
                                    if($value2['type'] == "C")
                                    {
                                        $tmp['id']   = $value2['id'];
                                        $tmp['type'] = "C";
                                        array_push($this->Atrail, $tmp);
                                    }
                                }
                            }
                            $i++;
                            $this->get_trail($Adb2['id'], $start_id, $i);
                            break 2;
                        }
                    }
                }
            }
        }
        return;
    }

    public function get_last_entries()
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Adb = $cat_result->fetch_assoc())
        {
            $this->Alastentries[$Adb['id']] = FALSE;
            $dir                            = unserialize($Adb['dir']);
            if(empty($dir) || (sizeof($dir) == 0))
            {
                $this->Alastentries[$Adb['id']] = TRUE;
                continue;
            }
            else
            {
                $match = FALSE;
                foreach($dir as $key => $value)
                {
                    if(($value['type'] == "C"))
                    {
                        $match = TRUE;
                        break;
                    }
                }
                if(!$match)
                {
                    $this->Alastentries[$Adb['id']] = TRUE;
                }
            }
        }
    }

    public function create_menu($start_id, $selected_cat, $i = 0)
    {
        $content = "";

        if(isset($this->Acategories[$start_id]))
        {
            if($i > 0)
            {
                $content .= "  <ul>\n";
            }
            foreach($this->Acategories[$start_id] as $key => $array_top)
            {
                if($this->Carticles->check_disabled($key)) continue;
                $show = $this->check_trail($key, "C");
                $content .= " <li class='";
                if(($key != $start_id) && ($this->check_last_entry($key)))
                {
                    $content .= "last_";
                }
                $content .= "category_";
                if($i == 0)
                {
                    $content .= "show";
                }
                elseif($show)
                {
                    $content .= "show";
                }
                else $content .= "hide";
                $content .= "'";

                $id_tag = "";
                if(strcmp($array_top['id'], $selected_cat) == 0)
                {
                    $id_tag = "selected_cat";
                }

                $content .= ">";

                $tmp = "";
                if(SHOW_PICTURE_CATEGORY_MENU)
                {
                    $tmp .= $this->Cpage->img(UPLOAD_DIR."tiny/".$array_top['picture'], $array_top['picture_text'])." ";
                }
                $tmp .= $array_top['name'];
                if(SHOW_ARTICLE_COUNT)
                {
                    $count = $this->Carticles->get_article_count_to_id($array_top['id']);
                    if(!SHOW_EMPTY_ARTICLE_COUNT && ($count == 0))
                    {
                        $tmp .= "";
                    }
                    else $tmp .= " (".$count.")";
                }
                $content .= $this->Cpage->link_id($tmp, "index.php", "category=".$key."#output", $array_top['style'], $id_tag);
                $content .= "  \n";
                $i++;
                $content .= $this->create_menu($key, $selected_cat, $i);
                $i--;
                //$content .= " </li>\n";
            }
            if($i > 0)
            {
                $content .= "  </li></ul>\n";
            }
        }
        return $content;
    }

    protected function cmp_category_list($a, $b)
    {
        // Wenn $a großer $b, Rückgabe 1
        // Wenn $a kleiner $b, Rückgabe -1
        // Wenn $a gleich $b, Rückgabe 0
        if($a['position'] == $b['position'])
        {
            return strcmp($a['name'], $b['name']);
        }
        if($a['position'] > $b['position'])
        {
            return 1;
        }
        if($a['position'] < $b['position'])
        {
            return -1;
        }
    }

    public function do_menu($start_id, $selected_cat)
    {
        // Hole die Kategorien
        if(sizeof($this->Acategories) == 0)
        {
            $this->get_all($start_id);
        }

        // Sortiere Kategorien Array:
        if(!empty($this->Acategories))
        {
            foreach($this->Acategories as $key_above_id => $value)
                uasort($this->Acategories[$key_above_id], array($this, 'cmp_category_list'));
        }

        unset($this->Atrail);
        $this->Atrail = array();
        $this->get_trail($selected_cat, $start_id);
        $this->get_last_entries();
        return $this->create_menu($start_id, $selected_cat);
    }

    public function check_trail($id, $type)
    {
        if(isset($this->Atrail))
        {
            foreach($this->Atrail as $key => $value)
                if(($value['id'] == $id) && ($value['type'] == $type))
                {
                    return TRUE;
                }
        }
        return FALSE;
    }

    public function check_last_entry($id)
    {
        return $this->Alastentries[$id];
    }

    public function get_subcategories_to_id($category_id)
    {
        $Acategories = array();

        $sql        = "SELECT dir FROM `".TBL_PREFIX."categories` WHERE `id` =$category_id;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $cat_field  = $cat_result->fetch_assoc();

        if(!empty($cat_field['dir']))
        {
            $dir = unserialize($cat_field['dir']);
            foreach($dir as $value)
            {
                if($value['type'] == "C")
                {
                    $sql               = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = ".$value['id']." AND `status` = 1  AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0);";
                    $categories_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    if($categories_result->num_rows == 1)
                    {
                        $categories_field = $categories_result->fetch_assoc();
                        if(empty($categories_field['picture']))
                        {
                            $categories_field['picture']      = NO_PICTURE;
                            $categories_field['picture_text'] = NO_PICTURE_TEXT;
                        }
                        array_push($Acategories, $categories_field);
                    }

                }
            }
        }
        else return NULL;
        return $Acategories;
    }

    public function get_category($category_id)
    {
        // Prüfe, ob Kategorie in einem deaktivieren Kategorie Strang
        if((empty($this->Carticles->Acategories)) && (empty($this->Carticles->Aarticles)))
        {
            $this->Carticles->fill_category_article_array($this->Carticles->start_id);
        }
        if($this->Carticles->check_disabled($category_id))
        {
            return NULL;
        }

        $sql               = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$category_id AND `status` = 1  AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0);";
        $categories_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);

        if($categories_result->num_rows == 1)
        {
            $categories_field = $categories_result->fetch_assoc();
            if(empty($categories_field['picture']))
            {
                $categories_field['picture']      = NO_PICTURE;
                $categories_field['picture_text'] = NO_PICTURE_TEXT;
            }
            return $categories_field;
        }
        else return NULL;
    }
}