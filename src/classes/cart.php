<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class cart
{
    private $Cpage;
    private $Cdb;
    private $Carticles;

    public function __construct($Cpage, $Cdb, $Carticles)
    {
        $this->Cpage     = $Cpage; // Allgemeines Seiten Framework
        $this->Cdb       = $Cdb; // Datenkbankanbindung
        $this->Carticles = $Carticles; // Artikel Framework
    }

    protected function prepare_cart_article($Aarticle)
    {
        // Prüfen, ob Artikel bereits hinzugefügt
        if(!isset($_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]))
        {
            $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']] = array();
            $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'] = 0;
            $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['netto']    = $Aarticle['netto'];
            $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['tax']      = $Aarticle['tax'];
        }
    }

    public function add_article($Aarticle, $quantity)
    {
        // Artikel werden in einer Session namens 'cart' gespeichert
        // Jeder Artikel hat eine eindeutige ID $Aarticle['id']
        // in Aarticle sind alle Eigenschaften des Artikels gespeichert, wie Menge, Netto und MwSt. Satz

        global $script;
        $content = 0;


        // Extra, weil Artikel ja bereits hinzugefügt sein kann, mit Lagerabfrage
        if(isset($Aarticle['attribute_quantity_warning']))
        {
            $article_quantity_warning = $Aarticle['attribute_quantity_warning'];
            $article_quantity = $Aarticle['attribute_quantity'] - $Aarticle['attribute_reserved_quantity'];
        }
        else
        {
            $article_quantity_warning = $Aarticle['quantity_warning'];
            $article_quantity = $Aarticle['quantity'] - $Aarticle['reserved_quantity'];
        }

        if(empty($article_quantity_warning))
        {
            $this->prepare_cart_article($Aarticle);
            $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'] += $quantity;
        }
        else
        {
            if((!isset($_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'])) && ($article_quantity >= $quantity))
            {
                $this->prepare_cart_article($Aarticle);
                $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'] += $quantity;
            }
            else
            {
                if(isset($_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity']))
                {
                    $tmp = $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'] + $quantity;
                    if($tmp > $article_quantity)
                    {
                        $script = $this->Cpage->alert(ARTICLE_OUT_OF_STOCK_01." ".$article_quantity." ".ARTICLE_OUT_OF_STOCK_02);
                    }
                    else
                    {

                        $_SESSION['cart'][$Aarticle['original_id']][$Aarticle['attribute_id']]['quantity'] += $quantity;
                    }
                }
                else $script = $this->Cpage->alert(ARTICLE_OUT_OF_STOCK_01." ".$article_quantity." ".ARTICLE_OUT_OF_STOCK_02);
            }

        }
        return $content;
    }

    // Hole den gesamten Warenkorbpreis, ohne die Versandkosten
    public function get_price($type = "brutto")
    {
        $price = 0;
        if(!empty($_SESSION['cart']))
        {
            foreach($_SESSION['cart'] as $id => $value1)
            {
                foreach($value1 as $attribute_id => $value2)
                {
                    if(isset($_SESSION['cart'][$id][$attribute_id]['netto']))
                    {
                        if($type == "netto")
                        {
                            $price += $_SESSION['cart'][$id][$attribute_id]['quantity']*$_SESSION['cart'][$id][$attribute_id]['netto'];
                        }
                        elseif($type == "brutto")
                        {
                            $price += $_SESSION['cart'][$id][$attribute_id]['quantity']*$this->Cpage->tax_calc($_SESSION['cart'][$id][$attribute_id]['netto'], "brutto", $this->Cpage->Aglobal['tax'][$_SESSION['cart'][$id][$attribute_id]['tax']]);
                        }
                    }
                }
            }
        }
        if(isset($_SESSION['coupon']))
        {
            if($type == "netto")
            {
                $price -= $_SESSION['coupon']['netto'];
            }
            elseif($type == "brutto")
            {
                $price -= $this->Cpage->tax_calc($_SESSION['coupon']['netto'], "brutto", $_SESSION['coupon']['tax']);
            }
        }

        return $price;
    }


    public function get_quantity()
    {
        $counter = 0;

        if(!isset($_SESSION['cart'])) return 0;
        if(sizeof($_SESSION['cart']) > 0)
        {
            foreach($_SESSION['cart'] as $id => $value)
                foreach($value as $attribute_id => $value)
                {
                    $counter += $_SESSION['cart'][$id][$attribute_id]['quantity'];
                }
        }
        else return 0;

        return $counter;


    }


    // Erniedrige einen Artikel um 1 oder anderen Wert oder lösche ihn wenn 0 erreicht, bzw. unerschritten
    public function article_down($id, $attribute_id, $quantity = 1)
    {
        $_SESSION['cart'][$id][$attribute_id]['quantity'] -= $quantity;
        if($_SESSION['cart'][$id][$attribute_id]['quantity'] <= 0)
        {
            unset($_SESSION['cart'][$id][$attribute_id]);
        }
    }

    // Erhöhe Artikel um 1 oder anderen Wert
    public function article_up($id, $attribute_id, $quantity = 1)
    {
        $content = "";
        $Aarticle = $this->Carticles->get_article($id, $attribute_id);
        $content .= $this->add_article($Aarticle, $quantity);
        return $content;
        //$_SESSION['cart'][$id][$attribute_id]['quantity'] += $quantity;
    }

    // Entferne Artikel
    public function article_remove($id, $attribute_id)
    {
        if($id=="0")
            unset($_SESSION['coupon']);
        else
            unset($_SESSION['cart'][$id][$attribute_id]);
    }

    // Entferne alle Artikel
    public function remove_all()
    {
        unset($_SESSION['cart']);
        $_SESSION['cart'] = array();
    }

    // Prüfung, ob Warenkorb leer
    public function is_empty()
    {
        if(empty($_SESSION['cart']))
        {
            return TRUE;
        }
        else return FALSE;
    }

    // Berechne den Warenkorb mit Versandkosten
    public function get_cart($delivery_country, $delivery_type)
    {
        $i                        = 0; // Wichtiger Zähler!
        $shipping_tax_percent     = $this->Cpage->Aglobal['tax'][SHIPPING_TAX_KEY];
        $cod_shipping_tax_percent = $this->Cpage->Aglobal['tax'][COD_TAX_KEY];
        $shipping_error           = 0; // Können Versandkosten berechnet werden?
        $cod_shipping_error       = 0; // Können Nachnahmekosten berechnet werden?
        $total_weight             = 0; // Gesamt Gewicht
        $shipping_cost_netto      = 0;
        $shipping_cost_brutto     = 0; // Versandkosten
        $cod_shipping_cost_netto  = 0;
        $cod_shipping_cost_brutto = 0;
        $shipping_tax             = 0;
        $cod_shipping_tax         = 0;
        $cart_price_netto         = 0;
        $cart_price_brutto        = 0;
        $entire_quantity          = 0;
        $positions                = 0;
        $Acart                    = array();
        $Atax                     = array(); // Mehrwertsteuer
        foreach($this->Cpage->Aglobal['tax'] as $key => $value)
            $Atax[$value] = 0;

        if(!empty($_SESSION['cart']))
        {
            // Gehe den gesamten Warenkorb durch
            foreach($_SESSION['cart'] as $id => $value2)
            {
                foreach($value2 as $attribute_id => $value)
                {
                    $quantity = $value['quantity'];

                    // Hole die Daten zur Artikel ID
                    $Aarticle = $this->Carticles->get_article($id, $attribute_id);

                    $Acart[$i]             = $Aarticle;
                    $Acart[$i]['id']       = $id;
                    $Acart[$i]['original_id']       = $Aarticle['original_id'];
                    $Acart[$i]['tax']      = $this->Cpage->Aglobal['tax'][$Aarticle['tax']]; // Überschreibe MwSt.
                    $Acart[$i]['quantity'] = $quantity; // Speichere Artikel Anzahl
                    $Acart[$i]['attribute_id'] = $attribute_id;
                    $entire_quantity += $quantity; // Zähle Gesamtzahl mit

                    // Falls ein Attribut gewählt wurde, dessen Artikelnummer speichern
                    if($attribute_id > 0)
                    {
                        $sql = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE id=".$attribute_id;
                        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                        if($result->num_rows > 0)
                        {
                            $row = $result->fetch_assoc();
                            $Acart[$i]['number'] = $row['number'];
                        }
                    }


                    // Zähle den Gesamt Nettopreis der Position
                    $Acart[$i]['total_netto'] = $quantity*$Aarticle['netto'];

                    // Zähle Gesamtgewicht des Warenkorbes mit
                    $total_weight += ($quantity*$Aarticle['weight']);
                    // Speichere Gesamtgewicht der Position
                    $Acart[$i]['total_weight'] = ($quantity*$Aarticle['weight']);

                    // Zähle den Gesamt Bruttopreis des Warenkorbs
                    $cart_price_brutto += $this->Cpage->tax_calc($Acart[$i]['total_netto'], "brutto", $Acart[$i]['tax']);
                    // Zähle den Gesamt Nettopreis des Warenkorbs
                    $cart_price_netto += $Acart[$i]['total_netto'];
                    // Zähle zu den zugehören Mehrwertsteuernsätzen (19% oder 7%) die gesamt Mehrwertsteuer des Warenkorbs
                    $Atax[$Acart[$i]['tax']] += $this->Cpage->tax_calc($Acart[$i]['total_netto'], "tax_of_netto", $Acart[$i]['tax']);

                    $i++; // Erhöhe den Zähler
                }
            }
            $positions = $i;


            if(DELIVERY_METHOD == "free")
            {
                // Nothing to do
            }
            elseif((DELIVERY_METHOD == "flatrate") && ($delivery_type != NO_SHIPPING_COST))
            {
                $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate`
	                    WHERE `targets` LIKE '%".$delivery_country."%'
	                          AND
	                          `type` Like 'normal'
	                          LIMIT 1;";
                $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                {
                    // Wenn Land nicht gefunden:
                    $shipping_cost_netto = "0";
                    $shipping_error      = 1; // Versandkosten können nicht berechnet werden!
                }
                else // Hole die Netto Kosten für den Versand, wenn Ergebnis gefunden
                {
                    // Wenn das Land gefunden:
                    $Aweight             = $result->fetch_assoc();
                    $shipping_cost_netto = $Aweight['cost_netto'];
                }
            }
            // Berechne Versandkosten nach Gewicht:
            // Wenn vom Kunden Versand gewählt
            elseif((DELIVERY_METHOD == "weight") && ($delivery_type != NO_SHIPPING_COST))
            {
                $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_weight`
	                    WHERE `targets` LIKE '%".$delivery_country."%'
	                          AND 
	                          `type` Like 'normal' 
	                          LIMIT 1;";
                $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                {
                    // Wenn Land nicht gefunden:
                    $shipping_cost_netto = "0";
                    $shipping_error      = 1; // Versandkosten können nicht berechnet werden!
                }
                else // Hole die Netto Kosten für den Versand, wenn Ergebnis gefunden
                {
                    // Wenn das Land gefunden:
                    $Aweight  = $result->fetch_assoc();
                    $Aweights = unserialize($Aweight['weight']);
                    array_unshift($Aweights, "0");
                    $Aweights_netto = unserialize($Aweight['cost_netto']);

                    $match = FALSE;
                    for($j = 0; $j < (sizeof($Aweights)-1); $j++)
                    {
                        if(($Aweights[$j] < $total_weight) && ($Aweights[$j+1] >= $total_weight))
                        {
                            if($Aweights[$j+1] == 0)
                            {
                                break;
                            }
                            $match               = TRUE;
                            $shipping_cost_netto = $Aweights_netto[$j];
                            break;
                        }
                    }
                    if(!$match)
                    {
                        $shipping_error = 1;
                    }
                }
            }
            // Falls per Nachnahme gewünscht, die zusätzlichen Nachnahmengebühren berechnen:
            if((DELIVERY_METHOD == "weight") && ($delivery_type == COD_SHIPPING_COST))
            {
                $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_weight`
	                    WHERE `targets` LIKE '%".$delivery_country."%'
	                          AND 
	                          `type` Like 'cod' 
	                          LIMIT 1;";
                $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                {
                    $shipping_cost_netto = "0";
                    $cod_shipping_error  = 1;
                }
                else
                {
                    $Aweight                 = $result->fetch_assoc();
                    $cod_shipping_cost_netto = $Aweight['cost_netto'];
                }
            }
            if((DELIVERY_METHOD == "flatrate") && ($delivery_type == COD_SHIPPING_COST))
            {
                $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate`
	                    WHERE `targets` LIKE '%".$delivery_country."%'
	                          AND
	                          `type` Like 'cod'
	                          LIMIT 1;";
                $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                {
                    $shipping_cost_netto = "0";
                    $cod_shipping_error  = 1;
                }
                else
                {
                    $Aflatrate               = $result->fetch_assoc();
                    $cod_shipping_cost_netto = $Aflatrate['cost_netto'];
                }
            }

            if(!$shipping_error && ($shipping_cost_netto != 0))
            {
                // Berechne die Höhe der Mehrwertsteuer für Versand:
                $shipping_tax = $this->Cpage->tax_calc($shipping_cost_netto, "tax_of_netto", $shipping_tax_percent);
                // Berechne Brutto Versandkosten
                $shipping_cost_brutto = $this->Cpage->tax_calc($shipping_cost_netto, "brutto", $shipping_tax_percent);
                // Füge die Steuer der Versandkosten den gesamten Steuern hinzu:
                $Atax[$this->Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]] += $shipping_tax;
            }
            if(!$cod_shipping_error && (!$shipping_error) && ($cod_shipping_cost_netto != 0))
            {
                $cod_shipping_tax = $this->Cpage->tax_calc($cod_shipping_cost_netto, "tax_of_netto", $cod_shipping_tax_percent);
                // Berechne Brutto Nachnahme kosten
                $cod_shipping_cost_brutto = $this->Cpage->tax_calc($cod_shipping_cost_netto, "brutto", $cod_shipping_tax_percent);
                // Füge die Steuer der Versandkosten den gesamten Steuern hinzu:
                $Atax[$this->Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]] += $cod_shipping_tax;
            }
        }
        // Falls Coupon eingetragen wurde
        if(isset($_SESSION['coupon']))
        {
            $Acart[$i]['name']       = COUPON." ".$_SESSION['coupon']['code'];
            $Acart[$i]['id']       = 0;
            $Acart[$i]['original_id']       = 0;
            $Acart[$i]['tax']      = $_SESSION['coupon']['tax'];
            $Acart[$i]['quantity'] = 1;
            $Acart[$i]['attribute_id'] = 0;
            $Acart[$i]['number'] = "";
            $Acart[$i]['netto'] = -$_SESSION['coupon']['netto'];
            $Acart[$i]['total_netto'] = -$_SESSION['coupon']['netto'];
            $Acart[$i]['total_weight'] = 0;
            $cart_price_brutto += $this->Cpage->tax_calc($Acart[$i]['total_netto'], "brutto", $Acart[$i]['tax']);
            $cart_price_netto += $Acart[$i]['total_netto'];
            $Atax[$Acart[$i]['tax']] += $this->Cpage->tax_calc($Acart[$i]['total_netto'], "tax_of_netto", $Acart[$i]['tax']);

            $i++;
            $positions = $i;
        }

        $Aresult['delivery_country_target'] = $delivery_country;
        $Aresult['delivery_type']           = $delivery_type;
        $Aresult['shipping_error']          = $shipping_error;
        $Aresult['cod_shipping_error']      = $cod_shipping_error;
        $Aresult['total_weight']            = $total_weight;
        $Aresult['entire_quantity']         = $entire_quantity;

        $Aresult['shipping_cost_netto']      = $shipping_cost_netto;
        $Aresult['shipping_cost_brutto']     = $shipping_cost_brutto;
        $Aresult['shipping_tax']             = $shipping_tax;
        $Aresult['shipping_tax_percent']     = $shipping_tax_percent;
        $Aresult['cod_shipping_cost_netto']  = $cod_shipping_cost_netto;
        $Aresult['cod_shipping_cost_brutto'] = $cod_shipping_cost_brutto;
        $Aresult['cod_shipping_tax']         = $cod_shipping_tax;
        $Aresult['cod_shipping_tax_percent'] = $cod_shipping_tax_percent;

        $Aresult['tax']               = $Atax;
        $Aresult['cart_price_netto']  = $cart_price_netto;
        $Aresult['cart_price_brutto'] = $cart_price_brutto;
        $Aresult['cart']              = $Acart;
        $Aresult['positions']         = $positions;

        return $Aresult;
    }
}