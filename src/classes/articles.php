<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class articles
{
    protected $Cdb;
    protected $Cpage;
    protected $article_post_fields;
    protected $category_post_fields;
    protected $do_no_escape;
    protected $do_htmlspecialchars;
    protected $do_sql_time;
    protected $post_defaults;
    protected $Aall_elements;
    protected $Aarticles;
    protected $Acategories;
    protected $menu;
    protected $Atrail;
    protected $Alastentries;
    protected $Adisabled;
    public $start_id;

    public function __construct($Cdb, $Cpage)
    {
        $this->Cdb   = $Cdb;
        $this->Cpage = $Cpage;
        // Für die Admin Oberfläche
        $this->article_post_fields = array("status", "name", "description", "style", "netto", "weight", "text", "picture", "picture_text", "quantity", "quantity_warning", "condition", "guarantee", "visible_from", "visible_to", "auto_number", "discount_from", "discount_to", "number", "top_offer", "keywords", "use_articlename_for_picture", "price_type", "category_target", "price", "brutto", "tax", "discount", "created", "clone_from", "article", "isbn", "link_article_to_add", "old_link_pos", "new_link_pos", "max_link_pos", "delete_link");
        // Für die Admin Oberfläche
        $this->category_post_fields = array("category", "status", "name", "style", "picture", "picture_text", "visible_from", "visible_to", "keywords", "category_target", "clone_from");
        // Für die Admin Oberfläche
        $this->do_no_escape = array("picture", "picture_text", "text");
        // Für die Admin Oberfläche
        $this->do_htmlspecialchars = array("name", "description");
        // Für die Admin Oberfläche
        $this->do_sql_time         = array("discount_to", "discount_from", "visible_to", "visible_from");
        $this->post_defaults       = array("number" => "", "visible_from" => 0, "visible_to" => 0, "discount_from" => 0, "discount_to" => 0, "status" => 1, "clone_from" => 0);
        $this->Aall_elements       = array();
        $this->Aarticles           = array();
        $this->Acategories         = array();
        $this->Atrail              = array();
        $this->Alastentries        = array();
        $this->Adisabled           = array();
        $this->start_id            = $this->get_dir_root();
    }

    public function check_disabled($id)
    {
        if((empty($this->Acategories)) && (empty($this->Aarticles)))
        {
            $this->fill_category_article_array($this->start_id);
        }
        //if(!isset($this->Adisabled[$id])) return FALSE;
        return $this->Adisabled[$id];
    }

    protected function get_time_left($hours)
    {
        $content = "";
        if($hours == 1) $content .= $hours." ".HOUR;
        elseif($hours <= 24) $content .= $hours." ".HOURS;
        else
        {
            $days = floor($hours / 24);
            $dummy = $days * 24;
            $hours = $hours - $dummy;

            if($days == 1)
            {
                $content .= $days." ".DAY;
                if($hours == 1) $content .= " ".$hours." ".HOUR;
                if($hours > 1) $content .= " ".$hours." ".HOURS;
            }
            else
            {
                $content .= $days." ".DAYS;
                if($hours == 1) $content .= " ".$hours." ".HOUR;
                if($hours > 1) $content .= " ".$hours." ".HOURS;
            }


        }
        return $content;
    }

    protected function get_reserved_quantity($id, $attribute_id)
    {
        if(RESERVE_ARTICLE)
        {
            $reserve_time = RESERVE_TIME;
            $reserve_time = $reserve_time * 60 * 60;
            $reserve_time = time() - $reserve_time;
            $reserve_time = date('Y-m-d H:i:s', $reserve_time);

            $sql = "DELETE FROM `".TBL_PREFIX."reserved_orders` WHERE `since` < '$reserve_time'";
            $this->Cdb->db_query($sql, __FILE__.":".__LINE__);

            $sql = "SELECT SUM(quantity) FROM `".TBL_PREFIX."reserved_orders` WHERE `article_id`=$id AND `attribute_id`=$attribute_id
            AND `since` >= '$reserve_time'";
            $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            if($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                return $row['SUM(quantity)'];
            }
            else
            {
                return 0;
            }
        }
        else return 0;



    }

    public function get_article_status($id)
    {
        // 0 = Enabled, 1 = Article disabled, 2 = Article not visible, 3 = Category disabled
        // Prüfe, ob Artikel in einem deaktivieren Kategorie Strang
        if((empty($this->Acategories)) && (empty($this->Aarticles)))
        {
            $this->fill_category_article_array($this->start_id);
        }
        $category_id = $this->get_category_id($id);
        if($this->check_disabled($category_id) && ($category_id != $this->start_id))
        {
            //var_dump($category_id);
            return "3";
        }
        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = $id AND `status` =1;";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 1)
        {
            $Aarticle = $result->fetch_assoc();
            $timestamp = $this->Cpage->get_timestamp($Aarticle['visible_from']);
            if($timestamp > time() && $Aarticle['visible_from'] != 0) return "2";
            $timestamp = $this->Cpage->get_timestamp($Aarticle['visible_to']);
            if($timestamp < time() && $Aarticle['visible_to'] != 0) return "2";
        }
        else return "1";

        return "0";
    }

    public function get_article($id, $attribute_id=0)
    {
        // Prüfe, ob Artikel in einem deaktivieren Kategorie Strang
        if((empty($this->Acategories)) && (empty($this->Aarticles)))
        {
            $this->fill_category_article_array($this->start_id);
        }
        $category_id = $this->get_category_id($id);
        if($this->check_disabled($category_id) && ($category_id != $this->start_id))
        {
            //var_dump($category_id);
            return NULL;
        }

        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = $id AND `status` =1;";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 1)
        {

            $Aarticle = $result->fetch_assoc();
            // Wenn Artikel ein Klon, hole die "echten" Daten
            $Aarticle['original_id'] = intval($Aarticle['id']);
            if($Aarticle['clone_from'] > 0)
            {
                $temp_id         = $Aarticle['id'];
                $temp_clone_from = $Aarticle['clone_from'];
                $temp_position   = $Aarticle['position'];
                $sql             = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$Aarticle['clone_from']." AND `status` =1;";
                $result          = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                {
                    return NULL;
                }
                $Aarticle = $result->fetch_assoc();
                // Prüfe, ob Klon in einem deaktiviertem Kategorie Strang
                $category_id = $this->get_category_id($Aarticle['id']);
                if($this->check_disabled($category_id) && ($category_id != $this->start_id))
                {
                    return NULL;
                }
                $Aarticle['original_id'] = intval($Aarticle['id']);
                $Aarticle['id']         = $temp_id;
                $Aarticle['position']   = $temp_position;
                $Aarticle['clone_from'] = $temp_clone_from;
            } // Klon Abschnitt Ende

            // Falls ein bestimmtes Attribut ausgewählt wurde:
            $Aarticle['attribute_id'] = $attribute_id;

            // Falls ausverkauft
            $Aarticle['sold_out'] = FALSE;


            // Prüfe, ob Attribute vorhanden
            $sql = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE `to_article` =".$Aarticle['original_id']." ORDER BY pos ASC, type ASC;";
            $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            $article_has_attributes = FALSE;
            if($result->num_rows > 1)
            {
                $article_has_attributes = TRUE;
                $i = 0;
                while($row = $result->fetch_assoc())
                {
                    if($attribute_id == $row['id'])
                    {
                        $Aarticle['attribute_quantity'] = $row['size'];
                        $Aarticle['attribute_quantity_warning'] = $row['warning'];
                        $reserved_quantity = $this->get_reserved_quantity($Aarticle['original_id'], $row['id']);
                        $Aarticle['attribute_reserved_quantity'] = $reserved_quantity;
                    }

                    $attribute['id'][$i]        = $row['id'];
                    $attribute['header'][$i]    = $row['header'];
                    $attribute['type'][$i]      = $row['type'];
                    $attribute['size'][$i]      = $row['size'];
                    $attribute['warning'][$i]   = $row['warning'];
                    $attribute['netto'][$i]     = $row['netto'];
                    $attribute['pos'][$i]       = $row['pos'];
                    $attribute['number'][$i]       = $row['number'];
                    $attribute['disabled'][$i] = FALSE;
                    $reserved_quantity = $this->get_reserved_quantity($Aarticle['id'], $row['id']);
                    $attribute['reserved_quantity'][$i] = $reserved_quantity;
                    if($attribute['warning'][$i] > 0)
                        if(($attribute['size'][$i]-$reserved_quantity) <= 0)
                            $attribute['disabled'][$i] = TRUE;
                    $i++;
                }
            }
            $Aarticle['reserved_quantity'] = 0;
            if($article_has_attributes)
            {
                $all_attributes_disabled = TRUE;
                foreach($attribute['disabled'] as $value)
                {
                    if(!$value) $all_attributes_disabled = FALSE;
                }
                if($all_attributes_disabled) $Aarticle['sold_out'] = TRUE;
            } // Wenn keine Attribute vorhanden, prüfe weitere Ausschlusskriterien
            else
            {
                $reserved_quantity = $this->get_reserved_quantity($Aarticle['id'], 0);
                $Aarticle['reserved_quantity'] = $reserved_quantity;
                if(($Aarticle['quantity']-$reserved_quantity) <= 0 && $Aarticle['quantity_warning'] > 0) $Aarticle['sold_out'] = TRUE;;
            }
            $timestamp = $this->Cpage->get_timestamp($Aarticle['visible_from']);
            if($timestamp > time() && $Aarticle['visible_from'] != 0) return NULL;
            $timestamp = $this->Cpage->get_timestamp($Aarticle['visible_to']);
            if($timestamp < time() && $Aarticle['visible_to'] != 0) return NULL;

            if($article_has_attributes)
                $Aarticle['attribute'] = $attribute;
            else
                $Aarticle['attribute'] = FALSE;

            // Lege fest, ob es einen Discount gibt
            $discount_from            = $this->Cpage->set_sqltime($Aarticle['discount_from'], "max");
            $discount_to              = $this->Cpage->set_sqltime($Aarticle['discount_to'], "min");

            $Aarticle['has_discount'] = FALSE;
            if(($discount_from < time()) || ($discount_to > time()))
            {
                $Aarticle['has_discount']  = TRUE;
                $dummy = round(($discount_to-time())/60/60);

                $dummy = $this->get_time_left($dummy);

                $Aarticle['discount_left'] = $dummy;
            }
            elseif ((($Aarticle['discount'] != 0) && ($Aarticle['discount_to'] == 0) && ($Aarticle['discount_from'] == 0)))
            {
                $Aarticle['has_discount']  = TRUE;
                $Aarticle['discount_left'] = NULL;
            }

            @$tmp = unserialize($Aarticle['picture']);
            if($tmp == NULL)
            {
                $Aarticle['picture']         = array();
                $Aarticle['picture_text']    = array();
                $Aarticle['picture'][0]      = NO_PICTURE;
                $Aarticle['picture_text'][0] = NO_PICTURE_TEXT;
            }
            else
            {
                $Aarticle['picture']      = unserialize($Aarticle['picture']);
                $Aarticle['picture_text'] = unserialize($Aarticle['picture_text']);
            }

            $Aarticle['netto_original'] = $Aarticle['netto'];
            $Aarticle['netto_with_attribute'] = $Aarticle['netto'];
            if($attribute_id > 0)
            {
                $sql = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE `id`=".$attribute_id;
                $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                $row = $result->fetch_assoc();

                $Aarticle['netto'] += $row['netto'];
                //var_dump($Aarticle['netto']);
                if(empty($Aarticle['description']))
                    $Aarticle['description'] = $row['type'];
                else
                    $Aarticle['description'] .= "\r\n".$row['type'];

            }
            // Berechne eventuell vorhanden Discount auf brutto Basis
            if($Aarticle['has_discount'])
            {
                $Aarticle['netto_with_attribute'] = $Aarticle['netto'];
                $dummy             = $this->Cpage->tax_calc($Aarticle['netto'], "brutto", $this->Cpage->Aglobal['tax'][$Aarticle['tax']]);
                $dummy             = $this->Cpage->price_discount($dummy, $Aarticle['discount']);
                $dummy             = $this->Cpage->tax_calc($dummy, "netto",  $this->Cpage->Aglobal['tax'][$Aarticle['tax']]);
                $Aarticle['netto'] = $dummy;
                if(empty($Aarticle['description']))
                    $Aarticle['description'] = INCLUSIVE." ".$Aarticle['discount']."% ".WITH_DISCOUNT_02;
                else
                    $Aarticle['description'] .= "\r\n".INCLUSIVE." ".$Aarticle['discount']."% ".WITH_DISCOUNT_02;

            }

            return $Aarticle;
        }
        else
        {
            //var_dump($sql);
            return NULL;
        }
    }

    public function get_articles_select($name, $target = "0", $dont_show = "", $class = "select")
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE clone_from=0 ORDER BY `name` ASC;";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 0)
        {
            return "<span class='information'>Keine Artikel vorhanden</span>";
        }
        $content = "";

        $content .= "<select name='$name' class='$class'>\n <option value='0'>Kein Artikel ausgewählt</option>\n";

        while($Aarticle = $result->fetch_assoc())
        {
            if(is_array($dont_show) && in_array($Aarticle['id'], $dont_show))
                continue;

            $brutto = $this->Cpage->money($this->Cpage->tax_calc($Aarticle['netto'], "brutto", $this->Cpage->Aglobal['tax'][$Aarticle['tax']]));
            $content .= " <option value='".$Aarticle['id']."' style='".$Aarticle['style']."'";
            if($target == $Aarticle['id'])
            {
                $content .= " selected";
            }
            if(USE_ARTICLE_NUMBER)
            {
                $article_number = $Aarticle['number']." ";
            }
            else $article_number = "";
            $content .= ">".$Aarticle['name']." ".$article_number.$Aarticle['isbn']." -> ".$brutto."</option>\n";
        }
        $content .= "</select>\n";
        return $content;
    }

    protected function check_value($value, $array)
    {
        if(in_array($value, $array))
        {
            return TRUE;
        }
        return FALSE;
    }

    // Admin Funktion für mod_articles;
    public function get_article_post()
    {
        $Aarticle_post = array();
        foreach($this->article_post_fields as $value)
        {
            if(isset($this->post_defaults[$value]))
            {
                $default = $this->post_defaults[$value];
            }
            else $default = NULL;
            if($this->check_value($value, $this->do_sql_time))
            {
                $temp = $this->Cpage->get_parameter($value, $default);
                if($temp != 0)
                {
                    $temp = strtotime($temp);
                    $temp = date("Y-m-d H:i:s", $temp);
                }
            }
            elseif($this->check_value($value, $this->do_no_escape))
            {
                $temp = $this->Cpage->get_parameter($value, $default, NO_ESCAPE);
            }
            elseif($this->check_value($value, $this->do_htmlspecialchars))
            {
                $temp = $this->Cpage->get_parameter($value, $default, NO_ESCAPE, DO_HTMLSPECIALCHARS);
            }
            else $temp = $this->Cpage->get_parameter($value, $default);
            $Aarticle_post[$value] = $temp;
        }
        return $Aarticle_post;
    }

    // Admin Funktion für mod_articles;
    public function get_category_post()
    {
        $Acategory_post = array();
        foreach($this->category_post_fields as $value)
        {
            if(isset($this->post_defaults[$value]))
            {
                $default = $this->post_defaults[$value];
            }
            else $default = NULL;
            if($this->check_value($value, $this->do_sql_time))
            {
                $temp = $this->Cpage->get_parameter($value, $default);
                if($temp != 0)
                {
                    $temp = strtotime($temp);
                    $temp = date("Y-m-d H:i:s", $temp);
                }
            }
            elseif($this->check_value($value, $this->do_no_escape))
            {
                $temp = $this->Cpage->get_parameter($value, $default, NO_ESCAPE);
            }
            elseif($this->check_value($value, $this->do_htmlspecialchars))
            {
                $temp = $this->Cpage->get_parameter($value, $default, NO_ESCAPE, DO_HTMLSPECIALCHARS);
            }
            else $temp = $this->Cpage->get_parameter($value, $default);
            $Acategory_post[$value] = $temp;
        }
        return $Acategory_post;
    }

    // Admin Funktion für mod_articles;
    public function isset_default($field, $type)
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."adm_default` WHERE `field` LIKE '$field' AND `type` LIKE '$type';";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows > 0)
        {
            return TRUE;
        }
        else return FALSE;
    }

    protected function get_select_categories($start_id, $target, $remove_sub, $i = 0)
    {
        $content = "";
        if(isset($this->Acategories[$start_id]))
        {
            foreach($this->Acategories[$start_id] as $key => $array_top)
            {
                $content .= " <option value='$key'";
                if($target == $key)
                {
                    $content .= " selected";
                }
                $content .= ">".(str_repeat('&#160;', $i*3)).$array_top['name']."</option>";
                $i++;
                // Zeige Unterkategorien ab $target nicht mehr an, z.B. damit keine Kategorie in eine Unterkategorie von sich geschoben werden kann
                if(empty($target) || ($target != $key) || !$remove_sub)
                {
                    $content .= $this->get_select_categories($key, $target, $remove_sub, $i);
                }
                $i--;
            }
        }
        return $content;
    }

    // Admin Funktion für mod_articles;
    public function select_categories($select_name = "", $target = "", $remove_sub = FALSE, $width = "", $select_class = "select", $onChange = SUBMIT_CHANGE)
    {
        $content = "";
        if((empty($this->Acategories)) && (empty($this->Aarticles)))
        {
            $this->fill_category_article_array($this->start_id);
        }
        $content .= "<select name='".$select_name."' class='".$select_class."' onChange='".$onChange."' style='width:".$width.";'>\n";
        $content .= " <option value='0'>Als Hauptkategorie</option>\n";
        $content .= $this->get_select_categories($this->start_id, $target, $remove_sub);
        $content .= "</select>\n";
        return $content;
    }

    // Verzeichnisfunktion der Kategorien mit Artikeln
    public function get_dir_root()
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $status     = 127; // Das Wurzelverzeichnis
        // Prüfe, ob Wurzelverzeichnis bereits existiert, ansonsten lege neues an
        if($cat_result->num_rows == 0)
        {
            $sql  = "INSERT INTO `".TBL_PREFIX."categories` (`id`,`status`) VALUES (NULL, ?);";
            $stmt = $this->Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('i', $status);
            $this->Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            $id = $this->Cdb->get_insert_id();
        }
        else
        {
            $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `status` =$status;";
            $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Adb        = $cat_result->fetch_assoc();
            $id         = $Adb['id'];
        }
        return $id; // ID des Wurzelverzeichnises
    }

    // Füge Eintrag ins Verzeichnis der betreffenden Kategorie hinzu
    public function dir_add($home, $id, $type)
    {
        $sql        = "SELECT `dir` FROM `".TBL_PREFIX."categories` WHERE `id` =$home;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $Adb        = $cat_result->fetch_assoc();
        $dir        = array();
        if(!empty($Adb['dir']))
        {
            $dir = unserialize($Adb['dir']);
        }
        $tmp         = array();
        $tmp['id']   = $id;
        $tmp['type'] = $type;
        array_push($dir, $tmp);
        $new_dir = serialize($dir);
        $sql     = "UPDATE `".TBL_PREFIX."categories` SET `dir` = ? WHERE `id` = ?;";
        $stmt    = $this->Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('si', $new_dir, $home);
        $this->Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        return TRUE;
    }

    // Entferne bestimmten Eintrag aus Verzeichnis der betreffenden Kategorie
    public function dir_remove($home, $id, $type)
    {
        $sql        = "SELECT `dir` FROM `".TBL_PREFIX."categories` WHERE `id` =$home;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $Adb        = $cat_result->fetch_assoc();
        $dir        = array();
        $dir        = unserialize($Adb['dir']);
        foreach($dir as $key => $value)
        {
            if(($value['id'] == $id) && ($value['type'] == $type))
            {
                unset($dir[$key]);
                break;
            }
        }
        sort($dir);
        $new_dir = serialize($dir);
        $sql     = "UPDATE `".TBL_PREFIX."categories` SET `dir` = ? WHERE `id` = ?;";
        $stmt    = $this->Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('si', $new_dir, $home);
        $this->Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        return TRUE;
    }

    // Entferne bestimmten Eintrag aus Verzeichnis einer unbekannten Kategorie
    public function dir_remove_id($id, $type)
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $dir        = array();
        $match      = FALSE;
        while($Adb = $cat_result->fetch_assoc())
        {
            $dir = unserialize($Adb['dir']);
            foreach($dir as $key => $value)
            {
                if(($value['id'] == $id) && ($value['type'] == $type))
                {
                    unset($dir[$key]);
                    $match = TRUE;
                    break 2;
                }
            }
        }
        if($match)
        {
            sort($dir);
            $new_dir = serialize($dir);
            $sql     = "UPDATE `".TBL_PREFIX."categories` SET `dir` = ? WHERE `id` = ?;";
            $stmt    = $this->Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $cat_id  = $Adb['id'];
            $stmt->bind_param('si', $new_dir, $cat_id);
            $this->Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
    }

    // Hole das ganze Verzeichnis in $this->Aall_Elements und gebe es zurück
    // Dabei geht es rekursiv ab der Kategorie $id vor
    public function dir_get_all_elements($id)
    {
        $dir         = array();
        $tmp         = array();
        $sql         = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = $id;";
        $cat_result  = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $Adb         = $cat_result->fetch_assoc();
        $tmp['id']   = $id; // Füge die Kategorie hinzu
        $tmp['type'] = "C";
        $tmp['name'] = $Adb['name'];
        array_push($this->Aall_elements, $tmp);
        $dir = unserialize($Adb['dir']);
        if(!empty($dir))
            // Gehe das ganze Verzeichnis der betreffenden Kategorie durch
        {
            foreach($dir as $key => $value)
            {
                if($value['type'] == "A")
                {
                    $sql          = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$value['id'].";";
                    $cat_result   = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $Adb          = $cat_result->fetch_assoc();
                    $tmp['id']    = $value['id'];
                    $tmp['type']  = "A"; // Artikel
                    $tmp['name']  = $Adb['name'];
                    $tmp['to_id'] = 0;
                    array_push($this->Aall_elements, $tmp);
                    continue;
                }
                if($value['type'] == "Ac")
                {
                    $sql          = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$value['id'].";";
                    $cat_result   = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $Adb          = $cat_result->fetch_assoc();
                    $tmp['id']    = $value['id'];
                    $tmp['type']  = "Ac"; // Artikel Klon
                    $tmp['name']  = $Adb['name'];
                    $tmp['to_id'] = $Adb['clone_from'];
                    array_push($this->Aall_elements, $tmp);
                    continue;
                }
                if($value['type'] == "C")
                {
                    // Rufe die Funktion rekursiv auf, da die gefundene Kategorie evtl. weitere Elemente besitzt
                    $this->dir_get_all_elements($value['id'], $id);
                }
            }
        }
        return $this->Aall_elements;
    }

    // Fülle die Arrays Kategorie und Artikel nach einem bestimmten Schema
    public function fill_category_article_array($start_id, $disabled = NULL)
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$start_id;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $cat_field  = $cat_result->fetch_assoc();

        if(!empty($cat_field['dir']))
        {
            $dir = unserialize($cat_field['dir']);
            foreach($dir as $key => $value)
            {
                // Falls Kategorie
                if($value['type'] == "C")
                {
                    $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =".$value['id'].";";
                    $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $cat_field  = $cat_result->fetch_assoc();

                    // Für die Funktion get_article, falls sich Artikel in einem deaktivieren Kategorie Stamm befindet
                    $visible_from_timestamp = $this->Cpage->set_sqltime($cat_field['visible_from'], "min");
                    $visible_to_timestamp   = $this->Cpage->set_sqltime($cat_field['visible_to'], "max");

                    if((!isset($disabled) && ($cat_field['status'] == 0)) || ($visible_from_timestamp > time()) || ($visible_to_timestamp < time()))
                    {
                        $this->Adisabled[$value['id']] = TRUE;
                        // Speichere Kategorie
                        $this->Acategories[$start_id][$value['id']] = $cat_field;
                        // Rufe Funktion nochmal auf, es könnten weitere Elemente vorhanden sein
                        $this->fill_category_article_array($value['id'], TRUE);
                    }
                    else
                    {
                        $this->Adisabled[$value['id']] = $disabled;
                        // Speichere Kategorie
                        $this->Acategories[$start_id][$value['id']] = $cat_field;
                        // Rufe Funktion nochmal auf, es könnten weitere Elemente vorhanden sein
                        $this->fill_category_article_array($value['id'], $disabled);
                    }

                }
                // Falls Artikel
                if($value['type'] == "A")
                {
                    $sql                                                 = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$value['id'].";";
                    $articles_result                                     = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $articles_field                                      = $articles_result->fetch_assoc();
                    $this->Aarticles[$start_id][$value['id']]            = $articles_field;
                    $this->Aarticles[$start_id][$value['id']]['isclone'] = NULL;
                }
                // Falls Artikel Klon
                if($value['type'] == "Ac")
                {
                    $sql                                                 = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$value['id'].";";
                    $articles_result                                     = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $articles_field                                      = $articles_result->fetch_assoc();
                    $sql                                                 = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` = ".$articles_field['clone_from'].";";
                    $articles_result                                     = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                    $articles_field                                      = $articles_result->fetch_assoc();
                    $this->Aarticles[$start_id][$value['id']]            = $articles_field;
                    $this->Aarticles[$start_id][$value['id']]['isclone'] = " (Klon)";
                }
            }
        }
        return;
    }

    // Hole alle Artikel von einer Kategorie
    public function get_articles_to_id($category_id)
    {
        $Aarticles = array();

        $sql        = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$category_id;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $cat_field  = $cat_result->fetch_assoc();

        if(!empty($cat_field['dir']))
        {
            $dir = unserialize($cat_field['dir']);
            foreach($dir as $key => $value)
            {
                if(($value['type'] == "A") || ($value['type'] == "Ac"))
                {
                    $articles_field = $this->get_article($value['id']);
                    if($articles_field != NULL)
                        array_push($Aarticles, $articles_field);
                }
            }
        }
        else return NULL;
        return $Aarticles;
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    protected function create_menu($start_id, $selected_cat, $i = 0)
    {
        $content = "";
            if(isset($this->Acategories[$start_id]))
            {
                foreach($this->Acategories[$start_id] as $key => $array_top)
                {
                    // Prüfe, ob Kategorie in der Spur zur ausgewählten Kategorie
                    $show = $this->check_trail($key, "C");
                    $content .= " <li class='";
                    // Prüfe, ob letzte Kategorie, wenn nicht im Wurzelverzeichnis
                    if(($key != $start_id) && ($this->check_last_entry($key)))
                    {
                        $content .= "last_";
                    }
                    $content .= "category_";
                    // Zeige Kategorien im Wurzelverzeichnis immer an
                    if($i == 0)
                    {
                        $content .= "show";
                    }
                    // Zeige an, wenn ausgewählt und in der Spur
                    elseif($show)
                    {
                        $content .= "show";
                    }
                    // Ausblenden
                    else $content .= "hide";
                    // Link zur Kategorie anzeigen
                    $content .= "'>".$this->Cpage->link($array_top['name'], "articles.php", "category=".$key);
                    $content .= " <span class='warning'>";
                    // Wenn inaktiv
                    if($array_top['status'] == 0)
                    {
                        $content .= "inaktiv";
                    }
                    $content .= "</span>\n";
                    $content .= "  <ul>\n";
                    $content .= "  <li class='menupoint_";
                    // Zeige weitere Optionen nur an, wenn es die ausgewählte Kategorie ist
                    if($key == $selected_cat)
                    {
                        $content .= "show";
                    }
                    else $content .= "hide";
                    $content .= "'>".$this->Cpage->link("Unterkategorie hinzufügen", "articles.php", "category=".$key."&do_action=add_category", "", "font-style:italic;")."</li>\n";
                    $content .= "  <li class='menupoint_";
                    if($key == $selected_cat)
                    {
                        $content .= "show";
                    }
                    else $content .= "hide";
                    $content .= "'>".$this->Cpage->link("Artikel hinzufügen", "articles.php", "category=".$key."&do_action=add_article", "", "font-style:italic;")."</li>\n";

                    $i++;
                    // Rufe Funktion rekursiv auf, um alle Kategorien zu holen
                    $content .= $this->create_menu($key, $selected_cat, $i);
                    $i--;
                    $content .= "  </ul>\n";
                    $content .= " </li>\n";
                }
            }
            // Zeige Artikel an, wenn vorhanden
            if(isset($this->Aarticles[$start_id]))
                foreach($this->Aarticles[$start_id] as $key2 => $array_top2)
                {
                    // Zeige im Wurzelverzeichnis die Artikel immer an
                    if($i == 0)
                        $show = TRUE;
                    // Zeige Artikel nur an, wenn in der Spur
                    else $show = $this->check_trail($key2, "A");
                    $content .= " <li class='article_";
                    if($show)
                        $content .= "show";
                    else $content .= "hide";
                    // Zeige Link zum Artikel an
                    $content .= "'>".$this->Cpage->link($array_top2['name'], "articles.php", "article=".$key2);
                    // Zeige verschiedene Hinweise an
                    $content .= " <span class='warning'>";
                    if($array_top2['status'] == 0)
                        $content .= "inaktiv";
                    if(($array_top2['quantity'] == 0) && ($array_top2['status'] == 1) && ($array_top2['quantity_warning'] != 0))
                        $content .= "! ";
                    if(($array_top2['quantity'] != 0) && ($array_top2['quantity'] < $array_top2['quantity_warning']) && ($array_top2['status'] == 1))
                        $content .= "# ";
                    if(($array_top2['discount'] > 0) && ($array_top2['status'] == 1))
                        $content .= "R ";
                    $content .= "</span>";
                    if(!empty($array_top2['isclone']))
                        $content .= "<span class='information'>".$array_top2['isclone']."</span> ";
                    if($array_top2['top_offer'])
                        $content .= "<span class='information'>Top</span>\n";
                    $content .= " </li>\n";
                }

        return $content;
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    // Prüfen, ob die Kategorie oder Artikel in der geöffneten Spur der Kategorien ist
    protected function check_trail($id, $type)
    {
        foreach($this->Atrail as $key => $value)
            if(($value['id'] == $id) && ($value['type'] == $type))
                return TRUE;
        return FALSE;
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    // Um die Spur von Hauptkategorie zur ausgewählten Unterkategorie herzustellen
    protected function get_trail($category, $start_id, $i = 0)
    {
        if($category != 0)
        {
            $dir = array();
            $tmp = array();
            // Hole im 1. Schritt die ausgewählte Kategorie
            // in den anderen Schritten, die Kategorie die über die Kategorie liegt
            $sql         = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = $category;";
            $cat_result  = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Adb         = $cat_result->fetch_assoc();
            $tmp['id']   = $category; // Füge Kategorie hinzu
            $tmp['type'] = "C";
            array_push($this->Atrail, $tmp);
            $dir = unserialize($Adb['dir']);
            // Gehe das ganze Verzeichnis der Kategorie durch
            if((!empty($dir)) && ($i == 0))
                foreach($dir as $key => $value)
                {
                    // Füge Artikel hinzu
                    if(($value['type'] == "A") || ($value['type'] == "Ac"))
                    {
                        $tmp['id']   = $value['id'];
                        $tmp['type'] = "A";
                        array_push($this->Atrail, $tmp);
                        continue;
                    }
                    // Füge Kategorien hinzu
                    if($value['type'] == "C")
                    {
                        $tmp['id']   = $value['id'];
                        $tmp['type'] = "C";
                        array_push($this->Atrail, $tmp);
                    }
                }
            // Hole alle Kategorien
            $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
            $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
            while($Adb = $cat_result->fetch_assoc())
            {
                // Das Wurzelverzeichnis auslassen
                if($Adb['id'] == $start_id)
                    continue;
                $dir = unserialize($Adb['dir']);
                if(!empty($dir))
                    // Gehe das Verzeichnis durch
                    foreach($dir as $key => $value)
                    {
                        // Falls eine übergeordnete Kategorie gefunden wurde
                        if(($value['id'] == $category) && ($value['type'] == "C"))
                        {
                            // Holde diese Kategorie
                            $sql         = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =".$Adb['id'].";";
                            $cat_result2 = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
                            $Adb2        = $cat_result2->fetch_assoc();
                            $dir2        = unserialize($Adb['dir']);
                            if(!empty($dir2))
                                // Gehe das Verzeichnis durch
                                foreach($dir2 as $key2 => $value2)
                                {
                                    // Wenn es eine Kategorie ist, füge es hinzu, es werden damit alle Kategorien der übergeordneten Kategeorie hinzugefügt
                                    if($value2['type'] == "C")
                                    {
                                        $tmp['id']   = $value2['id'];
                                        $tmp['type'] = "C";
                                        array_push($this->Atrail, $tmp);
                                    }
                                }
                            $i++;
                            // Rufe die Funktion nochmal auf, um die übergeordnete Kategorie hinzuzufügen und um weitere übergeordnete zu finden
                            $this->get_trail($Adb2['id'], $start_id, $i);
                            $i--;
                            break 2; // Breche die Schleifen ab, da bereits die Kategorie gefunden wurde
                        }
                    }
            }
        }
        return;
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    // Um die Kategorien zu finden, die keine Unterkategorien haben
    protected function get_last_entries()
    {
        // Hole alle Kategorien
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Adb = $cat_result->fetch_assoc())
        {
            $this->Alastentries[$Adb['id']] = FALSE; // Setze auf FALSE, kann später auf TRUE gesetzt werden
            $dir                            = unserialize($Adb['dir']);
            // Wenn das Verzeichnis leer, dann ist es auf jeden Fall der letzte Eintrag
            if(empty($dir) || (sizeof($dir) == 0))
            {
                $this->Alastentries[$Adb['id']] = TRUE; // Speichere Ergebnis in Array
                continue;
            }
            else
            {
                $match = FALSE;
                // Ansonsten gehe das ganze Verzeichnis durch und prüfe, ob eine Kategorie vorhanden
                foreach($dir as $key => $value)
                {
                    if(($value['type'] == "C"))
                    {
                        $match = TRUE; // Wenn eine oder mehrer Kategorien vorhanden, dann ist es nicht die letzte Kategorie
                        break; // Es muss nicht weiter geprüft werden
                    }
                }
                if(!$match)
                    $this->Alastentries[$Adb['id']] = TRUE; // Speichere Ergebnis in Array
            }
        }
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    // Gibt aus, ob die Kategorie die letzte ohne Unterkategorien ist
    protected function check_last_entry($id)
    {
        return $this->Alastentries[$id];
    }

    // Funktion, um Artikel-Kategorien Menü im Adminbereich zu erstellen
    public function get_article_category_list($selected_category, $selected_article = 0)
    {
        $categories = "";

        // Fülle das Array ab dem Wurzelverzeichnis
        if((empty($this->Acategories)) && (empty($this->Aarticles)))
            $this->fill_category_article_array($this->start_id);

        // Sortiere Kategorien Array:
        if(!empty($this->Acategories))
            foreach($this->Acategories as $key_above_id => $value)
                asort($this->Acategories[$key_above_id]);
        // Sortiere Artikel Array:
        if(!empty($this->Aarticles))
            foreach($this->Aarticles as $key_above_id => $value)
                asort($this->Aarticles[$key_above_id]);

        // Falls keine Kategorie, sondern Artikel ausgewählt
        if($selected_category == 0)
            $selected_cat = $this->get_category_id($selected_article);
        else $selected_cat = $selected_category;
        // Hole die Spur von der Hauptkategorie bis zur ausgewählten Unterkategorie, um im Menü die betreffenden Kategorien anzuzeigen und um die anderen auszublenden
        $this->get_trail($selected_cat, $this->start_id);
        // Bestimme die Kategorien, die keine Unterkategorien haben
        $this->get_last_entries();
        // Erstelle Menü vom Startverzeicbnis aus und lass alle Kategorien zur ausgewählten Kategorie geöffnet, blende die anderen aus
        $categories .= "<ul id='category_list'>\n";
        $categories .= " <li id='menupoint_open'>".$this->Cpage->link("Eine Hauptkategorie hinzufügen", "articles.php", "do_action=add_category", "", "font-style:italic;")."</li>\n";
        $categories .= " <li id='menupoint_open'>".$this->Cpage->link("Einen Hauptartikel hinzufügen", "articles.php", "do_action=add_article", "", "font-style:italic;")."</li>\n";
        $categories .= $this->create_menu($this->start_id, $selected_cat);
        $categories .= "</ul>\n";
        // Gebe Menü zurück
        return $categories;
    }

    // Hole Kategorie ID zu einem bestimmten Artikel
    public function get_category_id($article_id)
    {
        $sql        = "SELECT * FROM `".TBL_PREFIX."categories`;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Adb = $cat_result->fetch_assoc())
        {
            if(!empty($Adb['dir']))
            {
                $dir = unserialize($Adb['dir']);
                foreach($dir as $key => $value)
                {
                    if((($value['type'] == "A") || ($value['type'] == "Ac")) && ($value['id'] == $article_id))
                    {
                        return $Adb['id'];
                    }
                }
            }
        }
        return 0;
    }

    // Hole Artikelanzahl zu einer Kategorie
    public function get_article_count_to_id($category_id, $i=0)
    {

        if(empty($category_id)) return $i;
        $sql        = "SELECT dir FROM `".TBL_PREFIX."categories` WHERE `id` =$category_id;";
        $cat_result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        $cat_field  = $cat_result->fetch_assoc();

        if(!empty($cat_field['dir']))
        {
            $dir = unserialize($cat_field['dir']);
            foreach($dir as $value)
            {


                if($value['type'] == "A" || $value['type'] == "Ac")
                {
                    $Aarticle = $this->get_article($value['id']);
                    if($Aarticle != NULL)
                        $i++;
                }
                elseif($value['type'] == "C")
                {
                    $i = $this->get_article_count_to_id($value['id'], $i);
                }

            }
        }
        else return $i;
        return $i;
    }

    public function get_price($attribute_id = 0)
    {

    }
}
