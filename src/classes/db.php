<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class db
{
    public $id;
    protected $db_server;
    protected $db_user;
    protected $db_pass;
    protected $db_table;
    protected $tbl_prefix;
    protected $log;
    protected $error;

    public function __construct($db_server, $db_user, $db_pass, $db_table, $tbl_prefix, $set_utf8 = TRUE)
    {
        $this->db_server  = $db_server;
        $this->db_user    = $db_user;
        $this->db_pass    = $db_pass;
        $this->db_table   = $db_table;
        $this->tbl_prefix = $tbl_prefix;
        $this->id         = new mysqli($db_server, $db_user, $db_pass, $db_table);
        if($set_utf8)
        {
            $this->id->query("SET NAMES 'utf8'");
            $this->id->query("SET CHARACTER SET 'utf8'");
        }
        $this->log   = log::singleton();
        $this->error = error::singleton();
    }

    public function __destruct()
    {
        $this->id->close();
    }

    public function get_id()
    {
        return $this->id;
    }

    public function affected_rows()
    {
        return $this->id->affected_rows;
    }

    // Datenbank Query
    public function db_query($sql, $name = UNKNOWN)
    {
        $result = $this->id->query($sql);
        if(!$result)
        {
            $this->log->push_error("SQL Select Fehler in $name. Fehler: ".$this->id->error);
            $this->error->throw_error("SQL Query Fehler");
        }

        $this->log->push_log("$name - SQL Query $sql", 2);
        return $result;
    }

    // Datenbank Prepare
    public function db_prepare($sql, $name = UNKNOWN)
    {
        $stmt = $this->id->prepare($sql);
        if(!$stmt)
        {
            $this->log->push_error("SQL Query konnte nicht vorbereitet werden in $name. Fehler: ".$this->id->error);
            $this->error->throw_error("SQL Query konnte nicht vorbereitet werden");
        }

        $this->log->push_log("$name - SQL Prepare $sql", 2);
        return $stmt;
    }

    // Datenbank Execute
    public function db_execute($stmt, $name = UNKNOWN)
    {
        if(!$stmt->execute())
        {
            $this->log->push_error("Query konnte nicht ausgeführt werden, in $name. Fehler: ".$this->id->error);
            $this->error->throw_error("SQL Query konnte nicht ausgeführt werden");
            return FALSE;
        }
        $this->log->push_log("$name - SQL Execute", 2);
        return TRUE;
    }

    public function get_insert_id()
    {
        return $this->id->insert_id;
    }

    public function close()
    {
        $this->id->close();
    }
}
