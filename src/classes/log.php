<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class log
{
    private static $instance;

    protected $do_log;
    protected $log_level;
    protected $log_errors;

    protected $log_file = "log/events.log";
    protected $error_file = "log/errors.log";
    protected $log_file_handler;
    protected $error_file_handler;

    public $errors = array();
    public $log = array();

    public function __construct($do_log, $log_level, $log_errors)
    {
        $this->do_log     = $do_log;
        $this->log_level  = $log_level;
        $this->log_errors = $log_errors;
        if(class_exists("ZipArchive"))
        {
            $this->do_archive($this->log_file); // Erstelle Zip Archiv, falls nötig
            $this->do_archive($this->error_file);
        }
        $this->log_file_handler   = $this->open_file($this->log_file);
        $this->error_file_handler = $this->open_file($this->error_file);
        // Falls Fehler oder Meldungen in do_archive oder anderen Meldungen vor dem öffnen der Dateien aufgetaucht sind:
        if(sizeof($this->errors) > 0)
        {
            foreach($this->errors as $value)
                $this->push_error($value, 0);
        }
        if(sizeof($this->log) > 0)
        {
            foreach($this->log as $value)
                $this->push_log($value, 0);
        }
    }

    public function __destruct()
    {
        // Schliesse Daten, wenn sie ordnungsgemäß geöffnet waren
        if($this->log_file_handler != FALSE)
        {
            fclose($this->log_file_handler);
        }
        if($this->error_file_handler != FALSE)
        {
            fclose($this->error_file_handler);
        }
    }

    public static function singleton($do_log = FALSE, $log_level = FALSE, $log_errors = FALSE)
    {
        if(!isset(self::$instance))
        {
            $className      = __CLASS__;
            self::$instance = new $className($do_log, $log_level, $log_errors);
        }
        return self::$instance;
    }

    protected function open_file($file)
    {
        // Prüfe, ob Datei vorhanden
        if(!file_exists($file))
        {
            array_push($this->log, "Log Datei $file existiert nicht, versuche zu erstellen.");
        }
        // Öffne Datei zum schreiben, erstelle wenn nicht vorhanden
        $handler = fopen($file, "a");
        // Falls Fehler
        if($handler == FALSE)
        {
            array_push($this->errors, "Kann die Log Datei $file nicht zum schreiben öffnen. Eventuell fehlen Schreibrechte, auch zum Erstellen?.");
            return $handler;
        }
        // Falls OK, gebe Dateizeiger zurück
        return $handler;
    }

    protected function do_archive($file, $size_to_check = "32")
    {
        // Prüfe, ob Original Datei überhaupt vorhanden
        if(file_exists($file))
        {
            // Prüfe Größe und wandle in Megabytes um
            $size = filesize($file)/1024/1024;
            if($size >= $size_to_check)
            {
                // Hole eindeutigen Archiv Namen
                $archive = $this->check_filename($file);
                // Bestimme Dateinamen im Archiv
                $fileparts = pathinfo($file);
                $filename  = $fileparts['basename'];
                $lastdot   = strrpos($filename, ".");
                $filename  = substr($filename, 0, $lastdot);
                $filename  = $filename.".log";
                // Erstelle Zip Objekt
                $zip = new ZipArchive();
                // Lege Zip Archiv an
                if($zip->open($archive, ZIPARCHIVE::CREATE) !== TRUE)
                {
                    array_push($this->errors, "Kann kein Archiv von $archive erstellen");
                    return FALSE;
                }
                // Füge Datei hinzu
                $zip->addFile($file, $filename);
                // Schliesse Zip
                $zip->close();
                array_push($this->log, "Habe Zip File $archive aus $file erstellt. Lösche $file.");
                // Lösche Original Datei
                unlink($file);
                return TRUE;
            }
        }
        return FALSE;
    }

    protected function check_filename($file)
    {
        // Hole die einzelnen Teile des Dateinamen mit Pfad
        $fileparts = pathinfo($file);
        // Dateiname mit Typ
        $filename = $fileparts['basename'];
        // Extrahiere den Typ, damit nur der Name übrigbleibt
        $lastdot  = strrpos($filename, ".");
        $filename = substr($filename, 0, $lastdot);
        // Der neue Dateityp ist immer Zip
        $filetype = "zip";
        // Das Verzeichnis mit abschliessenden Slash
        $dir = $fileparts['dirname']."/";
        // Bestimme den neuen Dateinamen
        $newname = $this->create_filename($filename, $filetype, $dir, 1);
        return $newname;
    }

    protected function create_filename($name, $type, $dir, $extension = "")
    {
        // Falls Dateiname nach bestimmten Schema bereits existiert
        if(file_exists($dir.$name."_".$extension.".".$type))
        {
            // Erhöhe den Zähler und prüfe nochmal
            $newname = $this->create_filename($name, $type, $dir, ($extension+1));
            // (2) Wegen der Rekursion, gebe gefundenen Namen zurück
            return $newname;
        }
        else
        {
            // (1) Gebe den neuen gefundenen Namen an die aufgerufene Funktion zurück
            return $dir.$name."_".$extension.".".$type;
        }
    }

    public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public function push_error($string)
    {
        // Wenn kein Dateifehler
        if($this->error_file_handler != FALSE)
            // Wenn Log aktivieret
        {
            if($this->log_errors)
            {
                fwrite($this->error_file_handler, date("d-m-Y H:i:s", time()).": ".$_SERVER['REQUEST_URI'].", $string\r\n");
            }
        }
    }

    public function push_log($string, $log_level)
    {
        if($this->log_file_handler != FALSE)
            // Wenn der Loglevel paßt und wenn gelogt werden soll
        {
            if(($this->log_level >= $log_level) && ($this->do_log))
            {
                fwrite($this->log_file_handler, date("d-m-Y H:i:s", time()).": ".$_SERVER['REQUEST_URI'].", $string\r\n");
            }
        }
    }
}
