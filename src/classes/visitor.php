<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class visitor
{
    protected $ip;
    protected $referer;
    protected $browser;
    protected $page;
    protected $Cdb;
    public $visited;
    protected $log;
    protected $error;

    public function __construct($Cdb)
    {
        $this->log   = log::singleton();
        $this->error = error::singleton();

        $this->ip = $_SERVER['REMOTE_ADDR'];
        if(isset($_SERVER['HTTP_REFERER']))
        {
            $this->referer = $_SERVER['HTTP_REFERER'];
        }
        else
        {
            $this->referer = NULL;
        }
        $this->browser = $_SERVER['HTTP_USER_AGENT'];
        $this->page    = $_SERVER['REQUEST_URI'];
        $this->Cdb     = $Cdb;

        if(!$this->check_revisit())
        {
            $this->update_page_counter();
        }
    }

    public function check_revisit()
    {
        // Prüfe anhand von Cookie, ob der Besucher schonmal da war
        if(isset($_COOKIE["visited"]))
        {
            return TRUE;
        }
        else
        {
            setcookie("visited", "Yes", time()+60*60*24*30); // 30 Tage
            return FALSE;
        }
    }

    public function update_page_counter()
    {
        $this->log->push_log("Erhöhe Seitenzähler", 0);
        $sql = "UPDATE ".TBL_PREFIX."counter SET `count` = `count` + 1 WHERE `type` LIKE 'PAGE_COUNTER';";
        $this->Cdb->db_query($sql);
    }

    public function get_ip($encoded = TRUE)
    {
        if($encoded)
        {
            return md5(($this->ip)."SALT:124jhf435ukrf46234k5jf23");
        }
        else
        {
            return $this->ip;
        }
    }

    public function get_referer()
    {
        return $this->referer;
    }

    public function get_browser()
    {
        return $this->browser;
    }

    public function get_page()
    {
        return $this->page;
    }

    public function store_visit($table)
    {
        // Speichere jeden Seitenaufruf, um die Seite auswerten zu können
        if(STORE_EVERY_VISIT)
        {
            $sql  = "INSERT INTO ".TBL_PREFIX.$table." ( `ip`, `referer`, `browser`, `page`, `time` )
                  VALUES
                 ( ?, ?, ?, ?, NOW() )";
            $stmt = $this->Cdb->db_prepare($sql, __FILE__.":".__LINE__);

            if($table == "adm_visitors")
            {
                $ip = $this->get_ip(FALSE);
            }
            else
            {
                $ip = $this->get_ip();
            }
            $referer = $this->get_referer();
            $browser = $this->get_browser();
            $page    = $this->get_page();

            $stmt->bind_param('ssss', $ip, $referer, $browser, $page);
            $this->Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
    }
}
