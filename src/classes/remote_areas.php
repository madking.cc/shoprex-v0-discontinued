<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

class remote_areas
{
    protected $Cdb;
    public $Aremote_areas;
    protected $country;

    public function __construct($Cdb, $country)
    {
        $this->Cdb           = $Cdb;
        $this->Aremote_areas = array();
        $this->country       = $country;
        $this->fill_array();
    }

    protected function fill_array()
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."remote_areas` WHERE `country` LIKE '".$this->country."';";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Afield = $result->fetch_assoc())
        {
            array_push($this->Aremote_areas, $Afield);
        }
    }

    public function refill_array($country)
    {
        unset($this->Aremote_areas);
        $this->Aremote_areas = array();

        $sql    = "SELECT * FROM `".TBL_PREFIX."remote_areas` WHERE `country` LIKE '".$country."';";
        $result = $this->Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Afield = $result->fetch_assoc())
        {
            array_push($this->Aremote_areas, $Afield);
        }
    }

    public function check($city, $zip)
    {
        foreach($this->Aremote_areas as $key => $value)
        {
            if($value['end'] == NULL && $value['start'] != NULL)
            {
                $tmp = stristr((string)$zip, (string)$value['start']);
                if($tmp != FALSE)
                {
                    return TRUE;
                }
            }
            elseif($value['start'] != NULL)
            {
                for($i = (int)$value['start']; $i <= (int)$value['end']; $i++)
                {
                    $tmp = stristr((string)$zip, (string)$i);
                    if($tmp != FALSE)
                    {
                        return TRUE;
                    }
                }
            }

            if($value['code'] != NULL)
            {
                $tmp = strpos((string)$zip, $value['code']);
                if($tmp != FALSE)
                {
                    return TRUE;
                }
                $tmp = stristr($city, $value['code']);
                if($tmp != FALSE)
                {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }
}