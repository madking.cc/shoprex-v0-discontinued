<?php defined('SECURITY_CHECK') or die;

/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de

 */
class page
{
    protected $log;
    protected $Cdb;
    protected $cookies_enabled;
    protected $error;
    public $allowed_keys;
    public $check_numeric;
    public $allowed_pages;
    public $language;
    public $Aglobal;
    public $Amail;
    public $Apath;
    public $Awp;
    protected $image_width;
    protected $image_height;
    protected $image_type;
    protected $image_attributes;
    protected $Cmail;

    public function __construct($Cdb, $cookies_enabled, $Cmail)
    {
        $this->log = log::singleton();
        $this->error = error::singleton();
        $this->Cdb = $Cdb;
        $this->cookies_enabled = $cookies_enabled;
        $this->Aglobal = array();
        $this->Amail = array();
        $this->Apath = array();
        $this->Awp = array();
        $this->allowed_keys = array();
        $this->check_numeric = array();
        $this->allowed_pages = array();
        defined('SUBMIT_CHANGE') or define("SUBMIT_CHANGE", "");
        $this->Cmail = $Cmail;
    }

    // Hole Parameter per GET oder POST
    public function get_parameter($var, $default = NULL, $do_escape = TRUE, $do_htmlspecialchars = FALSE)
    {
        // Für die Log Klasse:
        if ($default === NULL) {
            $show_value = "NULL";
        } elseif ($default == 0 && (strlen($default) == 1)) {
            $show_value = "0";
        } elseif ($default == FALSE) {
            $show_value = "FALSE";
        } elseif ($default == TRUE) {
            $show_value = "TRUE";
        }
        // Bei Übermittling mit Post:
        if (isset($_POST[$var])) {
            // Wenn Post nicht leer:
            if (!empty($_POST[$var])) {
                if ($this->check_content($var, $_POST[$var])) {
                    if ($do_escape) {
                        if (!is_array($_POST[$var])) {
                            if ($do_htmlspecialchars) {
                                $value = htmlspecialchars($this->Cdb->id->real_escape_string($_POST[$var]), ENT_NOQUOTES);
                                $this->log->push_log("POST: key=$var htmlspecialchars und real_escape_string value=" . $value . "", 1);
                                return $value;
                            } else {
                                $value = $this->Cdb->id->real_escape_string($_POST[$var]);
                                $this->log->push_log("POST: key=$var real_escape_string value=" . $value . "", 1);
                                return $value;
                            }
                        } else {
                            $result = print_r($_POST[$var], TRUE);
                            $this->log->push_error("Post Variable ist ein array und kann nicht escaped werden: " . $var . " / Inhalt: " . $result . "");
                            $this->error->throw_error("Post Variable ist ein array und kann nicht escaped werden.");
                        }
                    } else {
                        if ($do_htmlspecialchars) {
                            $value = htmlspecialchars($_POST[$var], ENT_NOQUOTES);
                            $this->log->push_log("POST: key=$var htmlspecialchars value=" . $value . "", 1);
                            return $value;
                        } else {
                            if (is_array($_POST[$var]))
                                $result = print_r($_POST[$var], TRUE);
                            else

                                $result = $_POST[$var];
                            $this->log->push_log("POST: key=$var value=" . $result . "", 1);
                            return $_POST[$var];
                        }
                    }
                } else {
                    $this->log->push_error("POST Inhalt nicht erlaubt: " . $var . " / Inhalt: " . $_GET[$var] . "");
                    $this->error->throw_error("POST Inhalt von $var nicht erlaubt");
                }
            } // Wenn Post leer:
            else {
                // Überprüfen auf den Wert "0":
                if ($_POST[$var] == 0 && (strlen($_POST[$var]) == 1)) {
                    $this->log->push_log("POST: key=$var value=0", 1);
                    return 0; // Gebe 0 zurück
                } // Alles andere:
                else {
                    $this->log->push_log("POST: key=$var value=LEER -> return=$default", 1);
                    return $default; // Gebe default zurück
                }
            }
        } // Wenn per get übermittelt wurde:
        elseif (isset($_GET[$var])) {
            // Überprüfung auf erlaubte Variablennamen bei GET:
            if ($this->check_key($var)) {
                // Manche Browser übertragen ein ' am Ende der Parameterliste
                // Ggf. ' entfernen
                $tmp = substr($_GET[$var], -1, 1);
                if ($tmp == "'") {
                    $_GET[$var] = substr($_GET[$var], 0, -1);
                }
                if ($this->check_content($var, $_GET[$var])) {
                    // Wenn der Inhalt nicht leer ist:
                    if (!empty($_GET[$var])) {
                        if ($do_escape) {
                            if ($do_htmlspecialchars) {
                                $value = htmlspecialchars($this->Cdb->id->real_escape_string($_GET[$var]), ENT_NOQUOTES);
                                $this->log->push_log("GET: key=$var mit htmlspecialchars und real_escape_string: value=$value", 1);
                                return $value;
                            } else {
                                $value = $this->Cdb->id->real_escape_string($_GET[$var]);
                                $this->log->push_log("GET: key=$var mit real_escape_string: value=$value", 1);
                                return $value;
                            }
                        } else {
                            if ($do_htmlspecialchars) {
                                $value = htmlspecialchars($_GET[$var], ENT_NOQUOTES);
                                $this->log->push_log("GET: key=$var mit htmlspecialchars: value=$value", 1);
                                return $value;
                            } else {
                                $this->log->push_log("GET: key=$var value=$_GET[$var]", 1);
                                return $_GET[$var];
                            }
                        }
                    } // Wenn der Inhalt leer ist:
                    // Überprüfung auf die Zahl "0":
                    elseif ($_GET[$var] == 0 && (strlen($_GET[$var]) == 1)) {
                        $this->log->push_log("GET: key=$var value=0", 1);
                        return 0;
                    } // Sonst gebe default zurück:
                    else {
                        $this->log->push_log("GET: key=$var value=LEER -> return=$show_value", 1);
                        return $default;
                    }
                } else {
                    $this->log->push_error("GET Inhalt nicht erlaubt: " . $var . " / Inhalt: " . $_GET[$var] . "");
                    $this->error->throw_error("Get Inhalt von $var nicht erlaubt");
                }
            } // Wenn verbotene Variablennamen benutzt wurden:
            else {
                $this->log->push_error("GET Key nicht erlaubt: " . $var . " / Inhalt: " . $_GET[$var] . "");
                $this->error->throw_error("Get Variable $var nicht erlaubt");
            }
        }
        // Wenn keine Post oder Get Variable existiert, gebe den angegebenen default Wert zurück:
        // $this->log->push_log("GET/POST NOT SET: key=$var default-value=$show_value", 1);  // Erzeugt zuviel unnützen log
        return $default;
    }

    protected function check_key($var)
    {
        // Prüfe für GET, ob Schlüsselname auch erlaubt
        if (in_array($var, $this->allowed_keys)) {
            return TRUE;
        }
        return FALSE;
    }

    protected function check_content($var, $content)
    {
        if ($this->allowed_pages === FALSE) return TRUE;
        if ($var == "page") {
            if (in_array($content, $this->allowed_pages)) {
                return TRUE;
            } else return FALSE;
        } else {
            if (in_array($var, $this->check_numeric)) {
                if (is_numeric($content))
                    return TRUE;
                else

                    return FALSE;
            } else return TRUE;
        }
    }

    public function load_page($page = "index.php", $parameter = "", $time_in_ms = 0)
    {
        if ($time_in_ms == 0) {
            return "  window.location.href = '" . $page . $this->parameter_link($parameter) . "';\n";
        } else  return "  var start_load = window.setInterval('load_page()', " . $time_in_ms . ");\n" . "  public function load_page()\n" . "  { window.location.href = '" . $page . $this->parameter_link($parameter) . "'; }\n";
    }

    public function alert($text)
    {
        return "

		alert(\"" . $text . "\");

        ";
    }

    public function history_back()
    {
        return "

		history.back();

        ";
    }

    public function table($id = "", $class = "", $border = 0, $cellspacing = 0, $style = "")
    {
        return "<table border='" . $border . "' cellspacing='" . $cellspacing . "' class='" . $class . "' id='" . $id . "' style='empty-cells: show; " . $style . "'>";
    }

    public function get_mail_draft($name, $get_header_and_footer = TRUE)
    {
        $sql = "SELECT * FROM `" . TBL_PREFIX . "email_" . LANGUAGE . "`

              WHERE

              `name` = '" . $name . "'";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        $body = $result->fetch_assoc();
        if ($get_header_and_footer) {
            $sql = "SELECT * FROM `" . TBL_PREFIX . "email_" . LANGUAGE . "`

				  WHERE

				  `name` = 'mail_header'";
            $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
            $header = $result->fetch_assoc();
            $sql = "SELECT * FROM `" . TBL_PREFIX . "email_" . LANGUAGE . "`

				  WHERE

				  `name` = 'mail_footer'";
            $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
            $footer = $result->fetch_assoc();
        } else {
            $header = array();
            $header['text'] = "";
            $footer = array();
            $footer['text'] = "";
        }
        $text = array();
        $text['subject'] = $body['subject'];
        $text['text'] = $header['text'] . $body['text'] . $footer['text'];
        return $text;
    }

    public function get_mail_header()
    {
        $sql = "SELECT * FROM `" . TBL_PREFIX . "email_" . LANGUAGE . "`

				  WHERE

				  `name` = 'mail_header'";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        $header = $result->fetch_assoc();
        return $header['text'];
    }

    public function get_mail_footer()
    {
        $sql = "SELECT * FROM `" . TBL_PREFIX . "email_" . LANGUAGE . "`

				  WHERE

				  `name` = 'mail_footer'";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        $footer = $result->fetch_assoc();
        return $footer['text'];
    }

    public function get_invoice_draft($name)
    {
        $sql = "SELECT * FROM `" . TBL_PREFIX . "adm_templates_" . LANGUAGE . "`

              WHERE

              `name` = '" . $name . "'";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        $result = $result->fetch_assoc();
        return $result;
    }

    public function send_mail($from, $text, $subject, $target = "", $attachements = "all")
    {
        global $dir_root;
        if (empty($target)) {
            $target = $this->Amail['mail_default'];
        }
        $this->Cmail->IsHTML(true);
        $this->Cmail->CharSet = 'utf-8';
        $this->Cmail->SetLanguage(LANGUAGE);
        $this->Cmail->From = $from;
        $this->Cmail->FromName = $from;
        //$target = idn_to_ascii($target);
        $this->Cmail->addAddress($target, $target);
        $this->Cmail->Subject = $subject;
        $body = $text;
        $this->Cmail->Body = $body;
        $this->Cmail->AltBody = strip_tags($body);
        $attachement = FALSE;
        switch ($attachements) {
            case "order":
                $sql = "SELECT * FROM `" . TBL_PREFIX . "email_attachements_" . LANGUAGE . "` WHERE `type` = '1';";
                $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
                if ($result->num_rows != 0) {
                    while ($row = $result->fetch_assoc()) {
                        $this->Cmail->AddAttachment($dir_root . "download/" . $row['file'], $row['name']);
                        $attachement = TRUE;
                    }
                }
                break;
            case "all":
                $sql = "SELECT * FROM `" . TBL_PREFIX . "email_attachements_" . LANGUAGE . "` WHERE `type` = '2';";
                $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
                if ($result->num_rows != 0) {
                    while ($row = $result->fetch_assoc()) {
                        $this->Cmail->AddAttachment($dir_root . "download/" . $row['file'], $row['name']);
                        $attachement = TRUE;
                    }
                }
                break;
        }
        $status = $this->Cmail->Send();
        if ($status) {
            $this->log->push_log("E-Mail gesendet. Empfänger: " . $target . " Sender: " . $from . " Subject: " . $subject, 0);
        } else         $this->log->push_error("E-Mail Fehler, konnte nicht gesendet werden. Empfänger: " . $target . " Sender: " . $from . " Subject: " . $subject . "\nFehlermeldung: " . $this->Cmail->ErrorInfo, 0);
        if ($attachement)
            $this->Cmail->ClearAttachments();
        $this->Cmail->ClearAddresses();
        return $status;
    }

    public function salt_password($password)
    {
        if (strlen($password) > 5) {
            $password = substr($password, 0, 3) . SALT . substr($password, 4);
        } else $password .= SALT;
        return md5($password);
    }

    public function hash()
    {
        return md5(uniqid(rand(), TRUE));
    }

    public function get_timestamp($date)
    {
        if ($date == "0") return "0";
        $time = strtotime($date);
        return $time;
    }

    public function get_sqltime($timestamp = "")
    {
        if ($timestamp == "0") return "0000-00-00 00:00:00";
        if (empty($timestamp)) {
            $timestamp = time();
        }
        return date("Y-m-d H:i:s", $timestamp);
    }

    public function set_sqltime($date = "", $if_null = "min")
    {
        if (empty($date)) {
            return time();
        }
        $time = strtotime($date);
        if ($time == NULL) {
            if ($if_null == "min")
                $time = 0;
            else

                $time = mktime(0, 0, 0, 1, 1, 2030);
        }
        return $time;
    }

    public function format_time($datetime, $type = "", $type2 = "")
    {
        // $type = "date" gibt nur Datum aus
        // $type = "time" gibt nur Zeit aus
        if ($datetime == 0) {
            $tmp = "0";
        } else {
            $time = strtotime($datetime);
            $tmp = "";
            if ($time != -1) {
                if ($type == "sql") {
                    switch ($type2) {
                        case "date":
                            $tmp .= date("d.m.Y", $time);
                            break;
                        case "time":
                            $tmp .= date("H:i", $time);
                            break;
                        default:
                            $tmp .= date("d.m.Y H:i", $time);
                            break;
                    }
                } else {
                    if ($this->language == "de") {
                        if (($type == "date") || (empty($type))) {
                            $tmp .= date("d.m.Y", $time);
                            if (empty($type)) {
                                $tmp .= " um ";
                            }
                        }
                        if (($type == "time") || (empty($type))) {
                            $tmp .= date("H:i", $time);
                            $tmp .= " Uhr";
                        }
                    } else {
                        if (($type == "date") || (empty($type))) {
                            $tmp .= date("m.d.Y", $time);
                            if (empty($type)) {
                                $tmp .= " at ";
                            }
                        }
                        if (($type == "time") || (empty($type))) {
                            $tmp .= date("h A", $time);
                        }
                    }
                }
            } else $tmp = "Cannot translate time";
        }
        return $tmp;
    }

    public function link($text, $link, $parameter = "", $class = "shop_link", $style = "", $do_sid = TRUE, $do_change = SUBMIT_CHANGE)
    {
        if ($do_change != "") {
            return "<a href='javaScript:gotoPage(\"" . $link . $this->parameter_link($parameter, $do_sid) . "\");' class='" . $class . "' style=\"" . $style . "\">" . $text . "</a>";
        }
        return "<a href=\"" . $link . $this->parameter_link($parameter, $do_sid) . "\" class=\"" . $class . "\" style=\"" . $style . "\">" . $text . "</a>";
    }

    public function link_id($text, $link, $parameter = "", $style = "", $id = "", $class = "", $do_sid = TRUE, $do_change = SUBMIT_CHANGE)
    {
        if ($do_change != "") {
            return "<a href='javaScript:gotoPage(\"" . $link . $this->parameter_link($parameter, $do_sid) . "\");' class='" . $class . "' id=\"" . $id . "\" style=\"" . $style . "\">" . $text . "</a>";
        }
        return "<a href=\"" . $link . $this->parameter_link($parameter, $do_sid) . "\" id=\"" . $id . "\" class=\"" . $class . "\" style=\"" . $style . "\">" . $text . "</a>";
    }

    public function link_newPage($text, $link, $parameter = "", $class = "shop_link", $style = "", $do_sid = TRUE, $do_change = SUBMIT_CHANGE)
    {
        if ($do_change != "") {
            return "<a href='javaScript:gotoPage(\"" . $link . $this->parameter_link($parameter, $do_sid) . "\");' target='_blank' class='" . $class . "' style=\"" . $style . "\">" . $text . "</a>";
        }
        return "<a href=\"" . $link . $this->parameter_link($parameter, $do_sid) . "\" target=\"_blank\" class=\"" . $class . "\" style=\"" . $style . "\">" . $text . "</a>";
    }

    public function link_target($text, $link, $parameter = "", $target = "", $class = "shop_link", $style = "", $do_sid = TRUE, $do_change = SUBMIT_CHANGE)
    {
        if ($do_change != "") {
            return "<a href='javaScript:gotoPage(\"" . $link . $this->parameter_link($parameter, $do_sid) . "\");' target='" . $target . "' class='" . $class . "' style=\"" . $style . "\">" . $text . "</a>";
        }
        return "<a href=\"" . $link . $this->parameter_link($parameter, $do_sid) . "\" target=\"" . $target . "\" class=\"" . $class . "\" style=\"" . $style . "\">" . $text . "</a>";
    }

    public function link_onClick($text, $onclick, $class = "shop_link", $free_text = "", $style = "")
    {
        return "<a href=\"#\" onclick=\"" . $onclick . "return false;\" class=\"" . $class . "\" style=\"" . $style . "\" $free_text>" . $text . "</a>";
    }

    public function link_db($page, $target = "", $class = "shop_link", $style = "", $do_sid = TRUE)
    {
        $sql = "SELECT * FROM `" . TBL_PREFIX . "pages_" . $this->language . "` WHERE `page`='" . $page . "' LIMIT 1;";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        if ($result->num_rows == 1) {
            $Apages_db = $result->fetch_assoc();
            return "<a href=\"index.php" . $this->parameter_link("page=" . $page, $do_sid) . "\" target=\"" . $target . "\" class=\"" . $class . "\" id=\"nav_" . $page . "\" style=\"" . $style . "\">" . $Apages_db['link_text'] . "</a>";
        } else {
            return "No Link found";
        }
    }

    protected function parameter_link($parameter = "", $do_sid = TRUE)
    {
        if ($do_sid && !$this->cookies_enabled) {
            if (!empty($parameter)) {
                $parameter = "&" . $parameter;
            }
            $parameter = "?sid=" . session_id() . $parameter;
        } else {
            if (!empty($parameter)) {
                $parameter = "?" . $parameter;
            }
        }
        return $parameter;
    }

    public function img($path, $alt = "", $class = "", $onclick = "", $style = "", $border = "0")
    {
        return "<img src=\"" . $path . "\" alt=\"" . $alt . "\" class=\"" . $class . "\" style=\"" . $style . "\" border=\"" . $border . "\" onClick=\"" . $onclick . "\" />";
    }

    public function img_id($path, $alt = "", $id = "", $class = "", $onclick = "", $style = "", $border = "0")
    {
        return "<img src=\"" . $path . "\" alt=\"" . $alt . "\" id=\"" . $id . "\" class=\"" . $class . "\" style=\"" . $style . "\" border=\"" . $border . "\" onClick=\"" . $onclick . "\" />";
    }

    public function money($money = 0, $currency = CURRENCY_SYMBOL)
    {
        $money = round($money, 2);
        if (!empty($currency)) $currency = " " . $currency;
        return number_format($money, 2, ",", "") . $currency;
    }

    public function tax_calc($price, $type, $vat)
    {
        if (($price == "") || ($price == NULL)) {
            return "";
        }
        if ($price == 0) {
            return 0;
        }
        $decimals = 6;
        // Wenn der Brutto Wert (mit Mehrwertsteuer) von einem Netto Wert gewünscht ist:
        if ($type == "brutto") {
            return number_format(($price + (($price / 100) * $vat)), $decimals, ".", "");
        } // Wenn der Netto Wert von einem Brutto Wert gewünscht ist:
        elseif ($type == "netto") {
            return number_format((($price * 100) / (100 + $vat)), $decimals, ".", "");
        } // Wenn die Mehrwertsteuer von einem Brutto Wert gewünscht ist:
        elseif ($type == "tax_of_brutto") {
            return number_format(((($price * 100) / (100 + $vat)) * ($vat / 100)), $decimals, ".", "");
        } // Wenn die Mehrwertsteuer von einem Netto Wert gewünscht ist:
        elseif ($type == "tax_of_netto") //
        {
            return number_format((($price + (($price / 100) * $vat)) - $price), $decimals, ".", "");
        }
        return FALSE;
    }

    public function value_percent($value, $percent)
    {
        $result = $value / 100 * $percent;
        return $result;
    }

    public function price_discount($value, $percent)
    {
        $percent = 100 - $percent;
        $result = $value / 100 * $percent;
        return $result;
    }

    public function weight_kilogram($weight = 0, $decimals = 1, $divide = 1000, $weight_suffix = WEIGHT_SUFFIX)
    {
        return number_format($weight / $divide, $decimals, ",", ".") . " " . $weight_suffix;
    }

    public function switchit(&$match, $parameter1, $parameter2)
    {
        if ($match == $parameter1) {
            $match = $parameter2;
        } else {
            $match = $parameter1;
        }
    }

    public function form($form_name = "", $target_page = "", $hidden_action = "", $onSubmit = "", $location = "", $method = "POST", $form_class = "")
    {
        $content = "<form name='" . $form_name . "' action='" . $target_page . "' method='" . $method . "'  accept-charset='utf-8' onSubmit='" . $onSubmit . "' class='" . $form_class . "'>\n";
        if (!empty($hidden_action)) {
            $content .= " <input type='hidden' name='do_action' value='" . $hidden_action . "' />\n";
        }
        if (!empty($location)) {
            $content .= " <input type='hidden' name='location' value='" . $location . "' />\n";
        }
        if (!$this->cookies_enabled) {
            $content .= " <input type='hidden' name='sid' value='" . session_id() . "' />\n";
        }
        return $content;
    }

    public function form_file($form_name = "", $target_page = "", $hidden_action = "", $onSubmit = "", $location = "", $form_class = "form")
    {
        $content = "<form name='" . $form_name . "' action='" . $target_page . "' method='POST' accept-charset='utf-8' onSubmit='" . $onSubmit . "'

        class='" . $form_class . "' enctype='multipart/form-data'>\n";
        if (!empty($hidden_action)) {
            $content .= " <input type='hidden' name='do_action' value='" . $hidden_action . "' />\n";
        }
        if (!empty($location)) {
            $content .= " <input type='hidden' name='location' value='" . $location . "' />\n";
        }
        if (!$this->cookies_enabled) {
            $content .= " <input type='hidden' name='sid' value='" . session_id() . "' />\n";
        }
        return $content;
    }

    public function input_text($name = "", $value = "", $size = FORM_TEXT_SIZE, $readonly = FALSE, $freeText = "", $maxlength = "", $class = "input", $onchange = SUBMIT_CHANGE)
    {
        $content = "<input type='text' style='width:" . $size . "px;' name='" . $name . "' value='" . $value . "' maxlength='" . $maxlength . "' " . "class='" . $class;
        if ($readonly) {
            $content .= "_readonly";
        }
        $content .= "' " . $freeText;
        if ($readonly) {
            $content .= " readonly=\"readonly\"";
        }
        $content .= " onChange='" . $onchange . "' />";
        return $content;
    }

    public function input_password($name = "", $size = FORM_TEXT_SIZE, $readonly = FALSE, $freeText = "", $maxlength = "", $class = "input", $onchange = SUBMIT_CHANGE)
    {
        $content = "<input type='password' style='width:" . $size . "px;' name='" . $name . "' maxlength='" . $maxlength . "' " . "class='" . $class;
        if ($readonly) {
            $content .= "_readonly";
        }
        $content .= "' " . $freeText;
        if ($readonly) {
            $content .= " readonly=\"readonly\"";
        }
        $content .= " onChange='" . $onchange . "' />";
        return $content;
    }

    public function input_file($name = "", $size = "400", $freeText = "", $class = "input_file", $onchange = SUBMIT_CHANGE)
    {
        $content = "<input type='File' style='width:" . $size . "px;' name='" . $name . "' class='" . $class . " " . $freeText . "

        onChange='" . $onchange . "' />";
        return $content;
    }

    public function input_submit($value = "", $freeText = "", $class = "link_button")
    {
        $content = "<input type='submit' value='" . $value . "' " . $freeText . " class='" . $class . "' />";
        return $content;
    }

    public function input_reset($value = "", $onclick = "", $freetext = "", $class = "link_button")
    {
        $content = "<input type='reset' value='" . $value . "' onclick='" . $onclick . "' " . $freetext . " class='" . $class . "' />";
        return $content;
    }

    public function input_button($value = "", $onclick = "", $freetext = "", $class = "link_button")
    {
        $content = "<input type='button' value='" . $value . "' onclick='" . $onclick . "' " . $freetext . " class='" . $class . "' />";
        return $content;
    }

    public function input_hidden($name = "", $value = "")
    {
        $content = "<input type='hidden' name='" . $name . "' value='" . $value . "' />";
        return $content;
    }

    public function input_checkbox($name = "", $value = "", $checked = FALSE, $onclick = "", $class = "checkbox", $onchange = SUBMIT_CHANGE)
    {
        $content = "<input type='checkbox' name='" . $name . "' value='" . $value . "' class='" . $class . "' onClick='" . $onclick . "'";
        if ($checked) {
            $content .= " checked='checked'";
        }
        $content .= " onChange='" . $onchange . "' />";
        return $content;
    }

    public function input_radio($name = "", $value = "", $checked = FALSE, $onclick = "", $class = "radiobox", $onchange = SUBMIT_CHANGE)
    {
        $content = "<input type='radio' name='" . $name . "' value='" . $value . "' onclick='" . $onclick . "' class='" . $class . "'";
        if ($checked) {
            $content .= " checked='checked'";
        }
        $content .= " onChange='" . $onchange . "' />";
        return $content;
    }

    public function print_button($css, $text = "Drucken", $button_class = "button", $form_class = "")
    {
        $content = "<form class='" . $form_class . "'>" . "  <input type='button' class='" . $button_class . "' value='" . $text . "' " . "   onClick='document.getElementsByTagName(\"link\")[5].href = \"" . THEME . "css/" . $css . "\"; print();' />" . "</form>";
        return $content;
    }

    function textarea($name = "", $width = "", $height = "", $readonly = FALSE, $class = "textarea", $onchange = SUBMIT_CHANGE, $free_text = "")
    {
        $content = "<textarea $free_text name='" . $name . "' style='width:" . $width . "px; height:" . $height . "px;' onChange='" . $onchange . "' class='" . $class;
        if ($readonly) {
            $content .= "_readonly";
        }
        $content .= "'";
        if ($readonly) {
            $content .= " readonly=\"readonly\"";
        }
        $content .= ">";
        return $content;
    }

    function textarea_id($name = "", $width = "", $height = "", $id = "", $readonly = FALSE, $class = "", $onchange = SUBMIT_CHANGE)
    {
        $content = "<textarea name='" . $name . "' style='width:" . $width . "px; height:" . $height . "px;' onChange='" . $onchange . "' id='" . $id . "' class='" . $class;
        if ($readonly) {
            $content .= "_readonly";
        }
        $content .= "'";
        if ($readonly) {
            $content .= " readonly=\"readonly\"";
        }
        $content .= ">";
        return $content;
    }

    public function select_multi($name, $array, $match = -1, $start = 0, $begin_value_with = 0, $readonly = FALSE, $onchange = SUBMIT_CHANGE, $select_class = "select")
    {
        $content = "";
        if ($start == "") {
            $start = 0;
        }
        $array_size = sizeof($array);
        // Wenn nicht schreibgeschützt
        if (!$readonly) {
            $content .= "<select name='" . $name . "' class='" . $select_class . "' size='1' onChange='" . $onchange . "'>\n";
            // Falls das Array leer ist
            if (($array_size == 0) || (($array_size - $start) < 1)) {
                $content .= " <option>Fehler im Programmablauf</option>\n";
            } // Wenn kein Wert gegeben ist, der Standardmäßig ausgewählt werden soll
            elseif ($match == -1) // Gehe das Array von $start aus durch
            {
                for ($i = $start; $i < ($array_size); $i++) {
                    // Falls es sich um ein Multidimensionales Array handelt
                    if (is_array($array[$i])) {
                        // Wenn der Wert "eingeschaltet" ist
                        if ($array[$i]['enable']) // Gebe den Code (Wert) und den Namen dazu aus
                        {
                            $content .= " <option value='" . $array[$i]['code'] . "'>" . $array[$i]['value'] . "</option>\n";
                        }
                        // Wenn es ein normales Array ist
                    } else // Gebe den Code zusammen mit dem Zähler und einem Startwert aus, und natürlich den Inhalt vom array
                    {
                        $content .= " <option value='" . ($i + $begin_value_with) . "'>" . $array[$i] . "</option>\n";
                    }
                }
            } // Wenn ein Wert in der Select Auswahlliste von vornherein ausgewählt sein soll
            else {
                // Wenn es ein Multidimensionales Array ist
                if (is_array($array[0])) {
                    for ($i = $start; $i < ($array_size); $i++) {
                        if ($array[$i]['enable']) {
                            $content .= " <option value='" . $array[$i]['code'] . "'";
                            if ($match == $array[$i]['code']) {
                                $content .= " selected";
                            }
                            $content .= ">" . $array[$i]['value'] . "</option>\n";
                        }
                    }
                    // Wenn es ein normales Array ist
                } else {
                    for ($i = $start; $i < ($array_size); $i++) {
                        $content .= " <option value='" . ($i + $begin_value_with) . "'";
                        if ($match == ($i)) {
                            $content .= " selected";
                        }
                        $content .= ">" . $array[$i] . "</option>\n";
                    }
                }
            }
            $content .= "</select>";
            // Wenn ee Schreibgeschützt sein soll
        } else {
            if (($array_size == 0) || (($array_size - $start) < 1)) {
                $content .= "Fehler im Programmablauf</option>\n";
            } // Wenn kein Wert genannt ist
            elseif ($match == -1) {
                // Falls Multidimensionales Array
                if (is_array($array[$start])) {
                    // Falls eingeschaltet, ansonsten gebe nichts aus
                    if ($array[$start]['enable']) // Nehme erstes Element von $start beginnend
                    {
                        $content .= "<span class='" . $select_class . "_readonly'>" . $array[$start]['value'] . "</span>" . $this->input_hidden($name, $array[$start]['code']);
                    }
                    // Wenn normales Array
                } else {
                    // Nehme erstes Element von $start beginnend
                    $content .= "<span class='" . $select_class . "_readonly'>" . $array[$start] . "</span>" . $this->input_hidden($name, $start);
                }
            } // Wenn ein bestimmter Wert angezeigt werden soll
            else {
                if (is_array($array[$match])) {
                    if ($array[$match]['enable']) {
                        $content .= "<input type='text' value='" . $array[$match]['value'] . "' readonly='readonly' class='" . $select_class . "_readonly' name='" . uniqid() . "' />" . $this->input_hidden($name, $array[$match]['code']);
                    }
                } else {
                    $content .= "<input type='text' value='" . $array[$match] . "' readonly='readonly' class='" . $select_class . "_readonly' name='" . uniqid() . "' />" . $this->input_hidden($name, $match);
                }
            }
        }
        return $content;
    }

    public function select_key($name, $array, $match = "", $onchange = SUBMIT_CHANGE, $select_class = "select")
    {
        $content = "<select name='" . $name . "' class='" . $select_class . "' size='1' onChange='" . $onchange . "'>\n";
        $array_size = sizeof($array);
        if (($array_size == 0)) {
            $content .= " <option>Fehler im Programmablauf</option>\n";
        } elseif (empty($match)) {
            foreach ($array as $key => $value)
                $content .= " <option value='" . $value . "'>" . $key . "</option>\n";
        } else {
            foreach ($array as $key => $value) {
                $content .= " <option value='" . $value . "'";
                if ($value == $match) {
                    $content .= " selected";
                }
                $content .= ">" . $key . "</option>\n";
            }
        }
        $content .= "</select>";
        return $content;
    }

    public function select($name, $size = 1, $multiple = FALSE, $readonly = FALSE, $onchange = SUBMIT_CHANGE, $select_class = "select")
    {
        $content = "<select name='" . $name . "' class='" . $select_class;
        if ($readonly) {
            $content .= "_readonly";
        }
        $content .= "' onChange='" . $onchange . "' size='" . $size . "'";
        if ($multiple) {
            $content .= " multiple='multiple'";
        }
        $content .= ">\n";
        return $content;
    }

    public function select_countries($select_name = "", $target = "", $width = FORM_TEXT_SIZE, $onchange = SUBMIT_CHANGE, $readonly = FALSE, $select_class = "select")
    {
        $content = "";
        if (!$readonly) {
            // Datenbank abfrage:
            $sql = "SELECT * FROM `" . TBL_PREFIX . "countries_" . LANGUAGE . "`";
            $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
            $content .= "<select name='" . $select_name . "' class='" . $select_class . "' style='width:" . $width . "px;' " . "onChange='" . $onchange . "'>\n";
            // Leeren Eintrag einfügen
            $content .= " <option value=''";
            if (empty($target)) {
                $content .= " selected";
            }
            $content .= "></option>\n";
            while ($Acountry = $result->fetch_assoc()) {
                $content .= " <option value='" . $Acountry['code'] . "'";
                if (!empty($target)) {
                    if ($target == $Acountry['code']) {
                        $content .= " selected";
                    }
                }
                $content .= ">" . $Acountry['country'] . "</option>\n";
            }
            $content .= "</select>\n";
            return $content;
        } else {
            // Datenbank abfrage:
            $sql = "SELECT * FROM `" . TBL_PREFIX . "countries_" . LANGUAGE . "` WHERE `code` ='$target'";
            $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
            $Acountry = $result->fetch_assoc();
            $content .= "<input type='text' value='" . $Acountry['country'] . "' readonly='readonly' class='" . $select_class . "_readonly' name='" . uniqid() . "' style='width:" . $width . "px;' />" . $this->input_hidden($select_name, $Acountry['code']);
            return $content;
        }
    }

    public function get_country_name($target = DEFAULT_COUNTRY_CODE)
    {
        $sql = "SELECT `country` FROM `" . TBL_PREFIX . "countries_" . LANGUAGE . "` WHERE `code` = '" . $target . "' LIMIT 1;";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        if ($result->num_rows > 0) {
            $Acountry = $result->fetch_assoc();
            return $Acountry['country'];
        } else return ERROR_COUNTRY_NOT_FOUND;
    }

    public function get_country_code($target)
    {
        $sql = "SELECT `country` FROM `" . TBL_PREFIX . "countries_" . LANGUAGE . "` WHERE `country` LIKE '" . $target . "' LIMIT 1;";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        if ($result->num_rows > 0) {
            $Acountry = $result->fetch_assoc();
            return $Acountry['code'];
        } else return "";
    }

    public function get_country_names()
    {
        $sql = "SELECT `country` FROM `" . TBL_PREFIX . "countries_" . LANGUAGE . "`;";
        $result = $this->Cdb->db_query($sql, __FILE__ . ":" . __LINE__);
        if ($result->num_rows > 0) {
            $countries = array();
            while ($Acountry = $result->fetch_assoc()) {
                array_push($countries, $Acountry['country']);
            }
            return $countries;
        } else return NULL;
    }

    public function load_image($file)
    {
        list($this->image_width, $this->image_height, $this->image_type, $this->image_attr) = getimagesize($file);
    }

    public function get_image_width()
    {
        return $this->image_width;
    }

    public function get_image_height()
    {
        return $this->image_height;
    }

    public function get_image_type($convert = TRUE)
    {
        if ($convert) {
            switch ($this->image_type) {
                case 1:
                    return "gif";
                    break;
                case 2:
                    return "jpeg";
                    break;
                case 3:
                    return "png";
                    break;
                case 4:
                    return "swf";
                    break;
                default:
                    return UNKNOWN;
                    break;
            }
        } else return $this->image_type;
    }

    public function translate_special_characters($text)
    {
        $ersetzen = array(
            'ä' => 'ae',
            'ö' => 'oe',
            'ü' => 'ue',
            'ß' => 'ss',
            ' ' => '-',
            '\\' => '-',
            '/' => '-',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'å' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'þ' => 'b',
            '.' => '-',
            'ÿ' => 'y',
            '[' => '-',
            ']' => '-',
            '(' => '-',
            ')' => '-'
        );
        $text = strtr(mb_strtolower($text, 'UTF-8'), $ersetzen);
        return $text;
    }

    public function check_file($file, $dir)
    {
        $file = utf8_decode($file);
        $pos = strrpos($file, ".");
        $filename = substr($file, 0, $pos);
        $filetype = substr($file, $pos + 1, strlen($file));
        // Übersetze Umlaute, weil je nach Betriebssystem des Webservers der Dateiname nicht richtig gespeichert wird
        $filename = $this->translate_special_characters($filename);
        $newname = $this->create_filename($filename, $filetype, $dir);
        return $newname;
    }

    public function check_filename($filename, $filetype, $dir)
    {
        $filename = utf8_decode($filename);
        $filetype = utf8_decode($filetype);
        // Übersetze Umlaute, weil je nach Betriebssystem des Webservers der Dateiname nicht richtig gespeichert wird
        $filename = $this->translate_special_characters($filename);
        $newname = $this->create_filename($filename, $filetype, $dir);
        return $newname;
    }

    protected function create_filename($name, $type, $dir, $extension = "")
    {
        if (file_exists($dir . $name . $extension . "." . $type)) {
            $newname = $this->create_filename($name, $type, $dir, ($extension + 1));
            return $newname;
        } else {
            return $name . $extension . "." . $type;
        }
    }

    // Dafür zuständig, ob benötigte Dateien (für die Ausgabe) vom include Verzeichnis geladen werden, oder zuerst vom Template, wenn vorhanden
    public function require_file($path, $file)
    {

        $array = array();
        if (file_exists($this->Apath['theme'] . $path . "/" . $file)) {
            $array = file($this->Apath['theme'] . $path . "/" . $file);
            $size = sizeof($array);
            if($size == 1)
            {
                $text = $array[0];
                $array = explode("\r", $text);
            }

            array_shift($array);
            $str = implode("\r", $array);

        } elseif (file_exists($this->Apath[$path] . $file)) {
            $array = file($this->Apath[$path] . $file);
            $size = sizeof($array);
            if($size == 1)
            {
                $text = $array[0];
                $array = explode("\r", $text);
            }

            array_shift($array);
            $str = implode("\r", $array);

        } else {
            $str = "echo 'File $file not found'; die(); ";
        }

        return $str;
    }

    public function datepicker($id)
    {
        global $load_datepicker;
        $load_datepicker = TRUE;
        $content = "";
        $content .= "

        $(function() {

            $( \"#" . $id . "\" ).datetimepicker({

                changeMonth: true,

                changeYear: true

            });

        });

        ";
        return $content;
    }

    protected function return_size($val)
    {
        $val = trim($val); // Leerzeichen entfernen
        $last = strtolower($val[strlen($val) - 1]); // Typ
        switch ($last) // Typ
        {
            case 'g': // Gigabyte
                $val /= 1024;
                break;
            case 'm': // Megabyte
                $val *= 1; // In Zahl umwandeln
                break;
            case 'k': // Kilobyte
                $val *= 1024;
                break;
        }
        return $val;
    }

    public function return_max_upload()
    {
        $val1 = ini_get('post_max_size');
        $val2 = ini_get('upload_max_filesize');
        $val1 = $this->return_size($val1);
        $val2 = $this->return_size($val2);
        if ($val1 <= $val2) // Niedrigsten Wert bestimmen
        {
            return $val1;
        } else {
            return $val2;
        }
    }
}

