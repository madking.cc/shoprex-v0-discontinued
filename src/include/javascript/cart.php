<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "
function changeItem(id, attribute_id, type) {
    document.cart_form.article_id.value = id;
    document.cart_form.attribute_id.value = attribute_id;
    document.cart_form.do_action.value = type;
    document.cart_form.submit();
}
function order() {
    document.cart_form.action = 'order.php';
    document.cart_form.submit();
}
function remove_cart() {
    document.cart_form.do_action.value = 'remove_all';
    document.cart_form.submit();
}
";
