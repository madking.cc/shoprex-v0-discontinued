<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "
  function check_submit_password(idform)
  {
    if ((idform.password.value==null)||(idform.password.value==''))
    {
      alert('".ENTER_OLD_PASSWORD."');
      idform.password.focus();
      return false;   
    } 
    if ((idform.new_password.value==null)||(idform.new_password.value==''))
    {
      alert('".ENTER_NEW_PASSWORD."');
      idform.new_password.focus();
      return false;   
    } 
    if (idform.new_password.value.length < ".PASSWORD_LENGTH.")
    {
      alert('".WRONG_NEW_PASSWORD_LENGTH_PART01." ".PASSWORD_LENGTH." ".WRONG_PASSWORD_LENGTH_PART02." '+idform.new_password.value.length+' ".WRONG_PASSWORD_LENGTH_PART03."');
      idform.new_password.focus();
      return false;   
    }
    if ((idform.new_password_check.value==null)||(idform.new_password_check.value==''))
    {
      alert('".CONFIRM_NEW_PASSWORD."');
      idform.new_password_check.focus();
      return false;   
    } 
    if (idform.new_password.value != idform.new_password_check.value)
    {
      alert('".PASSWORD_DOES_NOT_MATCH."');
      idform.new_password.focus();
      return false;   
    }
      encoded_old_password = MD5(idform.password.value);
      idform.password.value = encoded_old_password;
      encoded_new_password = MD5(idform.new_password.value);
      idform.new_password.value = encoded_new_password;
      idform.new_password_check.value = encoded_new_password;  
    
    return true;
  }
";