<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "

  function login_check(formID)
  {
      var mailID=formID.mail;
      var passwordID=formID.password;
      
      if ((mailID.value==null)||(mailID.value==\"\"))
      {
        alert(\"".ENTER_EMAIL."\");
        mailID.focus();
        return false;
      }
      if (mail_check(mailID.value)==false)
      {
        mailID.focus();
        return false;
      }
      
      encoded_password = MD5(passwordID.value);
      passwordID.value = encoded_password;     
      return true;
   }
";


