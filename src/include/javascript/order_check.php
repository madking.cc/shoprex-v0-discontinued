<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script_footer .= "

		        function checkDeliveryAddress(target, formID, deliveryneed)
		        {
                    if(check_del(formID, deliveryneed))
                    {
                       formID.do_action.value = 'check_delivery_address';
                       formID.action = target;
                       formID.submit();
                    }
		        }

		        function check_read(formID)
		        {
		            if(formID.read_terms.checked == false && formID.read_privacy.checked  == false)
		            {
		                alert('".READ_TERMS_AND_PRIVACY."');
		                return false;
		            }
		            if(formID.read_terms.checked == false)
		            {
		                alert('".READ_TERMS."');
		                return false;
		            }
		            if(formID.read_privacy.checked  == false)
		            {
		                alert('".READ_PRIVACY."');
		                return false;
		            }
                    return true;


		        }

                function doRequest(target, formID, deliveryneed)
                {
                      if(!check_read(formID))
                        return false;

                      if(check_del(formID, deliveryneed))
                      {
                        formID.action = target;
                        formID.request.value = \"1\";
                        if(!formID.use_delivery_data.checked)
                        {
                            formID.use_delivery_data.checked = true;
                            formID.use_delivery_data.value = 0;
                        }
                        formID.submit();
                      }
                  }

                function doSubmit(target, formID, deliveryneed)
                {
                      if(target == \"order.php\")
                      {
                        if(!check_read(formID))
                            return false;

                          if(check_del(formID, deliveryneed))
                          {
                            formID.action = target;
                            if(!formID.use_delivery_data.checked)
                            {
                                formID.use_delivery_data.checked = true;
                                formID.use_delivery_data.value = 0;
                            }
                            formID.submit();
                          }
                      }
                      else
                      {
                             if(!formID.use_delivery_data.checked)
                             {
                                 formID.use_delivery_data.checked = true;
                                 formID.use_delivery_data.value = 0;
                             }
                             formID.action = target;
                             formID.do_action.value = '';
                             formID.submit();
                      }
                }
            
                function check_del(formID, deliveryneed)
                {\n";

if($target_mismatch || $remote_area)
{
    $script_footer .= "
              if(deliveryneed == \"delivery_need\")
                  if(!formID.use_delivery_data.checked)
                  {
                  	alert(\"".MUST_USE_DIFFERENT_DELIVERY_ADDRESS."\");
                  	return false;
                  }
                  ";
}
$script_footer .= "
                  if(formID.use_delivery_data.checked)
                  {
                     if((formID.del_name.value==\"\") || (formID.del_name.value==null))
                     {
                       alert(\"".SPECIFY_YOUR_NAME_OR_COMPANY."\");
                       formID.del_name.focus();
                       return false;
                     }

                     if((formID.del_address1.value==\"\") || (formID.del_address1.value==null))
                     {
                       alert(\"".SPECIFY_ADDRESS1."\");
                       formID.del_address1.focus();
                       return false;
                     }

                     if((formID.del_zip.value==\"\") || (formID.del_zip.value==null))
                     {
                       alert(\"".SPECIFY_ZIP."\");
                       formID.del_zip.focus();
                       return false;
                    }

                     if((formID.del_city.value==\"\") || (formID.del_city.value==null))
                     {
                       alert(\"".SPECIFY_CITY."\");
                       formID.del_city.focus();
                       return false;
                     }
                  }
                  return true;
                }
        ";

if(!$remote_area_delivery && $remote_area_delivery_check || (!$target_mismatch && !$remote_area && !$remote_area_delivery && !$remote_area_delivery_check))
{
    $script_footer .= "
            document.getElementById(\"send_order_link\").style.display = \"inline \";
            document.getElementById(\"check_delivery_address_link\").style.display = \"none\";
            document.getElementById(\"request_order_link\").style.display = \"none\";
            ";
}

$script_footer .= "
            function delivery_address_change()
            {

                document.getElementById(\"send_order_link\").style.display = \"none\";
                if(document.getElementById(\"check_delivery_address_link\")!=null)
                {
                    if(document.getElementById(\"check_delivery_address_link\").style.display == \"none\")
                    {
                        document.getElementById(\"check_delivery_address_link2\").style.display = \"inline \";
                        document.getElementById(\"request_order_link2\").style.display = \"inline \";
                    }
                }
                else
                {
                    document.getElementById(\"check_delivery_address_link2\").style.display = \"inline \";
                    document.getElementById(\"request_order_link2\").style.display = \"inline \";
                    document.getElementById(\"use_delivery_data\").checked = true;
                }
            }

            function useDeliveryDataChanged()
            {";
if(!$target_mismatch && !$remote_area)
{
    $script_footer .= "
                if(document.getElementById(\"use_delivery_data\").checked == true)
                {
                    document.getElementById(\"send_order_link\").style.display = \"none\";
                    document.getElementById(\"check_delivery_address_link2\").style.display = \"inline \";
                    document.getElementById(\"request_order_link2\").style.display = \"inline \";
                }
                else
                {
                    document.getElementById(\"send_order_link\").style.display = \"inline\";
                    document.getElementById(\"check_delivery_address_link2\").style.display = \"none \";
                    document.getElementById(\"request_order_link2\").style.display = \"none \";
                    document.getElementById(\"check_delivery_address_link\").style.display = \"none \";
                    document.getElementById(\"request_order_link\").style.display = \"none \";
                }\n";
}
$script_footer .= "
            }
";

