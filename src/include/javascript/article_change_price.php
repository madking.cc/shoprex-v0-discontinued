<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($Aarticle['attribute'] !== FALSE)
{

    $script_footer .= "

        new_price = new Array();
        new_price[0] = '".$Cpage->money($Cpage->tax_calc(($Aarticle['netto_original']), "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]), NO_CURRENCY)."';
    ";
    if(USE_ARTICLE_NUMBER)
        $script_footer .= "
        new_number = new Array();
        new_number[0] = '<br />';
    ";

    foreach($Aarticle['attribute']['netto'] as $key => $value)
    {
        $script_footer .= "new_price[".($key+1)."] = '".$Cpage->money($Cpage->tax_calc(($Aarticle['netto_original']+$value), "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]), NO_CURRENCY)."';\n";
        if(USE_ARTICLE_NUMBER)
        {
            if(!empty($Aarticle['attribute']['number'][$key]))
                $script_footer .= "new_number[".($key+1)."] = '".ARTICLE_NUMBER.": ".$Aarticle['attribute']['number'][$key]."';\n";
            else
                $script_footer .= "new_number[".($key+1)."] = '<br />';\n";
        }

    }

    if($Aarticle['has_discount'])
    {
        $script_footer .= "discount_price = new Array();
        discount_price[0] = '".$Cpage->money($Cpage->price_discount($Cpage->tax_calc($tpl_article['netto_original'], "brutto", $Atax[$tpl_article['tax']]), $tpl_article['discount']), NO_CURRENCY)."';\n";

        foreach($Aarticle['attribute']['netto'] as $key => $value)
        {
            $script_footer .= "discount_price[".($key+1)."] = '".$Cpage->money($Cpage->price_discount($Cpage->tax_calc(($Aarticle['netto_original']+$value), "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]), $tpl_article['discount']), NO_CURRENCY)."';\n";
        }

    }
    $script_footer .= "

      function fix_price(formID)
      {
        ";
    if(USE_ARTICLE_NUMBER)
        $script_footer .= "

            index = formID.attribute.selectedIndex;
            document.getElementById('article_number').innerHTML = new_number[index];
        ";

    if($Aarticle['has_discount'])
    {
        $script_footer .= "
        index = formID.attribute.selectedIndex;
        document.getElementById('old_price').innerHTML = new_price[index] + ' ".CURRENCY_SYMBOL."';
        document.getElementById('new_price').innerHTML = discount_price[index] + ' ".CURRENCY_SYMBOL."';
        valueA = new_price[index];
        valueA = valueA.replace(/,/, '.');
        valueA = parseFloat(valueA);
        valueB = discount_price[index];
        valueB = valueB.replace(/,/, '.');
        valueB = parseFloat(valueB);
        diff = valueA - valueB;
        diff = diff.toFixed(2)
        diff = diff.toString();
        diff = diff.replace(/\./, ',');
        document.getElementById('diff_price').innerHTML = diff + ' ".CURRENCY_SYMBOL."';
        ";
    }
    else
    {
        $script_footer .= "
        index = formID.attribute.selectedIndex;
        document.getElementById('price').innerHTML = new_price[index] + ' ".CURRENCY_SYMBOL."';
        ";
    }

    $script_footer .= "
      }";

    if(USE_ARTICLE_NUMBER)
        $script_footer .= "
          document.getElementById('article_number').innerHTML = new_number[0];
        ";

}