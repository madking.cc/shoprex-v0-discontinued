<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "
  function check_submit_article(formID)
  {
    if (formID.quantity.value==\"0\" || formID.quantity.value==\"\")
    {
      alert(\"".NO_0_ARTICLE."\");
      formID.quantity.focus();
      return false;   
    } 
    if (isNaN(formID.quantity.value))
    {
      alert(\"".NOT_A_NUMBER."\");
      formID.quantity.focus();
      return false;   
    } 
    if (formID.quantity.value>".MAX_ORDER_ARTICLE.")
    {
      alert(\"".MAX_ARTICLE_REACHED." ".MAX_IS." ".MAX_ORDER_ARTICLE." ".PIECES.".\");
      formID.quantity.focus();
      return false;   
    }";

if($Aarticle['attribute'] !== FALSE)
{
    $script .= "
    if (formID.attribute.options[0].selected == true)
    {
        alert(\"".YOU_MUST_CHOOSE_A_SELECTION."\");
        formID.attribute.focus();
        return false;
    }
    ";
    foreach($Aarticle['attribute']['type'] as $key => $value)
    {
        if($Aarticle['attribute']['disabled'][$key])
        {
            $script .= "
            if (formID.attribute.options[".($key+1)."].selected == true)
            {
                alert(\"".THIS_ARTICLE_IS_SOLD_OUT."\");
                formID.attribute.focus();
                return false;
            }
            ";
        }
    }
}

$script .= "
    return true;
  }
\n";
