<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "
  function check_submit_user(idform)
  {
    if ((idform.firstname.value==null)||(idform.firstname.value==\"\"))
    {
      alert(\"".ENTER_FIRSTNAME."\");
      idform.firstname.focus();
      return false;   
    }    
    if ((idform.lastname.value==null)||(idform.lastname.value==\"\"))
    {
      alert(\"".ENTER_LASTNAME."\");
      idform.lastname.focus();
      return false;   
    } 
    if ((idform.address1.value==null)||(idform.address1.value==\"\"))
    {
      alert(\"".ENTER_ADDRESS1."\");
      idform.address1.focus();
      return false;   
    } 
    if ((idform.zip.value==null)||(idform.zip.value==\"\"))
    {
      alert(\"".ENTER_ZIP."\");
      idform.zip.focus();
      return false;   
    } 
    if ((idform.city.value==null)||(idform.city.value==\"\"))
    {
      alert(\"".ENTER_TOWN."\");
      idform.city.focus();
      return false;   
    }
    return form_mail_check(idform.mail);
  }
";