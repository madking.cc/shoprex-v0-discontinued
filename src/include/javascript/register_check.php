<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "
  function check_submit_register(idform)
  {
    if (!idform.agb.checked)
    {
      alert(\"".ACCEPT_OUR_TERMS."\");
      idform.firstname.focus();
      return false;   
    }
	
    if (!idform.privacy.checked)
    {
      alert(\"".ACCEPT_OUR_PRIVACY_RULES."\");
      idform.firstname.focus();
      return false;   
    } 	    

    if ((idform.firstname.value==null)||(idform.firstname.value==\"\"))
    {
      alert(\"".ENTER_FIRSTNAME."\");
      idform.firstname.focus();
      return false;   
    }    
    if ((idform.lastname.value==null)||(idform.lastname.value==\"\"))
    {
      alert(\"".ENTER_LASTNAME."\");
      idform.lastname.focus();
      return false;   
    } 
    if ((idform.address1.value==null)||(idform.address1.value==\"\"))
    {
      alert(\"".ENTER_ADDRESS1."\");
      idform.address1.focus();
      return false;   
    } 
    if ((idform.zip.value==null)||(idform.zip.value==\"\"))
    {
      alert(\"".ENTER_ZIP."\");
      idform.zip.focus();
      return false;   
    } 
    if ((idform.city.value==null)||(idform.city.value==\"\"))
    {
      alert(\"".ENTER_TOWN."\");
      idform.city.focus();
      return false;   
    } 
    if ((idform.password.value==null)||(idform.password.value==\"\"))
    {
      alert(\"".ENTER_PASSWORD."\");
      idform.password.focus();
      return false;   
    } 
    if (idform.password.value.length < ".PASSWORD_LENGTH.")
    {
      alert(\"".WRONG_PASSWORD_LENGTH_PART01." ".PASSWORD_LENGTH." ".WRONG_PASSWORD_LENGTH_PART02." \"+idform.password.value.length+\" ".WRONG_PASSWORD_LENGTH_PART03."\");
      idform.password.focus();
      return false;   
    }   
    if ((idform.password_check.value==null)||(idform.password_check.value==\"\"))
    {
      alert(\"".CONFIRM_PASSWORD."\");
      idform.password_check.focus();
      return false;   
    } 
    if (idform.password.value != idform.password_check.value)
    {
      alert(\"".PASSWORD_DOES_NOT_MATCH."\");
      idform.password.focus();
      return false;   
    } 
    status_mail = form_mail_check(idform.mail);
    
    if(!idform.newsletter.checked && status_mail)
    {
      idform.newsletter.checked = true;
      idform.newsletter.value = 0;
      encoded_password = MD5(idform.password.value);
      idform.password.value = encoded_password;
      idform.password_check.value = encoded_password;
      return true;
    }  
    else if(status_mail)
          {
            encoded_password = MD5(idform.password.value);
            idform.password.value = encoded_password;
            idform.password_check.value = encoded_password;
            return true;
          }
    return false;
  }
";
