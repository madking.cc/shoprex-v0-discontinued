<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "

  function pwd_reset_check(formID)
  {
      var mailID=formID.pwd_mail;
      var new_passwordID=formID.pwd_new_password;
      var check_passwordID=formID.pwd_password_check;
      
      if ((mailID.value==null)||(mailID.value==\"\"))
      {
        alert(\"".ENTER_EMAIL."\");
        mailID.focus();
        return false;
      }
      if (mail_check(mailID.value)==false)
      {
        mailID.focus();
        return false;
      }

    if ((new_passwordID.value==null)||(new_passwordID.value==''))
    {
      alert('".ENTER_NEW_PASSWORD."');
      new_passwordID.focus();
      return false;   
    } 
    if (new_passwordID.value.length < ".PASSWORD_LENGTH.")
    {
      alert('".WRONG_NEW_PASSWORD_LENGTH_PART01." ".PASSWORD_LENGTH." ".WRONG_PASSWORD_LENGTH_PART02." '+new_passwordID.value.length+' ".WRONG_PASSWORD_LENGTH_PART03."');
      new_passwordID.focus();
      return false;   
    }
    if ((check_passwordID.value==null)||(check_passwordID.value==''))
    {
      alert('".CONFIRM_NEW_PASSWORD."');
      check_passwordID.focus();
      return false;   
    } 
    if (new_passwordID.value != check_passwordID.value)
    {
      alert('".PASSWORD_DOES_NOT_MATCH."');
      new_passwordID.focus();
      return false;   
    }      
      
      encoded_password = MD5(new_passwordID.value);
      new_passwordID.value = encoded_password; 
      check_passwordID.value = encoded_password;     
      return true;
   }
";
