<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$script .= "

  function mail_check(str)
  {
      var at=\"@\";
      var dot=\".\";
      var lat=str.indexOf(at);
      var lstr=str.length;
      var ldot=str.indexOf(dot);

      if (str.indexOf(at)==-1)
      {
         alert(\"".WRONG_EMAIL_TYPE."\");
         return false;
      }
      if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
      {
         alert(\"".WRONG_EMAIL_TYPE."\");
         return false;
      }
      if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
      {
          alert(\"".WRONG_EMAIL_TYPE."\");
          return false;
      }
      if (str.indexOf(at,(lat+1))!=-1)
      {
        alert(\"".WRONG_EMAIL_TYPE."\");
        return false;
      }
      if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
      {
        alert(\"".WRONG_EMAIL_TYPE."\");
        return false;
      }
      if (str.indexOf(dot,(lat+2))==-1)
      {
        alert(\"".WRONG_EMAIL_TYPE."\");
        return false;
      }
      if (str.indexOf(\" \")!=-1)
      {
        alert(\"".WRONG_EMAIL_TYPE."\");
        return false;
      }

      return true;
  }

  function form_mail_check(mail_field)
  {
      var mailID=mail_field;

      if ((mailID.value==null)||(mailID.value==\"\"))
      {
        alert(\"".ENTER_EMAIL."\");
        mailID.focus();
        return false;
      }
      if (mail_check(mailID.value)==false)
      {
        mailID.focus();
        return false;
      }

      return true;
   }
";