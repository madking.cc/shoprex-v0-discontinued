<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$uri                       = dirname(__FILE__)."/..";
$Apath                     = array();
$Apath['root']             = $uri."/";
$Apath['classes']          = $uri."/classes/";
$Apath['include']          = $uri."/include/";
$Apath['javascript']       = $uri."/include/javascript/";
$Apath['modules']          = $uri."/include/modules/";
$Apath['mail']             = $uri."/include/mail/";
$Apath['templates']             = $uri."/include/templates/";
$Apath['download']         = $uri."/download/";
$Apath['admin']            = $uri."/admin/";
$Apath['log']              = $uri."/log/";
$Apath['themes']           = $uri."/themes/";
$Apath['theme']            = $uri."/".THEME;
$Apath['upload']           = $uri."/upload/";
$Apath['upload_big']       = $uri."/upload/big/";
$Apath['upload_medium']    = $uri."/upload/medium/";
$Apath['upload_small']     = $uri."/upload/small/";
$Apath['upload_tiny']      = $uri."/upload/tiny/";
$Apath['theme_templates']  = $uri."/".THEME."templates/";
$Apath['theme_javascript'] = $uri."/".THEME."javascript/";
$Apath['theme_mail']       = $uri."/".THEME."mail/";
