<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(!isset($page))
{
    $page = $Cpage->get_parameter("page", "index");
}

// Für den Startbutton, setzte ausgewählte Kategorie zurück
if($page == "start")
{
    $_SESSION['selected_cat'] = "";
}

// Für Seiten aus der Datenbank festlegen, ob Shop gezeigt werden soll
if(!defined("SHOW_SHOP_".strtoupper($page)))
{
    $sql    = "SELECT `show_shop` FROM `".TBL_PREFIX."pages_".LANGUAGE."` WHERE `page` like '$page';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($result->num_rows > 0)
    {
        $Apage = $result->fetch_assoc();
        define("SHOW_SHOP_".strtoupper($page), $Apage['show_shop']);
    }
    else define("SHOW_SHOP_".strtoupper($page), 1); // Zeige Shop immer an, wenn keine Einstellung festgelegt
}

// Starte Content mit einem leeren String:
$content = "";

// Falls Kategorie angeklickt wurde, hole auch vorher ausgewählte Kategorie, wenn keine angeklickt
$_SESSION['selected_cat'] = $Cpage->get_parameter("category", $_SESSION['selected_cat']);

// Falls Artikel angeklickt wurde
if(($article_id = $Cpage->get_parameter("article", FALSE)) != FALSE)
{
    // Hole die direkte Kategorie ID zum zugehörigen Artikel
    $category_id              = $Carticles->get_category_id($article_id);
    $_SESSION['selected_cat'] = $category_id;
}

// Prüfe, ob sich der User im Shop (Artikeln/Kategorien) bewegt
if(($Cpage->get_parameter("article") > 0) || ($Cpage->get_parameter("category") > 0))
{
    $page = "user_is_in_shop";
}
elseif($page == "index") // Falls die Domain direkt aufgerufen wird
{
    $_SESSION['selected_cat'] = "";
}

// Wenn der Shop (sprich Kategorien, etc.) gezeigt werden soll
if(constant("SHOW_SHOP_".strtoupper($page)) && !HIDE_SHOP)
{
    // Lade das Modul für die Kategorien Ausgabe
    require $dir_root."include/modules/mod_categories.php";
    // Lade das Modul für Top Offer
    require $dir_root."include/modules/mod_top_offer.php";
}

// Hole den Subtitle
if(defined("SUBTITLE_".strtoupper($page)))
{
    $subtitle = SUBTITLE_SEPARATOR.constant("SUBTITLE_".strtoupper($page));
}
else
{
    $subtitle = "";
}

switch($page)
{
    // Falls eine andere Seite außer index.php aufgerufen wurde, deren Inhalt
    // nicht in der Datenbank ist, bzw. deren Typ dynamisch (PHP) ist:
    case "shipping_cost":
        $nav_control = $page;
        require $dir_root."include/modules/mod_shipping_cost.php";
        break;

    case "account":
        $nav_control = $page;
        require $dir_root."include/modules/mod_account.php";
        break;

    case "cart":
        $nav_control = $page;
        require $dir_root."include/modules/mod_cart.php";
        break;

    case "order":
        $nav_control = "cart";
        require $dir_root."include/modules/mod_order.php";
        break;

    case "question":
        $nav_control = $page;
        require $dir_root."include/modules/mod_question.php";
        break;

    case "search":
        $nav_control = $page;
        require $dir_root."include/modules/mod_search.php";
        break;

    case "password":
        $nav_control = "account";
        require $dir_root."include/modules/mod_password.php";
        break;

    case "newsletter":
        $nav_control = "account";
        require $dir_root."include/modules/mod_newsletter.php";
        break;

    case "register":
        $nav_control = "account";
        require $dir_root."include/modules/mod_register.php";
        break;

    // Shop Seiten:
    case "start":
    case "index":
    case "user_is_in_shop":
        $nav_control = "shop";

        // Ausgewählten Artikel anzeigen:
        if(($article_id = $Cpage->get_parameter("article")) != FALSE)
        {
            $Aarticle = $Carticles->get_article($article_id);
            if(!empty($Aarticle))
            {
                $subtitle = SUBTITLE_SEPARATOR.$Aarticle['name'];
                require $dir_root."include/modules/mod_article_show.php";
            }
            else
            {
                $subtitle = SUBTITLE_SEPARATOR.ARTICLE_NOT_AVAILABLE;
                $content .= ARTICLE_NOT_AVAILABLE;
            }
        } // Artikel-/Kategorieliste zur ausgewählten Kategorie anzeigen:
        elseif($_SESSION['selected_cat'] != FALSE)
        {
            $Acurrent_category = $Ccategories->get_category($_SESSION['selected_cat']);
            if(empty($Acurrent_category))
            {
                $subtitle = SUBTITLE_SEPARATOR.CATEGORY_NOT_AVAILABLE;
                $content .= CATEGORY_NOT_AVAILABLE;
            }
            else
            {
                $subtitle = SUBTITLE_SEPARATOR.$Acurrent_category['name'];
                require ($dir_root."include/modules/mod_article_list.php");
            }
        }
        // Die Startseite anzeigen:
        else
        {
            if(!SHOW_START_PAGE || ($page == "start"))
            {
                require ($dir_root."include/modules/mod_shopstart.php");
            }
        }
        if(!SHOW_START_PAGE || ($page == "start"))
        {
            break;
        }

    // Seite aus der Datenbank:

    default:

        $sql    = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."` WHERE `page` = '".$page."' LIMIT 1;";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 1)
        {
            $Apage       = $result->fetch_assoc();
            $nav_control = $Apage['page'];
            $subtitle    = SUBTITLE_SEPARATOR.$Apage['subtitle'];
            $content .= "<H2 class='content_header'>".$Apage['subtitle']."</H2>\n";
            $content .= $Apage['value'];
        }

        break;
}



