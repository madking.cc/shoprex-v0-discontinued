<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Definiere die Standard Zulieferungsart
$delivery_start = 0;
foreach($Adelivery as $key => $value)
{
    if($value['enable'])
    {
        $delivery_start = $key;
        break;
    }
}
if(!isset($_SESSION['order']['delivery_type']))
{
    $_SESSION['order']['delivery_type'] = $delivery_start;
}

if(!isset($_SESSION['selected_cat']))
{
    $_SESSION['selected_cat'] = "";
}

if(!isset($_SESSION['customer']['use_delivery_data']))
{
    $_SESSION['customer']['use_delivery_data'] = "";
}
if(!isset($_SESSION['customer']['del_name']))
{
    $_SESSION['customer']['del_name'] = "";
}
if(!isset($_SESSION['customer']['del_address1']))
{
    $_SESSION['customer']['del_address1'] = "";
}
if(!isset($_SESSION['customer']['del_address2']))
{
    $_SESSION['customer']['del_address2'] = "";
}
if(!isset($_SESSION['customer']['del_zip']))
{
    $_SESSION['customer']['del_zip'] = "";
}
if(!isset($_SESSION['customer']['del_city']))
{
    $_SESSION['customer']['del_city'] = "";
}
if(!isset($_SESSION['customer']['del_country']))
{
    $_SESSION['customer']['del_country'] = "";
}
if(!isset($_SESSION['customer']['del_phone']))
{
    $_SESSION['customer']['del_phone'] = "";
}
if(!isset($_SESSION['customer']['customer_message']))
{
    $_SESSION['customer']['customer_message'] = "";
}
if(!isset($_SESSION['customer']['order_id']))
{
    $_SESSION['customer']['order_id'] = "";
}

if(!isset($_SESSION['customer']['mail']))
{
    $_SESSION['customer']['mail'] = "";
}
if(!isset($_SESSION['customer']['password']))
{
    $_SESSION['customer']['password'] = "";
}
if(!isset($_SESSION['customer']['firstname']))
{
    $_SESSION['customer']['firstname'] = "";
}
if(!isset($_SESSION['customer']['lastname']))
{
    $_SESSION['customer']['lastname'] = "";
}
if(!isset($_SESSION['customer']['address1']))
{
    $_SESSION['customer']['address1'] = "";
}
if(!isset($_SESSION['customer']['address2']))
{
    $_SESSION['customer']['address2'] = "";
}
if(!isset($_SESSION['customer']['zip']))
{
    $_SESSION['customer']['zip'] = "";
}
if(!isset($_SESSION['customer']['city']))
{
    $_SESSION['customer']['city'] = "";
}
if(empty($_SESSION['customer']['country']))
{
    $_SESSION['customer']['country'] = DEFAULT_COUNTRY_CODE;
}
if(!isset($_SESSION['customer']['phone']))
{
    $_SESSION['customer']['phone'] = "";
}
if(!isset($_SESSION['customer']['type']))
{
    $_SESSION['customer']['type'] = "";
}
if(!isset($_SESSION['customer']['company']))
{
    $_SESSION['customer']['company'] = "";
}
if(!isset($_SESSION['customer']['newsletter']))
{
    $_SESSION['customer']['newsletter'] = 1;
}

if(!isset($_SESSION['order']['cart_price_brutto']))
{
    $_SESSION['order']['cart_price_brutto'] = "Fehler im Programmablauf";
}
if(!isset($_SESSION['order']['weight_to_heavy']))
{
    $_SESSION['order']['weight_to_heavy'] = "";
}
if(!isset($_SESSION['order']['cart_country_target']))
{
    $_SESSION['order']['cart_country_target'] = DEFAULT_COUNTRY_CODE;
}

if(!isset($_SESSION['pwd_key']))
{
    $_SESSION['pwd_key'] = NULL;
}
if(!isset($_SESSION['pwd_mail']))
{
    $_SESSION['pwd_mail'] = "";
}
if(!isset($_SESSION['pwd_customer_id']))
{
    $_SESSION['pwd_customer_id'] = "";
}
if(!isset($_SESSION['pwd_id']))
{
    $_SESSION['pwd_id'] = "";
}
if(!isset($_SESSION['pwd_new_password']))
{
    $_SESSION['pwd_new_password'] = "";
}

if(!isset($_SESSION['cart']))
{
    $_SESSION['cart'] = array();
}

if(!isset($load_md5_js))
{
    $load_md5_js = FALSE;
}

if(!isset($css)) $css = "";
if(!isset($script)) $script = "";
if(!isset($script_header)) $script_header = "";
if(!isset($script_footer)) $script_footer = "";
