<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer = $_SESSION['customer'];

$Acustomer['homepage']   = $Cpage->Aglobal['www'];
$Acustomer['time']       = $Cpage->Aglobal['now'];
$Acustomer['country']    = $Cpage->get_country_name($Acustomer['country']);
$Acustomer['type']       = $Cpage->Aglobal['customer_type'][$Acustomer['type']];
$Acustomer['newsletter'] = $Cpage->Aglobal['answer'][$Acustomer['newsletter']];
$Acustomer['theme']    = THEME;

$Amail = $Cpage->get_mail_draft("register");

foreach($Acustomer as $key => $value)
{
    $Amail['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['text']);
    $Amail['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['subject']);
}

$Cpage->send_mail(SHOP_MAIL, $Amail['text'], $Amail['subject'], $Acustomer['mail']);
$Cpage->send_mail($Acustomer['mail'], $Amail['text'], $Amail['subject'], SHOP_MAIL);
