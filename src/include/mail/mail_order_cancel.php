<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer['homepage'] = $Cpage->Aglobal['www'];
$Acustomer['time']     = $Cpage->Aglobal['now'];
$Acustomer['theme']    = THEME;

$Amail = $Cpage->get_mail_draft("order_cancel");

foreach($Acustomer as $key => $value)
{
    $Amail['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['text']);
    $Amail['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['subject']);
}

foreach($Aorder as $key => $value)
{
    $Amail['text']    = preg_replace("/\\\$order\['".$key."'\]/i", $value, $Amail['text']);
    $Amail['subject'] = preg_replace("/\\\$order\['".$key."'\]/i", $value, $Amail['subject']);
}

$Cpage->send_mail($Acustomer['mail'], $Amail['text'], $Amail['subject']);
