<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer['mail']     = $question_from_email;
$Acustomer['subject']  = $question_header;
$Acustomer['text']     = nl2br(preg_replace('/ /', "&#160;", $question_text));
$Acustomer['homepage'] = $Cpage->Aglobal['www'];
$Acustomer['time']     = $Cpage->Aglobal['now'];
$Acustomer['theme']    = THEME;

if(empty($Acustomer['subject']))
{
    $Acustomer['subject'] = NO_SUBJECT_GIVEN;
}
if(empty($Acustomer['text']))
{
    $Acustomer['text'] = NO_TEXT_GIVEN;
}

$Aarticle['picture']   = $Cpage->Aglobal['http'].UPLOAD_DIR."small/".$Aarticle['picture'][0];
$Aarticle['price']     = $Cpage->money($Cpage->tax_calc($Aarticle['netto'], "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]));
$Aarticle['tax']       = $Cpage->Aglobal['tax'][$Aarticle['tax']]."%";
$Aarticle['condition'] = $Cpage->Aglobal['condition'][$Aarticle['condition']];
$Aarticle['guarantee'] = $Cpage->Aglobal['guarantee'][$Aarticle['guarantee']];
$Aarticle['weight']    = $Cpage->weight_kilogram($Aarticle['weight']);
$Aarticle['link']      = $Cpage->Aglobal['http']."index.php?article=".$Aarticle['id'];

$Amail = $Cpage->get_mail_draft("question");

if(!ENABLE_TAX)
{
    $Amail['text'] = preg_replace("/\[!if_tax\](.*?)\[!end_if_tax\]/is", "", $Amail['text']);
}
else
{
    $Amail['text'] = preg_replace("/\[!if_tax\]/is", "", $Amail['text']);
    $Amail['text'] = preg_replace("/\[!end_if_tax\]/is", "", $Amail['text']);
}

foreach($Aarticle as $key => $value)
{
    if(!is_array($value))
    {
        $Amail['text']    = preg_replace("/\\\$article\['".$key."'\]/i", $value, $Amail['text']);
        $Amail['subject'] = preg_replace("/\\\$article\['".$key."'\]/i", $value, $Amail['subject']);
    }
}

foreach($Acustomer as $key => $value)
{
    $Amail['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['text']);
    $Amail['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $Amail['subject']);
}

$mail_send_status = $Cpage->send_mail($Acustomer['mail'], $Amail['text'], $Amail['subject'], MAIL_QUESTIONS);
