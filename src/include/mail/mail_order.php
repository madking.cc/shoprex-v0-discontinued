<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer = array();
$Acustomer = $_SESSION['customer'];

$Acustomer['time']     = $Cpage->Aglobal['now'];
$Acustomer['homepage'] = $Cpage->Aglobal['www'];
$Acustomer['theme']    = THEME;

$order['time_of_order'] = $Cpage->format_time($time_of_order);
$order['date_of_order'] = $Cpage->format_time($time_of_order, "date");
$order['order_phone']   = $order_phone;
$order['id']            = $order_id;

if($_SESSION['customer']['type'] == 2 && !empty($_SESSION['customer']['company']))
{
    $Acustomer['name'] = $_SESSION['customer']['company'];
}
else $Acustomer['name'] = $_SESSION['customer']['firstname']." ".$_SESSION['customer']['lastname'];

$Acustomer['address'] = $_SESSION['customer']['address1'];
if(!empty($_SESSION['customer']['address2']))
{
    $Acustomer['address'] .= "<br />".$_SESSION['customer']['address2'];
}

$Acustomer['del_address'] = $_SESSION['customer']['del_address1'];
if(!empty($_SESSION['customer']['del_address2']))
{
    $Acustomer['del_address'] .= "<br />".$_SESSION['customer']['del_address2'];
}

$bank_needed = FALSE;
if($_SESSION['order']['delivery_type'] == DO_ADVANCE)
{
    $bank_needed   = TRUE;
    $order['bank'] = TRANSFER_TO_BANK_ACCOUNT.":<br /><br />";
    $order['bank'] .= ACCOUNT_OWNER.": ".SHOP_BANK_ACCOUNT_OWNER."<br />";
    $order['bank'] .= BANK_NAME.": ".SHOP_BANK_NAME."<br />";
    $order['bank'] .= BANK_CODE.": ".SHOP_BANK_CODE."<br />";
    $order['bank'] .= BANK_NUMBER.": ".SHOP_BANK_NUMBER."<br /><br />";
    $dummy = SHOP_BANK_IBAN;
    if(!empty($dummy))
    {
        $order['bank'] .= FOR_INTERNATIONAL_TRANSFERS.":<br /><br />";
        $order['bank'] .= IBAN.": ".SHOP_BANK_IBAN."<br />";
        $order['bank'] .= SWIFT.": ".SHOP_BANK_SWIFT."<br><br />";
    }
    $order['bank'] .= "<strong>".REASON_FOR_TRANSFER."</strong>";
}

$Acustomer['delivery_type'] = $Cpage->Aglobal['delivery'][$_SESSION['order']['delivery_type']]['value'];
$Acustomer['id']            = $_SESSION['customer']['customer'];
$Acustomer['country']       = $Cpage->get_country_name($_SESSION['customer']['country']);
$Acustomer['del_country']   = $Cpage->get_country_name($_SESSION['customer']['del_country']);

$user_message = FALSE;
if(!empty($_SESSION['customer']['customer_message']))
{
    $order['message'] = nl2br(preg_replace('/ /', "&#160;", $_SESSION['customer']['customer_message']));
    $user_message     = TRUE;
}

$order['footer_text'] = INVOICE_FOOTER_TEXT;

$Amailcart                               = $Acart;
$Amailcart['delivery_target']            = $Cpage->get_country_name($delivery_target);
$Amailcart['total_weight']               = $Cpage->weight_kilogram($Acart['total_weight']);
$Amailcart['shipping_error']             = $Acart['shipping_error'];
$Amailcart['cod_shipping_error']         = $Acart['cod_shipping_error'];
$Amailcart['cod_shipping_cost_brutto']   = $Cpage->money($Acart['cod_shipping_cost_brutto']);
$Amailcart['cod_shipping_cost_netto']    = $Cpage->money($Acart['cod_shipping_cost_netto']);
$Amailcart['shipping_cost_netto']        = $Cpage->money($Acart['shipping_cost_netto']);
$Amailcart['shipping_cost_brutto']       = $Cpage->money($Acart['shipping_cost_brutto']);
$Amailcart['total_shipping_cost_netto']  = $Cpage->money($Acart['shipping_cost_netto']+$Acart['cod_shipping_cost_netto']);
$Amailcart['total_shipping_cost_brutto'] = $Cpage->money($Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto']);
$Amailcart['shipping_tax']               = $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]."%";
$Amailcart['total_brutto']               = $Cpage->money($Acart['cart_price_brutto']+$Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto']);
$Amailcart['total_netto']                = $Cpage->money($Acart['cart_price_netto']+$Acart['shipping_cost_netto']+$Acart['cod_shipping_cost_netto']);

$Amailcart['has_shipping_cost']     = FALSE;
$Amailcart['has_cod_shipping_cost'] = FALSE;
if($_SESSION['order']['delivery_type'] < NO_SHIPPING_COST)
{
    $Amailcart['has_shipping_cost'] = TRUE;
    if($_SESSION['order']['delivery_type'] == COD_SHIPPING_COST)
    {
        $Amailcart['has_cod_shipping_cost'] = TRUE;
    }
}

$taxlisting = "";
if(ENABLE_TAX)
{
    if(DELIVERY_METHOD == "weight")
    {
        $mail_order_tax = $Cpage->get_mail_draft("order_weight_tax", FALSE);
    }
    if(DELIVERY_METHOD == "flatrate")
    {
        $mail_order_tax = $Cpage->get_mail_draft("order_flatrate_tax", FALSE);
    }
    if(DELIVERY_METHOD == "free")
    {
        $mail_order_tax = $Cpage->get_mail_draft("order_free_tax", FALSE);
    }

    if(!empty($Acart['tax']))
    {
        if(!USE_ARTICLE_NUMBER)
        {
            $mail_order_tax['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $mail_order_tax['text']);
        }
        else
        {
            $mail_order_tax['text'] = preg_replace("/\[!if_number\]/is", "", $mail_order_tax['text']);
            $mail_order_tax['text'] = preg_replace("/\[!end_if_number\]/is", "", $mail_order_tax['text']);
        }

        foreach($Acart['tax'] as $tax_percent => $tax)
        {
            if(!empty($tax))
            {
                $text = preg_replace("/\\\$tax\['percent'\]/i", $tax_percent."%", $mail_order_tax['text']);
                $text = preg_replace("/\\\$tax\['value'\]/i", $Cpage->money($tax), $text);
                $taxlisting .= $text."\n";
            }
        }
    }
}

if(DELIVERY_METHOD == "weight")
{
    $mail_order_cart = $Cpage->get_mail_draft("order_weight_cart", FALSE);
}
if(DELIVERY_METHOD == "flatrate")
{
    $mail_order_cart = $Cpage->get_mail_draft("order_flatrate_cart", FALSE);
}
if(DELIVERY_METHOD == "free")
{
    $mail_order_cart = $Cpage->get_mail_draft("order_free_cart", FALSE);
}
$cartlisting = "";

if(!ENABLE_TAX)
{
    $mail_order_cart['text'] = preg_replace("/\[!if_tax\](.*?)\[!end_if_tax\]/is", "", $mail_order_cart['text']);
}
else
{
    $mail_order_cart['text'] = preg_replace("/\[!if_tax\]/is", "", $mail_order_cart['text']);
    $mail_order_cart['text'] = preg_replace("/\[!end_if_tax\]/is", "", $mail_order_cart['text']);
}

if(!USE_ARTICLE_NUMBER)
{
    $mail_order_cart['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $mail_order_cart['text']);
}
else
{
    $mail_order_cart['text'] = preg_replace("/\[!if_number\]/is", "", $mail_order_cart['text']);
    $mail_order_cart['text'] = preg_replace("/\[!end_if_number\]/is", "", $mail_order_cart['text']);
}

foreach($Acart['cart'] as $key => $value)
{
    $value['total_weight'] = "<nobr>".$Cpage->weight_kilogram($value['total_weight'])."</nobr>";
    $value['brutto']       = "<nobr>".$Cpage->money($Cpage->tax_calc($value['netto'], "brutto", $value['tax']))."</nobr>";
    $value['netto']        = "<nobr>".$Cpage->money($value['netto'])."</nobr>";
    $value['total_brutto'] = "<nobr>".$Cpage->money($Cpage->tax_calc($value['total_netto'], 'brutto', $value['tax']))."</nobr>";
    $value['tax']          = $value['tax']."%";
    $value['total_netto']  = "<nobr>".$Cpage->money($value['total_netto'])."</nobr>";

    $text = $mail_order_cart['text'];
    foreach($value as $key2 => $value2)
    {
        if(!is_array($value2))
        {
            $text = preg_replace("/\\\$cart\['".$key2."'\]/i", $value2, $text);
        }
    }
    $text = preg_replace("/\\\$cart\['pos'\]/i", ($key+1), $text);

    $cartlisting .= $text."\n";
}

if($Amailcart['has_shipping_cost'])
{
    $Ashipping['pos']          = ($Amailcart['positions']+1);
    $Ashipping['name']         = "Versandkosten";
    $Ashipping['number']       = "&#160;";
    $Ashipping['total_weight'] = $Amailcart['total_weight'];
    $Ashipping['netto']        = $Amailcart['shipping_cost_netto'];
    $Ashipping['tax']          = $Amailcart['shipping_tax'];
    $Ashipping['brutto']       = "&#160;";
    $Ashipping['quantity']     = "&#160;";
    $Ashipping['total_brutto'] = $Amailcart['shipping_cost_brutto'];
    $text                      = $mail_order_cart['text'];
    foreach($Ashipping as $key => $value)
        $text = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $text);
    $cartlisting .= $text."\n";
}

if($Amailcart['has_cod_shipping_cost'])
{
    $Ashipping['pos']          = ($Amailcart['positions']+2);
    $Ashipping['name']         = "Nachnamegebühr";
    $Ashipping['number']       = "&#160;";
    $Ashipping['total_weight'] = "&#160;";
    $Ashipping['netto']        = $Amailcart['cod_shipping_cost_netto'];
    $Ashipping['tax']          = $Amailcart['cod_shipping_tax'];
    $Ashipping['brutto']       = "&#160;";
    $Ashipping['quantity']     = "&#160;";
    $Ashipping['total_brutto'] = $Amailcart['cod_shipping_cost_brutto'];
    $text                      = $mail_order_cart['text'];
    foreach($Ashipping as $key => $value)
        $text = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $text);
    $cartlisting .= $text."\n";
}

if(DELIVERY_METHOD == "weight")
{
    $mail_field = $Cpage->get_mail_draft("order_weight");
}
if(DELIVERY_METHOD == "flatrate")
{
    $mail_field = $Cpage->get_mail_draft("order_flatrate");
}
if(DELIVERY_METHOD == "free")
{
    $mail_field = $Cpage->get_mail_draft("order_free");
}

if(!ENABLE_TAX)
{
    $mail_field['text'] = preg_replace("/\[!if_tax\](.*?)\[!end_if_tax\]/is", "", $mail_field['text']);
}
else
{
    $mail_field['text'] = preg_replace("/\[!if_tax\]/is", "", $mail_field['text']);
    $mail_field['text'] = preg_replace("/\[!end_if_tax\]/is", "", $mail_field['text']);
}

if(!USE_ARTICLE_NUMBER)
{
    $mail_field['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $mail_field['text']);
}
else
{
    $mail_field['text'] = preg_replace("/\[!if_number\]/is", "", $mail_field['text']);
    $mail_field['text'] = preg_replace("/\[!end_if_number\]/is", "", $mail_field['text']);
}


if(!$_SESSION['customer']['use_delivery_data'])
{
    $mail_field['text'] = preg_replace("/\[!if_delivery\](.*?)\[!end_if_delivery\]/is", "", $mail_field['text']);
}
else
{
    $mail_field['text'] = preg_replace("/\[!if_delivery\]/is", "", $mail_field['text']);
    $mail_field['text'] = preg_replace("/\[!end_if_delivery\]/is", "", $mail_field['text']);
}

if(!$user_message)
{
    $mail_field['text'] = preg_replace("/\[!if_message\](.*?)\[!end_if_message\]/is", "", $mail_field['text']);
}
else
{
    $mail_field['text'] = preg_replace("/\[!if_message\]/is", "", $mail_field['text']);
    $mail_field['text'] = preg_replace("/\[!end_if_message\]/is", "", $mail_field['text']);
}

if(!$bank_needed)
{
    $mail_field['text'] = preg_replace("/\[!if_bank\](.*?)\[!end_if_bank\]/is", "", $mail_field['text']);
}
else
{
    $mail_field['text'] = preg_replace("/\[!if_bank\]/is", "", $mail_field['text']);
    $mail_field['text'] = preg_replace("/\[!end_if_bank\]/is", "", $mail_field['text']);
}

foreach($order as $key => $value)
{
    $mail_field['text']    = preg_replace("/\\\$order\['".$key."'\]/i", $value, $mail_field['text']);
    $mail_field['subject'] = preg_replace("/\\\$order\['".$key."'\]/i", $value, $mail_field['subject']);
}

foreach($Amailcart as $key => $value)
{
    if(is_array($value))
    {
        continue;
    }
    $mail_field['text']    = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $mail_field['text']);
    $mail_field['subject'] = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $mail_field['subject']);
}

$mail_field['text'] = preg_replace("/\\\$cart_listing/i", $cartlisting, $mail_field['text']);
$mail_field['text'] = preg_replace("/\\\$tax_listing/i", $taxlisting, $mail_field['text']);

foreach($Acustomer as $key => $value)
{
    if(is_array($value))
    {
        continue;
    }
    $mail_field['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['text']);
    $mail_field['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['subject']);
}

// von, text, subject, target
$Cpage->send_mail(SHOP_MAIL, $mail_field['text'], $mail_field['subject'], $Acustomer['mail'], "order");
if(SEND_ORDERS_HOME)
{
    $Cpage->send_mail($Acustomer['mail'], $mail_field['text'], $mail_field['subject'], SHOP_MAIL, "order");
}


