<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer = $_SESSION['customer'];

$Acustomer['homepage']   = $Cpage->Aglobal['www'];
$Acustomer['ip']         = $Cpage->Aglobal['ip'];
$Acustomer['time']       = $Cpage->Aglobal['now'];
$Acustomer['country']    = $Cpage->get_country_name($Acustomer['country']);
$Acustomer['type']       = $Cpage->Aglobal['customer_type'][$Acustomer['type']];
$Acustomer['newsletter'] = $Cpage->Aglobal['answer'][$Acustomer['newsletter']];
$Acustomer['theme']    = THEME;

$mail_field = $Cpage->get_mail_draft("mail_change");

foreach($Acustomer as $key => $value)
{
    $mail_field['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['text']);
    $mail_field['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['subject']);
}

$Cpage->send_mail(SHOP_MAIL, $mail_field['text'], $mail_field['subject'], $old_mail);
$Cpage->send_mail(SHOP_MAIL, $mail_field['text'], $mail_field['subject'], $Acustomer['mail']);
