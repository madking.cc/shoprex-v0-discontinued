<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Hole die Settings aus der Datenbank:
$sql    = " SELECT `name`,`value` FROM `".TBL_PREFIX."settings`; ";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

if($result->num_rows > 0)
{
    while($settings_field = $result->fetch_assoc())
    {
        $settings[$settings_field['name']] = $settings_field['value'];
    }
}
else $settings = NULL;

foreach($settings as $setting_name => $setting_value)
    if(!defined($setting_name))
    {
        define($setting_name, $setting_value);
    }

// Erzeuge Arrays:
$defines = array('TAX');
foreach($defines as $value)
{
    if(defined("ARRAY_".$value))
    {
        ${"A".strtolower($value)} = unserialize(constant("ARRAY_".$value));
    }
}
  
