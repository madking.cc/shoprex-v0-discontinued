<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

 date_default_timezone_set('Europe/Berlin');
// Falls Installer vorhanden lade diese Datei
if(file_exists(getcwd()."/installer/index.php"))
{
    header("Location: ./installer/index.php");
    die();
}

// Einstellungen für Fehlerbericht, Logs und Cache
$do_debug  = FALSE; // Bitte im Produktiv Einsatz auf FALSE lassen!
$do_log    = TRUE;
$log_level = 0; // 0: Standard Log
// 1: Alle POST und GET Abfragen werden im Klartext gespeichert.
// 2: Datenbankabfragen werden im Klartext gespeichert.
$log_errors = TRUE; // Speichert schwere Fehler, die zum Programmabruch geführt haben
define('STORE_EVERY_VISIT', 0); // 0:nein 1:ja, speichert JEDEN Seitenaufruf, die Datenbank wird dadurch sehr groß

if($do_debug)
{
    error_reporting(E_ALL);
    ini_set('track_errors', '1');
    define('DO_DEBUG', 1);
}
else
{
    error_reporting(0);
    ini_set('display_errors', '0');
    define('DO_DEBUG', 0);
}

// Testen, ob Browser Cookies annimmt.
// Wichtig für die Session ID
$cookie_enabled = FALSE;
if(isset($_COOKIE['check_cookie']))
{
    $cookie_enabled = TRUE;
}
if(!$cookie_enabled)
{
    setcookie("check_cookie", "cookies_enabled", time()+60*60*24*30);
} // Cookie wird 30 Tage lang gespeichert

// Starte Session
session_start();

// Lade die Konfiguration (z.B. Datenbank Einstellungen)
require $dir_root."_configuration.php";

// Lade Klassen
require $dir_root."classes/error.php";
require $dir_root."classes/log.php";
require $dir_root."classes/db.php";
require $dir_root."classes/visitor.php";
require $dir_root."classes/cart.php";
require $dir_root."classes/page.php";
require $dir_root."classes/articles.php";
require $dir_root."classes/categories.php";
require $dir_root."classes/phpmailer.php";

// Starte die Singleton Fehler Klasse
$Cerror = error::singleton(dirname(__FILE__)."/../");

// Starte die Singleton Log Klasse
$Clog = log::singleton($do_log, $log_level, $log_errors);

// Stelle die Datenbankverbindung her:
$Cdb = new db(DB_SERVER, DB_USER, DB_PASS, DB_TABLE, TBL_PREFIX);

// Erstelle die Mail Klasse
$Cmail = new PHPMailer();

// Erstelle die Seitenklasse: page
$Cpage = new page($Cdb, $cookie_enabled, $Cmail);

// Erstelle die Artikelklasse
$Carticles = new articles($Cdb, $Cpage);

// Erstelle die Kategorienklasse
$Ccategories = new categories($Cdb, $Cpage, $Carticles);

// Einstellungen aus der Datenbank laden
require $dir_root."include/settings.php";

// Sprache aus der Datenbank laden
require $dir_root."include/language.php";

// Setze die Pfade
require $dir_root."include/path.php";

// Setze Variablen
require $dir_root."include/variables.php";

// Setzte Globale Variablen (auch in Klassen)
require $dir_root."include/globals.php";

// Erzeuge Besucher Statistik
$Cvisitor = new visitor($Cdb);
$Cvisitor->store_visit("visitors");

// Erstelle die Warenkorbklasse
$Ccart = new cart($Cpage, $Cdb, $Carticles);
