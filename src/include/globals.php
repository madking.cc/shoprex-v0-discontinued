<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Cpage->language     = LANGUAGE;
$Cpage->allowed_keys = array('category', 'article', 'key', 'page', 'login_error', 'data_changed', 'location', 'background');
$Cpage->check_numeric = array('category', 'article', 'login_error');

$tmp_array = array();
$tmp_array[] = "start";
$sql = "SELECT page FROM `".TBL_PREFIX."pages_".LANGUAGE."`";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
if($result->num_rows > 0)
    while($row = $result->fetch_assoc())
    {
        $tmp_array[] = $row['page'];
    }
$Cpage->allowed_pages = $tmp_array;


$Cpage->Aglobal['condition']     = $Acondition;
$Cpage->Aglobal['guarantee']     = $Aguarantee;
$Cpage->Aglobal['customer_type'] = $Acustomer_type;
$Cpage->Aglobal['answer']        = $Aanswer;
$Cpage->Aglobal['status']        = $Astatus;
$Cpage->Aglobal['delivery']      = $Adelivery;
$Cpage->Aglobal['tax']           = $Atax;
$Cpage->Apath                    = $Apath;

$host                   = $_SERVER['HTTP_HOST'];
$uri                    = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$Cpage->Aglobal['http'] = "http://".$host.$uri."/";
$Cpage->Aglobal['www']  = $host.$uri;
$Cpage->Aglobal['now']  = $Cpage->format_time($Cpage->get_sqltime());
$Cpage->Aglobal['time'] = time();

$Cpage->Amail['mail_default'] = SHOP_MAIL;
