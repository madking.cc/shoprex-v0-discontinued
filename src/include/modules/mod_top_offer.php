<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `top_offer` =1 AND `status` =1 AND ((`quantity` > 0) OR (`quantity` = 0 AND `quantity_warning` = 0)) AND `clone_from` =0;";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$Atop_offer = array();
if($result->num_rows != 0)
{

    while($top_offer_field = $result->fetch_assoc())
    {
        $article_array = $Carticles->get_article($top_offer_field['id']);
        if(!empty($article_array))
            array_push($Atop_offer, $article_array);
    }
}

if(sizeof($Atop_offer)>0)
{
    // Per Zufall ein Angebot bestimmen:
    $i         = rand(0, sizeof($Atop_offer)-1);
    $top_offer = $Atop_offer[$i];
}
else
{
    $top_offer = NULL;
}