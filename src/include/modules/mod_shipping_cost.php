<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$shipping_cost = array();
$cod_cost      = array();
$tmp           = array();
$tmp2          = array();
if(DELIVERY_METHOD != "free")
{
    if(DELIVERY_METHOD == "flatrate")
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate` WHERE `type` ='normal';";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Ashipping = $result->fetch_assoc())
        {
            unset($tmp);
            $tmp              = array();
            $tmp['countries'] = unserialize($Ashipping['targets']);
            foreach($tmp['countries'] as $key => $value)
                $tmp['countries'][$key] = $Cpage->get_country_name($value);
            $tmp['shipping_cost_netto']  = $Cpage->money($Ashipping['cost_netto']);
            $tmp['shipping_tax']         = $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]."%";
            $tmp['shipping_cost_brutto'] = $Cpage->money($Cpage->tax_calc($Ashipping['cost_netto'], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]));
            array_push($shipping_cost, $tmp);
        }

        if(ENABLE_COD)
        {
            $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate` WHERE `type` ='cod';";
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            while($Ashipping = $result->fetch_assoc())
            {
                unset($tmp);
                $tmp              = array();
                $tmp['countries'] = unserialize($Ashipping['targets']);
                foreach($tmp['countries'] as $key => $value)
                    $tmp['countries'][$key] = $Cpage->get_country_name($value);
                $tmp['shipping_cost_netto']  = $Cpage->money($Ashipping['cost_netto']);
                $tmp['shipping_tax']         = $Cpage->Aglobal['tax'][COD_TAX_KEY]."%";
                $tmp['shipping_cost_brutto'] = $Cpage->money($Cpage->tax_calc($Ashipping['cost_netto'], "brutto", $Cpage->Aglobal['tax'][COD_TAX_KEY]));
                array_push($cod_cost, $tmp);
            }
        }
    }
    elseif(DELIVERY_METHOD == "weight")
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_weight` WHERE `type` ='normal';";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        while($Ashipping = $result->fetch_assoc())
        {
            unset($tmp);
            $tmp              = array();
            $tmp['countries'] = unserialize($Ashipping['targets']);
            foreach($tmp['countries'] as $key => $value)
                $tmp['countries'][$key] = $Cpage->get_country_name($value);
            $tmp['shipping_cost_netto']  = unserialize($Ashipping['cost_netto']);
            $tmp['shipping_cost_brutto'] = array();
            foreach($tmp['shipping_cost_netto'] as $key => $value)
            {
                if(empty($value))
                {
                    unset($tmp['shipping_cost_netto'][$key]);
                    continue;
                }
                $tmp['shipping_cost_netto'][$key]  = $Cpage->money($value);
                $tmp['shipping_cost_brutto'][$key] = $Cpage->money($Cpage->tax_calc($value, "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]));
            }
            $tmp['shipping_tax'] = $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]."%";
            $tmp['weight']       = unserialize($Ashipping['weight']);
            foreach($tmp['weight'] as $key => $value)
            {
                if(empty($value))
                {
                    unset($tmp['weight'][$key]);
                    continue;
                }
                $tmp['weight'][$key] = $Cpage->weight_kilogram($value);
            }
            array_push($shipping_cost, $tmp);
        }

        if(ENABLE_COD)
        {
            $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_weight` WHERE `type` ='cod';";
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            while($Ashipping = $result->fetch_assoc())
            {
                unset($tmp);
                $tmp              = array();
                $tmp['countries'] = unserialize($Ashipping['targets']);
                foreach($tmp['countries'] as $key => $value)
                    $tmp['countries'][$key] = $Cpage->get_country_name($value);
                $tmp['shipping_cost_netto']  = $Cpage->money($Ashipping['cost_netto']);
                $tmp['shipping_cost_brutto'] = $Cpage->money($Cpage->tax_calc($Ashipping['cost_netto'], "brutto", $Cpage->Aglobal['tax'][COD_TAX_KEY]));
                $tmp['shipping_tax']         = $Cpage->Aglobal['tax'][COD_TAX_KEY]."%";
                array_push($cod_cost, $tmp);
            }
        }
    }
}

$tpl_shipping_cost = $shipping_cost;
$tpl_cod_cost      = $cod_cost;

eval($Cpage->require_file('templates', "tpl_shipping_cost.php"));