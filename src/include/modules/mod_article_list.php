<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

function cmp_list($a, $b)
{
    // Wenn $a großer $b, Rückgabe 1
    // Wenn $a kleiner $b, Rückgabe -1
    // Wenn $a gleich $b, Rückgabe 0
    if($a['position'] == $b['position'])
    {
        return strcmp($a['name'], $b['name']);
    }
    if($a['position'] > $b['position'])
    {
        return 1;
    }
    if($a['position'] < $b['position'])
    {
        return -1;
    }
}

// Wenn Artikelliste angezeigt werden soll:
if($_SESSION['selected_cat'] != FALSE)
{
    // Hole alle Artikel der gewählten Kategorie, auch Klone
    $Aarticle_list = $Carticles->get_articles_to_id($_SESSION['selected_cat']);
    // Sortiere nach Artikelnamen:
    if(isset($Aarticle_list))
    {
        foreach($Aarticle_list as $id => $value)
        {
            usort($Aarticle_list, "cmp_list");
        }
    }

    $Acategory_list = "";
    if(SHOW_SUB_CATEGORIES_IN_ARTICLE_LIST)
    {
        // Hole alle Unterkategorien der gewählten Kategorie
        $Acategory_list = $Ccategories->get_subcategories_to_id($_SESSION['selected_cat']);
        // Sortiere nach Kategoriennamen:
        if(isset($Acategory_list))
        {

                usort($Acategory_list, "cmp_list");

        }
    }
}

// Zeige Artikel an, wenn in Kategorie vorhanden:
if(!empty($Aarticle_list))
{
    $content .= "<H2 class='content_header'>".$Acurrent_category['name'].":</H2>\n";
    $tpl_article_count = sizeof($Aarticle_list);
    $tpl_article       = $Aarticle_list;
    $tpl_colspan       = ARTICLE_COLSPAN;
    eval($Cpage->require_file('templates', "tpl_article_list.php"));
}

// Zeige Unterkategorien an, wenn verhanden, bzw. gewünscht
if(!empty($Acategory_list))
{
    $content .= "<H2 class='content_header'>".SUB_CATEGORIES_FROM." ".$Acurrent_category['name'].":</H2>\n";
    $tpl_category_count = sizeof($Acategory_list);
    $tpl_category       = $Acategory_list;
    $tpl_colspan        = CATEGORY_COLSPAN;
    eval($Cpage->require_file('templates', "tpl_category_list.php"));
}

// Zeige an, dass keine Artikel vorhanden:
if((empty($Aarticle_list)) && (empty($Acategory_list)))
{
    $content .= "<p class='notice'>".NO_ARTICLES."</p>\n";
}

     
