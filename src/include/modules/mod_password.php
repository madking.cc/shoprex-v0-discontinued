<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$_SESSION['pwd_key']          = $Cpage->get_parameter("key", NULL);
$_SESSION['pwd_mail']         = $Cpage->get_parameter("pwd_mail", FALSE);
$_SESSION['pwd_customer_id']  = $Cpage->get_parameter("pwd_customer_id", FALSE);
$_SESSION['pwd_id']           = $Cpage->get_parameter("pwd_id", FALSE);
$_SESSION['pwd_new_password'] = $Cpage->get_parameter("pwd_new_password", FALSE);

if(!isset($location))
{
    $location = "";
}

$action = $Cpage->get_parameter("do_action");
if(isset($_SESSION['pwd_key']))
{
    $action = "set_password";
}

switch($action)
{
    // Email mit Instruktionen versenden
    case "password_send_mail":
        $content .= password_send_instructions($location);
        break;

    // Passwort neu setzen
    case "set_password":
        $content .= password_set();
        break;

    // E-Mail abfragen
    default:
        $content .= password_form($location);
        break;
}





// Passwort reset Anfrage, Eingabe von Daten
function password_form($location = "")
{
    global $Cpage;
    global $script;
    $content = "";

    eval($Cpage->require_file('javascript', "mail_check.php"));
    eval($Cpage->require_file('templates', "tpl_password_mail.php"));

    return $content;
}





// Passwort reset Anweisungen zusenden
function password_send_instructions($location = "")
{
    global $Cpage;
    global $Cdb;
    $content    = "";
    $mail_found = FALSE;
    $mail       = $_SESSION['pwd_mail'];

    $sql    = "SELECT `id` FROM `".TBL_PREFIX."customers` WHERE `mail` = '".$mail."' AND `banned` = 0 LIMIT 1;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    if($result->num_rows == 1)
    {
        $user_field   = $result->fetch_assoc();
        $key          = $Cpage->hash();
        $time_to_life = $Cpage->get_sqltime(time()+TIME_TO_PWD_RESET);
        $sql          = "INSERT INTO `".TBL_PREFIX."password_reset` (
                  `id` ,
                  `customer_id` ,
                  `key` ,
                  `time_to_life`
                  )
                  VALUES (
                  NULL , ?, ?, ?);";
        $stmt         = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $user_id      = $user_field['id'];
        $stmt->bind_param('iss', $user_id, $key, $time_to_life);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        $db_pwd_id = $stmt->insert_id;

        $mail_found = TRUE;
    }

    eval($Cpage->require_file('templates', "tpl_password_sent.php"));

    // E_MAIL an Kunden senden
    if($mail_found)
    {
        eval($Cpage->require_file('mail', "mail_password_reset.php"));
    }

    return $content;
}






// Wenn der Kunde seine E-Mail anklickt und das "Passwort Reset"
function password_set()
{
    global $Cpage;
    global $Cdb;
    global $load_md5_js;
    global $script;
    $content = "";
    $load_md5_js = TRUE;

    // Neues Passwort vergeben, Schritt 2:
    if((($mail = $_SESSION['pwd_mail'])                     != FALSE) &&
        (($customer_id = $_SESSION['pwd_customer_id'])      != FALSE) &&
        (($id = $_SESSION['pwd_id'])                        != FALSE) &&
        (($key = $_SESSION['pwd_key'])                      != NULL) &&
        (($new_password = $_SESSION['pwd_new_password'])    != FALSE))
    {
        // Hole den Datensatz zur E-Mail und Kunden ID:
        $sql    = "SELECT * FROM `".TBL_PREFIX."customers` WHERE `id` = ".$customer_id." AND `mail` = '".$mail."';";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

        if($result->num_rows == 1)
        {
            $sql    = "SELECT * FROM `".TBL_PREFIX."password_reset` WHERE BINARY `key` = '".$key."' AND `time_to_life` < 'NOW()' AND `used` = 0;";
            $result2 = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            if($result2->num_rows == 1)
            {
                $Acustomer = $result->fetch_assoc();
                // 	Trage neues Passwort ein:
                $sql                  = "UPDATE `".TBL_PREFIX."customers` SET `password` = ? WHERE `id` =? LIMIT 1 ;";
                $stmt                 = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
                $encoded_new_password = $Cpage->salt_password($new_password);
                $stmt->bind_param('si', $encoded_new_password, $customer_id);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

                // Sperre den Passwortreset:
                $sql  = "UPDATE `".TBL_PREFIX."password_reset` SET `used` = 1 WHERE `id` =? LIMIT 1 ;";
                $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
                $stmt->bind_param('i', $id);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

                eval($Cpage->require_file('templates', "tpl_password_ok.php"));
                eval($Cpage->require_file('mail', "mail_password_change.php"));
                session_unset();
                session_destroy();
            }
            else
            {
                session_unset();
                session_destroy();
                $error_code = 4;
                eval($Cpage->require_file('templates', "tpl_password_error.php"));
            }
        }
        else
        {
            session_unset();
            session_destroy();
            $error_code = 3;
            eval($Cpage->require_file('templates', "tpl_password_error.php"));
        }
    } // E-Mail abfrage + Neues Passwort abfragen, Schritt 1
    elseif(($key = $_SESSION['pwd_key']) != NULL)
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."password_reset` WHERE BINARY `key` = '".$key."' AND `time_to_life` < 'NOW()' AND `used` = 0;";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 1)
        {
            $password_field = $result->fetch_assoc();
            eval($Cpage->require_file('javascript', "mail_check.php"));
            eval($Cpage->require_file('javascript', "password_reset.php"));
            eval($Cpage->require_file('templates', "tpl_password_new.php"));
        }
        else
        {
            session_unset();
            session_destroy();
            $error_code = 2;
            eval($Cpage->require_file('templates', "tpl_password_error.php"));
        }
    }
    else
    {
        session_unset();
        session_destroy();
        $error_code = 1;
        eval($Cpage->require_file('templates', "tpl_password_error.php"));
    }

    return $content;
}
