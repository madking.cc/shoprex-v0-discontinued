<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "push":
        $article_id = $Cpage->get_parameter("id_to_cart");
        $quantity   = $Cpage->get_parameter("quantity");
        $attribute_id   = $Cpage->get_parameter("attribute", 0);
        $content .= cart_add_item($article_id, $quantity, $attribute_id);
        break;

    case "add_coupon":
        $content .= add_coupon();
        $content .= cart_show();
        break;

    default:
        $content .= cart_show();
        break;
}

// Warenkorb anzeigen:
function cart_show()
{
    global $Cpage;
    global $Ccart;
    global $action;
    global $dir_root;
    global $script;
    $content = "";

    $_SESSION['order']['delivery_type']       = $Cpage->get_parameter("delivery_type", $_SESSION['order']['delivery_type']);
    $_SESSION['order']['cart_country_target'] = $Cpage->get_parameter("cart_country_target", $_SESSION['order']['cart_country_target']);

    // Von mod_order.php, wenn "Zurück zum Warenkorb" Button gedrückt:
    require($dir_root."include/modules/inc_order_data.php");

    // Artikelanzahl einer Position ändern oder Warenkorb löschen:
    $change_item_id = $Cpage->get_parameter("article_id");
    $change_attribute_id = $Cpage->get_parameter("attribute_id", 0);
    switch($action)
    {
        case "down":
            $Ccart->article_down($change_item_id, $change_attribute_id);
            break;

        case "up":
            $check_quantity = $Ccart->article_up($change_item_id, $change_attribute_id);
            if($check_quantity != "0") $content .= $check_quantity;
            break;

        case "remove":
            $Ccart->article_remove($change_item_id, $change_attribute_id);
            break;

        case "remove_all":
            $Ccart->remove_all();
            break;

        default:
            // Nothing
            break;
    }

    // Warenkorb berechnen:
    $Acart = $Ccart->get_cart($_SESSION['order']['cart_country_target'], $_SESSION['order']['delivery_type']);

    eval($Cpage->require_file('javascript', "cart.php"));
    // Warenkorb anzeigen:
    $in_order_process = TRUE;
    eval($Cpage->require_file('templates', "tpl_cart.php"));

    return $content;
}

// Wenn Artikel hinzugefügt werden soll:
function cart_add_item($article_id, $article_quantity, $attribute_id)
{
    global $Cpage;
    global $Ccart;
    global $Carticles;
    global $script_footer;
    $content = "";

    // Hole Artikel aus Datenbank:
    $Aarticle = $Carticles->get_article($article_id, $attribute_id);

    // Artikelanzahl erhöhen, falls sich der Artikel bereits im Warenkorb befindet:
    $check_quantity = $Ccart->add_article($Aarticle, $article_quantity);

    //var_dump($check_quantity);
    if($check_quantity == "0")
    {
        // Für die Description:
        $li_list_size = 0;
        if(!empty($Aarticle['description']))
        {
            $li_list      = explode("\r\n", $Aarticle['description']);
            $li_list_size = sizeof($li_list);
        }

        // Link zum Weitereinkaufen:
        if(isset($_SESSION['selected_cat']))
        {
            $path_to_last_location = "category=".$_SESSION['selected_cat'];
        }
        else $path_to_last_location = "article=".$article_id;

        // Zeige hinzugefügten Artikel an:
        eval($Cpage->require_file('templates', "tpl_cart_add.php"));
    }
    else
    {
        $content .= $check_quantity;
        $script_footer .= $Cpage->history_back();
    }

    return $content;
}


function add_coupon()
{
    global $Cpage;
    global $Cdb;
    global $script_footer;
    $content = "";

    $coupon = $Cpage->get_parameter("coupon", NULL);
    if(empty($coupon)) return "";

    $sql = "SELECT * FROM `".TBL_PREFIX."coupons` WHERE `code` LIKE '$coupon' AND ((`till` >= NOW()) OR (`till` like '0000-00-00 00:00:00'))";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($result->num_rows == 0)
    {
        $script_footer .= $Cpage->alert(COUPON_NOT_AVAILABLE);
        return $content;
    }
    $row = $result->fetch_assoc();

    if(isset($_SESSION['customer']['id']))
    {
        $sql = "SELECT * FROM `".TBL_PREFIX."coupons_used` WHERE `customer_id` = ".$_SESSION['customer']['id']." AND `coupon_id` = ".$row['id']."";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows > 0)
        {
            $script_footer .= $Cpage->alert(COUPON_ALREADY_USED);
            return $content;
        }
    }

    $_SESSION['coupon'] = array();
    $_SESSION['coupon']['minimum'] = $row['minimum'];
    $_SESSION['coupon']['id'] = $row['id'];
    $_SESSION['coupon']['code'] = $row['code'];
    $_SESSION['coupon']['netto'] = $row['netto'];
    $_SESSION['coupon']['tax'] = $Cpage->Aglobal['tax'][$row['tax']];


    return $content;
}