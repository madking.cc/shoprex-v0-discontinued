<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

eval($Cpage->require_file('templates', "tpl_search.php"));

$text_search = $Cpage->get_parameter("text_search");

if(($search = $Cpage->get_parameter("search")) != FALSE)
{
    $content .= search_result_header();

    // Suche in Kategorien:
    $sql_cat    = "SELECT * FROM `".TBL_PREFIX."categories`
          WHERE 
          (`name` LIKE '%".$search."%'  OR `keywords` LIKE '%".$search."%' OR `picture` LIKE '%".$search."%' OR `picture_text` LIKE '%".$search."%')
          AND 
          `status` = 1  AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0)
          ORDER BY `name` ASC;";
    $result_cat = $Cdb->db_query($sql_cat, __FILE__.":".__LINE__);

    // Suche in Artikeln und Seiten:
    if(!$text_search)
    {
        $sql_article = "SELECT * FROM `".TBL_PREFIX."articles`
                    WHERE 
                    ( `name` LIKE '%".$search."%' OR `description` LIKE '%".$search."%'
                    OR `number` LIKE '%".$search."%' OR `picture` LIKE '%".$search."%'
                    OR `picture_text` LIKE '%".$search."%'  OR `keywords` LIKE '%".$search."%' )
                    AND 
                    `clone_from` =0 
                    AND 
                    (`quantity` >0 OR `quantity_warning` =0)
                    AND 
          			`status` = 1  AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0)
                    ORDER BY `name` ASC;";
        $sql_page    = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."`
                 WHERE 
                 `link_text` LIKE '%".$search."%'
                 ORDER BY `link_text` ASC;";
    } // Volltextsuche in Artikeln und Seiten:
    else
    {
        $sql_article = "SELECT * FROM `".TBL_PREFIX."articles`
                    WHERE 
                    ( `name` LIKE '%".$search."%' OR `description` LIKE '%".$search."%'
                    OR `text` LIKE '%".$search."%' OR `number` LIKE '%".$search."%'
                    OR `picture` LIKE '%".$search."%' OR `picture_text` LIKE '%".$search."%'
                    OR `keywords` LIKE '%".$search."%' )
                    AND 
                    `clone_from` =0 
                    AND 
                    (`quantity` >0 OR `quantity_warning` =0)
                    AND 
          			`status` = 1  AND (`visible_from` < NOW() OR `visible_from` = 0) AND (`visible_to` > NOW() OR `visible_to` = 0)
                    ORDER BY `name` ASC;";
        $sql_page    = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."`
                  WHERE 
                  ( `link_text` LIKE '%".$search."%' OR `value` LIKE '%".$search."%')
                  ORDER BY `link_text` ASC;";
    }
    $result_article = $Cdb->db_query($sql_article, __FILE__.":".__LINE__);
    $result_page    = $Cdb->db_query($sql_page, __FILE__.":".__LINE__);


    $Acategory = array();
    while($category_array = $result_cat->fetch_assoc())
    {
        $dummy = $Carticles->check_disabled($category_array['id']);
        if(!$dummy)
            array_push($Acategory, $category_array);
    }

    $Aarticle = array();
    while($article_array = $result_article->fetch_assoc())
    {
        $category_id = $Carticles->get_category_id($article_array['id']);
        $dummy = $Carticles->check_disabled($category_id);
        if(!$dummy)
            array_push($Aarticle, $article_array);
    }


    // Wenn keine Treffer:
    if((sizeof($Acategory) == 0) && (sizeof($Aarticle) == 0) && ($result_page->num_rows == 0))
    {
        $content .= search_no_result();
    } // Wenn Treffer:
    else
    {
        $hits = sizeof($Acategory)+sizeof($Aarticle)+$result_page->num_rows;

        $content .= search_hits($hits);

        // Kategorien Ergebnisse:
        if(sizeof($Acategory) != 0)
        {
            $content .= category_result_header();
            foreach($Acategory as $value)
            {
                if(empty($value['picture']))
                {
                    $value['picture_text'] = NO_PICTURE_TEXT;
                    $value['picture'] = NO_PICTURE_TINY;
                }

                $content .= category_result($value);
            }
            $content .= category_result_footer();
        }

        // Artikel Ergebnisse:
        if(sizeof($Aarticle) != 0)
        {
            $content .= article_result_header();

            foreach($Aarticle as $value)
            {
                @$tmp = unserialize($value['picture']);
                if($tmp == NULL)
                {
                    $value['picture_text']    = array();
                    $value['picture']    = array();
                    $value['picture_text'][0] = NO_PICTURE_TEXT;
                    $value['picture'][0] = NO_PICTURE_TINY;
                }
                else
                {
                    $value['picture_text'] = unserialize($value['picture_text']);
                    $value['picture'] = unserialize($value['picture']);
                }

                $content .= article_result($value);
            }
            $content .= article_result_footer();
        }

        // Seiten Ergebnissen:
        if($result_page->num_rows != 0)
        {
            $content .= page_result_header();

            while($Apage = $result_page->fetch_assoc())
            {
                $content .= page_result($Apage);
            }
            $content .= page_result_footer();
        }
    }

    $content .= "\n";
}

// Neue Suche + Volltext Möglichkeit:
if(empty($text_search))
{
    $text_search = FALSE;
}
$content .= new_search($search, $text_search);

  
