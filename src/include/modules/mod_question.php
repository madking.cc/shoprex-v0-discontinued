<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(($question_to_id = $Cpage->get_parameter("question_to_id")) != FALSE)
{
    $Aarticle = $Carticles->get_article($question_to_id);

    if(sizeof($Aarticle)==0)
    {
        $content .= ARTICLE_NOT_AVAILABLE;
    }
    else
    {
        $action = $Cpage->get_parameter("do_action");
        switch($action)
        {
            case "send_question":
                $content .= question_send_mail($Aarticle);
                break;

            default:
                $content .= question_start($Aarticle);
                break;
        }
    }
}
else $content .= ERROR_QUESTION_ARTICLE_NOT_SELECTED;





function question_send_mail($Aarticle)
{
    global $Cpage;
    global $Cdb;

    $question_from_email = $Cpage->get_parameter("question_from_mail");
    $question_header     = $Cpage->get_parameter("question_header", "");
    $question_text       = $Cpage->get_parameter("question_text", "", NO_ESCAPE, DO_HTMLSPECIALCHARS);
    eval($Cpage->require_file('mail', "mail_question.php"));

    $content             = "";
    $tpl_article         = $Aarticle;
    eval($Cpage->require_file('templates', "tpl_question_sent.php"));

    if(!empty($question_header) || !empty($question_text))
    {
        $Aquestions = array();
        @$tmp = unserialize($Aarticle['questions']);
        if($tmp)
        {
            $Aquestions = $tmp;
        }
        $tmp            = array();
        $tmp['subject'] = $question_header;
        $tmp['text']    = $question_text;
        $tmp['mail']    = $question_from_email;
        array_push($Aquestions, $tmp);
        $questions = serialize($Aquestions);

        $sql        = "UPDATE `".TBL_PREFIX."articles` SET
  	`questions`     = ?
  	WHERE `id` = ?;";
        $stmt       = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $article_id = $Aarticle['id'];
        $stmt->bind_param('si', $questions, $article_id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }

    return $content;
}

function question_start($Aarticle)
{
    global $Cpage;
    global $script;

    $content = "";

    if(!isset($_SESSION['customer']['mail']))
    {
        $_SESSION['customer']['mail'] = "";
    }

    eval($Cpage->require_file('javascript', "mail_check.php"));

    $tpl_article         = $Aarticle;
    eval($Cpage->require_file('templates', "tpl_question.php"));

    return $content;
}
