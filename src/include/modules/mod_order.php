<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "send_order":
        $content .= order_finish();
        break;

    case "cancel_order":
        $content .= order_user_cancel_request();
        break;

    case "show_order":
        $content .= show_order();
        break;

    case "check_delivery_address":
    default:
        // Variablen vom Warenkorb:
        $_SESSION['order']['delivery_type']       = $Cpage->get_parameter("delivery_type", $_SESSION['order']['delivery_type']);
        $_SESSION['order']['cart_country_target'] = $Cpage->get_parameter("cart_country_target", $_SESSION['order']['cart_country_target']);
        $_SESSION['order']['weight_to_heavy']     = $Cpage->get_parameter("weight_to_heavy", $_SESSION['order']['weight_to_heavy']);
        $_SESSION['order']['cart_price_brutto']   = $Cpage->get_parameter("cart_price_brutto", $_SESSION['order']['cart_price_brutto']);

        if(empty($_SESSION['customer']['id']))
        {
            $location = "order_process";
            require ($dir_root."include/modules/mod_account.php");
        }
        elseif(!empty($_SESSION['cart']))
        {
            $content .= order_check_address($action);
        }
        else
        {
            $content .= NO_ORDER_PROCESS;
        }
        break;
}

function order_check_address($action = NULL)
{
    global $Cpage;
    global $Cdb;
    global $dir_root;
    global $script;
    global $script_footer;

    $content = "";

    // Wenn Adresse überprüft wird
    require($dir_root."include/modules/inc_order_data.php");

    // Überprüfung, ob Gutschein schon verwendet wurde
    if(isset($_SESSION['coupon']['id']))
    {
        $sql = "SELECT * FROM `".TBL_PREFIX."coupons_used` WHERE `customer_id` = ".$_SESSION['customer']['id']." AND `coupon_id` = ".$_SESSION['coupon']['id']."";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows > 0)
        {
            $script_footer .= $Cpage->alert(COUPON_ALREADY_USED_AND_REMOVED_FROM_CART);
            unset($_SESSION['coupon']);
            $script .= $Cpage->load_page("cart.php");
            return $content;
        }
    }



    require ($Cpage->Apath['classes']."remote_areas.php");
    $Cremote_areas = new remote_areas($Cdb, $_SESSION["customer"]["country"]);

    $remote_area = $Cremote_areas->check($_SESSION["customer"]["city"], $_SESSION["customer"]["zip"]);

    $remote_area_delivery_check = FALSE;
    if($action == "check_delivery_address")
    {
        $remote_area_delivery_check = TRUE;
    }

    $cart_country_name = $Cpage->get_country_name($_SESSION['order']['cart_country_target']);

    $target_mismatch = FALSE;
    if($_SESSION['order']['cart_country_target'] != $_SESSION["customer"]["country"])
    {
        $target_mismatch = TRUE;
    }

    $remote_area_delivery = FALSE;
    if($remote_area_delivery_check || ((!empty($_SESSION["customer"]["del_city"]) && ($target_mismatch || $remote_area))))
    {
        if($_SESSION['customer']['del_country'] != $_SESSION["customer"]["country"])
        {
            $Cremote_areas->refill_array($_SESSION['customer']['del_country']);
        }
        $remote_area_delivery = $Cremote_areas->check($_SESSION["customer"]["del_city"], $_SESSION["customer"]["del_zip"]);
    }

    eval($Cpage->require_file('templates', "tpl_order_address.php"));
    eval($Cpage->require_file('javascript', "order_check.php"));

    return $content;
}

// Bestellung abschicken:
function order_finish()
{
    global $Cpage;
    global $Cdb;
    global $Ccart;
    global $dir_root;
    global $css;
    global $script;
    $content = "";

    $css .= "    <link rel=\"stylesheet\"
          href=\"".THEME."css/order_print.css\" type=\"text/css\" media=\"print\"/>";

    if($Ccart->is_empty())
    {
        $script .= $Cpage->load_page();
        $_SESSION['selected_cat'] = "";
        return $content;
    }

    $request = $Cpage->get_parameter("request");

    // Von evtl. zusätzlichen Daten der Adressen Überprüfung
    require($dir_root."include/modules/inc_order_data.php");

    // Lieferadresse speichern:
    if($_SESSION['customer']['use_delivery_data'])
    {
        $sql       = "UPDATE `".TBL_PREFIX."customers` SET
        `del_name` = ?,
        `del_address1` = ?,
        `del_address2` = ?,
        `del_zip` = ?,
        `del_city` = ?,
        `del_country` = ?,
        `del_phone` = ?,
        `changed` = NOW() 
        WHERE `id` = ? LIMIT 1 ;";
        $stmt      = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $Acustomer = $_SESSION['customer'];
        $stmt->bind_param('sssssssi', $Acustomer['del_name'], $Acustomer['del_address1'], $Acustomer['del_address2'], $Acustomer['del_zip'], $Acustomer['del_city'], $Acustomer['del_country'], $Acustomer['del_phone'], $Acustomer['id']);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }

    // Rechnungsadresse oder Lieferadresse?:
    if($_SESSION['customer']['use_delivery_data'])
    {
        $delivery_target = $_SESSION['customer']['del_country'];
    }
    else
    {
        $delivery_target = $_SESSION['customer']['country'];
    }

    // Berechne nochmal den Warenkorb:
    $Acart = $Ccart->get_cart($delivery_target, $_SESSION['order']['delivery_type']);

    // Bereite die Description vor
    foreach($Acart['cart'] as $pos => $value)
    {
        if(!empty($Acart['cart'][$pos]['description']))
        {
            $description = "";
            $li_list = explode("\r\n", $Acart['cart'][$pos]['description']);
            for($u = 0; $u < sizeof($li_list); $u++)
            {
                $description .= $li_list[$u];
                if($u != sizeof($li_list)-1)
                {
                    $description .= ", ";
                }
            }
            $Acart['cart'][$pos]['description'] = $description;
        }
    }

    $time_of_order = $Cpage->get_sqltime();

    if(!empty($_SESSION['customer']['phone']))
    {
        $order_phone = $_SESSION['customer']['phone'];
    }
    else
    {
        $order_phone = "";
    }

    // Hole Auftrags Zähler
    $sql           = "SELECT * FROM `".TBL_PREFIX."counter`
                    WHERE `type` LIKE 'ORDER_COUNTER' LIMIT 1;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $order_counter = $result->fetch_assoc();
    $order_counter = $order_counter['count'];
    // Bestimme Auftrags Nummer
    $order_id = ORDER_PREFIX.($order_counter+ORDER_START).ORDER_SUFFIX;

    // Speichere die Bestellung in der Datenbank:
    $sql                   = "INSERT INTO `".TBL_PREFIX."orders` (
              `id` ,
              `customer_id` ,
              `changed` ,
      		  `order_id` ,
              `order_time` ,
              `invoice_id` ,
              `invoice_time` ,
              `status` ,
              `customer_type` ,
              `company` ,
              `firstname` ,
              `lastname` ,
              `address1` ,
              `address2` ,
              `zip` ,
              `city` ,
              `country` ,
              `phone` ,
              `email` ,
              `del_name` ,
              `del_address1` ,
              `del_address2` ,
              `del_zip` ,
              `del_city` ,
              `del_country` ,
              `del_phone` ,
              `message` ,
              `internal` ,
              `cart` ,
              `delivery_type` ,
      		  `delivery_method` ,
      		  `request` ,
              `documents` )
              VALUES (
              NULL, ?, NULL, ?, ?, NULL, NULL, 2, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, ?, ?, ?, ?, NULL);";
    $stmt                  = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $ser_order             = serialize($Acart);
    $Acustomer             = $_SESSION['customer'];
    $Acustomer['order_id'] = $order_id;
    if(!$_SESSION['customer']['use_delivery_data'])
    {
        $Acustomer['del_name']     = NULL;
        $Acustomer['del_address1'] = NULL;
        $Acustomer['del_address2'] = NULL;
        $Acustomer['del_zip']      = NULL;
        $Acustomer['del_city']     = NULL;
        $Acustomer['del_country']  = NULL;
        $Acustomer['del_phone']    = NULL;
    }
    $Acustomer['delivery_type'] = $_SESSION['order']['delivery_type'];
    $delivery_method            = DELIVERY_METHOD;

    $stmt->bind_param('ississsssssssssssssssssisi', $Acustomer['id'], $Acustomer['order_id'], $time_of_order, $Acustomer['type'], $Acustomer['company'], $Acustomer['firstname'], $Acustomer['lastname'], $Acustomer['address1'], $Acustomer['address2'], $Acustomer['zip'], $Acustomer['city'], $Acustomer['country'], $order_phone, $Acustomer['mail'], $Acustomer['del_name'], $Acustomer['del_address1'], $Acustomer['del_address2'], $Acustomer['del_zip'], $Acustomer['del_city'], $Acustomer['del_country'], $Acustomer['del_phone'], $Acustomer['customer_message'], $ser_order, $Acustomer['delivery_type'], $delivery_method, $request);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    $order_id_insert = $Cdb->get_insert_id();

    // Erhöhe den Bestellungszähler
    $sql = "UPDATE `".TBL_PREFIX."counter` SET `count` = `count` + 1 WHERE `type` ='ORDER_COUNTER';";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    // Reserviere die bestellten Artikel
    if(RESERVE_ARTICLE)
        foreach($Acart['cart'] as $pos => $value)
        {
            $sql  = "INSERT INTO `".TBL_PREFIX."reserved_orders` (
                  `order_id` ,
                  `article_id` ,
                  `attribute_id` ,
                  `quantity` ,
                  `since`
                  )
                  VALUES (
                  '$order_id_insert', '".$Acart['cart'][$pos]['original_id']."', '".$Acart['cart'][$pos]['attribute_id']."', '".$Acart['cart'][$pos]['quantity']."', NOW());";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }

    // Gebe die Bestellung am Bildschirm aus:
    eval($Cpage->require_file('templates', "tpl_order_finish.php"));

    /// EMAIL AN GESCHÄFT und Kunden, das neue Bestellung rein ist....
    eval($Cpage->require_file('mail', "mail_order.php"));

    // Leere Warenkorb
    $Ccart->remove_all();
    // Trage eventuellen Gutschein in die Datenbank ein, damit der Kunde den Gutschein nicht mehrmals verwenden kann
    if(isset($_SESSION['coupon']))
    {
        $sql = "INSERT INTO `".TBL_PREFIX."coupons_used` (
		`coupon_id` ,
		`customer_id`
		)
		VALUES ( '".$_SESSION['coupon']['id']."', '".$_SESSION['customer']['id']."');";
        $Cdb->db_query($sql, __FILE__.":".__LINE__);
        // Entferne evtl. vorhandenen Gutschein
        unset($_SESSION['coupon']);
    }



    return $content;
}

function order_user_cancel_request()
{
    global $Cpage;
    global $Cdb;
    global $script;
    $content = "";

    $order_id = $Cpage->get_parameter("order_id");

    $sql      = "
        UPDATE `".TBL_PREFIX."orders` SET `changed` = NOW( ) ,
        `status` = '1' WHERE `id` =? ;";
    $stmt     = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('i', $order_id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    // Lösche die reservierten Bestellungen
    $sql = "DELETE FROM `".TBL_PREFIX."reserved_orders` WHERE `order_id` =$order_id";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    // Hole den Auftrag
    $sql = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$order_id";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Aorder = $result->fetch_assoc();

    // Korrigiere Lager, falls Rechnung
    if(!empty($Aorder['invoice_id']))
    {
        $Acart = unserialize($Aorder['cart']);

        $difference = array();
        $difference_attribute = array();
        if(sizeof($Acart['cart']) > 0)
        {
            foreach($Acart['cart'] as $key => $value)
            {
                if($value['id'] != 0)
                {
                    if($value['attribute_id'] >0 )
                    {
                        $difference_attribute[$value['attribute_id']] = -$value['quantity'];
                    }
                    else
                        $difference[$value['id']] = -$value['quantity'];
                }
            }
        }

        if(sizeof($difference) > 0)
        {
            foreach($difference as $article_id => $value)
            {
                $sql = "UPDATE ".TBL_PREFIX."articles SET `quantity` = `quantity` - $value WHERE `id` =$article_id AND `quantity_warning` > 0;";
                $Cdb->db_query($sql);
                $sql = "UPDATE ".TBL_PREFIX."articles SET `bought` = `bought` + $value WHERE `id` =$article_id;";
                $Cdb->db_query($sql);
            }
        }
        if(sizeof($difference_attribute) > 0)
        {
            foreach($difference_attribute as $attribute_id => $value)
            {
                $sql = "UPDATE ".TBL_PREFIX."attributes SET `size` = `size` - $value WHERE `id` =$attribute_id AND `warning` > 0;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
                $sql = "UPDATE ".TBL_PREFIX."attributes SET `bought` = `bought` + $value WHERE `id` =$attribute_id;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
            }
        }
    }

    // MAIL AN UNS
    $Acustomer = $_SESSION['customer'];
    eval($Cpage->require_file('mail', "mail_order_cancel.php"));

    $script .= $Cpage->load_page("account.php");

    return $content;
}

function show_order()
{
    global $Cpage;
    global $Cdb;

    $content = "";

    $order_id = $Cpage->get_parameter("order_id");

    $sql    = "SELECT * FROM `".TBL_PREFIX."orders`
    WHERE `id`=".$order_id."
    AND `customer_id`=".$_SESSION['customer']['id'].";";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    if($result->num_rows == 1)
    {
        $Aorder = $result->fetch_assoc();
        $Acart  = unserialize($Aorder['cart']);

        eval($Cpage->require_file('templates', "tpl_order_detail.php"));
    }
    else $content .= NO_ORDER_FOUND;

    return $content;
}


