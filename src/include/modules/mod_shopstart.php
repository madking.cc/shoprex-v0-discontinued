<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */
if(SHOW_HINT_ON_START_PAGE)
{
    var_dump(SHOW_HINT_ON_START_PAGE);
    $sql    = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."` WHERE `page` = 'start_text_box' LIMIT 1;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($result->num_rows == 1)
    {
        $Atextbox       = $result->fetch_assoc();
        //$nav_control = $Apage['page'];
        //$subtitle    = SUBTITLE_SEPARATOR.$Apage['subtitle'];
        $content .= "<H2 class='content_header'>".$Atextbox['subtitle']."</H2>\n";
        $content .= $Atextbox['value'];
    }


}

// Artikel in der Hauptkategorie

function cmp_list2($a, $b)
{
    // Wenn $a großer $b, Rückgabe 1
    // Wenn $a kleiner $b, Rückgabe -1
    // Wenn $a gleich $b, Rückgabe 0
    if($a['position'] == $b['position'])
    {
        return strcmp($a['name'], $b['name']);
    }
    if($a['position'] > $b['position'])
    {
        return 1;
    }
    if($a['position'] < $b['position'])
    {
        return -1;
    }
}

// Hole alle Artikel der Hauptkategorie, auch Klone
$Aarticle_list_root = $Carticles->get_articles_to_id($Carticles->get_dir_root());
// Sortiere nach Artikelnamen und Position:
if(isset($Aarticle_list_root))
{
    foreach($Aarticle_list_root as $id => $value)
    {
        usort($Aarticle_list_root, "cmp_list2");
    }
}

// Zeige Artikel an, wenn in Kategorie vorhanden:
if(!empty($Aarticle_list_root))
{
    $content .= "<H2>".START_ARTICLE.":</H2>\n";
    $tpl_article_count = sizeof($Aarticle_list_root);
    $tpl_article       = $Aarticle_list_root;
    $tpl_colspan       = MAIN_ARTICLE_COLSPAN;
    eval($Cpage->require_file('templates', "tpl_article_list.php"));
}

// Die letzten x eingefügten Artikel
if(SHOW_CHANGED_ITEMS > 0)
{
    $sql                   = "SELECT *
	      FROM `".TBL_PREFIX."articles`
	      WHERE `clone_from` =0 AND (`quantity` > 0 OR `quantity_warning` = 0) AND `status` =1
	      ORDER BY `changed` DESC;";
    $result                = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Alast_x_articles      = array();
    $counter = 0;
    if($result->num_rows > 0)
    {
        while($article_array = $result->fetch_assoc())
        {
            $Aarticle = $Carticles->get_article($article_array['id']);
            //var_dump($Aarticle);
            if(!empty($Aarticle))
            {
                array_push($Alast_x_articles, $Aarticle);
                $counter++;
            }
            if($counter == SHOW_CHANGED_ITEMS) break;
        }
    }

    $content .= "<H2 class='content_header'>".NEW_PRODUCTS.":</H2>\n";

    if(sizeof($Alast_x_articles)>0)
    {
        $tpl_article_count = $counter;
        //($Alast_x_articles);
        $tpl_article       = $Alast_x_articles;
        $tpl_colspan       = CHANGED_ITEMS_COLSPAN;
        //var_dump($tpl_article);
        eval($Cpage->require_file('templates', "tpl_article_list.php"));
    }
    else $content .= "<p class='information_important'>".NO_ARTICLES_YET."</p>";
}

// Die meist x bestellten Artikel:
if(SHOW_BOUGHT_ITEMS > 0)
{
    $sql                 = "SELECT *  FROM `".TBL_PREFIX."articles`
	                 WHERE `clone_from` =0 AND (`quantity` > 0 OR `quantity_warning` = 0) AND `status` =1 AND `bought` > 0     
	                 ORDER BY `bought` DESC;";
    $result              = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $counter             = 0;
    $Alast_x_bought      = array();
    if($result->num_rows > 0)
    {
        while($article_array = $result->fetch_assoc())
        {
            $Aarticle = $Carticles->get_article($article_array['id']);
            if(!empty($Aarticle))
            {
                array_push($Alast_x_bought, $Aarticle);
                $counter++;
            }
            if($counter == SHOW_BOUGHT_ITEMS) break;
        }
    }

    $content .= "<H2 class='content_header'>".MOST_QUESTIONED.":</H2>\n";

    if(sizeof($Alast_x_bought)>0)
    {
        $tpl_article_count = $counter;
        $tpl_article       = $Alast_x_bought;
        $tpl_colspan       = BOUGHT_ITEMS_COLSPAN;
        eval($Cpage->require_file('templates', "tpl_article_list.php"));
    }
    else $content .= "<p class='information_important'>".NO_ARTICLES_SOLD."</p>";
}