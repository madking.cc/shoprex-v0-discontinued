<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$_SESSION['customer']['use_delivery_data'] = $Cpage->get_parameter("use_delivery_data");
$_SESSION['customer']['del_name']          = $Cpage->get_parameter("del_name", $_SESSION['customer']['del_name']);
$_SESSION['customer']['del_address1']      = $Cpage->get_parameter("del_address1", $_SESSION['customer']['del_address1']);
$_SESSION['customer']['del_address2']      = $Cpage->get_parameter("del_address2", $_SESSION['customer']['del_address2']);
$_SESSION['customer']['del_zip']           = $Cpage->get_parameter("del_zip", $_SESSION['customer']['del_zip']);
$_SESSION['customer']['del_city']          = $Cpage->get_parameter("del_city", $_SESSION['customer']['del_city']);
$_SESSION['customer']['del_country']       = $Cpage->get_parameter("del_country", $_SESSION['customer']['del_country']);
$_SESSION['customer']['del_phone']         = $Cpage->get_parameter("del_phone", $_SESSION['customer']['del_phone']);
$_SESSION['customer']['customer_message']  = $Cpage->get_parameter("customer_message", "", NO_ESCAPE, DO_HTMLSPECIALCHARS);
$_SESSION['customer']['order_id']          = $Cpage->get_parameter("order_id", $_SESSION['customer']['order_id']);
$_SESSION['customer']['phone']             = $Cpage->get_parameter("phone", $_SESSION['customer']['phone']);

