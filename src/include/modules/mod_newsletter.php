<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{

    // Zum Newsletter anmelden:
    case "get_newsletter":
        $content .= newsletter();
        break;

    default:
        $key = $Cpage->get_parameter("key");
        if(empty($key))
        {
            $script .= $Cpage->load_page();
        }
        else
        {
            $content .= newsletter_sign_out($key);
        }
        break;
}

function newsletter_sign_out($key)
{
    global $Cpage;
    global $Cdb;
    $content = "";

    $entry_found = FALSE;

    $sql = "UPDATE `".TBL_PREFIX."customers` SET `newsletter` = 0 WHERE BINARY `hash` = '$key' LIMIT 1;";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($Cdb->affected_rows() == 1)
    {
        $entry_found = TRUE;
    }

    if(!$entry_found)
    {
        $sql = "DELETE FROM `".TBL_PREFIX."newsletter` WHERE BINARY `key` = '$key' LIMIT 1;";
        $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($Cdb->affected_rows() == 1)
        {
            $entry_found = TRUE;
        }
    }

    $tpl_entry_found = $entry_found;
    eval($Cpage->require_file('templates', "tpl_newsletter_sign_out.php"));

    return $content;
}

// Newsletter eintragen
function newsletter()
{
    global $Cpage;
    global $Cdb;
    global $script;
    $content = "";

    $only_newsletter_signing = FALSE;
    $multiple_mails          = FALSE;
    $nl_in_account           = FALSE;

    $customer_type = $Cpage->get_parameter("nl_customer_type");
    $mail          = $Cpage->get_parameter("nl_mail");

    // Prüfe, ob E-Mail bereits in Kunden Tabelle vorhanden
    $sql    = " SELECT `id` FROM `".TBL_PREFIX."customers`
             WHERE
             `mail` = '".$mail."'
             AND
             `banned` = 0; ";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    // Wenn nicht, einfache Newsletter Anmeldung
    if($result->num_rows == 0)
    {

        // Prüfe ob E-Mail bereits in Newsletter Tabelle vorhanden
        $sql    = " SELECT * FROM `".TBL_PREFIX."newsletter`
               WHERE
               `mail` = '".$mail."';";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

        // Wenn ja, markiere die Einträge als gelöscht
        if($result->num_rows > 0)
        {
            $sql = "DELETE FROM `".TBL_PREFIX."newsletter`
                WHERE 
                `mail` = '".$mail."';";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }

        // Trage E-Mail in Newsletter Tabelle ein
        $sql  = "INSERT INTO `".TBL_PREFIX."newsletter` (
                `id` ,
                `mail` ,
                `type` ,
                `key`  
                )
                VALUES (
                NULL , ?, ?, ?
                );";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $key  = $Cpage->hash();
        $stmt->bind_param('sis', $mail, $customer_type, $key);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        $only_newsletter_signing = TRUE;
    } // Falls E-Mail bereits in Kunden Tabelle vorhanden, aktiviere dort Newsletter
    elseif($result->num_rows == 1)
    {
        $Acustomer   = $result->fetch_assoc();
        $sql         = "UPDATE `".TBL_PREFIX."customers`
                SET 
                `newsletter` = '1',
                `type` = ? 
                WHERE 
                `id` = ?;";
        $stmt        = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $customer_id = $Acustomer['id'];
        $stmt->bind_param('ii', $customer_type, $customer_id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        $nl_in_account = TRUE;
    }

    if($only_newsletter_signing || $nl_in_account)
    {

        eval($Cpage->require_file('mail', "mail_newsletter.php"));
        eval($Cpage->require_file('templates', "tpl_newsletter_ok.php"));
    }
    else
    {
        $script .= $Cpage->load_page();
    }

    return $content;
}

