<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Bestimmt, ob User gerade von einem Bestellprozess kommt und sich nur einloggen muss
if(!isset($location))
{
    $location = $Cpage->get_parameter("location");
}

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    // Userdaten aktualisieren:
    case "update": // von user_site()
        $mail_old = $_SESSION['customer']['mail']; // Überprüfen, ob E-Mail bereits in der Datenbank, wenn E-Mail geändert werden soll
        $mail_new = $Cpage->get_parameter("mail");
        if($mail_old != $mail_new)
        {
            $sql    = "SELECT `id` FROM `".TBL_PREFIX."customers`
            WHERE `mail` = '".$mail_new."'
                  LIMIT 1;";
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            if($result->num_rows > 0)
            {
                $content .= user_site(MAIL_ERROR);
                break;
            }
        }
        $content .= update();
        break;

    // User einloggen:
    case "login":
        $content .= login($location);
        break;

    // Das Passwort ändern in der Datenbank:
    case "change_password":
        $content .= change_password();
        break;

    // Ausloggen:
    case "logout":
        $content .= logout();
        break;

    default:
        if(isset($_SESSION['customer']['id']))
            // Wenn User bereits eingeloggt:
        {
            $content .= user_site();
        }
        else
            // Wenn User noch einloggen muss
        {
            $content .= start_site($Cpage->get_parameter("login_error", FALSE), $location);
        }
        break;
}

// Login/Anmelden/Newsletter Seite:
function start_site($login_error = FALSE, $location = "")
{
    global $Cpage;
    global $load_md5_js;
    global $script;
    $content = "";
    $load_md5_js = TRUE;

    // E-Mail Check:
    eval($Cpage->require_file('javascript', "mail_check.php"));
    eval($Cpage->require_file('javascript', "login_check.php"));

    // Einloggen:
    eval($Cpage->require_file('templates', "tpl_account_login.php"));
    // Registrieren:
    eval($Cpage->require_file('templates', "tpl_account_register.php"));
    // Newsletter:
    if(empty($location))
    {
        eval($Cpage->require_file('templates', "tpl_newsletter_sign.php"));
    }

    return $content;
}

// User Eintrag in der Datenbank updaten
function update()
{
    global $Cpage;
    global $Cdb;
    global $script;
    $content = "";

    // Übertrage Daten in die aktuelle Session:

    $old_mail                           = $_SESSION['customer']['mail'];
    $_SESSION['customer']['mail']       = $Cpage->get_parameter("mail");
    $_SESSION['customer']['firstname']  = $Cpage->get_parameter("firstname");
    $_SESSION['customer']['lastname']   = $Cpage->get_parameter("lastname");
    $_SESSION['customer']['address1']   = $Cpage->get_parameter("address1");
    $_SESSION['customer']['address2']   = $Cpage->get_parameter("address2");
    $_SESSION['customer']['zip']        = $Cpage->get_parameter("zip");
    $_SESSION['customer']['city']       = $Cpage->get_parameter("city");
    $_SESSION['customer']['country']    = $Cpage->get_parameter("country");
    $_SESSION['customer']['phone']      = $Cpage->get_parameter("phone");
    $_SESSION['customer']['type']       = $Cpage->get_parameter("customer_type");
    $_SESSION['customer']['company']    = $Cpage->get_parameter("company");
    $_SESSION['customer']['newsletter'] = $Cpage->get_parameter("newsletter");

    // Aktualisiere Datenbankeintrag des Users:
    $sql = "UPDATE `".TBL_PREFIX."customers` SET
      `firstname`   = ?,
      `lastname`    = ?,
      `address1`    = ?,
      `address2`    = ?,
      `zip`         = ?,
      `city`        = ?,
      `country`     = ?,
      `phone`       = ?,
      `type`        = ?,
      `company`     = ?,
      `mail`        = ?,
      `newsletter`  = ?,
      `changed` = NOW( ) 
      WHERE 
      `id` = ?  
      LIMIT 1 ;";

    $stmt      = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $Acustomer = $_SESSION['customer'];
    $stmt->bind_param('ssssssssissii', $Acustomer['firstname'], $Acustomer['lastname'], $Acustomer['address1'], $Acustomer['address2'], $Acustomer['zip'], $Acustomer['city'], $Acustomer['country'], $Acustomer['phone'], $Acustomer['type'], $Acustomer['company'], $Acustomer['mail'], $Acustomer['newsletter'], $Acustomer['id']);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    // E-Mail Senden, wenn sich die E-Mail Adresse geändert hat
    if($old_mail != $_SESSION['customer']['mail'])
    {
        eval($Cpage->require_file('mail', "mail_change.php"));
    }
    // Lade Startseite der Accountverwaltung
    $script .= $Cpage->load_page('account.php', "data_changed=1");

    return $content;
}

// Login überprüfen
function login($location = "")
{
    global $Cpage;
    global $Cdb;
    global $script;
    $content = "";

    $_SESSION['customer']['mail']     = $Cpage->get_parameter("mail");
    $_SESSION['customer']['password'] = $Cpage->get_parameter("password");
    if((empty($_SESSION['customer']['mail'])) || (empty($_SESSION['customer']['password']))) // Security
    {
        $script .= $Cpage->load_page("account.php", "location=".$location."login_error=1"); // Lade Account Seite
    }
    else
    {
        $mail                             = $_SESSION['customer']['mail'];
        $password                         = $Cpage->salt_password($_SESSION['customer']['password']);
        $sql                              = "SELECT * FROM `".TBL_PREFIX."customers`
              WHERE `mail` = '".$mail."'
                    AND 
                    BINARY `password` = '".$password."'
                    LIMIT 1;";
        $_SESSION['customer']['password'] = NULL;
        $password                         = NULL;
        $result                           = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        // Wenn Passwort und Mail richtig:
        if($result->num_rows == 1)
        {
            $sql2    = "SELECT * FROM `".TBL_PREFIX."customers`
              WHERE `mail` = '".$mail."'
                    AND
                    `banned` = 1
                    LIMIT 1;";
            $result2 = $Cdb->db_query($sql2, __FILE__.":".__LINE__);
            if($result2->num_rows == 1)
            {
                $script .= $Cpage->load_page("account.php", "location=".$location."&login_error=2"); // Lade Startseite von Account Seite, weil Account gebannt
                return $content;
            }
            $Auser = $result->fetch_assoc();
            // Speichere Userdaten in Session:
            $_SESSION['customer'] = $Auser;

            // Speichere Login Zeit
            $sql         = "UPDATE `".TBL_PREFIX."customers`
                SET 
                `last_login` = NOW() 
                WHERE 
                `id` = ?;";
            $stmt        = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $customer_id = $_SESSION['customer']['id'];
            $stmt->bind_param('i', $customer_id);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

            if($location == "order_process")
            {
                // Falls im Bestellprozess // Lade Seite von order.php
                $script .= $Cpage->load_page('order.php');
            }
            else
                // Falls normale Anmeldung // Lade Startseite vom Shop
            {
                $script .= $Cpage->load_page('account.php');
            }
        } // Wenn Passwort oder Mail falsch:
        else
        {
            $script .= $Cpage->load_page("account.php", "location=".$location."&login_error=1"); // Lade Startseite von Account Seite
        }
    }
    return $content;
}

// Passwort ändern, Schritt 2
function change_password()
{
    global $Cpage;
    global $Cdb;
    global $script;
    global $script_footer;
    $content = "";

    $login_error = 0;

    $password = $Cpage->salt_password($Cpage->get_parameter("password"));

    $sql      = "SELECT `password` FROM `".TBL_PREFIX."customers`
          WHERE `id` = ".$_SESSION['customer']['id']."
          AND 
          BINARY `password` = '".$password."'
          LIMIT 1;";
    $result   = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $password = NULL;
    if($result->num_rows == 0)
    {
        $login_error = 1;
    }
    else
    {
        $new_password = $Cpage->salt_password($Cpage->get_parameter("new_password"));
        $sql          = "UPDATE `".TBL_PREFIX."customers`
            SET 
            `password` = ?,
            `changed` = NOW( ) 
            WHERE 
            `id` = ? 
            LIMIT 1 ;";
        $stmt         = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $customer_id  = $_SESSION['customer']['id'];
        $stmt->bind_param('si', $new_password, $customer_id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }
    $new_password = NULL;

    if(!$login_error)
    {
        // E-Mail Senden
        $Acustomer = $_SESSION['customer'];
        eval($Cpage->require_file('mail', "mail_password_change.php"));
        // Meldung
        $script_footer .= $Cpage->alert(CHANGE_PASSWORD_SUCCESS);
    }
    // Lade Startseite von Account
    $script .= $Cpage->load_page('account.php', "login_error=".$login_error);

    return $content;
}

function logout()
{
    global $Cpage;
    global $script;
    $content = "";

    session_unset();
    session_destroy();
    $script .= $Cpage->load_page(); // Lade Startseite vom Shop

    return $content;
}

// Falls Angemeldet:
function user_site($mail_error = FALSE)
{
    global $Cpage;
    global $Cdb;
    global $load_md5_js;
    global $script;
    global $script_footer;
    $content = "";
    $load_md5_js = TRUE;
    $login_error = $Cpage->get_parameter("login_error", FALSE);

    // Userinfo
    eval($Cpage->require_file('templates', "tpl_account_info.php"));

    // Getätigte Bestellungen + Storno Möglichkeit:
    $sql         = "SELECT * FROM `".TBL_PREFIX."orders`
        WHERE 
        `customer_id` = ".$_SESSION['customer']['id'].";";
    $result      = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $order_count = $result->num_rows;
    if($order_count > 0)
    {
        $Auser_order = array();
        $counter     = 0;
        while($Aorder = $result->fetch_assoc())
        {
            $Auser_order[$counter]           = $Aorder;
            $Auser_order[$counter++]['cart'] = unserialize($Aorder['cart']);
        }
    }
    eval($Cpage->require_file('javascript', "confirm_cancellation.php"));
    eval($Cpage->require_file('templates', "tpl_account_orders.php"));

    // Userdetails + Updatemöglichkeit::
    // Javascript E-Mail Überprüfung auf Syntax
    eval($Cpage->require_file('javascript', "mail_check.php"));
    eval($Cpage->require_file('javascript', "userdata_check.php"));
    $data_changed = $Cpage->get_parameter("data_changed", FALSE);
    eval($Cpage->require_file('templates', "tpl_account_details.php"));

    // Passwort ändern:
    eval($Cpage->require_file('javascript', "password_check.php"));
    eval($Cpage->require_file('templates', "tpl_account_password.php"));

    // Ausloggen:
    eval($Cpage->require_file('templates', "tpl_account_logout.php"));

    // Wichtige Meldungen:
    if($login_error)
    {
        $script_footer .= $Cpage->alert(ERROR_CANNOT_CHANGE_PASSWORD);
    }

    return $content;
}
