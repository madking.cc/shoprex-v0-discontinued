<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */


$css .= "    <link rel=\"stylesheet\"
          href=\"".THEME."css/article_print.css\" type=\"text/css\" media=\"print\"/>";

// Bestimme die Bilder für Javascript
$script .= "
var bild = new Array();
";
if(sizeof($Aarticle['picture']) == 1)
{
    $script .= "   bild[0] = \"<a href='".UPLOAD_DIR."big/".$Aarticle['picture'][0]."' target='_blank'><img src='".UPLOAD_DIR."medium/".$Aarticle['picture'][0]."'></a>\";\n";
}
elseif(sizeof($Aarticle['picture']) == 0)
{
    $script .= "   bild[0] = \"\";\n";
}
else
{
    for($i = 0; $i < sizeof($Aarticle['picture']); $i++)
    {
        if($i == 0)
        {
            $picture_back = (sizeof($Aarticle['picture'])-1);
        }
        else $picture_back = $i-1;
        if($i == (sizeof($Aarticle['picture'])-1))
        {
            $picture_forward = 0;
        }
        else $picture_forward = $i+1;

        $script .= "   bild[$i] = \"<table border='0'><tr><td align='right'>\"+
        \"<span id='pic_select'>".$Cpage->input_button("&#60;&#60;", "document.getElementsByName(\\\"picture_show\\\")[0].innerHTML = bild[".$picture_back."];")."\"+
        \"&#160;&#160;Bild ".($i+1)." von ".(sizeof($Aarticle['picture']))."&#160;&#160;\"+
        \"".$Cpage->input_button(">>", "document.getElementsByName(\\\"picture_show\\\")[0].innerHTML = bild[".$picture_forward."];")."<br /><br />\"+
        \"</span></td></tr><tr><td>\"+
        \"<a href='".UPLOAD_DIR."big/".$Aarticle['picture'][$i]."' target='_blank'><img src='".UPLOAD_DIR."medium/".$Aarticle['picture'][$i]."'></a>\"+
        \"</td></tr></table>\";\n";
    }
}


// Generate Attributes if available
if($Aarticle['attribute'] !== FALSE)
{
    $attribute_box = "<p id='article_attribute'>";
    $attribute_box .= $Cpage->select("attribute", 1, NOT_MULTIPLE, NOT_READONLY, "fix_price(this.form)")."\n";
    $attribute_box .= "<option value='0'>".$Aarticle['attribute']['header'][0]."</option>\n";
    foreach($Aarticle['attribute']['type'] as $key => $value)
    {
        $attribute_box .= "<option value='".$Aarticle['attribute']['id'][$key]."'>$value";
        if($Aarticle['attribute']['disabled'][$key]) $attribute_box .= " - ".SOLD_OUT;
        $attribute_box .= "</option>\n";
    }
    $attribute_box .= "</select></p>\n";
}
else $attribute_box = "";


// Hole evtl. Verlinkungen

$sql = "SELECT * FROM `".TBL_PREFIX."articles_link` WHERE `main_article` =".$Aarticle['id']." ORDER BY pos;";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$Aarticle_links = NULL;
if($result->num_rows > 0)
{

    while($row = $result->fetch_assoc())
    {
        $status = $Carticles->get_article_status($row['linked_article']);
        if($status != 0) continue;
        $tmp = $row['linked_article'];
        $Aarticle_links[] = $Carticles->get_article($tmp);
    }
}



$tpl_article         = $Aarticle;
$tpl_article_links   = $Aarticle_links;
//$tpl_article['text'] = nl2br($tpl_article['text']);

eval($Cpage->require_file('templates', "tpl_article_show.php"));
eval($Cpage->require_file('javascript', "article_add_check.php"));
eval($Cpage->require_file('javascript', "article_change_price.php"));

$script_footer .= "
    document.getElementsByName('picture_show')[0].innerHTML = bild[0];
    ";