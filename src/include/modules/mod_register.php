<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(!isset($location))
{
    $location = $Cpage->get_parameter("location");
}

$_SESSION['customer']['mail']       = $Cpage->get_parameter("mail", $_SESSION['customer']['mail']);
$_SESSION['customer']['password']   = $Cpage->get_parameter("password");
$_SESSION['customer']['firstname']  = $Cpage->get_parameter("firstname", $_SESSION['customer']['firstname']);
$_SESSION['customer']['lastname']   = $Cpage->get_parameter("lastname", $_SESSION['customer']['lastname']);
$_SESSION['customer']['address1']   = $Cpage->get_parameter("address1", $_SESSION['customer']['address1']);
$_SESSION['customer']['address2']   = $Cpage->get_parameter("address2", $_SESSION['customer']['address2']);
$_SESSION['customer']['zip']        = $Cpage->get_parameter("zip", $_SESSION['customer']['zip']);
$_SESSION['customer']['city']       = $Cpage->get_parameter("city", $_SESSION['customer']['city']);
$_SESSION['customer']['country']    = $Cpage->get_parameter("country", $_SESSION['customer']['country']);
$_SESSION['customer']['phone']      = $Cpage->get_parameter("phone", $_SESSION['customer']['phone']);
$_SESSION['customer']['type']       = $Cpage->get_parameter("customer_type", $_SESSION['customer']['type']);
$_SESSION['customer']['company']    = $Cpage->get_parameter("company", $_SESSION['customer']['company']);
$_SESSION['customer']['newsletter'] = $Cpage->get_parameter("newsletter", $_SESSION['customer']['newsletter']);


$action = $Cpage->get_parameter("do_action");
switch($action)
{

    // Neuen User speichern:
    case "store":

        // Überprüfen, ob E-Mail bereits in der Datenbank:
        $sql    = "SELECT `id` FROM `".TBL_PREFIX."customers`
            WHERE `mail` = '".$_SESSION['customer']['mail']."'
                  LIMIT 1;";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

        if($result->num_rows > 0)
        {  // Falls Neuregistrierung nicht geht
            $content .= register($location, MAIL_ERROR);
        }
        else
        {   // Speichere die Neuregistrierung
            $content .= store($location);
        }
        break;

    default:
        // Neuen User Daten abfragen:
        $content .= register($location);
        break;
}


function register($location = "", $mail_error = FALSE)
{
    global $Cpage;
    global $load_md5_js;
    global $script;
    $content = "";
    $load_md5_js = TRUE;

    // Javascript E-Mail Überprüfung auf Syntax
    eval($Cpage->require_file('javascript', "mail_check.php"));

    // Javascript: Überprüfung sonstiger Felder
    eval($Cpage->require_file('javascript', "register_check.php"));

    if($_SESSION['customer']['newsletter'] == 1)
    {
        $nl_check = TRUE;
    }
    else
    {
        $nl_check = FALSE;
    }

    // Felder zum Neu Anmelden:
    eval($Cpage->require_file('templates', "tpl_register.php"));

    return $content;
}

// Eine neue Registrierung in die Datenbank speichern:
function store($location = "")
{
    global $Cpage;
    global $Cdb;
    global $script_footer;
    global $script;
    $content = "";

    $sql              = "SELECT * FROM `".TBL_PREFIX."counter`
                        WHERE `type` LIKE 'CUSTOMER_COUNTER' LIMIT 1;";
    $result           = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $customer_counter = $result->fetch_assoc();
    $customer_counter = $customer_counter['count'];
    $customer_id = CUSTOMER_PREFIX.($customer_counter+CUSTOMER_START).CUSTOMER_SUFFIX;

    $_SESSION['customer']['customer']   = $customer_id;
    $_SESSION['customer']['created']    = $Cpage->get_sqltime();
    $_SESSION['customer']['last_login'] = $_SESSION['customer']['created'];
    $password                           = $Cpage->salt_password($_SESSION['customer']['password']);
    $_SESSION['customer']['password']   = NULL;
    $hash                               = $Cpage->hash();

    $sql = "INSERT INTO `".TBL_PREFIX."customers` (
                    `id` ,
            		`customer` ,
                    `firstname` ,
                    `lastname` ,
                    `address1` ,
                    `address2` ,
                    `zip` ,
                    `city` ,
                    `country` ,
                    `phone` ,
                    `type` ,
                    `company` ,
                    `mail` ,
                    `password` ,
                    `newsletter` ,
                    `created` ,
                    `last_login` ,
                    `hash` 
                    )
                    VALUES (
                    NULL , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    $stmt      = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $Acustomer = $_SESSION['customer'];
    $stmt->bind_param('sssssssssisssisss', $Acustomer['customer'], $Acustomer['firstname'], $Acustomer['lastname'], $Acustomer['address1'], $Acustomer['address2'], $Acustomer['zip'], $Acustomer['city'], $Acustomer['country'], $Acustomer['phone'], $Acustomer['type'], $Acustomer['company'], $Acustomer['mail'], $password, $Acustomer['newsletter'], $Acustomer['created'], $Acustomer['last_login'], $hash);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    $password                   = NULL;
    $id = $Cdb->get_insert_id();
    $_SESSION['customer']['id'] = $id;

    // Erhöhe den Kundenzähler:
    $sql  = "UPDATE `".TBL_PREFIX."counter` SET  `count`    = `count` + 1
	      WHERE
	      `type` = 'CUSTOMER_COUNTER'
	      LIMIT 1 ;";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    // Schönheitskorrektur, da Neu Registrierung:
    $_SESSION['customer']['last_login'] = "";

    // E-Mail Senden
    eval($Cpage->require_file('mail', "mail_register.php"));

    if($location == "order_process")
    {
        $script .= $Cpage->load_page('order.php');
    }
    else
    {
        $script_footer .= $Cpage->alert(REGISTER_SUCCESS_FORWARD);
        $script_footer .= $Cpage->load_page('account.php');
    }

    return $content;
}
