<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$categories = "";

// Erzeuge nach einem Schema ein Menü aus Kategorien mit Unterkategorien
$menu = $Ccategories->do_menu($Carticles->start_id, $_SESSION['selected_cat']);

if(!empty($menu))
{
    $categories .= "<ul id='category_list'>\n";
    $categories .= $menu;
    $categories .= "</ul>\n";
} // Wenn insgesamt keine Kategorien vorhanden:
else $categories .= "<span class='notice'>".NO_CATEGORIES."</span>\n";
  
