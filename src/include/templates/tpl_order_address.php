<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($target_mismatch || $remote_area)
{
    $content .= "<H2>".INVOICE_ADDRESS.":</H2>\n";
}
else
{
    $content .= "<H2>".INVOICE_AND_DELIVERY_ADDRESS.":</H2>\n";
}

$content .= $Cpage->form("check", "order.php", "send_order").$Cpage->input_hidden("request", 0);
$content .= $Cpage->table("order_address_table")."\n";
// Rechnungs(und Liefer)adresse
// Wenn Firma, ansonsten Namen:
if($_SESSION['customer']['type'] == 2)
{
    $content .= "
         <tr>
          <td>".$Cpage->Aglobal['customer_type'][$_SESSION['customer']['type']].":</td>
          <td>";
    if(!empty($_SESSION['customer']['company']))
    {
        $content .= $_SESSION['customer']['company'];
    }
    else
    {
        $content .= $_SESSION['customer']['firstname']." ".$_SESSION['customer']['lastname'];
    }
    $content .= "</tr>\n";
}
else
{
    $content .= "
         <tr>
          <td>".NAME.":</td>
          <td>".$_SESSION['customer']['firstname']." ".$_SESSION['customer']['lastname']."</td>
         </tr>\n";
}

$content .= "
       <tr>
        <td>".ADDRESS.":</td>
        <td>".$_SESSION['customer']['address1']."</td>
       </tr>\n";

if(!empty($_SESSION['customer']['address2']))
{
    $content .= "
         <tr>
          <td>".ADDRESS2.":</td>
          <td>".$_SESSION['customer']['address2']."</td>
         </tr>\n";
}

$content .= "
       <tr>
        <td>".ZIP.", ".CITY.":</td>
        <td>".$_SESSION['customer']['zip']." ".$_SESSION['customer']['city']."</td>
       </tr>
       <tr>
        <td>".COUNTRY.":</td>
        <td>".$Cpage->get_country_name($_SESSION['customer']['country'])."</td>
       </tr>\n";

$content .= "<tr>
        <td>".PHONE.":</td>
        <td>".$Cpage->input_text("phone", $_SESSION['customer']['phone'])."</td>
       </tr>\n";

$content .= " <tr>
        <td>".DELIVERY_TYPE.":</td>
        <td>".$Cpage->Aglobal['delivery'][$_SESSION['order']['delivery_type']]['value']."</td>
       </tr>\n</table>\n";

// Hinweis, falls Gewicht zu hoch
if($_SESSION['order']['weight_to_heavy'])
{
    $content .= "<br /><p class='warning'><span class='notice'>".NOTICE.":</span> ".SHIPPING_TO_HEAVY."</p>";
}
else
{
    // Hinweise zur Lieferadresse:
    if($target_mismatch && !$remote_area_delivery)
    {
        $content .= "<br /><p class='warning'><span class='notice'>".NOTICE.":</span> ".CART_TARGET_DIFFERENT_PART01." ".$cart_country_name." ".CART_TARGET_DIFFERENT_PART02."<br />".CART_TARGET_DIFFERENT_PART03."</p>";
    }
    elseif($remote_area && !$target_mismatch && !$remote_area_delivery_check)
    {
        $content .= "<br /><p class='warning'><span class='notice'>".NOTICE.":</span> ".INVOICE_ADDRESS_IS_IN_REMOTE_AREA."</p>";
    }
    elseif($remote_area_delivery)
    {
        $content .= "<br /><p class='warning'><span class='notice'>".NOTICE.":</span> ".DELIVERY_ADDRESS_IS_IN_REMOTE_AREA."</p>";
    }
}
$content .= "<div class='spacer'></div>\n";

// Anmerkungen vom Kunden:
$content .= "<div id='user_comment'>
      <H2>".YOUR_NOTES_TO_YOUR_ORDER.":</H2>\n".$Cpage->textarea("customer_message", 600, 100).stripslashes($_SESSION['customer']['customer_message'])."</textarea>
      </div>";

// Bestätigung vom User

$content .= "<div id='terms_check'>".$Cpage->input_checkbox("read_terms", "1")." ".I_HAVE_READ_THE_TERMS_PART01." ".$Cpage->link_db("agb", NEW_PAGE)." ".I_HAVE_READ_THE_TERMS_PART02."<br />"
    .$Cpage->input_checkbox("read_privacy", "1")." ".I_HAVE_READ_THE_PRIVACY_PART01." ".$Cpage->link_db("privacy", NEW_PAGE)." ".I_HAVE_READ_THE_PRIVACY_PART02."<br /><br /></div>\n";


// Aktionbuttons:
$content .= "<div id='order_buttons_wrapper'>".$Cpage->link_onClick(BACK_TO_CART, "doSubmit('cart.php', document.check);", "link_button")."\n";
if($_SESSION['order']['weight_to_heavy'])
{
    $content .= $Cpage->link_onClick(REQUEST_ORDER, "doRequest('order.php', document.check, 'no_delivery_need');", "link_button", "id='send_order_link'")."\n";
}
else
{
    $content .= $Cpage->link_onClick(REQUEST_ORDER, "doRequest('order.php', document.check, 'no_delivery_need');", "link_button", "id='request_order_link2'", "display:none;")."\n";
    $content .= $Cpage->link_onClick(CHECK_DELIVERY_ADDRESS, "checkDeliveryAddress('order.php', document.check, 'delivery_need');", "link_button", "id='check_delivery_address_link2'", "display:none;")."\n";

    if($target_mismatch || $remote_area || ($remote_area_delivery && $remote_area_delivery_check))
    {
        $content .= $Cpage->link_onClick(REQUEST_ORDER, "doRequest('order.php', document.check, 'no_delivery_need');", "link_button", "id='request_order_link'")."\n";
        $content .= $Cpage->link_onClick(CHECK_DELIVERY_ADDRESS, "checkDeliveryAddress('order.php', document.check, 'delivery_need');", "link_button", "id='check_delivery_address_link'")."\n";
    }
    $content .= $Cpage->link_onClick(SEND_ORDER_PART01." ".$_SESSION['order']['cart_price_brutto']." ".SEND_ORDER_PART02, "doSubmit('order.php', document.check, 'delivery_need');", "link_button", "id='send_order_link'", "display:none;")."\n";
}
$content .= "</div>\n";

// Lieferadresse:
$content .= "<div class='spacer'></div>
        <H2>
        <input type='checkbox' class='checkbox delivery_checkbox' onchange='useDeliveryDataChanged()' id='use_delivery_data' name='use_delivery_data' value='1'";
if($target_mismatch || $remote_area || $_SESSION['customer']['use_delivery_data'])
{
    $content .= " checked='checked'";
}
$content .= "> ".DIFFERENT_TARGET_ADDRESS.":</H2>\n";

$content .= $Cpage->table("delivery_table")."\n
        <tr>
         <td><span class='needed'>".NEED_SYMBOL."</span></td>
         <td>".NAME_OR_COMPANY.":</td>
         <td>".$Cpage->input_text("del_name", $_SESSION['customer']['del_name'])."</td>
        </tr>
        <tr>
         <td><span class='needed'>".NEED_SYMBOL."</span></td>
         <td>".ADDRESS.":</td>
         <td>".$Cpage->input_text("del_address1", $_SESSION['customer']['del_address1'])."</td>
        </tr>
        <tr>
         <td></td>
         <td>".ADDRESS2.":</td>
         <td>".$Cpage->input_text("del_address2", $_SESSION['customer']['del_address2'])."</td>
        </tr>
        <tr>
         <td><span class='needed'>".NEED_SYMBOL."</span></td>
         <td>".ZIP.":</td>
         <td>".$Cpage->input_text("del_zip", $_SESSION['customer']['del_zip'], FORM_TEXT_SIZE, NOT_READONLY, NO_FREETEXT, NO_MAXLENGTH, "input, delivery_address_change();")."</td>
        </tr>
        <tr>
         <td><span class='needed'>".NEED_SYMBOL."</span></td>
         <td>".CITY.":</td>
         <td>".$Cpage->input_text("del_city", $_SESSION['customer']['del_city'], FORM_TEXT_SIZE, NOT_READONLY, NO_FREETEXT, NO_MAXLENGTH, "input", "delivery_address_change();")."</td>
        </tr>
        <tr>
         <td></td>
         <td>".COUNTRY."</td>
         <td>\n";
$content .= $Cpage->select_countries("del_country_name", $_SESSION['order']['cart_country_target'], FORM_TEXT_SIZE, NO_CHANGE, READONLY).$Cpage->input_hidden("del_country", $_SESSION['order']['cart_country_target']);

$content .= " <span class='information'>(Änderung geht nur über Warenkorb)</span>
         </td>
        </tr>
        <tr>
         <td></td>
         <td>".PHONE.":</td>
         <td>".$Cpage->input_text("del_phone", $_SESSION['customer']['del_phone'])."</td>
        </tr>        
        <tr>
         <td></td>
         <td colspan='2'><p class='information'>".NEED_TO_BE_FILLED_PART01." <span class='needed'>".NEED_SYMBOL."</span> ".NEED_TO_BE_FILLED_PART02."</p></td>
        </tr>
        </table></form>                
        \n";


