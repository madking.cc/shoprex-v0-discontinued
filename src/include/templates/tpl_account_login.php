<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<div id='account_login'>
   <H2>".LOGIN.":</H2>\n".$Cpage->form("login_form", "account.php", "login", "return login_check(this);", $location).$Cpage->table("login_table")."
   <tr>
    <th>".EMAIL.":</th>
    <td>".$Cpage->input_text("mail", $_SESSION['customer']['mail'], 200)."</td>
   </tr>
   <tr>
    <th>".PASSWORD.":</th>
    <td>".$Cpage->input_password("password", 200)."</td>
   </tr>\n";
if($login_error == 1)
{
    $content .= "<tr>
    <td></td>
    <th><span class='error'>".WRONG_LOGIN_DATA."</span></td>
   </tr>\n";
}
elseif($login_error == 2)
{
    $content .= "<tr>
    <td></td>
    <th><span class='error'>".ACCOUNT_BANNED."</span></td>
   </tr>\n";
}

$content .= "   <tr>
    <td></td>
    <td>".$Cpage->input_submit(BUTTON_LOGIN)."</td>
   </tr>
   <tr>
    <td></td>
    <td>".$Cpage->link(FORGOT_PASSWORD, "password.php")."</td>
   </tr>
  </table>
 </form><br />\n
 </div>\n";
 