<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$t = 0;
if(!ENABLE_TAX)
    $t = -2;


$content .= "
    <H2 class='content_header'>".SHPPING_COST."</H2>";

if(DELIVERY_METHOD != "free")
{
    if(DELIVERY_METHOD == "flatrate")
    {
        $content .= $Cpage->table("shipping_table")."
          <tr class='line_bottom'>
           <th>Zielland</th>";
           if(ENABLE_TAX)
            $content .= "<th>Netto</th>
            <th>MwSt.</th>";
           $content .= "<th>Brutto</th>
          </tr>
        ";

        foreach($tpl_shipping_cost as $key => $value)
        {
            $content .= "<tr><td valign='top'>";
            foreach($value['countries'] as $key2 => $value2)
            {
                $content .= $value2;
                if(($key2+1) != sizeof($value['countries']))
                {
                    $content .= ", ";
                }
            }
            $content .= "</td>";
            if(ENABLE_TAX)
                $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_netto']."</nobr></td><td valign='top'><nobr>".$value['shipping_tax']."</nobr></td>";
            $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_brutto']."</nobr></td></tr>";
        }

        $content .= "</table>";
    }
    elseif(DELIVERY_METHOD == "weight")
    {
        $content .= $Cpage->table("shipping_table")."
          <tr class='line_bottom'>
           <th>Zielland</th>
           <th><nobr>Gewicht bis</nobr></th>";
            if(ENABLE_TAX)
                $content .= "<th>Netto</th>
                <th>MwSt.</th>";
           $content .= "<th>Brutto</th>
          </tr>
        ";

        foreach($tpl_shipping_cost as $key => $value)
        {
            $content .= "<tr><td valign='top' rowspan='".(sizeof($value['weight']))."'>";
            foreach($value['countries'] as $key2 => $value2)
            {
                $content .= $value2;
                if(($key2+1) != sizeof($value['countries']))
                {
                    $content .= ", ";
                }
            }
            $content .= "</td>";
            foreach($value['weight'] as $key2 => $value2)
            {
                $content .= "<td align='right' valign='top'><nobr>".$value2."</nobr></td>";
                if(ENABLE_TAX) $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_netto'][$key2]."</nobr></td><td valign='top'><nobr>".$value['shipping_tax']."</nobr></td>";
                $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_brutto'][$key2]."</nobr></td></tr>";
                if(($key+1) != sizeof($value['weight']))
                {
                    $content .= "\n<tr>";
                }
            }
        }

        $content .= "</table>";
    }
    if(ENABLE_COD && (sizeof($tpl_cod_cost) > 0))
    {
        $content .= "<br /><br />
            <H2 class='content_header'>".COD_COST."</H2>";

        $content .= $Cpage->table("shipping_table")."
          <tr class='line_bottom'>
           <th>Zielland</th>";
        if(ENABLE_TAX)
                $content .= "<th>Netto</th>
                <th>MwSt.</th>";
           $content .= "<th>Brutto</th>
          </tr>
        ";

        foreach($tpl_cod_cost as $key => $value)
        {
            $content .= "<tr><td valign='top'>";
            foreach($value['countries'] as $key2 => $value2)
            {
                $content .= $value2;
                if(($key2+1) != sizeof($value['countries']))
                {
                    $content .= ", ";
                }
            }
            $content .= "</td>";
            if(ENABLE_TAX)
                $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_netto']."</nobr></td><td valign='top'><nobr>".$value['shipping_tax']."</nobr></td>";
            $content .= "<td align='right' valign='top'><nobr>".$value['shipping_cost_brutto']."</nobr></td></tr>";
            $content .= "\n<tr>";
        }

        $content .= "</table>";
    }

    if(!ENABLE_TAX) $content .= "<br /><br /><span class='information'>".REASON_FOR_NO_TAX."</span>";
}
else
{
    $content .= NO_SHIPPING_CALCULATION;
}