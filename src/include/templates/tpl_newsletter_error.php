<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".ERROR.":</H2>\n".UNFORSEEN_ERROR."<br /><br />\n".
    $Cpage->link(TO_START_PAGE, "index.php", NO_PARAMETER, NO_TARGET, "nl_error_back", "link_button")."\n";
 