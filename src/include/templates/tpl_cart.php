<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

eval($Cpage->require_file('templates', "tpl_cart_pattern.php"));

// Aktionen für User ermöglichen oder auch nicht:
if(sizeof($Acart['cart']) > 0)
if($Acart['cart'][0]['id'] > 0 || $Acart['positions'] > 1)
{
    $content .= "<p id='cart_buttons'>".$Cpage->link_onClick(DELETE_CART, "remove_cart();", "link_button");

    if(isset($_SESSION['coupon']['minimum']))
    {
        $coupon_brutto = $Cpage->tax_calc($_SESSION['coupon']['netto'], "brutto", $_SESSION['coupon']['tax']);
        if($_SESSION['coupon']['minimum'] > ($Acart['cart_price_brutto']+$coupon_brutto))
            $content .= MINIMUM_ORDER_QUANTITY_01." ".$Cpage->money($_SESSION['coupon']['minimum'])." ".MINIMUM_ORDER_QUANTITY_02;
        else
            $content .= add_button();
    }
    else $content .= add_button();



    $content .= "</p>\n";
}

function add_button()
{
    global $Acart;
    global $Cpage;
    $content = "";
    if($Acart['shipping_error'] && !$Acart['cod_shipping_error'] && ENABLE_REQUEST)
    {
        $content .= $Cpage->link_onClick(REQUEST_ARTICLE, "order('cart.php');", "link_button");
    }
    elseif(!$Acart['cod_shipping_error'] && !$Acart['shipping_error'])
    {
        $content .= $Cpage->link_onClick(ORDER_ARTICLE, "order('cart.php');", "link_button");
    }
    return $content;
}


// Coupon hinzufügen
eval($Cpage->require_file('templates', "tpl_add_coupon.php"));