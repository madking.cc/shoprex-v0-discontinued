<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(!isset($_SESSION['coupon']))
{
    $content .= "<div class='spacer'></div>";
    $content .= "<div id='coupon_add'>";
    $content .= $Cpage->form("add_coupon", "cart.php", "add_coupon");
    $content .= COUPON.": ".$Cpage->input_text("coupon", "", 120)." ".$Cpage->input_submit("Hinzufügen");
    $content .= "</form>";
    $content .= "</div>";
}