<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".REQUEST_FOR.":</H2>
  <div id='question_wrapper'>\n".$Cpage->form("article_question", "question.php", "send_question", "return form_mail_check(this.question_from_mail);").
    $Cpage->input_hidden("question_to_id", $tpl_article['id']).
    $Cpage->table("question_table")."\n
  <tr>
   <th>".ARTICLE.":</th>
   <td>".$tpl_article['name']."</td>
   <td rowspan='4' align='right' valign='top'>\n".$Cpage->link_newPage($Cpage->img(UPLOAD_DIR."small/".$tpl_article['picture'][0], $tpl_article['picture_text'][0]),
    UPLOAD_DIR."big/".$tpl_article['picture'][0])."\n
   </td>
  </tr>";


if(!empty($tpl_article['isbn']))
    $content .= "  <tr>
   <th>".ISBN.":</th>
   <td>".$tpl_article['isbn']."</td>
  </tr>";

$content .= "  <tr>
   <th valign='top'>".CONDITION.":</th>
   <td><p>";
   if($tpl_article['condition'] != 0)
       $content .= $Cpage->Aglobal['condition'][$tpl_article['condition']];
$content .= " </p>
       <p>";
    if($tpl_article['guarantee'] != 0)
        $content .= $Cpage->Aglobal['guarantee'][$tpl_article['guarantee']];
$content .= " </p><br /><br /><br /></td>
  </tr>
  <tr>
   <th>".YOUR_EMAIL.":</th>
   <td colspan='2'>".$Cpage->input_text("question_from_mail", $_SESSION['customer']['mail'])."</td>
  </tr>
  <tr>
   <th>".MESSAGE_HEADER.":</th>
   <td colspan='2'>".$Cpage->input_text("question_header", NO_VALUE, FORM_TEXT_SIZE_DOUBLE)."</td>
  </tr>
  <tr>
   <th>".YOUR_MESSAGE.":</th>
   <td colspan='2'>".$Cpage->textarea("question_text", FORM_TEXT_SIZE_DOUBLE, 200)."</textarea></td>
  </tr>
  <tr>
   <th></th>
   <td colspan='2'>".$Cpage->input_submit(SEND_QUESTION)." ".$Cpage->input_button(BACK_TO_ARTICLE, "history.back();")."
  </tr>
 </table>
</form> 
</div>\n";
