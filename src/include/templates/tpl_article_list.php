<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

for($t = 0; $t < ($tpl_article_count); $t += $tpl_colspan)
{
    // Name:
    $content .= "<div class='article_line1'>\n";
    for($i = $t; ($i < ($t+$tpl_colspan)) && ($i < $tpl_article_count); $i++)
        $content .= " <div class='article_header'>".$Cpage->link($tpl_article[$i]['name'], "index.php", "article=".$tpl_article[$i]['id']."#output", "article_list_header", $tpl_article[$i]['style'])."</div>\n";
    $content .= "</div>\n";

    $content .= "<div class='clear_float'></div>\n";

    // Bild:
    $content .= "<div class='article_line2'>\n";
    for($i = $t; ($i < ($t+$tpl_colspan)) && ($i < $tpl_article_count); $i++)
    {
        $content .= " <div class='article_picture'><div class='article_list_image_wrapper'><div class='image_inner'>".$Cpage->link($Cpage->img(UPLOAD_DIR."small/".$tpl_article[$i]['picture'][0], $tpl_article[$i]['picture_text'][0], "article_list_image"), "index.php", "article=".$tpl_article[$i]['id']."#output", "article_list_image_link")."</div></div></div>\n";
    }
    $content .= "</div>\n";

    $content .= "<div class='clear_float'></div>\n";

    // Kurzbeschreibung + Preis:
    $content .= "<div class='article_line3'>\n";

    for($i = $t; ($i < ($t+$tpl_colspan)) && ($i < $tpl_article_count); $i++)
    {
        $content .= " <div class='article_line3_box'>\n";
        $content .= " <span class='article_list_price'>".OUR_PRICE.": ".$Cpage->money($Cpage->tax_calc($tpl_article[$i]['netto'], "brutto", $Atax[$tpl_article[$i]['tax']]))."</span>\n";
        if(!empty($tpl_article[$i]['description']))
        {
            $li_list = explode("\r\n", $tpl_article[$i]['description']);

            $content .= "  <ul class='description_list' id='description_list_".$tpl_article[$i]['id']."'>\n";
            for($u = 0; $u < sizeof($li_list); $u++)
                $content .= "   <li>".$li_list[$u]."</li>\n";
            $content .= "  </ul>";
        }

        $content .= "</div>\n";
    }
    $content .= "</div>\n";

    $content .= "<div class='clear_float'></div>\n";
    $content .= "<div class='article_list_spacer'></div>\n";
}
