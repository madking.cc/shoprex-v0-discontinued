<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Verschiedene Formatierungshilfen
$t   = -1;
$tt  = -2;
$ttt = -1;
if(DELIVERY_METHOD == "weight")
{
    $t = 0;
}
if(ENABLE_TAX)
{
    $tt = 0;
}
if(USE_ARTICLE_NUMBER)
{
    $ttt = 0;
}

if(isset($in_order_process))
    $content .= "<H2 class='content_header' id='cart_header'>".CART.":</H2>";




// Verschiedne Felder, die manchmal benötigt werden
if(isset($in_order_process))
{
    $content .= $Cpage->form("cart_form", "cart.php", "no_action").$Cpage->input_hidden("article_id").$Cpage->input_hidden("attribute_id").$Cpage->input_hidden("cart_price_brutto", $Cpage->money($Acart['cart_price_brutto']+$Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto'])).$Cpage->input_hidden("weight_to_heavy", $Acart['shipping_error']);
}



// Wenn die Lieferart ausgewählt werden soll
if(isset($in_order_process))
{
    $content .= DELIVERY_TYPE.":\n".$Cpage->select_multi("delivery_type", $Cpage->Aglobal['delivery'], $_SESSION['order']['delivery_type'], 0, 0, NOT_READONLY, "this.form.submit();")."\n";
    $content .= "<div class='spacer'></div>";
}

// Zeige Warenkorb an
$content .= $Cpage->table("cart_table")."
<tr class='line_bottom_big'>
<td>".POS.":</th>
<td>".ARTICLE.":</th>";
if(USE_ARTICLE_NUMBER)
{
    $content .= "<td align='right'>".ARTICLE_NUMBER_SHORT.":</th>";
}
if(DELIVERY_METHOD == "weight")
{
    $content .= "<td align='right'>".WEIGHT.":</th>";
}
if(ENABLE_TAX)
{
    $content .= "<td align='right'>".AFTER_TAX.":</th>
    <td align='right'>".TAX_SHORT.":</th>";
}
$content .= "<td align='right'>".PRE_TAX.":</th>
<td align='right'>".LOT.":</th>
<td align='right'>".ITEMS.":</th>
<td></th>
</tr>\n";


// Warenkorbinhalt anzeigen:
if(sizeof($Acart['cart']) > 0)
{
    for($pos = 0; $pos < $Acart['positions']; $pos++)
    {
        $content .= "<tr";
        if(empty($Acart['cart'][$pos]['description']))
        {
            $content .= "  class='line_bottom'";
        }
        $content .= ">\n";
        // Position
        $content .= " <td>".($pos+1)."</td>\n";
        // Artikel Name
        if($Acart['cart'][$pos]['id'] > 0)
            $content .= " <td>".$Cpage->link($Acart['cart'][$pos]['name'], "index.php", "article=".$Acart['cart'][$pos]['id'])."</td>\n";
        else
            $content .= " <td>".$Acart['cart'][$pos]['name']."</td>\n";
        // Artikel Nummer
        if(USE_ARTICLE_NUMBER)
        {
            $content .= " <td align='right'>".$Acart['cart'][$pos]['number']."</td>\n";
        }
        // Gewicht
        if(DELIVERY_METHOD == "weight")
        {
            if($Acart['cart'][$pos]['id'] > 0)
                $content .= " <td align='right'><nobr>".$Cpage->weight_kilogram($Acart['cart'][$pos]['total_weight'])."</nobr></td>\n";
            else
                $content .= " <td align='right'></td>\n";
        }
        // Netto Preis
        if(ENABLE_TAX)
        {
            $content .= " <td align='right'><nobr>".$Cpage->money($Acart['cart'][$pos]['netto'])."</nobr></td>\n";
        }
        // MwSt. in Prozent
        if(ENABLE_TAX)
        {
            $content .= " <td align='right'>".$Acart['cart'][$pos]['tax']."%</td>\n";
        }
        // Brutto Preis
        $content .= " <td align='right'><nobr>".$Cpage->money($Cpage->tax_calc($Acart['cart'][$pos]['netto'], "brutto", $Acart['cart'][$pos]['tax']))."</nobr></td>\n";
        // Menge + Buttons zum erhöhen oder erniedrigen der Menge
        $content .= " <td align='right'><nobr>".$Acart['cart'][$pos]['quantity'];
        if(isset($in_order_process))
        {
            if($Acart['cart'][$pos]['id'] > 0)
                $content .= " ".$Cpage->input_button(SYMBOL_MINUS, "changeItem(\"".$Acart['cart'][$pos]['id']."\", \"".$Acart['cart'][$pos]['attribute_id']."\", \"down\");", "", "button_change").$Cpage->input_button(SYMBOL_PLUS, "changeItem(\"".$Acart['cart'][$pos]['id']."\", \"".$Acart['cart'][$pos]['attribute_id']."\", \"up\");", "", "button_change")."</nobr></td>\n";
        }
        // Brutto Preis * Menge
        $content .= " <td align='right'><nobr>".$Cpage->money($Cpage->tax_calc($Acart['cart'][$pos]['total_netto'], "brutto", $Acart['cart'][$pos]['tax']))."</nobr></td>\n";
        // Löschen?
        $content .= " <td>";
        if(isset($in_order_process)) $content .= $Cpage->img(THEME."images/trash.gif", REMOVE, NO_CLASS, "changeItem('".$Acart['cart'][$pos]['id']."', '".$Acart['cart'][$pos]['attribute_id']."', 'remove');");
        $content .= "</td>\n";
        $content .= "</tr>\n";

        if(!empty($Acart['cart'][$pos]['description']))
        {
            $content .= "<tr class='line_bottom'>\n";
            $content .= " <td></td>\n";
            $content .= " <td colspan='".(9+$t+$tt+$ttt)."'>";
            $li_list = explode("\r\n", $Acart['cart'][$pos]['description']);
            for($u = 0; $u < sizeof($li_list); $u++)
            {
                $content .= $li_list[$u];
                if($u != sizeof($li_list)-1)
                {
                    $content .= ", ";
                }
            }
            $content .= "</td>\n";
            $content .= "</tr>\n";
        }
    }

    if($Acart['cart'][0]['id'] > 0 || $Acart['positions'] > 1)
    {
        // Versandziel + Kosten:
        if(DELIVERY_METHOD != "free")
        {
            if($_SESSION['order']['delivery_type'] != NO_SHIPPING_COST)
            {
                $content .= "<tr class='line_bottom'><td>";
                $content .= ($Acart['positions']+1);
                $content .= "</td><td colspan='".($ttt+$t+3)."'>".SHIPPING_COST_TO." ";
                // Zielland:
                if(isset($in_order_process))
                {
                    $content .= $Cpage->select_countries("cart_country_target", $_SESSION['order']['cart_country_target'], FORM_TEXT_SIZE, "submit();");
                }
                else
                {
                    $content .= $Cpage->select_countries("cart_country_target", $_SESSION['order']['cart_country_target'], FORM_TEXT_SIZE, NO_SUBMIT, READONLY);
                }
                $content .= "\n";
                if(DELIVERY_METHOD == "weight")
                {
                    $content .= "<br /><nobr>(ca. ".$Cpage->weight_kilogram($Acart['total_weight'])." ".WEIGHT.")</nobr>\n";
                }
                $content .= "</td>";

                // Wenn Berechnung nicht möglich, ansonsten Versandkostenpreis:
                if($Acart['shipping_error'])
                {
                    $content .= "<td colspan='".(5+$tt)."'><span class='error'>".SHIPPING_TO_HEAVY."</span></td><td></td>";
                }
                else
                {
                    if(ENABLE_TAX)
                    {
                        $content .= "<td align='right'><nobr>".$Cpage->money($Acart['shipping_cost_netto'])."</nobr></td><td align='right'><nobr>".$Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]."%</nobr></td>";
                    }
                    $content .= "<td colspan='3' align='right'><nobr>".$Cpage->money($Acart['shipping_cost_brutto'])."</nobr></td><td></td>";
                }
                $content .= "</tr>\n";

                // Wenn per Nachnahme:
                if($_SESSION['order']['delivery_type'] == COD_SHIPPING_COST)
                {
                    $content .= "<tr class='line_bottom'><td>";
                    $content .= ($Acart['positions']+2);
                    $content .= "</td><td colspan='".(3+$t+$ttt)."'><span class='cod_cost' id='cod_cost_show'>".INCL_COD_FEE_OF."</span></td>";
                    if($Acart['cod_shipping_error'])
                    {
                        $content .= "<td colspan='".(5+$tt)."'><span class='medium_error' id='cod_cost_error'>".COD_SHIPPING_FOR_COUNTRY_NOT_POSSIBLE."</span></td><td></td>";
                    }
                    elseif($Acart['shipping_error'])
                    {
                        $content .= "<td colspan='".(5+$tt)."'><span class='medium_error' id='cod_cost_error'>".COD_CALCULATION_NOT_POSSIBLE."</span></td><td></td>";
                    }
                    else
                    {
                        if(ENABLE_TAX)
                        {
                            $content .= "<td align='right'><nobr>".$Cpage->money($Acart['cod_shipping_cost_netto'])."</nobr></td><td align='right'><nobr>".$Cpage->Aglobal['tax'][COD_TAX_KEY]."%</nobr></td>";
                        }
                        $content .= "<td colspan='3' align='right'><nobr>".$Cpage->money($Acart['cod_shipping_cost_brutto'])."</nobr></td><td></td>";
                    }
                    $content .= "</tr>\n";
                }
            } // Wenn die Ware abgeholt wird:
            else
            {
                $content .= "<tr class='line_bottom'><td></td><td colspan='".(8+$t+$tt+$ttt)."'>".NO_SHIPPING_COST_BY_PICKUP."</td><td></td></tr>\n";
            }
        } // Wenn keine Versandkosten berechnet werden:
        else
        {
            $content .= "<tr class='line_bottom'><td></td><td colspan='".(8+$t+$tt+$ttt)."'>".NO_SHIPPING_CALCULATION."</td><td></td></tr>\n";
        }
    }
}
else
{
    $content .= "<tr class='empty_cart'><td colspan='".(10+$t+$tt+$ttt)."' align='center'><H4>".YOUR_CART_CONTAINS_NO_ARTICLE."</H4></td></tr>";
    $last_pos = -1;
}

if(sizeof($Acart['cart']) > 0)
if($Acart['cart'][0]['id'] > 0 || $Acart['positions'] > 1)
{
    // Brutto Summe
    $content .= "
       <tr>
        <td colspan='".(6+$t+$tt+$ttt)."'></td>
        <td colspan='2' class='td_line_bottom_big' align='right'><span class='important'>".PRE_TAX_PRICE.":</span></th>
        <td class='td_line_bottom_big' align='right'><nobr><span class='important'>".$Cpage->money($Acart['cart_price_brutto']+$Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto'])."</span></nobr></td>
        <td></td>
       </tr>\n";

    // Die (verschiedenen) Mehrwertsteuer
    if(!empty($Acart['tax']) && ENABLE_TAX)
    {
        foreach($Acart['tax'] as $tax_percent => $tax)
        {
            if(!empty($tax))
            {
                $content .= "
           <tr>
            <td colspan='".(6+$t+$ttt)."'></td>
            <td colspan='2' class='td_line_bottom' align='right'><nobr>".$tax_percent."% ".TAX_SHORT.":</nobr></th>
            <td class='td_line_bottom' align='right'><nobr>".$Cpage->money($tax)."</nobr></td>
            <td></td>
           </tr>\n";
            }
        }
    }

    // Netto Summe
    if(ENABLE_TAX)
    {
        $content .= "
       <tr>
        <td colspan='".(6+$t+$ttt)."'></td>
        <td colspan='2' class='td_line_bottom' align='right'>".AFTER_TAX_PRICE.":</th>
        <td class='td_line_bottom' align='right'><nobr>".$Cpage->money($Acart['cart_price_netto']+$Acart['shipping_cost_netto']+$Acart['cod_shipping_cost_netto'])."</nobr></td>
        <td></td>
       </tr>";
    }
}
else
{
    $content .= "<tr>
        <tr class='empty_cart'><td colspan='".(10+$t+$tt+$ttt)."' align='center'><H4>".YOUR_CART_CONTAINS_NO_ARTICLE."</H4></td></tr>
       </tr>";
}

// Ende des Warenkorbs
$content .= "</table>\n";
if(isset($in_order_process))
{
    $content .= "</form>\n\n";
}

if(!ENABLE_TAX)
{
    $content .= "<br /><p class='information'>".REASON_FOR_NO_TAX."</p><br />";
}

