<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".MAKE_NEW_PASSWORD.":</H2>\n";
$content .= $Cpage->form("password_reset", "password.php", NO_HIDDEN_ACTION, "return pwd_reset_check(this);");
$content .= $Cpage->input_hidden("pwd_id", $password_field['id']);
$content .= $Cpage->input_hidden("pwd_customer_id", $password_field['customer_id']);
$content .= $Cpage->input_hidden("key", $key);
$content .= $Cpage->table("password_reset_table");
$content .= "   <tr><td align='right'>".YOUR_EMAIL.":</td><td>".$Cpage->input_text("pwd_mail", NO_VALUE, 200)."</td></tr>\n";
$content .= "   <tr><td align='right'>".NEW_PASSWORD.":</td><td>".$Cpage->input_password("pwd_new_password", 200)."<span class='information'> (".MINIMUM." ".PASSWORD_LENGTH." ".TOKEN.")</span></td></tr>\n";
$content .= "   <tr><td align='right'>".CONFIRM_NEW_PASSWORD.":</td><td>".$Cpage->input_password("pwd_password_check", 200)."</td></tr>\n";
$content .= "   <tr><td> </td><td>".$Cpage->input_submit(SEND)."</td></tr>\n";
$content .= "  </table></form>\n";
    
 