<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "
        <H2 class='content_header'>".FORGOT_PASSWORD.":</H2>\n".
    $Cpage->form("password_send_form", "password.php", "password_send_mail", "return form_mail_check(this.pwd_mail);", $location).
    $Cpage->table("password_mail_table")."
          <tr class='R1'>
           <th class='D1'>".YOUR_EMAIL.":</th>
           <td class='D2'>".$Cpage->input_text("pwd_mail", $_SESSION['customer']['mail'])."</td>
          </tr>
          <tr class='R2'>
           <td class='D1'></td>
           <td class='D2'>".$Cpage->input_submit(SEND_EMAIL_WITH_INSTRUCTIONS)."</td>
          </tr>
         </table>
        </form>\n";
 