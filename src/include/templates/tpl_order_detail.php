<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>Ihre Bestellung vom ".$Cpage->format_time($Aorder['order_time']).":</H2>\n";

eval($Cpage->require_file('templates', "tpl_cart_pattern.php"));

// Kundenvermerk:
if(!empty($Aorder['message']))
{
    $content .= "<p id='customer_message_header'>".YOUR_COMMENT_TO_YOUR_ORDER.":</p>\n";
    $customer_message_text = nl2br(preg_replace('/ /', "&#160;", stripslashes($Aorder['message'])));
    $content .= "<p id='customer_message_text'>".$customer_message_text."</p>\n";
}

$content .= "<br /><br />".$Cpage->link(BACK_TO_ACCOUNT, "account.php", NO_PARAMETER, "link_button")."<br /><br />";

