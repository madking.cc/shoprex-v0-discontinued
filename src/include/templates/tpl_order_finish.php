<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".ORDER_RECIEVED_AT_PART_01." ".$Cpage->format_time($time_of_order)." ".ORDER_RECIEVED_AT_PART_02.":</H2>\n".

    $Cpage->print_button("order_print.css", PRINT_ORDER, "print_button_top");

//Rechnungs und Lieferadresse:
$content .= "
      <div id='pre_invoice_wrapper'>\n
        <div id='customer_invoice_address_wrapper'>\n <p id='invoice_header'>".INVOICE_SHORT;
if(!$_SESSION['customer']['use_delivery_data'])
{
    $content .= "- ".AND_DELIVERY;
}
$content .= ADDRESS_LOWER.":</p>\n
      
      <p>";
if($_SESSION['customer']['type'] == 2 && !empty($_SESSION['customer']['company']))
{
    $content .= $_SESSION['customer']['company'];
}
else $content .= $_SESSION['customer']['firstname']." ".$_SESSION['customer']['lastname'];
$content .= "</p>\n  <p>".$_SESSION['customer']['address1']."</p>\n";

if(!empty($_SESSION['customer']['address2']))
{
    $content .= "  <p>".$_SESSION['customer']['address2']."</p>\n";
}

$content .= "  <p>".$_SESSION['customer']['zip']." <font id='invoice_city'>".$_SESSION['customer']['city']."</font></p>\n";
$content .= "  <p>".$Cpage->get_country_name($_SESSION['customer']['country'])."</p>\n";
if(!empty($order_phone))
{
    $content .= "<p>".PHONE.": ".$order_phone."</p>\n";
}
$content .= "  <p>".DELIVERY_TYPE.": ".$Cpage->Aglobal['delivery'][$_SESSION['order']['delivery_type']]['value']."</p>\n";
$content .= "</div>\n\n";

// Abweichende Lieferadresse:
if($_SESSION['customer']['use_delivery_data'])
{
    $content .= "<div id='customer_delivery_address_wrapper'>\n <p id='invoice_header'>Lieferadresse:</p>\n";
    $content .= "  <p>".$_SESSION['customer']['del_name']."</p>\n";
    $content .= "  <p>".$_SESSION['customer']['del_address1']."</p>\n";
    if(!empty($_SESSION['customer']['del_address2']))
    {
        $content .= "<p>".$_SESSION['customer']['del_address2']."</p>\n";
    }
    $content .= "  <p>".$_SESSION['customer']['del_zip']." <font class='Del_city'>".$_SESSION['customer']['del_city']."</font></p>\n";
    $content .= "  <p>".$Cpage->get_country_name($_SESSION['customer']['del_country'])."</p>\n";
    if(!empty($_SESSION['customer']['del_phone']))
    {
        $content .= "<p>".PHONE.": ".$_SESSION['customer']['del_phone']."</p>\n";
    }
    $content .= " </div>\n\n";
}
$content .= "<div class='clear_float'></div>\n";
// Auftrags- und Kundennummer:
$content .= "<p id='order_id'>".ORDER_NUMBER.": ".$order_id."<p>\n";
$content .= "<p>".CUSTOMER_NUMBER.": ".$_SESSION['customer']['customer']."</p>\n";
$content .= "<p id='order_time'>".ORDER_DATE.": ".$Cpage->format_time($time_of_order, "date")."</p>\n";
$content .= "<p id='order_salutation'>".ORDER_HEADER.":</p>\n";

// bestellter Warenkorb:
eval($Cpage->require_file('templates', "tpl_cart_pattern.php"));

// Kundenvermerk:
if(!empty($_SESSION['customer']['customer_message']))
{
    $content .= "<p>".YOUR_COMMENT_TO_YOUR_ORDER.":</p>\n";
    $customer_message_text = nl2br(preg_replace('/ /', "&#160;", stripslashes($_SESSION['customer']['customer_message'])));
    $content .= "<p id='customer_message_text'>".$customer_message_text."</p>\n";
}

// Abschliessender Text: // Bankverbindung, etc...
$content .= "<div id='footer_text'>
        <p>".INVOICE_FOOTER_TEXT."</p>
        <br />\n";
if(($_SESSION['order']['delivery_type'] == DO_ADVANCE) && !$Acart['shipping_error'] && !$request)
{
    $content .= "<p>".TRANSFER_TO_BANK_ACCOUNT.":<br /><br />";
    $content .= ACCOUNT_OWNER.": ".SHOP_BANK_ACCOUNT_OWNER."<br />";
    $content .= BANK_NAME.": ".SHOP_BANK_NAME."<br />";
    $content .= BANK_CODE.": ".SHOP_BANK_CODE."<br />";
    $content .= BANK_NUMBER.": ".SHOP_BANK_NUMBER."<br /><br />";
    $dummy = SHOP_BANK_IBAN;
    if(!empty($dummy))
    {
        $content .= FOR_INTERNATIONAL_TRANSFERS.":<br /><br />";
        $content .= IBAN.": ".SHOP_BANK_IBAN."<br />";
        $content .= SWIFT.": ".SHOP_BANK_SWIFT."<br><br />";
    }
    $content .= "<strong>".REASON_FOR_TRANSFER."</strong></p>";
}
$content .= "
      </div>\n";

$content .= "</div>\n".$Cpage->print_button("order_print.css", PRINT_ORDER, "print_button_bottom");
    
