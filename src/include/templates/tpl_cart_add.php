<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "
      <div id='show_pushed_article_wrapper'>
       <H2 class='content_header'>".YOU_HAVE_IN_CART_PART01." ".$article_quantity." ".YOU_HAVE_IN_CART_PART02.":</H2>
       <div id='pushed_image'>".$Cpage->img(UPLOAD_DIR."small/".$Aarticle['picture'][0], $Aarticle['picture_text'][0])."
       </div>
       <div id='pushed_informations'><p id='pushed_article_name'>".$Aarticle['name']."</p>\n";

if($li_list_size > 0)
{
    $content .= " <ul id='pushed_description'>\n";
    for($t = 0; $t < $li_list_size; $t++)
        $content .= "   <li>".$li_list[$t]."</li>\n";
    $content .= " </ul>\n";
}

if($Aarticle['has_discount'])
{
    $original_price = $Cpage->tax_calc($Aarticle['netto_with_attribute'], "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]);
    $discount_price = $Cpage->tax_calc($Aarticle['netto'], "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]);
    $content .= "<p id='pushed_discount'><span id='price_text'>".PIECE_PRICE.":</span> <span id='old_price'>".$Cpage->money($original_price)."</span> <span id='new_price'>".$Cpage->money($discount_price)."</span>";
}
else
{
    $content .= "<p id='pushed_price'><span id='price_text'>".PIECE_PRICE.": ".$Cpage->money($Cpage->tax_calc($Aarticle['netto'], "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]))."</span>";
}
if(ENABLE_TAX)
{
    $content .= " ".INCLUSIVE." ".$Cpage->Aglobal['tax'][$Aarticle['tax']]."% ".TAX;
}
if(DELIVERY_METHOD != "free")
{
    $content .= ", ".$Cpage->link_newPage(WITH_SHIPPING_COST, $Cpage->Aglobal['http']."shipping_cost.php");
}
$content .= "</p></div>
                  <div class='clear_float'></div>
                  <div class='div_line'></div>\n".$Cpage->link(MORE_SHOPPING, "index.php", $path_to_last_location, "link_button")." ".$Cpage->link(CHECK_OUT, "cart.php", NO_PARAMETER, "link_button")."
                  </div>\n";