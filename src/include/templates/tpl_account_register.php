<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<div id='account_register'>
  <H2>".REGISTER_NEW.":</H2>\n".$Cpage->form("register_form", "register.php", "register", NO_ON_SUBMIT, $location).$Cpage->input_submit(REGISTER_NEW_NOW, "id='register_button'")."
  </form>
  <p class='information' id='register_information'>".REGISTER_INFORMATION."</p>
 </div>
 <div class='clear_float'></div>\n";
 