<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= " <div class='spacer'></div>
  <div id='account_newsletter'>
   <H2>".REQUEST_NEWSLETTER.":</H2>\n".$Cpage->form("newsletter_form", "newsletter.php", "get_newsletter", "return form_mail_check(this.nl_mail);").
    $Cpage->table("register_table")."
   <tr>
    <th>".YOUR_EMAIL.":</th>
    <td>".$Cpage->input_text("nl_mail")."</td>
    <td>".$Cpage->select_multi("nl_customer_type", $Cpage->Aglobal['customer_type'], NO_MATCH, 1)."</td>
    <td>".$Cpage->input_submit(SIGN_IN)."
    </td>
   </tr>
  </table>
 </form>
 </div>\n";
 