<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".CUSTOMER_DETAILS.":</H2>
  ".$Cpage->form("update_user_data", "account.php", "update", "return check_submit_user(this);")."\n".$Cpage->table("customer_table")."\n";

if($data_changed)
{
    $content .= " <tr>
    <td></td>
    <td></td>
    <td><p class='information_important' id='user_data_changed'>".YOUR_DATA_HAS_BEEN_CHANGED."</p></td>
   </tr>\n";
}

$content .= " <tr>
    <td><span class='needed'>".NEED_SYMBOL."</span></td>
    <th>".NAME.":</th>
    <td>
    ".$Cpage->input_text("firstname", $_SESSION['customer']['firstname'], FORM_TEXT_SIZE_HALF)."
    ".$Cpage->input_text("lastname", $_SESSION['customer']['lastname'], FORM_TEXT_SIZE_HALF)."</td>
   </tr>
   <tr>
    <td><span class='needed'>".NEED_SYMBOL."</span></td>
    <th>".EMAIL."</th>
    <td>".$Cpage->input_text("mail", $_SESSION['customer']['mail'])."
    <span class='information'> ".SYMBOL_EQUALS." ".LOGIN_NAME."</span></td>
   </tr>\n";

if($mail_error)
{
    $content .= " <tr>
    <td></td>
    <th></th>
    <td><p class='error'>".USER_UPDATE_EMAIL_ERROR."</p></td>
   </tr>\n";
}

$content .= " <tr>
    <td></td>
    <th>".CUSTOMER_STATUS.":</th>
    <td>".$Cpage->select_multi("customer_type", $Cpage->Aglobal['customer_type'], $_SESSION['customer']['type'], 1)."</td>
   <tr>
    <td></td>
    <th>".COMPANY_NAME.":</th>
    <td>".$Cpage->input_text("company", $_SESSION['customer']['company'])."</td>
   </tr>     
   <tr>
    <td><span class='needed'>".NEED_SYMBOL."</span></td>
    <th>".ADDRESS1.":</th>
    <td>".$Cpage->input_text("address1", $_SESSION['customer']['address1'])."</td>
   </tr>
   <tr>
    <td></td>
    <th>".ADDRESS2.":</th>
    <td>".$Cpage->input_text("address2", $_SESSION['customer']['address2'])."</td>
   </tr>
   <tr>
    <td><span class='needed'>".NEED_SYMBOL."</span></td>
    <th>".ZIP.", ".CITY.":</th>
    <td>".$Cpage->input_text("zip", $_SESSION['customer']['zip'], FORM_TEXT_SIZE_HALF)."
                   ".$Cpage->input_text("city", $_SESSION['customer']['city'], FORM_TEXT_SIZE_HALF)."</td>
   </tr>
   <tr>
    <td></td>
    <th>".COUNTRY.":</th>
    <td>".$Cpage->select_countries("country", $_SESSION['customer']['country'])."</td>
   </tr>
   <tr>
    <td></td>
    <th>".PHONE.":</th>
    <td>".$Cpage->input_text("phone", $_SESSION['customer']['phone'])."</td>
   </tr>
   <tr>
    <td></td>
    <th>".NEWSLETTER."?:</th>
    <td>".$Cpage->select_multi("newsletter", $Cpage->Aglobal['answer'], $_SESSION['customer']['newsletter'])."</td>
   </tr>
   <tr>
    <td></td>
    <th></th>
    <td>".$Cpage->input_submit(SAVE_CHANGES)."</td>
   </tr>
   <tr class='R14'>
    <td></td>
    <th></th>
    <td><p class='information'>".NEED_TO_BE_FILLED_PART01." <span class='needed'>".NEED_SYMBOL."</span>
    ".NEED_TO_BE_FILLED_PART02."</p></td>
   </tr>             
  </table>
  </form>
  <div class='spacer'></div>\n";
 