<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".LOGOUT.":</H2>
  ".$Cpage->form("session_destroy", "account.php", "logout")."\n".$Cpage->input_submit(LOGOUT_ACCOUNT)."
  </form>
  <div class='spacer'></div>\n";
