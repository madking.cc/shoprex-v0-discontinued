<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($tpl_entry_found)
{
    $content .= "<H2 class='content_header'>".SUCCESS.":</H2>\n<p>".NEWSLETTER_REMOVED."</p><br />\n";
    $content .= $Cpage->link(TO_START_PAGE, $Cpage->Aglobal['http'], NO_PARAMETER, "link_button")."</a>\n";
}
else
{
    $content .= "<H2 class='content_header'>".NOTICE.":</H2>\n<p>".NO_NEWSLETTER_FOUND."</p><br />\n";
    $content .= $Cpage->link(TO_START_PAGE, $Cpage->Aglobal['http'], NO_PARAMETER, "link_button")."</a>\n";
}
 