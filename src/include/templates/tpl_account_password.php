<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".CHANGE_PASSWORD.":</H2>".$Cpage->form("change_password", "account.php", "change_password", "return check_submit_password(this);")."\n".
    $Cpage->table("password_table")."\n
   <tr>
    <td>".OLD_PASSWORD.":</td>
    <td>".$Cpage->input_password("password")."</td>
   </tr>\n";

if($login_error)
{
    $content .= " <tr><td></td><td><p class='error'>".WRONG_PASSWORD."</p></td></tr>\n";
}

$content .= " <tr>
    <td>".NEW_PASSWORD.":</td>
    <td>".$Cpage->input_password("new_password")."<span class='information'> (".MINIMUM." ".PASSWORD_LENGTH." ".TOKEN.")</span></td>
   </tr>
   <tr class='R4'>
    <td>".CONFIRM_NEW_PASSWORD.":</td>
    <td>".$Cpage->input_password("new_password_check")."</td>
   </tr>
   <tr>
    <td></td>
    <td>".$Cpage->input_submit(CHANGE_PASSWORD)."</td>
   </tr>
   <tr>
    <td></td><td><p class='information_small'>(".NOTICE_PASSWORD_ENCODED.")</p></td>
   </tr>
  </table>
  </form>
  <div class='spacer'></div>\n";
