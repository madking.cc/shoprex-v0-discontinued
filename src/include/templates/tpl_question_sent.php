<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($mail_send_status)
{
    $content .= "<p>".REQUEST_SENT."<p><br /><br />\n";
}
else
{
    $content .= "<p>".REQUEST_NOT_SENT."<p><br /><br />\n";
}

$content .= $Cpage->link(BACK_TO_ARTICLE, "index.php", "article=".$tpl_article['id'], "link_button")."\n";

 