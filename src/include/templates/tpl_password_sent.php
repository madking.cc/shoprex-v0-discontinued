<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($mail_found)
{
    $content .= "<H2 class='content_header'>".CONFIRMATION.":</H2>\n";
    $content .= "<p>".SENT_EMAIL_WITH_PASSWORD_INSTRUCTIONS."<br />\n";
    $content .= YOU_HAVE." ".(TIME_TO_PWD_RESET/60/60)." ".HOURS_TIME_FOR_NEW_PASSWORD."</p>\n";

    $content .= "<br /><br />".$Cpage->link(TO_LOGIN_PAGE, "account.php", "location=".$location, "link_button")."</a>\n";
}
else
{
    $content .= "<H2 class='content_header'>".ERROR.":</H2>\n";
    $content .= "<p>".EMAIL_UNKNOWN_OR_BANNED."</p>\n";
    // Location?
    $content .= "<br /><br />".$Cpage->link(TO_LOGIN_PAGE, "account.php", "location=".$location, "link_button")."</a>\n";
}

