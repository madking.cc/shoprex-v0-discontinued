<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".ERROR.":</H2>\n<p>".PASSWORD_RESET_NOT_POSSIBLE."</p><p>".ERROR_CODE.": ".$error_code."</p><br />\n";
$content .= $Cpage->link(TO_LOGIN_PAGE, "account.php", NO_PARAMETER, "link_button")."</a>\n";
    
 