<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".ORDERS_DONE.":</H2>\n".$Cpage->table("orders_done_table")."\n
 <tr>
  <th>".ORDERED_AT.":</th>
  <th>".ARTICLE.":</th>
  <th>".AMOUNT.":</th>
  <th>".STATUS.":</th>
  <th></th>
 </tr>\n";

if($order_count == 0)
{
    $content .= " <tr><td colspan='5'><p class='information'>".NO_ORDERS."</p></td></tr>\n";
}
else
{
    $counter = 0;
    foreach($Auser_order as $key => $value)
    {
        $content .= " <tr><td class='D1'><nobr>".$Cpage->format_time($Auser_order[$key]['order_time'])."</nobr></td>
    <td><nobr>".$Auser_order[$key]['cart']['entire_quantity']."</nobr></td>
    <td><nobr>".$Cpage->money($Auser_order[$key]['cart']['cart_price_brutto']+$Auser_order[$key]['cart']['shipping_cost_brutto']+$Auser_order[$key]['cart']['cod_shipping_cost_brutto'])."</nobr></td>
    <td><nobr><span class='status_".$Auser_order[$key]['status']."'>".$Cpage->Aglobal['status'][$Auser_order[$key]['status']]."</span></nobr></td>
    <td><nobr>".$Cpage->form("detail_".$Auser_order[$key]['id'], "order.php", "show_order").$Cpage->input_hidden("order_id", $Auser_order[$key]['id']).$Cpage->link_onClick(DETAILS, "document.detail_".$Auser_order[$key]['id'].".submit();")."</form>";
        if($Auser_order[$key]['status'] == 2)
        {
            $content .= $Cpage->form("storno_".$Auser_order[$key]['id'], "order.php", "cancel_order", "return confirm_it();").$Cpage->input_hidden("order_id", $Auser_order[$key]['id'])."&#160;&#160;&#160;&#160;&#160;".$Cpage->input_submit(CANCELLATION)."</form>";
        }
        $content .= "</nobr></td>\n</tr>\n";
        $counter++;
    }
}
$content .= "</table>
<div class='spacer'></div>\n";
 