<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

function search_result_header()
{
    $content = "<H2 class='content_header'>".SEARCH_RESULT.":</H2>\n";
    return $content;
}

function search_no_result()
{
    $content = "<H3>".NO_SEARCH_RESULTS."</H3>";
    return $content;
}

function search_hits($hits)
{
    $content = "<H3 id='search_hits'>".$hits." ".HITS."</H3>\n";
    return $content;
}

function category_result_header()
{
    global $Cpage;
    $content = "<H4>".CATEGORIES.":</H4>\n";
    $content .= $Cpage->table("category_result");
    return $content;
}

function category_result($Acategory)
{
    global $Cpage;
    $content = "<tr>
	  <td style='height:".MAX_PICTURE_SIZE_TINY_HEIGHT."px;'> ".$Cpage->link($Cpage->img(UPLOAD_DIR."tiny/".$Acategory['picture'], $Acategory['picture_text']), "index.php", "category=".$Acategory['id'])."
	  </td>
	  <td style='height:".MAX_PICTURE_SIZE_TINY_HEIGHT."px;'> ".$Cpage->link($Acategory['name'], "index.php", "category=".$Acategory['id'], NO_PARAMETER, "shop_link", $Acategory['style'])."
	  </td>
	 </tr>\n";
    return $content;
}

function category_result_footer()
{
    $content = "</table>\n";
    $content .= "<div  class='spacer'></div>";
    $content .= "\n";
    return $content;
}

function article_result_header()
{
    global $Cpage;
    $content = "<H4>".ARTICLE.":</H4>\n";
    $content .= $Cpage->table("article_result");
    return $content;
}

function article_result($Aarticle)
{
    global $Cpage;
    $content = "<tr>
	    <td style='height:".MAX_PICTURE_SIZE_TINY_HEIGHT."px;'> ".$Cpage->link($Cpage->img(UPLOAD_DIR."tiny/".$Aarticle['picture'][0], $Aarticle['picture_text'][0]), "index.php", "article=".$Aarticle['id'])."
	    </td>
	    <td style='height:".MAX_PICTURE_SIZE_TINY_HEIGHT."px;'> ".$Cpage->link($Aarticle['name'], "index.php", "article=".$Aarticle['id'], NO_PARAMETER, "shop_link", $Aarticle['style'])."
	    </td>
	   </tr>\n";
    return $content;
}

function article_result_footer()
{
    $content = "</table>\n";
    $content .= "<div  class='spacer'></div>";
    $content .= "\n";
    return $content;
}

function page_result_header()
{
    global $Cpage;
    $content = "<H4>".PAGES.":</H4>\n";
    $content .= $Cpage->table("page_result");
    return $content;
}

function page_result($Apage)
{
    global $Cpage;
    $content = " <tr><td style='height:".MAX_PICTURE_SIZE_TINY_HEIGHT."px;'> ".$Cpage->link_db($Apage['page'])."</td></tr>\n";
    return $content;
}

function page_result_footer()
{
    $content = "</table>\n";
    $content .= "<div  class='spacer'></div>";
    $content .= "\n";
    return $content;
}

function new_search($search, $text_search)
{
    global $Cpage;
    $content = "<H2>".NEW_SEARCH.":</H2>\n";
    $content .= $Cpage->form("search_form", "search.php").$Cpage->input_text("search", $search)." ".$Cpage->input_submit(SEARCH)."&#160;&#160;&#160;".$Cpage->input_checkbox("text_search", TRUE, $text_search)." ".FULL_TEXT_SEARCH."</form>\n";
    $content .= "<div  class='search_spacer'></div>";
    return $content;
}