<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<div id='register_form_wrapper'>
 <H2>".NEW_SIGN_IN.":</H2>\n";
$content .= $Cpage->form("register_form2", "register.php", "store", "return check_submit_register(this);", $location)."\n".$Cpage->table("register2_table")."
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".FIRSTNAME.":</th>
   <td>".$Cpage->input_text("firstname", $_SESSION['customer']['firstname'])."</td>
  </tr>
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".LASTNAME.":</th>
   <td>".$Cpage->input_text("lastname", $_SESSION['customer']['lastname'])."</td>
  </tr>
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".ADDRESS1.":</th>
   <td>".$Cpage->input_text("address1", $_SESSION['customer']['address1'])."</td>
  </tr>
  <tr>
   <td></td>
   <th>".ADDRESS2.":</th>
   <td>".$Cpage->input_text("address2", $_SESSION['customer']['address2'])."</td>
  </tr>
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".ZIP.":</th>
   <td>".$Cpage->input_text("zip", $_SESSION['customer']['zip'])."</td>
  </tr>
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".CITY.":</th>
   <td>".$Cpage->input_text("city", $_SESSION['customer']['city'])."</td>
  </tr>
  <tr class='R7'>
   <td></td>
   <th>".COUNTRY.":</th>
   <td>".$Cpage->select_countries("country", $_SESSION['customer']['country'])."</td>
  </tr>
  <tr>
   <td></td>
   <th>".PHONE.":</th>
   <td>".$Cpage->input_text("phone", $_SESSION['customer']['phone'])."</td>
  </tr>
  <tr>
   <td></td>
   <th>".CUSTOMER_STATUS.":</th>
   <td>".$Cpage->select_multi("customer_type", $Cpage->Aglobal['customer_type'], $_SESSION['customer']['type'], 1)."</td>
  </tr>   
  <tr>
   <td></td>
   <th>".COMPANY_NAME.":</th>
   <td>".$Cpage->input_text("company", $_SESSION['customer']['company'])."</td>
  </tr>    
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".EMAIL.":</th>
   <td>".$Cpage->input_text("mail", $_SESSION['customer']['mail'])."<span class='information'> ".SYMBOL_EQUALS." ".LOGIN_NAME."</span></td>
  </tr>\n";
if($mail_error)
{
    $content .= "  <tr><td></td><th></th><td><p class='error'>".ERROR_EMAIL_REGISTERED_OR_BANNED."</p></td></tr>\n";
}
$content .= "  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".PASSWORD.":</th>
   <td>".$Cpage->input_password("password")."<span class='information'> (".MINIMUM." ".PASSWORD_LENGTH." ".TOKEN.")</span></td></td>
  </tr>    
  <tr>
   <td><span class='needed'>".NEED_SYMBOL."</span></td>
   <th>".VALIDATE_PASSWORD.":</th>
   <td>".$Cpage->input_password("password_check")."</td></td>
  </tr>    
  <tr>
   <td></td>
   <th></th>
   <td>".$Cpage->input_checkbox("newsletter", "1", $nl_check)." ".SUBSCRIBE_NEWSLETTER."</td>
  </tr>                              
  <tr>
   <td></td>
   <th></th>
   <td>".$Cpage->input_checkbox("agb", "1")." ".I_HAVE_READ_THE_TERMS_PART01." ".$Cpage->link_db("agb", NEW_PAGE)." ".I_HAVE_READ_THE_TERMS_PART02."</td>
  </tr>
  <tr>
   <td></td>
   <th></th>
   <td>".$Cpage->input_checkbox("privacy", "1")." ".I_HAVE_READ_THE_PRIVACY_PART01." ".$Cpage->link_db("privacy", NEW_PAGE)." ".I_HAVE_READ_THE_PRIVACY_PART02."</td>
  </tr>    
  <tr>
   <td></td>
   <th></th>
   <td>".$Cpage->input_submit(REGISTER_NOW)."</td>
  </tr>                              
  <tr>
   <td></td>
   <th></th>
   <td><p class='information'>".NEED_TO_BE_FILLED_PART01." <span class='needed'>".NEED_SYMBOL."</span> ".NEED_TO_BE_FILLED_PART02."</p>
   <p class='information_small'>(".NOTICE_PASSWORD_ENCODED.")</p></td>
  </tr> 
 </table>
</form>
</div>
";
 