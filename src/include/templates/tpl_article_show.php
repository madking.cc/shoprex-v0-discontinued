<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<div id='show_article_wrapper'>
   <p id='article_header' style='".$tpl_article['style']."'>".$tpl_article['name']."</p>
   <div id='article_informations_wrapper'>";

if($tpl_article['has_discount'])
{
    $original_price = $Cpage->tax_calc($tpl_article['netto_original'], "brutto", $Atax[$tpl_article['tax']]);
    $discount_price = $Cpage->tax_calc($tpl_article['netto'], "brutto", $Atax[$tpl_article['tax']]);
    $content .= "<p id='article_discount'><span id='price_text'>".PRICE.":</span> <span id='old_price'>".$Cpage->money($original_price)."</span>
    <span id='new_price'>".$Cpage->money($discount_price)."</span><br />
    <span id='discount_info'>".$tpl_article['discount']."% ".DISCOUNT.", ".SAVE_MONEY." <span id='diff_price'>".$Cpage->money($original_price-$discount_price)."</span></span>";
    if(isset($tpl_article['discount_left'])) $content .= "<br /><span id='discount_info'> ".ONLY_LEFT." ".$tpl_article['discount_left']." </span>";
   $content .= "</p>\n";
}
else
{
    $content .= "<p id='article_price'>".PRICE.": <span id='price'>".$Cpage->money($Cpage->tax_calc($tpl_article['netto'], "brutto", $Atax[$tpl_article['tax']]))."</span></p>\n";
}

if(ENABLE_TAX || (DELIVERY_METHOD != "free"))
{
    $content .= "<p id='article_tax'>";
    if(ENABLE_TAX)
    {
        $content .= INCLUSIVE." ".$Atax[$tpl_article['tax']]."% ".TAX;
    }
    if(DELIVERY_METHOD != "free")
    {
        if(ENABLE_TAX)
        {
            $content .= ", ";
        }
        $content .= $Cpage->link_newPage(WITH_SHIPPING_COST, $Cpage->Aglobal['http']."shipping_cost.php")."\n";
    }
    $content .= "</p>";
}
if(USE_ARTICLE_NUMBER)
{
    $content .= "<p id='article_number'>".ARTICLE_NUMBER.": ".$tpl_article['number']."</p>\n";
}
if(!empty($tpl_article['isbn']))
{
    $content .= "<p id='article_isbn'>".ARTICLE_ISBN.": ".$tpl_article['isbn']."</p>\n";
}

$content .= $Cpage->form("article_form", "cart.php", "push", "return check_submit_article(this);")."\n".$attribute_box;

if($tpl_article['condition'] > 0)
{
    $content .= "<p id='article_condition'>".CONDITION.": ".$Acondition[$tpl_article['condition']]."</p>";
}
if($tpl_article['guarantee'] > 0)
{
    $content .= "<p id='article_guarantee'>".$Aguarantee[$tpl_article['guarantee']]."</p>";
}
$content .= "<div id='article_add_form'>\n";
    if($tpl_article['sold_out'])
        $content .= ARTICLE_IS_SOLD_OUT;
    else
        $content .= $Cpage->input_hidden("id_to_cart", $tpl_article['id'])."\n".$Cpage->input_text("quantity", "1", 40)." x ".$Cpage->input_submit(PUT_IN_CART)."\n";
    $content .= "</form></div>
     <div id='article_aktions_buttons'>\n".$Cpage->form("article_question_form", "question.php")."\n".$Cpage->input_hidden("question_to_id", $tpl_article['id'])."\n".$Cpage->input_submit(ASK_QUESTION)."\n
    </form>\n".$Cpage->print_button("article_print.css", "Artikel drucken", "link_button")."\n
   </div>
  </div>
  <div id='article_image_wrapper' name='picture'>\n<span name='picture_show' id='picture_show'></span>\n
  </div>    
  <div class='clear_float'></div>
  <div id='article_text'>".$tpl_article['text']."</div>";

if(is_array($tpl_article_links))
{
    $content .= "<div id='article_links'>
        <H2>Das könnte Sie vielleicht auch interessieren:</H2>";

    foreach($tpl_article_links AS $key => $value)
    {
        $content .= "<div class='article_link_show'>";
        if(isset($value['picture'][0]))
            $content .= $Cpage->img(UPLOAD_DIR."tiny/".$value['picture'][0], "Artikel Vorschau Bild")." ";
        $content .= $Cpage->link($value['name'], "index.php", "article=".$value['id'])."</div>";
    }
    $content .= "</div>";
}

$content .= "
 </div>\n";

