<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$content .= "<H2 class='content_header'>".CUSTOMER_INFO.":</H2>\n".$Cpage->table("info_table")."
  <tr>
   <td>".CUSTOMER_ID.":</td>
   <td>".$_SESSION['customer']['customer']."</td>
  </tr>
  <tr>
   <td>".CUSTOMER_SINCE.":</td>
   <td>".$Cpage->format_time($_SESSION['customer']['created'])."</td>
  </tr>\n";
if(!empty($_SESSION['customer']['last_login']))
{
    $content .= "
    <tr>
     <td>".LAST_VISIT.":</td>
     <td>".$Cpage->format_time($_SESSION['customer']['last_login'])."</td>
    </tr>\n";
}
$content .= "
 </table>
 <div class='spacer'></div>
 \n";
