<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

for($t = 0; $t < ($tpl_category_count); $t += $tpl_colspan)
{
    // Name:
    $content .= "<div class='category_line1'>\n";
    for($i = $t; ($i < ($t+$tpl_colspan)) && ($i < $tpl_category_count); $i++)
        $content .= " <div class='category_header'>".$Cpage->link($tpl_category[$i]['name'], "index.php", "category=".$tpl_category[$i]['id']."#output", "category_list_header", $tpl_category[$i]['style'])."</div>\n";
    $content .= "</div>\n";

    $content .= "<div class='clear_float'></div>\n";

    // Bild:
    $content .= "<div class='category_line2'>\n";
    for($i = $t; ($i < ($t+$tpl_colspan)) && ($i < $tpl_category_count); $i++)
    {
        $content .= " <div class='category_picture'><div class='category_list_image_wrapper'><div class='image_inner'>".$Cpage->link($Cpage->img(UPLOAD_DIR."small/".$tpl_category[$i]['picture'], $tpl_category[$i]['picture_text'], "category_list_image"), "index.php", "category=".$tpl_category[$i]['id']."#output", "category_list_image_link")."</div></div></div>\n";
    }
    $content .= "</div>\n";

    $content .= "<div class='clear_float'></div>\n";

    $content .= "<div class='category_list_spacer'></div>\n";
}


 