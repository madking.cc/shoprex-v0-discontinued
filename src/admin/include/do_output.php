<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if($show_page)
{
    require $dir_root."admin/include/load_code.php";
    require ($dir_root."admin/layout/header.php");
    require ($dir_root."admin/include/javascript/header.php");
    require ($dir_root."admin/layout/main.php");
    require ($dir_root."admin/layout/footer.php");
}
if(!$show_page) echo $content;

