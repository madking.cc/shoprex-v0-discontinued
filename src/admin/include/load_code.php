<?php

if($load_text_editor)
{
    $script_head .= "<script src='".$Cpage->Apath['tiny_mce']."tinymce.min.js'></script>\n";
    $script .= "
            tinyMCE.init({
                mode : \"exact\",
                elements : \"text\",
                theme: \"modern\",
                plugins: [
                    \"print hr anchor link charmap\",
                    \"searchreplace wordcount visualblocks visualchars code\",
                    \"insertdatetime nonbreaking table contextmenu\",
                    \"emoticons paste\"
                ],
                toolbar1: \"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\",
                toolbar2: \"print preview media | forecolor backcolor emoticons\",

                content_css : \"".$Apath['shop_root'].THEME."css/screen_content.css\",
                entity_encoding : \"raw\",
                convert_urls : false,
                width : \"850\",
                height: \"500\",
                language : \"de\"

                
            });
    

    ";

}
if($load_syntax_highlighter)
{
    $script_head .= "<script src='".$Cpage->Apath['edit_area']."edit_area_full.js'></script>\n";

}

if($load_md5)
{
    $script_head .= "<script src='".$Cpage->Apath['md5']."md5.js'></script>\n";
}

if($load_box)
{
    $script_head .= "
<script src='".$Cpage->Apath['box']."thickbox.js'></script>
";
    $css .= "
      <link rel=\"stylesheet\"
      href=\"".$Cpage->Apath['box']."thickbox.css\" type=\"text/css\" media=\"all\" />\n";


}





if($load_datepicker)
{
    $css .= "
    <style type='text/css'>
    <!--

    /* css for timepicker */
    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; }
    .ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
    -->
    </style>
";
    $script_head .= "<script src='".$Cpage->Apath['md5']."jquery-ui-timepicker-addon.js'></script>\n";
    $script .= "
        jQuery(function($){
            $.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
                    closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
                    prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
                    nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
                    currentText: 'heute', currentStatus: '',
                    monthNames: ['Januar','Februar','März','April','Mai','Juni',
                    'Juli','August','September','Oktober','November','Dezember'],
                    monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
                    'Jul','Aug','Sep','Okt','Nov','Dez'],
                    monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
                    weekHeader: 'Wo', weekStatus: 'Woche des Monats',
                    dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                    dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                    dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
                    dateFormat: 'dd.mm.yy', firstDay: 1,
                    initStatus: 'Wähle ein Datum', isRTL: false};
            $.datepicker.setDefaults($.datepicker.regional['de']);
    });
    $.timepicker.regional['de'] = {
        timeOnlyTitle: 'Uhrzeit auswählen',
        timeText: 'Zeit',
        hourText: 'Stunde',
        minuteText: 'Minute',
        secondText: 'Sekunde',
        currentText: 'Jetzt',
        closeText: 'Auswählen',
        ampm: false
    };
    $.timepicker.setDefaults($.timepicker.regional['de']);
    ";
}

if($load_datepicker || $load_box)
{
    $script_head = " <script src='http://code.jquery.com/jquery-1.9.1.js'></script>
    <script src='http://code.jquery.com/ui/1.10.2/jquery-ui.js'></script>\n".$script_head;
    $css .= "<link rel='stylesheet' href='http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css' />\n";
}
