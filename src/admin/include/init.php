<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Einstellungen für Fehlerbericht, Logs und Cache
$do_debug  = FALSE; // Bitte im Produktiv Einsatz auf FALSE lassen!
$do_log    = FALSE;
$log_level = 1; // 0: Standard Log
date_default_timezone_set('Europe/Berlin');
// 1: Alle POST und GET Abfragen werden im Klartext gespeichert.
// 2: Datenbankabfragen werden im Klartext gespeichert.
$log_errors = TRUE;
define('STORE_EVERY_VISIT', 1); // 0:nein 1:ja

if($do_debug)
{
    error_reporting(E_ALL);
    ini_set('track_errors', '1');
    define('DO_DEBUG', 1);
}
else
{
    error_reporting(0);
    ini_set('display_errors', '0');
    define('DO_DEBUG', 0);
}

// Testen, ob Browser Cookies annimmt.
// Wichtig für die Session ID
$cookie_enabled = FALSE;
if(isset($_COOKIE['check_cookie']))
{
    $cookie_enabled = TRUE;
}

if(!$cookie_enabled)
{
    setcookie("check_cookie", "cookies_enabled");
}

// Starte Session
session_start();

// Lade die Konfiguration (z.B. Datenbank Einstellungen)
require $dir_root."/_configuration.php";

// Lade Klassen
require $dir_root."classes/error.php";
require $dir_root."classes/log.php";
require $dir_root."classes/db.php";
require $dir_root."classes/visitor.php";
require $dir_root."classes/cart.php";
require $dir_root."classes/page.php";
require $dir_root."classes/articles.php";
require $dir_root."classes/categories.php";
require $dir_root."classes/phpmailer.php";

// Starte die Singleton Fehler Klasse
$Cerror = error::singleton(dirname(__FILE__)."/../../");

// Starte die Singleton Log Klasse
$Clog = log::singleton($do_log, $log_level, $log_errors);

// Stelle die Datenbankverbindung her:
$Cdb = new db(DB_SERVER, DB_USER, DB_PASS, DB_TABLE, TBL_PREFIX);

// Erstelle die Mail Klasse
$Cmail = new PHPMailer();

// Erstelle die Seitenklasse: page
define("SUBMIT_CHANGE", "changesDone();");
$Cpage = new page($Cdb, $cookie_enabled, $Cmail);

// Erstelle die Artikelklasse
$Carticles = new articles($Cdb, $Cpage);

// Erstelle die Kategorienklasse
//$Ccategories = new categories($Cdb);

// Einstellungen aus der Datenbank laden
require $dir_root."admin/include/settings.php";

// Sprache aus der Datenbank laden
require $dir_root."admin/include/language.php";

// Setze die Pfade
require $dir_root."admin/include/path.php";

// Setze Variablen
require $dir_root."admin/include/variables.php";

// Setzte Globale Variablen (auch in Klassen)
require $dir_root."admin/include/globals.php";

// Erzeuge Besucher Statistik
$Cvisitor = new visitor($Cdb);
$Cvisitor->store_visit("adm_visitors");

// Erstelle die Warenkorbklasse
//$Ccart = new cart($Cpage, $Cdb, $Carticles);


