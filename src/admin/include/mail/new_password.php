<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Acustomer['time']      = $Cpage->Aglobal['now'];
$Acustomer['mail']      = $mail;
$Acustomer['homepage']  = $Cpage->Aglobal['www'];
$Acustomer['firstname'] = $firstname;
$Acustomer['lastname']  = $lastname;

$mail_field = $Cpage->get_mail_draft("new_password");

foreach($Acustomer as $key => $value)
{
    $mail_field['text']    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['text']);
    $mail_field['subject'] = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $mail_field['subject']);
}

$Cpage->send_mail(SHOP_MAIL, $mail_field['text'], $mail_field['subject'], $mail);