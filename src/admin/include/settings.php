<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Hole die Settings aus der adm Datenbank:
$sql    = " SELECT `name`,`value` FROM `".TBL_PREFIX."adm_settings`; ";
$result = $Cdb->db_query($sql, "load_settings.php");

if($result->num_rows > 0)
{
    while($settings_adm_field = $result->fetch_assoc())
    {
        $_settings[$settings_adm_field['name']] = $settings_adm_field['value'];
    }
}
else $_settings = NULL;

$result->close();

foreach($_settings as $setting_name => $setting_value)
    define($setting_name, $setting_value);

// Hole die Settings aus der shop Datenbank:
$sql    = " SELECT `name`,`value` FROM `".TBL_PREFIX."settings`; ";
$result = $Cdb->db_query($sql, "load_settings.php");

if($result->num_rows > 0)
{
    while($settings_field = $result->fetch_assoc())
    {
        $_settings[$settings_field['name']] = $settings_field['value'];
    }
}
else $_settings = NULL;

$result->close();

foreach($_settings as $setting_name => $setting_value)
    if(!defined($setting_name))
    {
        define($setting_name, $setting_value);
    }

// Erzeuge Arrays:
$defines = array('TAX');
foreach($defines as $value)
{
    if(defined("ARRAY_".$value))
    {
        ${"A".strtolower($value)} = unserialize(constant("ARRAY_".$value));
    }
}

