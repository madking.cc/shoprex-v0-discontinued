<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Sprache laden: (load_language.php)
$sql    = " SELECT * FROM `".TBL_PREFIX."language_".LANGUAGE."`; ";
$result = $Cdb->db_query($sql, "load_language.php");

if($result->num_rows > 0)
{
    while($language_field = $result->fetch_assoc())
    {
        $_language[$language_field['name']] = $language_field['value'];
    }
}
else $_language = NULL;

foreach($_language as $language_name => $language_value)
    if(!defined($language_name))
    {
        define($language_name, $language_value);
    }

$defines = array('ANSWER');
foreach($defines as $value)
{
    for($i = 0; TRUE; $i++)
        if(defined($value."_".$i))
        {
            ${"A".strtolower($value)}[$i] = constant($value."_".$i);
        }
        else break;
}

unset($defines);
$defines = array('CONDITION', 'GUARANTEE', 'CUSTOMER_TYPE', 'CUSTOMER_TYPES', 'STATUS', 'DELIVERY');
foreach($defines as $value)
{
    if(defined("ARRAY_".$value))
    {
        ${"A".strtolower($value)} = unserialize(constant("ARRAY_".$value));
    }
}