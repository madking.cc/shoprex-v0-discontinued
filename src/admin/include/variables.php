<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(!isset($show_menu))
{
    $show_menu = TRUE;
}
if(!isset($load_shop_default_style))
{
    $load_shop_default_style = FALSE;
}
if(!isset($_SESSION['category']))
{
    $_SESSION['category'] = "";
}
if(!isset($NO_LAYOUT))
{
    $NO_LAYOUT = FALSE;
}
if(!isset($load_text_editor))
{
    $load_text_editor = FALSE;
}
if(!isset($load_syntax_highlighter))
{
    $load_syntax_highlighter = FALSE;
}
if(!isset($load_md5))
{
    $load_md5 = FALSE;
}
if(!isset($load_datepicker))
{
    $load_datepicker = FALSE;
}
if(!isset($load_box))
{
    $load_box = FALSE;
}


if(!isset($css)) $css = "";
if(!isset($script_head)) $script_head="";
if(!isset($script)) $script="";
if(!isset($script_footer)) $script_footer="";
if(!isset($show_page)) $show_page=TRUE;
