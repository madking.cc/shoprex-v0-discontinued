<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */



$uploaddir = $Cpage->get_parameter("uploaddir", NULL);

if(empty($uploaddir))
{
    echo "Upload-Verzeichnis nicht angegeben";
    die();
}

if(isset($_POST['do_action']))
    $action = $Cpage->get_parameter('do_action');
else
    $action = "init";

switch($action)
{
    case "init":
        $content .= init();
        break;
    case "upload":
        $content .= upload();
        break;
    case "finished":
        $content .= finished();
        break;
}

function init()
{
    global $Cpage;
    global $uploaddir;
    $content = "";

$content .= $Cpage->form_file().$Cpage->input_hidden("uploaddir", $uploaddir).$Cpage->input_hidden("action", "upload")."<fieldset style=\"width:550px; margin-left: 25px;\"><legend>Picture upload</legend>
    ".$Cpage->table("", "", "0", "0", "style=\"width:450px;\"")."
    <tr>
        <td>Wählen Sie die Datei:
        </td>
        <td>".$Cpage->input_file("file_to_upload", "300")."
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">Maximale Datei Größe: ".$Cpage->return_max_upload()." Megabyte
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>".$Cpage->input_submit("Hochladen")."
        </td>
    </tr>
    </table>
    </form>";

    return $content;
}

function upload()
{
    global $uploaddir;
    global $Cpage;
    global $script_footer;
    global $dir_root;
    global $Apath;
    $content = "";

    // Überprüfe, ob das Bild vom Formular richtig übertragen wurde:
    if(isset($_FILES['file_to_upload']) && !empty($_FILES['file_to_upload']['name']))
    {
        // Prüfe, ob Bild:
        $picture_type = str_replace('image/', '', $_FILES['file_to_upload']['type']);
        if(!(($picture_type == "jpeg") || ($picture_type == "gif") || ($picture_type == "png")))
        {
            // Kein Bild:
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Es werden nur Bilder der Formate \".jpg\", \".gif\" und \".png\" als Upload akzeptiert! (\".bmp\" Bilder sind ebenfalls ausgeschlossen.)</p>
                    ";
            $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></form>";
            $content .= "</fieldset>";
            return $content;
        }
        else
        {
            // Bestimme die Formatierung des Dateinamens:
            $file_parts = pathinfo($_FILES['file_to_upload']['name']);
            $file_name  = $file_parts['basename'];
            $last_dot   = strrpos($file_name, ".");
            $file_name  = substr($file_name, 0, $last_dot);
            $file_type  = $file_parts['extension'];

            $file_name_original = $Cpage->check_filename($file_name, $file_type, $dir_root.$uploaddir);

            // Bestimme Zieldatei mit Pfad:
            $upload_file_destination        = $dir_root.$uploaddir.$file_name_original;

            // Die Original Datei hochladen:
            if(move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $upload_file_destination))
            {
                $picture = $file_name_original;
                list($image_width, $image_height, $image_type, $image_attr) = getimagesize($upload_file_destination);
            }
            else
            {
                $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Es gab ein Problem beim Bild Upload, evtl. sind keine Schreibrechte vorhanden.</p>
                ";
                $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></form>";
                $content .= "</fieldset>";
                return $content;
            }

        }

    }
    else
    {
        $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Die Datei wurde nicht korrekt vom Formular übertragen.</p>
                    ";
        $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></form>";
        $content .= "</fieldset>";
        return $content;
    }

    $script_footer .= "
        var xSizeOriginal = $image_width;
        var ySizeOriginal = $image_height;
        var imgPreview = document.getElementById('imgPreview');

        function changeX(formID)
        {
                if(formID.proportional.checked)
                {
                    var ratio = xSizeOriginal / ySizeOriginal;
                    var yNewSize    = Math.round(formID.x_size.value / ratio);
                    formID.y_size.value = yNewSize;
                    imgPreview.style.height = yNewSize + 'px';
                }
                imgPreview.style.width = formID.x_size.value + 'px';
        }

        function changeY(formID)
        {
                if(formID.proportional.checked)
                {
                    var ratio = ySizeOriginal / xSizeOriginal;
                    var xNewSize    = Math.round(formID.y_size.value / ratio);
                    formID.x_size.value = xNewSize;
                    imgPreview.style.width = xNewSize + 'px';
                }
               imgPreview.style.height = formID.y_size.value + 'px';
        }
    ";

    $content .=
        $Cpage->form().$Cpage->input_hidden("do_action", "finished").$Cpage->input_hidden("uploaddir", $uploaddir).$Cpage->input_hidden("picture", $picture).
    "<fieldset style=\"width:550px; margin-left: 25px;\"><legend>Bild bearbeiten</legend>
    ".$Cpage->table("", "", "0", "0", "style=\"width:500px;\"")."
    <tr>
        <td>".$Cpage->select("type_of_using")."<option value=\"0\">Normales Bild</option><option value=\"1\">Vorschaubild</option></select>
        </td>
        <td>Größe:<br />".$Cpage->input_text("x_size", $image_width, "50", NOT_READONLY, "onkeyup=\"changeX(this.form)\"")." X ".
            $Cpage->input_text("y_size", $image_height, "50", NOT_READONLY, "onkeyup=\"changeY(this.form)\"")." Y Pixel<br />
            ".$Cpage->input_checkbox("proportional", "1", TRUE)." Proportional?
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">Alternativer Text: ".$Cpage->input_text("alt", "", "400")."
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>".$Cpage->input_submit("Bild in Text einfügen")."
        </td>
    </tr>
    </table>
    </form><br />
    <img src=\"".$Apath['shop_root'].$uploaddir.$picture."\" id=\"imgPreview\">";

    return $content;
}

function finished()
{
    global $Cpage;
    global $Apath;
    global $script_footer;
    global $uploaddir;
    global $dir_root;
    $content = "";

    $picture = $Cpage->get_parameter('picture');
    $new_x_size = $Cpage->get_parameter('x_size');
    $new_y_size = $Cpage->get_parameter('y_size');
    $type_of_using = $Cpage->get_parameter('type_of_using');
    $alt = $Cpage->get_parameter('alt');

    if($type_of_using == 1)
        $picture_save = "preview_".$picture;
    else
        $picture_save = $picture;



    $Cpage->load_image($dir_root.$uploaddir.$picture);
    $picture_type = $Cpage->get_image_type();
    if($new_x_size != $Cpage->get_image_width() || $new_y_size != $Cpage->get_image_height())
    {
        if(($picture_type == "jpeg") || ($picture_type == "jpg"))
        {
            $image = imagecreatefromjpeg($dir_root.$uploaddir.$picture);
        }
        elseif($picture_type == "gif")
        {
            $image = imagecreatefromgif($dir_root.$uploaddir.$picture);
        }
        elseif($picture_type == "png")
        {
            $image = imagecreatefrompng($dir_root.$uploaddir.$picture);
        }
        $image_b = $Cpage->get_image_width();
        $image_h = $Cpage->get_image_height();

        if($image_b > $image_h)
        {
            $ratio = $image_b/$image_h;

            $new_b    = $new_x_size;
            $new_h    = round($new_b/$ratio);
        }
        else
        {
            $ratio = $image_h/$image_b;

            $new_h    = $new_y_size;
            $new_b    = round($new_h/$ratio);
        }

        if(($picture_type == "jpg") || ($picture_type == "jpeg") || ($picture_type == "png"))
        {
            $image_new = imagecreatetruecolor($new_b, $new_h);
        }
        if($picture_type == "gif")
        {
            $image_new = imagecreate($new_b, $new_h);
        }
        if(!$image_new)
        {
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Beim Erzeugen des Platzhalters für das neue Bild ist ein Fehler aufgetreten.</p>
                        ";
            $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back(2);")."</p></form>";
            $content .= "</fieldset>";
            return $content;
        }

        $noerror_new    = imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_b, $new_h, $image_b, $image_h);
        if(!$noerror_new)
        {
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Beim kopieren es Bildes in das neue Größenformat ist ein Fehler aufgetreten.</p>
                    ";
            $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back(2);")."</p></form>";
            $content .= "</fieldset>";
            return $content;
        }

        if(($picture_type == "jpeg") || ($picture_type == "jpg"))
        {
            $noerror_new    = imagejpeg($image_new, $dir_root.$uploaddir.$picture_save, 100);
        }
        elseif($picture_type == "gif")
        {
            $noerror_new    = imagegif($image_new, $dir_root.$uploaddir.$picture_save);
        }

        elseif($picture_type == "png")
        {
            $noerror_new    = imagepng($image_new, $dir_root.$uploaddir.$picture_save);
        }
        if(!$noerror_new)
        {
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend><p class='top'>Beim speichern des neuen Bildes ist ein Fehler aufgetreten.</p>
                    ";
            $content .= $Cpage->form()."<p class='bottom'>".$Cpage->input_button("Zurück", "history.back(2);")."</p></form>";
            $content .= "</fieldset>";
            return $content;
        }

        imagedestroy($image);
    }

    if($type_of_using == 0)
    {
        $text = "<img src=\\\"".$Apath['shop_root'].$uploaddir.$picture."\\\" border=\\\"0\\\" alt=\\\"".$alt."\\\">";
        $script_footer .= "
            window.parent.tinyMCE.activeEditor.selection.setContent(\"".$text."\");
            self.parent.tb_remove();
        ";
    }
    else
    {
        $text = "<a href=\\\"".$Apath['shop_root'].$uploaddir.$picture."\\\" target=\\\"_blank\\\"><img src=\\\"".$Apath['shop_root'].$uploaddir."preview_".$picture."\\\" border=\\\"0\\\" alt=\\\"".$alt."\\\"></a>";
        $script_footer .= "
            window.parent.tinyMCE.activeEditor.selection.setContent(\"".$text."\");
            self.parent.tb_remove();
        ";
    }

}