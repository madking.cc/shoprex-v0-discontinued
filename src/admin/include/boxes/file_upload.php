<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$uploaddir = $Cpage->get_parameter("uploaddir", NULL);

if(empty($uploaddir))
{
    echo "Upload-Verzeichnis nicht angegeben";
    die();
}

if(isset($_POST['do_action']))
    $action = $Cpage->get_parameter('do_action');
else
    $action = "init";

switch($action)
{
    case "init":
        $content .= init();
        break;
    case "upload":
        $content .= upload();
        break;
    case "finished":
        finished();
        break;
}


function init()
{
    global $Cpage;
    global $uploaddir;


    $content = "";

    $content .= $Cpage->form_file().$Cpage->input_hidden("uploaddir", $uploaddir).$Cpage->input_hidden("do_action", "upload")."
    <fieldset style=\"width:550px; margin-left: 25px;\"><legend>Datei Upload</legend>
    ".$Cpage->table("style=\"width:550px;\"")."
    <tr>
        <td>Wählen Sie die Datei:
        </td>
        <td>".$Cpage->input_file("file_to_upload", "300")."
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">Maximale Datei Größe: ".$Cpage->return_max_upload()." Megabyte
        </td>
    </tr>


    <tr>
        <td>
        </td>
        <td>".$Cpage->input_submit("Hochladen")."
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td><span class='info'>Es sind keine Ausführbaren und Script Dateien erlaubt.</td>
    </tr>
    </table>
    </form>";

    return $content;
}

function upload()
{
    global $dir_root;
    global $Cpage;
    global $uploaddir;
    $content = "";
    $not_allowed_filetypes = array ("php", "exe", "com", "bat", "cgi", "cmd", "scr", "pif", "hta", "scf", "sh", "msi", "html", "htm",
    "js", "mde", "nws", "reg", "url", "ws.bas", "bas", "cpl", "folder", "inf", "jse", "msc", "scf", "vb", "shs");

    // Überprüfe, ob das Bild vom Formular richtig übertragen wurde:
    if(isset($_FILES['file_to_upload']) && !empty($_FILES['file_to_upload']['name']))
    {
            // Bestimme die Formatierung des Dateinamens:
            $file_parts = pathinfo($_FILES['file_to_upload']['name']);
            $file_name  = $file_parts['basename'];
            $last_dot   = strrpos($file_name, ".");
            $file_name  = substr($file_name, 0, $last_dot);
            $file_type  = $file_parts['extension'];

            if(in_array($file_type, $not_allowed_filetypes))
            {
                $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Es gab ein Problem beim Upload, dieser Dateityp ist nicht erlaubt: ".$file_type.".<br/>
                </p><p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></fieldset>";
                return $content;
            }

            $file_name_original = $Cpage->check_filename($file_name, $file_type, $dir_root.$uploaddir);

            // Bestimme Zieldatei mit Pfad:
            $upload_file_destination        = $dir_root.$uploaddir.$file_name_original;

            // Die Original Datei hochladen:
            if(move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $upload_file_destination))
            {
                $file = $file_name_original;
            }
            else
            {
                $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Es gab ein Problem beim Upload, evtl. sind keine Schreibrechte vorhanden, oder die Datei ist zu groß.<br/>
                Quellpfad: ".$_FILES['file_to_upload']['tmp_name']."<br />
                Zielpfad: ".$upload_file_destination."</p><p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></fieldset>";

                return $content;
            }
    }
    else
    {
        $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Die Datei wurde nicht korrekt vom Formular übertragen.</p>
                <p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></fieldset>";
        return $content;

    }

    $content .=
        $Cpage->form().$Cpage->input_hidden("uploaddir", $uploaddir).$Cpage->input_hidden("do_action", "finished").
            "<fieldset style=\"width:550px; margin-left: 25px;\"><legend>Datei bearbeiten</legend>
    ".$Cpage->table("style=\"width:550px;\"")."
    <tr>
        <td>Dateiname:
        </td>
        <td>".$Cpage->input_text("file", $file, "300", "readonly=\"readonly\"")."
        </td>
    </tr>
    <tr>
        <td>Link Text:
        </td>
        <td>".$Cpage->input_text("link_text", $file, "300")."
        </td>
    </tr>
    <tr>
        <td>Art des Öffnens:
        </td>
        <td>".$Cpage->select("target")."<option value=\"0\">Link im selben Fenster öffnen</option><option value=\"1\">Link in neuem Fenster öffnen</option></input>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>".$Cpage->input_submit("Dateilink in Text einfügen")."
        </td>
    </tr>
    </table>
    </form><br />";

    return $content;
}

function finished()
{
    global $uploaddir;
    global $script_footer;
    global $Apath;
    global $Cpage;

    $file = $Cpage->get_parameter('file');
    $target = $Cpage->get_parameter('target');
    $link_text = $Cpage->get_parameter('link_text');

    if($target == 0)
    {
        $text = "<a href=\\\"".$Apath['shop_root'].$uploaddir.$file."\\\">".$link_text."</a>";
        $script_footer .= "
            window.parent.tinyMCE.activeEditor.selection.setContent(\"".$text."\");
            self.parent.tb_remove();
        ";
    }
    else
    {
        $text = "<a href=\\\"".$Apath['shop_root'].$uploaddir.$file."\\\" target=\\\"_blank\\\">".$link_text."</a>";
        $script_footer .= "
            window.parent.tinyMCE.activeEditor.selection.setContent(\"".$text."\");
            self.parent.tb_remove();
        ";
    }

}