<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */


if(isset($_POST['do_action']))
    $action = $Cpage->get_parameter('do_action');
else
    $action = "init";

switch($action)
{
    case "init":
        $content .= init();
        break;
    case "finished":
        finished();
        break;
}


function init()
{
    global $Cpage;
    global $Cdb;

    $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `clone_from` =0 AND `status` =1 ORDER BY name;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);


    $content = "";

    $content .= $Cpage->form().$Cpage->input_hidden("do_action", "finished")."
    <fieldset style=\"width:550px; margin-left: 25px;\"><legend>Artikel einfügen</legend>
    ".$Cpage->table("style=\"width:550px;\"")."
    <tr>
        <td>Wählen Sie den Artikel:
        </td>
        <td>".$Cpage->select("id")."\n";

        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $content .= "<option value='".$row['id']."'>".$row['name'];
                if(!empty($row['number'])) $content .= " - ".$row['number'];
                $content .= " - ".$Cpage->money($Cpage->tax_calc($row['netto'], "brutto", $Cpage->Aglobal['tax'][$row['tax']]))."</option>\n";
            }
        }
        else $content .= "<option value='0'>Keine Artikel vorhanden</option>\n";

        $content .= "</select>
        </td>
    </tr>\n";
    if($result->num_rows > 0)
        $content .= "
    <tr>
        <td>
        </td>
        <td>".$Cpage->input_submit("Einfügen")."
        </td>
    </tr>";
    $content .= "
    </table>
    </form>";

    return $content;
}


function finished()
{
    global $script_footer;
    global $Apath;
    global $Cpage;
    global $Cdb;

    $id = $Cpage->get_parameter("id");

    $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$id;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $row = $result->fetch_assoc();

        $text = "<a href=\\\"".$Apath['shop_root']."index.php?article=".$id."\\\">".$row['name']."</a>";
        $script_footer .= "
            window.parent.tinyMCE.activeEditor.selection.setContent(\"".$text."\");
            self.parent.tb_remove();
        ";


}