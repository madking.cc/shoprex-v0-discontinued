<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Setzte den Content leer
$content = "";

if(!isset($_SESSION['login']))
{
    $page = "login";
}

switch($page)
{
    case "email_attachement";
        $subtitle = " - Email Anhänge";
        require($dir_root."admin/include/modules/mod_email_attachement.php");
        break;

    case "test";
        $subtitle = " - Test";
        require($dir_root."admin/include/modules/mod_test.php");
        break;

    case "picture_upload";
        $subtitle = " - Bild Upload";
        require($dir_root."admin/include/boxes/picture_upload.php");
        break;

    case "file_upload";
        $subtitle = " - Datei Upload";
        require($dir_root."admin/include/boxes/file_upload.php");
        break;

    case "article_add";
        $subtitle = " - Artikel hinzufügen";
        require($dir_root."admin/include/boxes/article_add.php");
        break;

    case "login":
        $subtitle    = " - Login";
        $nav_control = "body_login";
        $show_menu   = FALSE;
        require ($dir_root."admin/include/modules/mod_login.php");
        break;

    case "logout":
        $subtitle    = " - Logout";
        $nav_control = "body_logout";
        require ($dir_root."admin/include/modules/mod_logout.php");
        break;

    case "newsletter":
        $subtitle    = " - Newsletter";
        $nav_control = "body_menu_newsletter body_customers";
        require ($dir_root."admin/include/modules/mod_newsletter.php");
        break;

    case "newsletter_show":
        $subtitle    = " - Newslettervorschau";
        $nav_control = "body_menu_newsletter body_customers";
        require ($dir_root."admin/include/modules/mod_newsletter_show.php");
        break;

    case "delivery":
        $subtitle    = " - Versandoptionen";
        $nav_control = "body_menu_settings body_delivery";
        require ($dir_root."admin/include/modules/mod_delivery.php");
        break;

    case "stock":
        $subtitle    = " - Warenbestand";
        $nav_control = "body_menu_articles body_stock";
        require ($dir_root."admin/include/modules/mod_stock.php");
        break;

    case "correspondence":
        $subtitle    = " - Schriftverkehr";
        $nav_control = "body_menu_orders body_correspondence";
        require ($dir_root."admin/include/modules/mod_correspondence.php");
        break;

    case "correspondence_edit":
        $subtitle    = " - Schriftstück";
        $nav_control = "body_menu_orders body_correspondence";
        require ($dir_root."admin/include/modules/mod_correspondence_edit.php");
        break;

    case "correspondence_cart":
        $subtitle    = " - Warenkorb editieren";
        $nav_control = "body_menu_orders body_correspondence";
        require ($dir_root."admin/include/modules/mod_correspondence_cart.php");
        break;

    case "print_invoice":
        $subtitle    = " - Rechnung drucken";
        $nav_control = "";
        require ($dir_root."admin/include/modules/mod_print_invoice.php");
        break;

    case "print_parcel":
        $subtitle    = " - Paketschein drucken";
        $nav_control = "";
        require ($dir_root."admin/include/modules/mod_print_parcel.php");
        break;

    case "picture":
        $subtitle    = " - Bilder Upload";
        $nav_control = "body_menu_articles body_single_article";
        require ($dir_root."admin/include/modules/mod_picture.php");
        break;

    case "articles":
        $subtitle    = " - Artikel und Kategorien";
        $nav_control = "body_menu_articles body_articles";
        require ($dir_root."admin/include/modules/mod_articles.php");
        break;

    case "picture_show":
        $subtitle    = " - Bild Vorschau";
        $nav_control = "body_menu_articles body_single_article";
        require ($dir_root."admin/include/modules/mod_picture_show.php");
        break;

    case "customers":
        $subtitle    = " - Kundenverwaltung";
        $nav_control = "body_customers";
        require ($dir_root."admin/include/modules/mod_customers.php");
        break;

    case "resize_pictures":
        $subtitle    = " - Bildgröße anpassen";
        $nav_control = "body_settings body_resize_pictures";
        require ($dir_root."admin/include/modules/mod_resize_pictures.php");
        break;

    case "templates":
        $subtitle    = " - Vorlagen";
        $nav_control = "body_settings body_templates";
        require ($dir_root."admin/include/modules/mod_templates.php");
        break;

    case "settings":
        $subtitle    = " - Einstellungen";
        $nav_control = "body_settings body_setup";
        require ($dir_root."admin/include/modules/mod_settings.php");
        break;

    case "addresses":
        $subtitle    = " - Adressen und Daten";
        $nav_control = "body_settings body_addresses";
        require ($dir_root."admin/include/modules/mod_addresses.php");
        break;

    case "language":
        $subtitle    = " - Sprache";
        $nav_control = "body_settings body_language";
        require ($dir_root."admin/include/modules/mod_language.php");
        break;

    case "pages":
        $subtitle    = " - Weitere Seiten";
        $nav_control = "body_settings body_pages";
        require ($dir_root."admin/include/modules/mod_pages.php");
        break;

    case "coupons":
        $subtitle    = " - Gutscheine";
        $nav_control = "body_coupons";
        require ($dir_root."admin/include/modules/mod_coupons.php");
        break;

    case "index":
        $subtitle    = " - Status";
        $nav_control = "body_home";
        require ($dir_root."admin/include/modules/mod_status.php");
        break;

    default:

        $subtitle    = " - Status";
        $nav_control = "body_home";
        require ($dir_root."admin/include/modules/mod_status.php");

        break;
}

