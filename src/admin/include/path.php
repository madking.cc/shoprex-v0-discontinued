<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$uri                   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$working_path          = getcwd();
$Apath                 = array();
$Apath['working_path'] = $working_path;

$Apath['root']       = $uri."/";
$Apath['include']    = $uri."/include/";
$Apath['javascript'] = $working_path."/include/javascript/";
$Apath['modules']    = $uri."/include/modules/";
$Apath['mail']       = $working_path."/include/mail/";
$Apath['download']   = $uri."/download/";
$Apath['log']        = $uri."/log/";
$Apath['upload']     = $uri."/upload/";
$Apath['layout']     = $uri."/layout/";
$Apath['edit_area']  = $uri."/include/external/edit_area/";
$Apath['tiny_mce']   = $uri."/include/external/tinymce/";
$Apath['box']        = $uri."/include/external/thickbox/";
$Apath['md5']        = $uri."/include/javascript/";

$shop = str_replace("admin", "", $uri);
$shop_dir = str_replace("admin", "", $working_path);
$Apath['shop_root']             = $shop;
$Apath['shop_include']          = $shop_dir."/include/";
$Apath['shop_api']              = $shop_dir."/include/api/";
$Apath['shop_javascript']       = $shop_dir."/include/javascript/";
$Apath['shop_modules']          = $shop_dir."/include/modules/";
$Apath['shop_mail']             = $shop_dir."/include/mail/";
$Apath['shop_download']         = $shop."download/";
$Apath['shop_log']              = $shop."log/";
$Apath['shop_themes']           = $shop."themes/";
$Apath['shop_upload']['root']   = $shop."upload/";
$Apath['shop_upload']['big']    = $shop."upload/big/";
$Apath['shop_upload']['medium'] = $shop."upload/medium/";
$Apath['shop_upload']['small']  = $shop."upload/small/";
$Apath['shop_upload']['tiny']   = $shop."upload/tiny/";
$Apath['shop_templates']        = $shop.THEME."templates/";

$Awp = array();

$Awp['shop_pictures']['big']    = $shop_dir."upload/big/";
$Awp['shop_pictures']['medium'] = $shop_dir."upload/medium/";
$Awp['shop_pictures']['small']  = $shop_dir."upload/small/";
$Awp['shop_pictures']['tiny']   = $shop_dir."upload/tiny/";
