<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

?>
<script>
    /* <![CDATA[ */
    function StartPage()
    {
        document.reset_form.submit();
    }
    var didChanges=false;

    function changesDone()
    {
        didChanges=true;
    }

    function gotoPage(link)
    {
        changePage=false;
        if(didChanges)
        {
            check=confirm('Wollen Sie die Änderungen wirklich verwerfen?\n\nOK = Ja\nAbbruch = Zurück zur Eingabe');
            if(check == true)
            {
                changePage=true;
            }
            else
            {
                // Do Nothing
            }
        }
        else
        {
            changePage=true;
        }
        if(changePage)
        {
            if(link == "index.php")
            {
                document.reset_form.submit();
            }
            else
            {
                window.location.href=link;
            }
        }
    }
    /* ]]> */
</script>

<?php
echo $Cpage->form("reset_form", "index.php");
echo "</form>";
