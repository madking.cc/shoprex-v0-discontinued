<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "store_addresses":
        $Aitem = $Cpage->get_parameter("item", "", NO_ESCAPE);

        if(!empty($Aitem["settings"]["adm_password"]))
        {
            $Aitem["settings"]["adm_password"] = $Cpage->salt_password($Aitem["settings"]["adm_password"]);
            $password                          = $Aitem["settings"]["adm_password"];
            $sql                               = "UPDATE `".TBL_PREFIX."adm_access` SET `password` = ?, `changed` =NOW() WHERE `id` =1;";
            $stmt                              = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('s', $password);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
        if(!empty($Aitem["settings"]["adm_access"]))
        {
            $name = $Aitem["settings"]["adm_access"];
            $sql  = "UPDATE `".TBL_PREFIX."adm_access` SET `name` = ? WHERE `id` =1;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('s', $name);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }

        foreach($Aitem['language'] as $key => $value)
        {
            $sql  = "UPDATE `".TBL_PREFIX."language_".LANGUAGE."` SET `value` = ? WHERE `name` =?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('ss', $value, $key);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
        foreach($Aitem['settings'] as $key => $value)
        {
            $sql  = "UPDATE `".TBL_PREFIX."settings` SET `value` = ? WHERE `name` =?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('ss', $value, $key);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }

    default:
        $content .= show_addresses();
        break;
}

function show_addresses()
{
    global $Cpage;
    global $Cdb;
    global $script;

    global $load_md5;
    $content = "";

    $load_md5 = TRUE;

    $sql    = "SELECT * FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE (`type` like 'address');";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Aitem = $result->fetch_assoc())
    {
        $Aaddresses['language'][$Aitem['name']] = $Aitem['value'];
    }

    $sql    = "SELECT * FROM `".TBL_PREFIX."settings` WHERE (`type` like 'address');";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Aitem = $result->fetch_assoc())
    {
        $Aaddresses['settings'][$Aitem['name']] = $Aitem['value'];
    }

    $sql    = "SELECT `name` FROM `".TBL_PREFIX."adm_access` WHERE (`id` =1);";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Aitem = $result->fetch_assoc())
    {
        $Aaddresses['adm_access'] = $Aitem['name'];
    }

    $script .= "
                     function checkSubmit(formID)
                     {
                        	if(formID.elements[\"item[settings][adm_password]\"].value != '')
                            {
                               if(formID.elements[\"item[settings][adm_password]\"].value != formID.elements[\"item[settings][adm_password_check]\"].value)
                               {
                                    alert('Die Passwörter stimmen nicht überein');
                                    return false;
                               }
                               encoded_new_password = MD5(formID.elements[\"item[settings][adm_password]\"].value);
                               formID.elements[\"item[settings][adm_password]\"].value = encoded_new_password;
                               formID.elements[\"item[settings][adm_password_check]\"].value = encoded_new_password;
                            }
                            return true;
                     }
                    ";

    $content .= $Cpage->form("form_addresses", "addresses.php", "store_addresses", "return checkSubmit(this);")."
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>E-Mail Adressen</div></div><div class='item_content'>
	".$Cpage->table()."<tr><td align='right'>E-Mail Adresse des Shops:</td><td>".$Cpage->input_text("item[language][SHOP_MAIL]", $Aaddresses['language']['SHOP_MAIL'])."</td></tr>
	 <tr><td align='right'>E-Mail Adresse, welche die Fragen beantworten soll:</td><td>".$Cpage->input_text("item[language][MAIL_QUESTIONS]", $Aaddresses['language']['MAIL_QUESTIONS'])."</td></tr>";
    //$content .= "<tr><td align='right'>E-Mail Adresse, die alle E-Mails als Kopie bekommt:</td><td>".$Cpage->input_text("item[language][MAIL_BCC]", $Aaddresses['language']['MAIL_BCC'])."</td></tr>";
    $content .= "</table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>Shop Adresse</div></div><div class='item_content'>
	".$Cpage->table()."<tr><td align='right'>Name:</td><td>".$Cpage->input_text("item[settings][SHOP_NAME]", $Aaddresses['settings']['SHOP_NAME'])."</td></tr>
	 <tr><td align='right'>Straße:</td><td>".$Cpage->input_text("item[settings][SHOP_STREET]", $Aaddresses['settings']['SHOP_STREET'])."</td></tr>
	 <tr><td align='right'>Postleitzahl:</td><td>".$Cpage->input_text("item[settings][SHOP_ZIP]", $Aaddresses['settings']['SHOP_ZIP'])."</td></tr>
	 <tr><td align='right'>Stadt:</td><td>".$Cpage->input_text("item[settings][SHOP_CITY]", $Aaddresses['settings']['SHOP_CITY'])."</td></tr>
	 <tr><td align='right'>Telefon:</td><td>".$Cpage->input_text("item[settings][SHOP_PHONE]", $Aaddresses['settings']['SHOP_PHONE'])."</td></tr>
	 <tr><td align='right'>Fax:</td><td>".$Cpage->input_text("item[settings][SHOP_FAX]", $Aaddresses['settings']['SHOP_FAX'])."</td></tr>
	 </table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>Steuer und Register</div></div><div class='item_content'>	 
	".$Cpage->table()."<tr><td align='right'>Steuernummer:</td><td>".$Cpage->input_text("item[settings][SHOP_TAX_NUMBER]", $Aaddresses['settings']['SHOP_TAX_NUMBER'])."</td></tr>
	 <tr><td align='right'>Umsatzsteuer-Identifikationsnummer:</td><td>".$Cpage->input_text("item[settings][SALES_TAX_IDENTIFICATION_NUMBER]", $Aaddresses['settings']['SALES_TAX_IDENTIFICATION_NUMBER'])."</td></tr>
	 <tr><td align='right'>Handelsregister:</td><td>".$Cpage->input_text("item[settings][TRADE_REGISTER]", $Aaddresses['settings']['TRADE_REGISTER'])."</td></tr>
	 </table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>Bank Verbindung</div></div><div class='item_content'>	 	 
	".$Cpage->table()."<tr><td align='right'>Konto Inhaber:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_ACCOUNT_OWNER]", $Aaddresses['settings']['SHOP_BANK_ACCOUNT_OWNER'])."</td></tr>
	 <tr><td align='right'>Konto Nummer:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_NUMBER]", $Aaddresses['settings']['SHOP_BANK_NUMBER'])."</td></tr>
	 <tr><td align='right'>Name der Bank:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_NAME]", $Aaddresses['settings']['SHOP_BANK_NAME'])."</td></tr>
	 <tr><td align='right'>Bankleitzahl:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_CODE]", $Aaddresses['settings']['SHOP_BANK_CODE'])."</td></tr>
	 <tr><td align='right'>IBAN:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_IBAN]", $Aaddresses['settings']['SHOP_BANK_IBAN'])."</td></tr>
	 <tr><td align='right'>SWIFT:</td><td>".$Cpage->input_text("item[settings][SHOP_BANK_SWIFT]", $Aaddresses['settings']['SHOP_BANK_SWIFT'])."</td></tr>
	 </table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>Admin Zugang</div></div><div class='item_content'>		 
	".$Cpage->table()."<tr><td align='right'>Login Name:</td><td>".$Cpage->input_text("item[settings][adm_access]", $Aaddresses['adm_access'])."</td></tr>
	 <tr><td align='right'>Neues Passwort:</td><td>".$Cpage->input_password("item[settings][adm_password]")."</td></tr>
	 <tr><td align='right'>Passwort bestätigen:</td><td>".$Cpage->input_password("item[settings][adm_password_check]")."</td></tr>
	 </table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'>Website Daten (Wichtig für Suchmaschinen)</div></div><div class='item_content'>
	".$Cpage->table()."<tr><td align='right'>Autor:</td><td>".$Cpage->input_text("item[settings][author]", $Aaddresses['settings']['AUTHOR'])."</td></tr>
	 <tr><td align='right'>Autor E-Mail:</td><td>".$Cpage->input_text("item[settings][author_mail]", $Aaddresses['settings']['AUTHOR_MAIL'])."</td></tr>
	 <tr><td align='right' valign='top'>Beschreibung des Shops:</td><td>".$Cpage->input_text("item[settings][description]", $Aaddresses['settings']['DESCRIPTION'], 400, NOT_READONLY, NO_FREETEXT, "150")."<br /><span class='information'>Die Länge der Beschreibung darf max. 150 Zeichen lang sein.</span></td></tr>
	 <tr><td align='right' valign='top'>Stichpunkte über den Shop:</td><td>".$Cpage->input_text("item[settings][keywords]", $Aaddresses['settings']['KEYWORDS'], 400)."<br /><span class='information'>Maximal 8 eindeutige Keywords, durch Komma getrennt.</span></td></tr>
	 </table>
	</div></div>
	<div class='item_border' style='min-height:190px;'><div class='item_header'><div class='item_point_single'></div></div><div class='item_content'>		 
	".$Cpage->input_submit('Speichern')."
	</div></div> 
	</form>
	
	";
    return $content;
}