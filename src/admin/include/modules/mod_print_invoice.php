<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$id = $Cpage->get_parameter("id");
if(empty($id))
{
    return;
}

$sql      = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
$result   = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$Ainvoice = $result->fetch_assoc();

if(empty($Ainvoice['invoice_id']))
{
    return FALSE;
}

$sql                     = "SELECT * FROM `".TBL_PREFIX."customers` WHERE `id` =".$Ainvoice['customer_id'].";";
$result                  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$Acustomer               = $result->fetch_assoc();
$Ainvoice['customer_id'] = $Acustomer['customer'];

$Ainvoice['invoice_date']   = $Cpage->format_time($Ainvoice['invoice_time'], "date");
$Ainvoice['invoice_footer'] = INVOICE_FOOTER_TEXT;

$Ainvoice['use_delivery_data'] = FALSE;
if(!empty($Ainvoice['del_name']))
{
    $Ainvoice['use_delivery_data'] = TRUE;
}

$Ainvoice['address']     = $Ainvoice['address1'];
$Ainvoice['del_address'] = $Ainvoice['del_address1'];
if(!empty($Ainvoice['address2']))
{
    $Ainvoice['address'] .= "<br />".$Ainvoice['address2'];
}
if(!empty($Ainvoice['del_address2']))
{
    $Ainvoice['del_address'] .= "<br />".$Ainvoice['del_address2'];
}

$Ainvoice['name'] = $Ainvoice['firstname']." ".$Ainvoice['lastname'];
if(!empty($Ainvoice['company']))
{
    $Ainvoice['name'] = $Ainvoice['company']."<br />".$Ainvoice['name'];
}

$bank_needed = FALSE;
if($Ainvoice['delivery_type'] == DO_ADVANCE)
{
    $bank_needed      = TRUE;
    $Ainvoice['bank'] = TRANSFER_TO_BANK_ACCOUNT.":<br /><br />";
    $Ainvoice['bank'] .= ACCOUNT_OWNER.": ".SHOP_BANK_ACCOUNT_OWNER."<br />";
    $Ainvoice['bank'] .= BANK_NAME.": ".SHOP_BANK_NAME."<br />";
    $Ainvoice['bank'] .= BANK_CODE.": ".SHOP_BANK_CODE."<br />";
    $Ainvoice['bank'] .= BANK_NUMBER.": ".SHOP_BANK_NUMBER."<br /><br />";
    $dummy = SHOP_BANK_IBAN;
    if(!empty($dummy))
    {
        $Ainvoice['bank'] .= FOR_INTERNATIONAL_TRANSFERS.":<br /><br />";
        $Ainvoice['bank'] .= IBAN.": ".SHOP_BANK_IBAN."<br />";
        $Ainvoice['bank'] .= SWIFT.": ".SHOP_BANK_SWIFT."<br><br />";
    }
    $Ainvoice['bank'] .= "<strong>".REASON_FOR_TRANSFER."</strong>";
}

$Acart                          = unserialize($Ainvoice['cart']);
$Acart['has_shipping_cost']     = FALSE;
$Acart['has_cod_shipping_cost'] = FALSE;
if(!empty($Acart['shipping_cost_netto']))
{
    $Acart['has_shipping_cost'] = TRUE;
}

if(!empty($Acart['cod_shipping_cost_netto']))
{
    $Acart['has_cod_shipping_cost'] = TRUE;
}

$Ainvoice['delivery_type'] = $Cpage->Aglobal['delivery'][$Ainvoice['delivery_type']]['value'];
$Ainvoice['country']       = $Cpage->get_country_name($Ainvoice['country']);
$Ainvoice['del_country']   = $Cpage->get_country_name($Ainvoice['del_country']);

$invoice_message = FALSE;
if(!empty($Ainvoice['invoice_text']))
{
    $Ainvoice['message'] = nl2br(preg_replace('/ /', "&#160;", $Ainvoice['invoice_text']));
    $invoice_message     = TRUE;
}

$Ainvoice['footer_text'] = INVOICE_FOOTER_TEXT;

$Acart['delivery_target']            = $Cpage->get_country_name($Acart['delivery_country_target']);
$Acart['total_weight']               = $Cpage->weight_kilogram($Acart['total_weight']);
$Acart['total_brutto']               = $Cpage->money($Acart['cart_price_brutto']+$Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto']);
$Acart['total_netto']                = $Cpage->money($Acart['cart_price_netto']+$Acart['shipping_cost_netto']+$Acart['cod_shipping_cost_netto']);
$Acart['total_shipping_cost_netto']  = $Cpage->money($Acart['shipping_cost_netto']+$Acart['cod_shipping_cost_netto']);
$Acart['total_shipping_cost_brutto'] = $Cpage->money($Acart['shipping_cost_brutto']+$Acart['cod_shipping_cost_brutto']);
$Acart['cod_shipping_cost_brutto']   = $Cpage->money($Acart['cod_shipping_cost_brutto']);
$Acart['cod_shipping_cost_netto']    = $Cpage->money($Acart['cod_shipping_cost_netto']);
$Acart['shipping_cost_netto']        = $Cpage->money($Acart['shipping_cost_netto']);
$Acart['shipping_cost_brutto']       = $Cpage->money($Acart['shipping_cost_brutto']);
$Acart['shipping_tax']               = $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]."%";


$taxlisting = "";
if(ENABLE_TAX)
{
    if($Ainvoice['delivery_method'] == "weight")
    {
        $invoice_draft_tax = $Cpage->get_invoice_draft("invoice_weight_tax");
    }
    if($Ainvoice['delivery_method'] == "flatrate")
    {
        $invoice_draft_tax = $Cpage->get_invoice_draft("invoice_flatrate_tax");
    }
    if($Ainvoice['delivery_method'] == "free")
    {
        $invoice_draft_tax = $Cpage->get_invoice_draft("invoice_free_tax");
    }

    if(!empty($Acart['tax']))
    {

        if(!USE_ARTICLE_NUMBER)
        {
            $invoice_draft_tax['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $invoice_draft_tax['text']);
        }
        else
        {
            $invoice_draft_tax['text'] = preg_replace("/\[!if_number\]/is", "", $invoice_draft_tax['text']);
            $invoice_draft_tax['text'] = preg_replace("/\[!end_if_number\]/is", "", $invoice_draft_tax['text']);
        }

        foreach($Acart['tax'] as $tax_percent => $tax)
        {
            if(!empty($tax))
            {
                $text = preg_replace("/\\\$tax\['percent'\]/i", $tax_percent."%", $invoice_draft_tax['text']);
                $text = preg_replace("/\\\$tax\['value'\]/i", $Cpage->money($tax), $text);
                $taxlisting .= $text."\n";
            }
        }
    }
}

if($Ainvoice['delivery_method'] == "weight")
{
    $invoice_draft_cart = $Cpage->get_invoice_draft("invoice_weight_cart");
}
if($Ainvoice['delivery_method'] == "flatrate")
{
    $invoice_draft_cart = $Cpage->get_invoice_draft("invoice_flatrate_cart");
}
if($Ainvoice['delivery_method'] == "free")
{
    $invoice_draft_cart = $Cpage->get_invoice_draft("invoice_free_cart");
}
$cartlisting = "";

if(!ENABLE_TAX)
{
    $invoice_draft_cart['text'] = preg_replace("/\[!if_tax\](.*?)\[!end_if_tax\]/is", "", $invoice_draft_cart['text']);
}
else
{
    $invoice_draft_cart['text'] = preg_replace("/\[!if_tax\]/is", "", $invoice_draft_cart['text']);
    $invoice_draft_cart['text'] = preg_replace("/\[!end_if_tax\]/is", "", $invoice_draft_cart['text']);
}

if(!USE_ARTICLE_NUMBER)
{
    $invoice_draft_cart['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $invoice_draft_cart['text']);
}
else
{
    $invoice_draft_cart['text'] = preg_replace("/\[!if_number\]/is", "", $invoice_draft_cart['text']);
    $invoice_draft_cart['text'] = preg_replace("/\[!end_if_number\]/is", "", $invoice_draft_cart['text']);
}

foreach($Acart['cart'] as $key => $value)
{
    $value['total_weight'] = "<nobr>".$Cpage->weight_kilogram($value['total_weight'])."</nobr>";
    $value['brutto']       = "<nobr>".$Cpage->money($Cpage->tax_calc($value['netto'], "brutto", $value['tax']))."</nobr>";
    $value['netto']        = "<nobr>".$Cpage->money($value['netto'])."</nobr>";
    $value['total_brutto'] = "<nobr>".$Cpage->money($Cpage->tax_calc($value['total_netto'], 'brutto', $value['tax']))."</nobr>";
    $value['tax']          = $value['tax']."%";
    $value['total_netto']  = "<nobr>".$Cpage->money($value['total_netto'])."</nobr>";

    $text = $invoice_draft_cart['text'];
    foreach($value as $key2 => $value2)
    {
        if(!is_array($value2))
        {
            $text = preg_replace("/\\\$cart\['".$key2."'\]/i", $value2, $text);
        }
    }
    $text = preg_replace("/\\\$cart\['pos'\]/i", ($key+1), $text);

    $cartlisting .= $text."\n";
}

$tmp = 1;
if($Acart['has_shipping_cost'])
{
    $Ashipping['pos']          = ($Acart['positions']+1);
    $tmp                       = 2;
    $Ashipping['name']         = "Versandkosten";
    $Ashipping['number']       = "&#160;";
    $Ashipping['total_weight'] = $Acart['total_weight'];
    $Ashipping['netto']        = $Acart['shipping_cost_netto'];
    $Ashipping['tax']          = $Acart['shipping_tax'];
    $Ashipping['brutto']       = "&#160;";
    $Ashipping['quantity']     = "&#160;";
    $Ashipping['total_brutto'] = $Acart['shipping_cost_brutto'];
    $text                      = $invoice_draft_cart['text'];
    foreach($Ashipping as $key => $value)
        $text = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $text);
    $cartlisting .= $text."\n";
}

if($Acart['has_cod_shipping_cost'])
{
    $Acod_shipping['pos']          = ($Acart['positions']+$tmp);
    $Acod_shipping['name']         = "Nachnahmegebühr";
    $Acod_shipping['number']       = "&#160;";
    $Acod_shipping['total_weight'] = "&#160;";
    $Acod_shipping['netto']        = $Acart['cod_shipping_cost_netto'];
    $Acod_shipping['tax']          = $Acart['shipping_tax'];
    $Acod_shipping['brutto']       = "&#160;";
    $Acod_shipping['quantity']     = "&#160;";
    $Acod_shipping['total_brutto'] = $Acart['cod_shipping_cost_brutto'];
    $text                          = $invoice_draft_cart['text'];
    foreach($Acod_shipping as $key => $value)
        $text = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $text);
    $cartlisting .= $text."\n";
}

if($Ainvoice['delivery_method'] == "weight")
{
    $invoice_draft = $Cpage->get_invoice_draft("invoice_weight");
}
if($Ainvoice['delivery_method'] == "flatrate")
{
    $invoice_draft = $Cpage->get_invoice_draft("invoice_flatrate");
}
if($Ainvoice['delivery_method'] == "free")
{
    $invoice_draft = $Cpage->get_invoice_draft("invoice_free");
}

if(!ENABLE_TAX)
{
    $invoice_draft['text'] = preg_replace("/\[!if_tax\](.*?)\[!end_if_tax\]/is", "", $invoice_draft['text']);
}
else
{
    $invoice_draft['text'] = preg_replace("/\[!if_tax\]/is", "", $invoice_draft['text']);
    $invoice_draft['text'] = preg_replace("/\[!end_if_tax\]/is", "", $invoice_draft['text']);
}

if(!USE_ARTICLE_NUMBER)
{
    $invoice_draft['text'] = preg_replace("/\[!if_number\](.*?)\[!end_if_number\]/is", "", $invoice_draft['text']);
}
else
{
    $invoice_draft['text'] = preg_replace("/\[!if_number\]/is", "", $invoice_draft['text']);
    $invoice_draft['text'] = preg_replace("/\[!end_if_number\]/is", "", $invoice_draft['text']);
}

if(!$Ainvoice['use_delivery_data'])
{
    $invoice_draft['text'] = preg_replace("/\[!if_delivery\](.*?)\[!end_if_delivery\]/is", "", $invoice_draft['text']);
}
else
{
    $invoice_draft['text'] = preg_replace("/\[!if_delivery\]/is", "", $invoice_draft['text']);
    $invoice_draft['text'] = preg_replace("/\[!end_if_delivery\]/is", "", $invoice_draft['text']);
}

if(!$invoice_message)
{
    $invoice_draft['text'] = preg_replace("/\[!if_message\](.*?)\[!end_if_message\]/is", "", $invoice_draft['text']);
}
else
{
    $invoice_draft['text'] = preg_replace("/\[!if_message\]/is", "", $invoice_draft['text']);
    $invoice_draft['text'] = preg_replace("/\[!end_if_message\]/is", "", $invoice_draft['text']);
}

if(!$bank_needed)
{
    $invoice_draft['text'] = preg_replace("/\[!if_bank\](.*?)\[!end_if_bank\]/is", "", $invoice_draft['text']);
}
else
{
    $invoice_draft['text'] = preg_replace("/\[!if_bank\]/is", "", $invoice_draft['text']);
    $invoice_draft['text'] = preg_replace("/\[!end_if_bank\]/is", "", $invoice_draft['text']);
}

foreach($Ainvoice as $key => $value)
{
    $invoice_draft['text'] = preg_replace("/\\\$invoice\['".$key."'\]/i", $value, $invoice_draft['text']);
}

foreach($Acart as $key => $value)
{
    if(is_array($value))
    {
        continue;
    }
    $invoice_draft['text'] = preg_replace("/\\\$cart\['".$key."'\]/i", $value, $invoice_draft['text']);
}

$invoice_draft['text'] = preg_replace("/\\\$cart_listing/i", $cartlisting, $invoice_draft['text']);
$invoice_draft['text'] = preg_replace("/\\\$tax_listing/i", $taxlisting, $invoice_draft['text']);

if($Ainvoice['delivery_method'] == "weight")
{
    $invoice_settings = $Cpage->get_invoice_draft("invoice_weight_settings");
}
if($Ainvoice['delivery_method'] == "flatrate")
{
    $invoice_settings = $Cpage->get_invoice_draft("invoice_flatrate_settings");
}
if($Ainvoice['delivery_method'] == "free")
{
    $invoice_settings = $Cpage->get_invoice_draft("invoice_free_settings");
}

$invoice_settings = unserialize($invoice_settings['text']);

$head = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
        \"http://www.w3.org/TR/html4/strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\"
  xml:lang=\"de\" lang=\"de\">
<head>
<title>".PAGE_TITLE.$subtitle."</title>
<meta http-equiv='content-type' content='text/html; charset=utf-8' />
<link rel='stylesheet' href='layout/css/screen_template.css' type='text/css' media='screen' />
<link rel='stylesheet' href='layout/css/print_template.css' type='text/css' media='print' />
<link rel='shortcut icon' href='layout/favicon.ico' type='image/x-icon' />
</head>
<body>
";
$invoice_draft['text'] = $head.$Cpage->input_button("Drucken","window.print();")." ".$Cpage->input_button("Fenster schliessen", "window.close()")."<br />"."<div style='padding-top:".$invoice_settings['top']."mm;padding-left:".$invoice_settings['left']."mm;'>\n".$invoice_draft['text']."</div>";
$invoice_draft['text'] .= "</body></html>";

$content .= $invoice_draft['text'];
