<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$id     = $Cpage->get_parameter("id");
$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "update_page":
        $page      = $Cpage->get_parameter("page");
        $link_text = $Cpage->get_parameter("link_text");
        $subtitle  = $Cpage->get_parameter("subtitle");
        $value     = $Cpage->get_parameter("text", "", NO_ESCAPE);
        $show_shop = $Cpage->get_parameter("show_shop");
        $sql       = "UPDATE `".TBL_PREFIX."pages_".LANGUAGE."` SET `page` = ?, `link_text` = ?, `subtitle` = ?, `value` = ?, `show_shop` = ? WHERE `id` =?;";
        $stmt      = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('ssssii', $page, $link_text, $subtitle, $value, $show_shop, $id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        $content .= load_menu();
        $content .= show_item($id);
        break;
    case "new_page":
        $content .= load_menu();
        $content .= new_page();
        break;
    case "store_page":
        $typ       = $Cpage->get_parameter("typ");
        $page      = $Cpage->get_parameter("page");
        $link_text = $Cpage->get_parameter("link_text");
        $subtitle  = $Cpage->get_parameter("subtitle");
        $value     = $Cpage->get_parameter("text", "", NO_ESCAPE);
        $show_shop = $Cpage->get_parameter("show_shop");
        $sql       = "INSERT INTO `".TBL_PREFIX."pages_".LANGUAGE."` (`type`, `page`, `link_text`, `subtitle`, `value`, `show_shop`) VALUES (?, ?, ?, ?, ?, ?);";
        $stmt      = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('sssssi', $typ, $page, $link_text, $subtitle, $value, $show_shop);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        $content .= load_menu();
        $id = $stmt->insert_id;
        $content .= show_item($id);
        break;
    case "delete_page":
        $sql = "DELETE FROM `".TBL_PREFIX."pages_".LANGUAGE."`  WHERE `id` =$id;";
        $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $content .= load_menu();
        break;
    default:
        $content .= load_menu();
        $content .= show_item($id);
        break;
}

function load_menu()
{
    global $Cpage;
    global $Cdb;
    $content = "";
    $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Seitenmenü</div></div><div class='item_content'>";
    $sql    = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."` ORDER BY `link_text` ASC ;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= "<div id='left_content'>\n";
    $content .= "<ul class='sub_menu'>\n";
    while($Apages_db = $result->fetch_assoc())
    {
        $content .= "<li>".$Cpage->link($Apages_db['link_text'], "pages.php", "id=".$Apages_db['id'])."</li>";
    }
    $content .= "<li>".$Cpage->link("Neue Seite", "pages.php", "do_action=new_page")."</li>";
    $content .= "</ul>\n";
    $content .= "</div>\n";
    $content .= "</div></div>\n";
    return $content;
}

function show_item($id)
{
    global $Cpage;
    global $Cdb;
    global $load_text_editor;
    global $load_box;
    global $Apath;
    global $script;

    $load_text_editor = TRUE;
    $load_box = TRUE;

    $uploaddir_picture = "upload/pages/";
    $uploaddir_file = "download/";

    $content = "";
    $content .= "<div class='item_border'><div class='item_header'></div><div class='item_content'>\n";

    if(!empty($id))
    {
        $sql = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."` WHERE `id`=".$id." LIMIT 1;";
    }
    else $sql = "SELECT * FROM `".TBL_PREFIX."pages_".LANGUAGE."` ORDER BY `link_text` ASC LIMIT 1;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Apage  = $result->fetch_assoc();
    $id     = $Apage['id'];


    $script .= "function delete_page(form) { form.do_action.value='delete_page'; form.submit(); }
    ";

    $content .= $Cpage->form("page", "pages.php", "update_page").$Cpage->input_hidden("id", $id).$Cpage->table()."<tr>"."<td>Seite: </td><td>".$Cpage->input_text("page", $Apage['page'])."</td>"."</tr><tr><td>Link Text: </td><td>".$Cpage->input_text("link_text", $Apage['link_text'])."</td>"."</tr><tr><td>Subtitel: </td><td>".$Cpage->input_text("subtitle", $Apage['subtitle'])."</td>"."</tr><tr><td>Shop zeigen: </td><td>".$Cpage->select_multi("show_shop", $Cpage->Aglobal['answer'], $Apage['show_shop'])."</td>"."</tr></table>".
        $Cpage->textarea("text", 800, 400).$Apage['value']."</textarea><br />
            ".$Cpage->link("Bild einfügen", $Apath['root']."picture_upload.php", "KeepThis=true&uploaddir=$uploaddir_picture&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Datei einfügen", $Apath['root']."file_upload.php", "KeepThis=true&uploaddir=$uploaddir_file&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Artikel Link einfügen", $Apath['root']."article_add.php", "KeepThis=true&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
        <br /><br />
        ".$Cpage->input_submit("Speichern")." ".$Cpage->input_button("Löschen", "delete_page(document.page);")."</form>";
    $content .= "</div></div>\n";
    return $content;
}

function new_page()
{
    global $Cpage;
    global $load_text_editor;
    global $load_box;
    global $Apath;
    $load_text_editor = TRUE;
    $load_box = TRUE;

    $uploaddir_picture = "upload/pages/";
    $uploaddir_file = "download/";

    $content          = "";
    $content .= "<div class='item_border'><div class='item_header'>Neue Seite</div><div class='item_content'>\n";

    $Atypes = array("Seite" => "page", "Links" => "links", "Kontakt" => "contact");


    $content .= $Cpage->form("page", "pages.php", "store_page").$Cpage->table()."<tr>"."<td>Typ: </td><td>".$Cpage->select_key("typ", $Atypes)."</td>"."</tr><tr><td>Seite: </td><td>".$Cpage->input_text("page")."</td>"."</tr><tr><td>Link Text: </td><td>".$Cpage->input_text("link_text")."</td>"."</tr><tr><td>Subtitel: </td><td>".$Cpage->input_text("subtitle")."</td>"."</tr><tr><td>Shop zeigen: </td><td>".$Cpage->select_multi("show_shop", $Cpage->Aglobal['answer'], 1)."</td>"."</tr></table>".
        $Cpage->textarea("text", 800, 400)."</textarea><br />".
            $Cpage->link("Bild einfügen", $Apath['root']."picture_upload.php", "KeepThis=true&uploaddir=$uploaddir_picture&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Datei einfügen", $Apath['root']."file_upload.php", "KeepThis=true&uploaddir=$uploaddir_file&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Artikel Link einfügen", $Apath['root']."article_add.php", "KeepThis=true&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
        <br /><br />".
        $Cpage->input_submit("Speichern")."</form>";
    $content .= "</div></div>\n";
    return $content;
}