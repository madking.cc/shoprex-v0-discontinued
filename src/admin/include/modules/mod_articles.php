<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Klone sollen deaktivierbar sein

$get_category = $Cpage->get_parameter("category", 0);
$get_article  = $Cpage->get_parameter("article");
$show_table   = $Cpage->get_parameter("show_table", "table_0");

$categories = $Carticles->get_article_category_list($get_category, $get_article);

$content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Artikel-/Kategoriemenü</div></div><div class='item_content'>";
$content .= $categories."</div></div>";

/////////////////////////////////////////////////////////////////////////////////////////////////////

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "add_category":
        $content .= add_category();
        break;
    case "add_article":
        $content .= add_article();
        break;

    case "save_default_category":
        save_default("category");
        break;
    case "save_default_article":
        save_default("article");
        break;

    case "save_category":
        save_category();
        break;
    case "save_article":
        save_article();
        break;

    case "update_article":
        update_article();
        break;
    case "update_category":
        update_category();
        break;

    case "clone_article":
        clone_article();
        break;
    case "copy_article":
        copy_article();
        break;

    case "delete_article":
        delete_article();
        break;
    case "delete_category":
        delete_category();
        break;

    case "article_picture_add":
    case "article_picture_modify":
        update_article();
        picture_article();
        break;
    case "save_picture_article":
        save_picture_article();
        break;
    case "category_picture_modify":
        update_category();
        picture_category();
        break;
    case "save_picture_category":
        save_picture_category();
        break;
    case "add_link":
        update_article();
        show_article();
        break;
    case "move_link":
        update_article();
        show_article();
        break;
    case "delete_link":
        update_article();
        show_article();
        break;
    default:
        if(isset($get_article))
        {
            $content .= show_article();
        }
        elseif(!empty($get_category))
        {
            $content .= show_category();
        }
        break;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function add_category()
{
    global $Cpage;
    global $Cdb;
    global $get_category;
    global $script;
    global $script_footer;

    // Hole die Default Werte
    $sql    = "SELECT * FROM `".TBL_PREFIX."adm_default` WHERE `type` LIKE 'add_category';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($default_dbfield = $result->fetch_assoc())
    {
        $default_field[$default_dbfield['field']] = $default_dbfield['value'];
    }

    $content = "<div class='item_border'><div class='item_header'>Kategorie hinzufügen</div>
		<div class='item_content'>\n";

    $script .= "
	// Speichere Default Werde
	function save_default(formID)
	{
		formID.do_action.value = 'save_default_category';
		formID.submit();
	}

    // Bestimme die Anzahl der eingegebenen Zeichen vom Kategoriennamen
	function userDoNameChanges(formID)
	{
		count_length(formID.name, document.getElementsByName(\"name_count\")[0]);
		return true;
	}

	// Hilfsfunktion, gibt Anzahl der Elemente eines Feldes zurück
	function count_length(elementID, targetID)
	{
		targetID.innerHTML = elementID.value.length;
		return true;
	}

	// Bevor das Formular abgesendet werden soll, Eingaben überprüfen, bzw. setzen
	function check_submit(formID)
	{
		if ((formID.visible_from.value==null)||(formID.visible_from.value==\"\"))
		{
			formID.visible_from.value = 0;
		}
		if ((formID.visible_to.value==null)||(formID.visible_to.value==\"\"))
		{
			formID.visible_to.value = 0;
		}

		if(formID.name.value.length == 0)
		{
			alert(\"Sie müssen einen Kategoriennamen vergeben.\");
			formID.name.focus();
			return false;
		}
		return true;
	}

	function reset_time(elementID)
	{
	    elementID.value = 0;
	}
    ";

    $content .= $Cpage->form('category_add_form', 'articles.php', 'save_category', 'return check_submit(this);').$Cpage->input_hidden("category", $get_category).$Cpage->table("table_category")."
		<tr class='line_bottom'>
		<td colspan='2'>".$Cpage->input_submit("Kategorie speichern")." ".$Cpage->input_button('Alle Eingaben als Default speichern', 'save_default(this.form);')." ".$Cpage->input_reset("Eingaben zurücksetzen", "didChanges = false;", "onmouseout='count_length(this.form.name, document.getElementsByName(\"name_count\")[0]);'")."
        </td></tr>
		<tr class=''><td>Kategorienname:</td>
		<td>".$Cpage->input_text('name', $default_field['name'], 250, NOT_READONLY, "onkeyup='userDoNameChanges(document.category_add_form);'")."
		<font class='information'><span name='name_count'></span> Zeichen lang</font></td><td rowspan='9'></td></tr>
		<tr class='line_bottom'><td>Such Wörter:</td><td>".$Cpage->input_text('keywords', $default_field['keywords'], 150)."
		<span class='information'>Mehrere durch Leerzeichen trennen</span></td></tr>
		<tr class='line_bottom'><td>Status:</td>";
    $inactive = FALSE;
    if(!$default_field['status'])
    {
        $inactive = TRUE;
    }
    $content .= "<td>".$Cpage->input_radio("status", 1, $default_field['status'])."
        Aktiv / ".$Cpage->input_radio("status", 0, $inactive)."
        Inaktiv</td></tr>
		<tr class='line_bottom'><td>Bild:</td><td><span class='information'>Ein Bild können Sie nach dem Speichern hinzufügen.</span></td></tr>

    <tr class=''><td>Sichtbar:</td><td>von ".
    $Cpage->input_text('visible_from', $default_field['visible_from'], 120, NOT_READONLY, "id='visible_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_from);")."
    bis ".$Cpage->input_text('visible_to', $default_field['visible_to'], 120, NOT_READONLY, "id='visible_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_to);")."</td>
    </tr>
    <tr class=''><td></td><td><span class='information'>0 = Immer, sonst Format: TT.MM.JJJJ HH:MM</span></td></tr>


		<tr><td>Stil Kategorienname:</td><td>".$Cpage->input_text('style', $default_field['style'], 250)."
        </td></tr>
		<tr><td></td><td><span class='information'>= CSS Font Style, siehe <a href='http://de.selfhtml.org/css/eigenschaften/schrift.htm' target='_blank'>de.selfhtml.org</a>, z.B. font-weight:bold;</span></td></tr>
		</table></form></div></div>\n";
    $script_footer .= "
            count_length(document.category_add_form.name, document.getElementsByName('name_count')[0]);
        ";
    $script_footer .= $Cpage->datepicker("visible_from");
    $script_footer .= $Cpage->datepicker("visible_to");
    return $content;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function add_article()
{
    global $Cdb;
    global $Cpage;
    global $get_category;
    global $load_text_editor;
    global $load_box;
    global $Apath;
    global $script;
    global $script_footer;
    $content = "";

    $uploaddir_picture = "upload/article_text/";
    $uploaddir_file = "download/";

    $load_text_editor = TRUE;
    $load_box = TRUE;

    $content .= "<div class='item_border'><div class='item_header'>Artikel hinzufügen</div>
		<div class='item_content'>\n";

    // Hole die Default Werte:
    $sql    = "SELECT * FROM `".TBL_PREFIX."adm_default` WHERE `type` LIKE 'add_article';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($default_db_field = $result->fetch_assoc())
    {
        $default_field[$default_db_field['field']] = $default_db_field['value'];
    }

    // Falls Automatische Artikelnummer gewünscht, hole einzigartige ID
    $sql            = "SELECT * FROM `".TBL_PREFIX."counter` WHERE `type` LIKE 'ARTICLE_COUNTER';";
    $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acount         = $result->fetch_assoc();
    $count          = $Acount['count'];
    $default_number = ARTICLE_PREFIX.($count+ARTICLE_START).ARTICLE_SUFFIX;

    if($default_field['auto_number'] == TRUE)
    {
        $default_field['number'] = $default_number;
    }

    $script .= "
      // Speichere Default Werte
      function save_default(formID)
      {
        formID.do_action.value = 'save_default_article';
        formID.submit();
      }

      // Vorschau des Artikels
      function previewArticle(formID)
      {
        formID.target = '_blank';
        formID.target_page.value = 'article_preview.php';
        formID.submit();
        formID.target = '';
        formID.target_page.value = 'articles.php';
      }

      // Wenn der Artikelname geändert wurde, bestimme 'on-the-fly' die Zeicheanzahl
      function userDoNameChanges(formID)
      {
        count_length(formID.name, document.getElementsByName(\"name_count\")[0]);
        return true;
      }

      // Hilfsfunktion
      function count_length(elementID, targetID)
      {
        targetID.innerHTML = elementID.value.length;
        return true;
      }

      // Prüfe, bzw. Setze Werte, bevor das Formular abgesendet wird
      function check_submit(formID)
      {
            if ((formID.number.value==null)||(formID.number.value==\"\"))
            {
              formID.number.value = 0;
            }
            if ((formID.price.value==null)||(formID.price.value==\"\"))
            {
              formID.price.value = 0;
            }
            if ((formID.quantity.value==null)||(formID.quantity.value==\"\"))
            {
              formID.quantity.value = 0;
            }
            if ((formID.quantity_warning.value==null)||(formID.quantity_warning.value==\"\"))
            {
              formID.quantity_warning.value = 0;
            }
            if ((formID.weight.value==null)||(formID.weight.value==\"\"))
            {
              formID.weight.value = 0;
            }
            if ((formID.visible_from.value==null)||(formID.visible_from.value==\"\"))
            {
              formID.visible_from.value = 0;
            }
            if ((formID.visible_to.value==null)||(formID.visible_to.value==\"\"))
            {
              formID.visible_to.value = 0;
            }
            if ((formID.discount.value==null)||(formID.discount.value==\"\"))
            {
              formID.discount.value = 0;
            }
            if ((formID.discount_from.value==null)||(formID.discount_from.value==\"\"))
            {
              formID.discount_from.value = 0;
            }
            if ((formID.discount_to.value==null)||(formID.discount_to.value==\"\"))
            {
              formID.discount_to.value = 0;
            }
            if (isNaN(formID.discount.value))
            {
              alert(\"Bei Rabatt sind nur Zahlen erlaubt.\");
              formID.discount.focus();
              return false;
            }
            formID.discount.value = formID.price.value.replace(/,/, \".\");

            if (isNaN(formID.quantity.value))
            {
              alert(\"Bei Anzahl sind nur Zahlen erlaubt.\");
              formID.quantity.focus();
              return false;
            }
            if (isNaN(formID.quantity_warning.value))
            {
              alert(\"Bei Warnung sind nur Zahlen erlaubt.\");
              formID.quantity_warning.focus();
              return false;
            }
            if (isNaN(formID.weight.value))
            {
              alert(\"Bei Gewicht sind nur Zahlen erlaubt.\");
              formID.weight.focus();
              return false;
            }
            formID.price.value = formID.price.value.replace(/,/, \".\");
            if(formID.name.value.length == 0)
            {
              alert(\"Sie müssen einen Arikelnamen vergeben.\");
              formID.name.focus();
              return false;
            }
        return true;
    }

    // Eindeutige ID für Artikelnummer, falls gewünscht
    function auto_number_change(formID)
    {
       if(formID.auto_number.selectedIndex == 1)
        formID.number.value = \"".$default_number."\";
       else formID.number.value = \"\";
    }

	function reset_time(elementID)
	{
	    elementID.value = 0;
	}
    ";

    $content .= $Cpage->form('article_add_form', 'articles.php', 'save_article', 'return check_submit(this);').$Cpage->input_hidden("category", $get_category).$Cpage->table('table_article')."
		<tr class='line_bottom'>
         <td colspan='2'>".$Cpage->input_submit("Artikel speichern")." ".$Cpage->input_button('Alle Eingaben als Default speichern', 'save_default(this.form);')." ".$Cpage->input_reset("Eingaben löschen", "didChanges = false;", "onmouseout='count_length(this.form.name, document.getElementsByName(\"name_count\")[0]);'")."
        </tr>
        <tr class=''>
         <td>Artikelname:</td>
         <td>".$Cpage->input_text('name', $default_field['name'], 400, NOT_READONLY, "onkeyup='userDoNameChanges(this.form);'")." <font class='information'><span name='name_count'></span> Zeichen lang</font></td>
        </tr>
        <tr>
         <td>Such Wörter:</td>
         <td>".$Cpage->input_text('keywords', $default_field['keywords'], 250)." <font class='information'>Mehrere durch Leerzeichen trennen</font></td>
        </tr>
        <tr>
         <td>ISBN:</td>
         <td>".$Cpage->input_text('isbn', $default_field['isbn'], 250)."</td>
        </tr>
        <tr class='line_bottom'>
         <td>Artikelnummer:</td>
         <td>".$Cpage->input_text('number', $default_field['number'], 150)." Autoartikelnummer: ".$Cpage->select_multi("auto_number", $Cpage->Aglobal['answer'], $default_field['auto_number'], 0, 0, 0, NOT_READONLY, "auto_number_change(this.form);")."</td>
        </tr>
        <tr class=''>
         <td>Status:</td>";
    $inactive = FALSE;
    if(!$default_field['status'])
    {
        $inactive = TRUE;
    }
    $content .= "<td>".$Cpage->input_radio("status", 1, $default_field['status'])." Aktiv / ".$Cpage->input_radio("status", 0, $inactive)." Inaktiv</td>
        </tr>
		<tr class='line_bottom'> 
		 <td>Als Top-Angebot:</td>
         <td>".$Cpage->select_multi('top_offer', $Cpage->Aglobal['answer'], $default_field['top_offer'])."</td>
        </tr>
        <tr class=''>
         <td>Preis:</td>
         <td>".$Cpage->input_text('price', $Cpage->money($default_field['price'], NO_CURRENCY_SYMBOL), 60)." ".CURRENCY_SYMBOL." ".$Cpage->select("price_type", 1, NOT_MULTIPLE)."<option value='brutto'>Brutto</option><option value='netto'";
    if($default_field['price_type'] == "netto")
    {
        $content .= " selected";
    }
    $content .= ">Netto</option></select>
         </td>
		</tr>
		<tr class='line_bottom'>
         <td>MwSt.:</td>
         <td>".$Cpage->select_multi('tax', $Cpage->Aglobal['tax_percent'], $default_field['tax'])."</td>
        </tr>
        <tr";
    if($default_field['quantity'] <= $default_field['quantity_warning'])
    {
        $content .= " class='low_quantity_warning_td'";
    }
    $content .= ">
         <td class=''>Auf Lager:</td>
         <td class=''>".$Cpage->input_text('quantity', $default_field['quantity'], 40)." Stück</td>
        </tr>
        <tr";
    if($default_field['quantity'] <= $default_field['quantity_warning'])
    {
        $content .= " class='low_quantity_warning_td'";
    }
    $content .= ">
         <td class=''>Warnung unter:</td>
         <td class=''>".$Cpage->input_text('quantity_warning', $default_field['quantity_warning'], 40)." Stück übrig <span class='information'>Der Wert 0 deaktiviert die Lagerbestandskontrolle</span></td>
        </tr>
		<tr>
		 <td";
    if(DELIVERY_METHOD == "weight")
    {
        $content .= " class='warning_td'";
    }
    $content .= ">Gewicht:</td>
         <td";
    if(DELIVERY_METHOD == "weight")
    {
        $content .= " class='warning_td'";
    }
    $content .= ">".$Cpage->input_text('weight', $default_field['weight'], 100)." Gramm</td>
        </tr>
        <tr class=''>
         <td>Zustand:</td>
         <td>".$Cpage->select_multi('condition', $Cpage->Aglobal['condition'], $default_field['condition'])."</td>
        </tr>
		<tr class='line_bottom'>
		 <td>Garantie:</td>
         <td>".$Cpage->select_multi('guarantee', $Cpage->Aglobal['guarantee'], $default_field['guarantee'])."</td>
        </tr>
        <tr class='line_bottom'>
         <td>Bild:</td>
         <td><span class='information'>Bilder können Sie nach dem Speichern des Artikels hinzufügen</span></td>
        </tr>
        <tr class='line_bottom'>
         <td>Attribute:</td>
         <td><span class='information'>Attribute können Sie nach dem Speichern des Artikels hinzufügen</span></td>
        </tr>
        <tr class=''>
         <td valign='top'>Kurzbeschreibung:</td>
         <td>".$Cpage->textarea("description", 200, 60).$default_field['description']."</textarea> <span class='information'>Jede Kurzbeschreibung ist durch eine neue Zeile zu trennen</span></td>
        </tr>
        <tr class='line_bottom'>
         <td valign='top'>Beschreibung:</td>
         <td>".$Cpage->textarea("text", 600, 400).$default_field['text']."</textarea><br />
            ".$Cpage->link("Bild einfügen", $Apath['root']."picture_upload.php", "KeepThis=true&uploaddir=$uploaddir_picture&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Datei einfügen", $Apath['root']."file_upload.php", "KeepThis=true&uploaddir=$uploaddir_file&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Artikel Link einfügen", $Apath['root']."article_add.php", "KeepThis=true&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
         </td>
        </tr>
        <tr class=''>
         <td>Sichtbar:</td>
         <td>von ".$Cpage->input_text('visible_from', $Cpage->format_time($default_field['visible_from'], "sql"), 120, NOT_READONLY, "id='visible_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_from);")."
         bis ".$Cpage->input_text('visible_to', $Cpage->format_time($default_field['visible_to'], "sql"), 120, NOT_READONLY, "id='visible_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_to);")." <span class='information'>0 = Immer, sonst Format: TT.MM.JJJJ HH:MM</span></td>
        </tr>
        <tr";
    if($default_field['discount'] > 0) $content .= " class='warning_tr'";
    $content .= ">
         <td>Rabatt:</td>
         <td>".$Cpage->input_text('discount', $default_field['discount'], 25)."%
         von ".$Cpage->input_text('discount_from', $Cpage->format_time($default_field['discount_from'], "sql"), 120, NOT_READONLY, "id='discount_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.discount_from);")."
		 bis ".$Cpage->input_text('discount_to', $Cpage->format_time($default_field['discount_to'], "sql"), 120, NOT_READONLY, "id='discount_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.discount_to);")."</td>
        </tr>
        <tr>
         <td>Stil Artikelnamen:</td>
         <td>".$Cpage->input_text('style', $default_field['style'], 300)." <span class='information'>CSS Font Style, siehe <a href='http://de.selfhtml.org/css/eigenschaften/schrift.htm' target='_blank'>de.selfhtml.org</a>, z.B. font-weight:bold;</span></td>
        </tr>
        </table>
      </form></div></div>\n";

    $script_footer .= $Cpage->datepicker("visible_from");
    $script_footer .= $Cpage->datepicker("visible_to");
    $script_footer .= $Cpage->datepicker("discount_from");
    $script_footer .= $Cpage->datepicker("discount_to");

    $script_footer .= "
            // Bestimme Länge des Artikelnamens
            count_length(document.article_add_form.name, document.getElementsByName('name_count')[0]);
        ";

    return $content;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

// Speichere Default Werte
function save_default($type)
{
    global $Cpage;
    global $Carticles;
    global $Cdb;
    global $get_category;
    global $script;


    // Bereite SQL vor
    $sql  = "UPDATE `".TBL_PREFIX."adm_default` SET `value` = ? WHERE `type` =? AND `field` =? LIMIT 1;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);

    if($type == "article")
    {
        $Aarticle = array();
        // Holle alle möglichen Postings vom der Artikelseite
        $Aarticle = $Carticles->get_article_post();

        $type = "add_article";
        foreach($Aarticle as $key => $value)
        {
            // Prüfe, ob der Schlüssel $key in der Datenbank für Artikel vorhanden ist
            // Da bei get_article_post() mehr als die Default Variablen geholt werden könnten
            if($Carticles->isset_default($key, $type))
            {
                // Binde Variablen
                $stmt->bind_param('sss', $value, $type, $key);
                // Führe SQL aus
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }

        $script .= $Cpage->load_page("articles.php", "category=$get_category&do_action=add_article");
    }
    elseif($type == "category")
    {
        $Acategory = array();
        $Acategory = $Carticles->get_category_post();

        $type = "add_category";
        foreach($Acategory as $key => $value)
        {
            if($Carticles->isset_default($key, $type))
            {
                $stmt->bind_param('sss', $value, $type, $key);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }

        $script .= $Cpage->load_page("articles.php", "category=$get_category&do_action=add_category");
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function save_category()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $get_category;
    global $script;



    $category_post = array();
    // Holle alle Kategoriens Posts
    $category_post = $Carticles->get_category_post();
    foreach($category_post as $key => $value)
    {
        // Erstelle für jeden Post eine eigene Variable mit Inhalt:
        $$key = $value;
    }

    $visible_from = $Cpage->get_timestamp($visible_from);
    $visible_from = $Cpage->get_sqltime($visible_from);

    $visible_to = $Cpage->get_timestamp($visible_to);
    $visible_to = $Cpage->get_sqltime($visible_to);


    // Erzeuge eindeutigen Hash
    $hash = $Cpage->hash();
    // Hole die ID vom Root Knoten des Kategorien Verzeichnisses
    $root_id = $Carticles->get_dir_root();
    // Bereite SQL vor:
    $sql  = "INSERT INTO `".TBL_PREFIX."categories` (
        `id` ,
        `status` ,
        `name` ,
        `keywords` ,
        `style` ,
        `clone_from` ,
        `picture` ,
        `picture_text` ,
        `changed` ,
        `created` ,
        `visible_from` ,
        `visible_to`,
		`hash`
        )
        VALUES (NULL , ?, ?, ?, ?, '', ?, ?, NOW( ) , NOW( ), ?, ?, ? );";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    // Binde Variablen:
    $stmt->bind_param('issssssss', $status, $name, $keywords, $style, $picture, $picture_text, $visible_from, $visible_to, $hash);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    // Hole die ID
    $id = $Cdb->get_insert_id();
    // Wenn Hauptkategorie, dann hole die ID des Root Knotens
    if($get_category == 0)
    {
        $save_id = $root_id;
    }
    // ansonsten von der ausgewählten Kategorie
    else $save_id = $get_category;
    // Speichere Verzeichnig im root oder ausgewählte Kategorie:
    $Carticles->dir_add($save_id, $id, "C");
    // Zeige die neu erstellte Kategorie an:
    $script .= $Cpage->load_page("articles.php", "category=$id");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function save_article()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $get_category;
    global $script;


    $article_post = array();
    $article_post = $Carticles->get_article_post();
    foreach($article_post as $key => $value)
    {
        $$key = $value;
    }

    $visible_from = $Cpage->get_timestamp($visible_from);
    $visible_from = $Cpage->get_sqltime($visible_from);

    $visible_to = $Cpage->get_timestamp($visible_to);
    $visible_to = $Cpage->get_sqltime($visible_to);

    $discount_from = $Cpage->get_timestamp($discount_from);
    $discount_from = $Cpage->get_sqltime($discount_from);

    $discount_to = $Cpage->get_timestamp($discount_to);
    $discount_to = $Cpage->get_sqltime($discount_to);

    $hash = $Cpage->hash();
    if($price_type == "brutto")
    {
        $price = $Cpage->tax_calc($price, "netto", $Cpage->Aglobal['tax'][$tax]);
    }
    $root_id = $Carticles->get_dir_root();
    $sql     = "INSERT INTO `".TBL_PREFIX."articles` (
		`id` ,
		`status` ,
		`name` ,
		`keywords` ,
		`isbn` ,
		`description` ,
		`style` ,
		`top_offer` ,
		`clone_from` ,
		`netto` ,
		`currency` ,
		`tax` ,
		`weight` ,
		`text` ,
		`number` ,
		`quantity` ,
		`quantity_warning` ,
		`condition` ,
		`guarantee` ,
		`changed` ,
		`created` ,
		`visible_from` ,
		`visible_to` ,
		`discount` ,
		`discount_from` ,
		`discount_to` ,
		`hash`
		)
		VALUES ( NULL, ?, ?, ?, ?, ?, ?, ?, '0', ?, '0', ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW(), ?, ?, ?, ?, ?, ?);";
    $stmt    = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('isssssidiissiiiissdsss', $status, $name, $keywords, $isbn, $description, $style, $top_offer, $price, $tax, $weight, $text, $number, $quantity, $quantity_warning, $condition, $guarantee, $visible_from, $visible_to, $discount, $discount_from, $discount_to, $hash);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    $id = $Cdb->get_insert_id();
    if($get_category == 0)
    {
        $save_id = $root_id;
    }
    else $save_id = $get_category;
    $Carticles->dir_add($save_id, $id, "A");
    $script .= $Cpage->load_page("articles.php", "article=$id&category=$get_category");

    // Hole die Default Werte:
    $sql    = "SELECT * FROM `".TBL_PREFIX."adm_default` WHERE `type` LIKE 'add_article';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($default_db_field = $result->fetch_assoc())
    {
        $default_field[$default_db_field['field']] = $default_db_field['value'];
    }

    // Update Artikelnummer, wenn Auto Artikelnummer aktiviert
    if($default_field['auto_number'] == TRUE)
    {
        $sql = "UPDATE `".TBL_PREFIX."counter` SET `count` = `count` + 1  WHERE `type` LIKE 'ARTICLE_COUNTER';";
        $Cdb->db_query($sql);
    }


}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function update_article()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $show_table;
    global $get_category;
    global $script;

    $article_post = array();
    $article_post = $Carticles->get_article_post();
    foreach($article_post as $key => $value)
    {
        $$key = $value;
    }

    $visible_from = $Cpage->get_timestamp($visible_from);
    $visible_from = $Cpage->get_sqltime($visible_from);

    $visible_to = $Cpage->get_timestamp($visible_to);
    $visible_to = $Cpage->get_sqltime($visible_to);

    $discount_from = $Cpage->get_timestamp($discount_from);
    $discount_from = $Cpage->get_sqltime($discount_from);

    $discount_to = $Cpage->get_timestamp($discount_to);
    $discount_to = $Cpage->get_sqltime($discount_to);

    // Wenn kein Klon
    if($clone_from == 0)
    {
        // Hole die eventuell bereits gespeicherten Bilder des Artikels
        $sql                = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$article;";
        $result             = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $article_field      = $result->fetch_assoc();
        $picture_old        = array();
        // Wandle die gespeicherten Daten in arrays um
        $picture_old        = unserialize($article_field['picture']);

        $picture_diff        = array();
        // Wenn Bilder gepostet wurden
        if($picture != NULL)
        {
            // Bestimme die Bilder, die gelöscht wurden
            $picture_diff        = array_diff($picture_old, $picture);
            // Lösche die betreffenden Bilder
            if(!empty($picture_diff))
            {
                foreach($picture_diff as $value)
                {
                    unlink(UPLOAD_DIR.$value);
                    unlink(UPLOAD_DIR."big/".$value);
                    unlink(UPLOAD_DIR."medium/".$value);
                    unlink(UPLOAD_DIR."small/".$value);
                    unlink(UPLOAD_DIR."tiny/".$value);
                }
            }
        } // Wenn keine Bilder gepostet wurden
        else
        {
            // Lösche alle gespeicherten Bilder, wenn vorhanden
            if(!empty($picture_old))
            {
                foreach($picture_old as $value)
                {
                    unlink(UPLOAD_DIR.$value);
                    unlink(UPLOAD_DIR."big/".$value);
                    unlink(UPLOAD_DIR."medium/".$value);
                    unlink(UPLOAD_DIR."small/".$value);
                    unlink(UPLOAD_DIR."tiny/".$value);
                }
            }
        }

        // Wandle arrays in Text um
        $picture        = serialize($picture);
        $picture_text   = serialize($picture_text);

        // Wenn nur der Brutto Preis angegeben wurde (Neuer Preis), dann bestimme Netto, weil nur Netto gespeichert wird
        if(empty($netto) || !empty($brutto))
        {
            $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][$tax]);
        }
        $sql  = "UPDATE `".TBL_PREFIX."articles` SET
		`status`          =  ? ,
		`name`            =  ? ,
		`keywords`        = ? ,
		`isbn`            = ? ,
		`description`     = ? ,
		`style`           = ? ,
		`top_offer`       =  ? ,
		`netto`           = ? ,
		`tax`             =  ? ,
		`weight`          =  ? ,
		`text`            = ? ,
		`picture`         = ? ,
		`picture_text`    = ? ,
		`number`          = ? ,
		`quantity`        =  ? ,
		`quantity_warning` = ? ,
		`condition`       =  ? ,
		`guarantee`       =  ? ,
		`changed`         =    NOW() ,
		`visible_from`    = ? ,
		`visible_to`      = ? ,
		`discount`        =  ? ,
		`discount_from`   = ? ,
		`discount_to`     = ?
		WHERE `id` = ?;";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('isssssidiissssiiiississi', $status, $name, $keywords, $isbn, $description, $style, $top_offer, $netto, $tax, $weight, $text, $picture, $picture_text, $number, $quantity, $quantity_warning, $condition, $guarantee, $visible_from, $visible_to, $discount, $discount_from, $discount_to, $article);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);


        // Attribute:
        $delete_attribute_id = $Cpage->get_parameter("delete_attribute_id", 0);
        if($delete_attribute_id > 0)
        {
            $sql = "DELETE FROM `".TBL_PREFIX."attributes` WHERE id=".$delete_attribute_id;
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }



        $attribute['header'][0] = $Cpage->get_parameter("attribute_header");
        $attribute['type'] = $Cpage->get_parameter("attribute_type", NULL, NO_ESCAPE);
        $attribute['size'] = $Cpage->get_parameter("attribute_size", NULL, NO_ESCAPE);
        $attribute['warning'] = $Cpage->get_parameter("attribute_warning", NULL, NO_ESCAPE);
        $attribute['netto'] = $Cpage->get_parameter("attribute_netto", NULL, NO_ESCAPE);
        $attribute['brutto'] = $Cpage->get_parameter("attribute_brutto", NULL, NO_ESCAPE);
        $attribute['pos'] = $Cpage->get_parameter("attribute_pos", NULL, NO_ESCAPE);
        $attribute['number'] = $Cpage->get_parameter("attribute_number", NULL, NO_ESCAPE);
        $attribute['attribute_id'] = $Cpage->get_parameter("attribute_id", NULL, NO_ESCAPE);


        foreach($attribute['type'] as $key => $value)
        {
            if(!empty($attribute['type'][$key]))
            {
                if(empty($attribute['attribute_id'][$key])) $attribute['attribute_id'][$key] = 0;
                if($attribute['attribute_id'][$key] != 0 && $attribute['attribute_id'][$key] == $delete_attribute_id) continue;

                if(!empty($attribute['brutto'][$key]))
                    $attribute['netto'][$key] = $Cpage->tax_calc($attribute['brutto'][$key], "netto", $Cpage->Aglobal['tax_percent'][$tax]);

                $sql = "SELECT id FROM `".TBL_PREFIX."attributes` WHERE id=".$attribute['attribute_id'][$key];
                $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 1)
                {

                    $sql = "UPDATE `".TBL_PREFIX."attributes`
                    SET
                    `header` = '".$attribute['header'][0]."',
                    `type` = '".$attribute['type'][$key]."',
                    `size` = '".$attribute['size'][$key]."',
                    `warning` = '".$attribute['warning'][$key]."',
                    `netto` = '".$attribute['netto'][$key]."',
                    `pos` = '".$attribute['pos'][$key]."',
                    `number` = '".$attribute['number'][$key]."'
                    WHERE
                    `id` = ".$attribute['attribute_id'][$key];
                    $Cdb->db_query($sql, __FILE__.":".__LINE__);
                }
                else
                {

                    $sql = "INSERT INTO `".TBL_PREFIX."attributes`
                    (`to_article`, `header`, `type`, `size`, `warning`, `netto`, `pos`, `number`)
                    VALUES ('$article', '".$attribute['header'][0]."', '".$attribute['type'][$key]."', '".$attribute['size'][$key]."', '".$attribute['warning'][$key]."', '".$attribute['netto'][$key]."', '".$attribute['pos'][$key]."', '".$attribute['number'][$key]."')
                    ";
                    $Cdb->db_query($sql, __FILE__.":".__LINE__);
                }
            }
        }


    } // Wenn Klon (Wenn Klon in der Kategorie verschoben wird)
    else
    {
        $sql  = "UPDATE `".TBL_PREFIX."articles` SET
		`status`          =  ? ,
		`changed`         =    NOW()
		WHERE `id` = ?;";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('ii', $status, $article);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }

    // Falls sich die Kategorie geändert hat:
    if($get_category != $category_target)
    {
        // Bestimme Typ, ob Original oder Klon
        $type = "A";
        if($clone_from != 0)
        {
            $type = "Ac";
        }
        // Entferne alten Eintrag
        $Carticles->dir_remove($get_category, $article, $type);
        // Wenn keine Hauptkategorie
        if($category_target != 0)
            // Füge neuen Eintrag hinzu
        {
            $Carticles->dir_add($category_target, $article, $type);
        }
        else
        {
            // Bestimme Root Knoten
            $first_id = $Carticles->start_id;
            // Füge in Root Knoten ein
            $Carticles->dir_add($first_id, $article, $type);
        }
    }

    // Falls Verlinkung hinzugefügt
    if($link_article_to_add > 0)
    {
        $sql = "SELECT id FROM `".TBL_PREFIX."articles_link` WHERE main_article=".$article;
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $pos = $result->num_rows;

        $sql = "INSERT INTO `".TBL_PREFIX."articles_link` (main_article, linked_article, pos) VALUES
        ('$article', '$link_article_to_add', '$pos');";
        $Cdb->db_query($sql, __FILE__.":".__LINE__);
    }

    $script .= $Cpage->load_page("articles.php", "article=$article&category=$category_target&show_table=$show_table");

    // Falls Verlinkung in Reihenfolge geändert
    if($old_link_pos > -1)
    {
        if($new_link_pos == -1)
            $new_link_pos = $max_link_pos-1;
        elseif($new_link_pos == $max_link_pos)
            $new_link_pos = 0;

        if($new_link_pos > $old_link_pos)
        {
            $sql = "SELECT id FROM `".TBL_PREFIX."articles_link` WHERE main_article=".$article." AND pos = ".$old_link_pos;
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $pos_tmp_id = $result->fetch_assoc();
            $pos_tmp_id = $pos_tmp_id['id'];

            $sql = "UPDATE `".TBL_PREFIX."articles_link` SET pos = pos - 1 WHERE pos > $old_link_pos AND pos <= $new_link_pos AND main_article=".$article;
            $Cdb->db_query($sql, __FILE__.":".__LINE__);

            $sql = "UPDATE `".TBL_PREFIX."articles_link` SET pos = $new_link_pos WHERE id=".$pos_tmp_id;
            $Cdb->db_query($sql, __FILE__.":".__LINE__);

        }
        elseif($new_link_pos < $old_link_pos)
        {
            $sql = "SELECT id FROM `".TBL_PREFIX."articles_link` WHERE main_article=".$article." AND pos = ".$old_link_pos;
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $pos_tmp_id = $result->fetch_assoc();
            $pos_tmp_id = $pos_tmp_id['id'];

            $sql = "UPDATE `".TBL_PREFIX."articles_link` SET pos = pos + 1 WHERE pos < $old_link_pos AND pos >= $new_link_pos AND main_article=".$article;
            $Cdb->db_query($sql, __FILE__.":".__LINE__);

            $sql = "UPDATE `".TBL_PREFIX."articles_link` SET pos = $new_link_pos WHERE id=".$pos_tmp_id;
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }

    // Falls Link gelöscht
    if($delete_link > -1)
    {
        $sql = "SELECT pos FROM `".TBL_PREFIX."articles_link` WHERE main_article=".$article." AND linked_article=".$delete_link;
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $pos_tmp = $result->fetch_assoc();
        $pos_tmp = $pos_tmp['pos'];

        $sql = "DELETE FROM `".TBL_PREFIX."articles_link` WHERE main_article=".$article." AND linked_article=".$delete_link;
        $Cdb->db_query($sql, __FILE__.":".__LINE__);

        if($pos_tmp < $max_link_pos-1)
        {
            $sql = "UPDATE `".TBL_PREFIX."articles_link` SET pos = pos -1 WHERE main_article=".$article." AND pos > $pos_tmp";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }


}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function update_category()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $get_category;
    global $script;


    // Hole die eventuell bereits gespeicherten Bilder der Kategorie
    $sql            = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$get_category;";
    $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $category_field = $result->fetch_assoc();

    $category_post = array();
    $category_post = $Carticles->get_category_post();
    foreach($category_post as $key => $value)
    {
        $$key = $value;
    }

    $visible_from = $Cpage->get_timestamp($visible_from);
    $visible_from = $Cpage->get_sqltime($visible_from);

    $visible_to = $Cpage->get_timestamp($visible_to);
    $visible_to = $Cpage->get_sqltime($visible_to);

    if(!empty($category_field['picture']) && empty($picture))
    {
        unlink(UPLOAD_DIR.$category_field['picture']);
        unlink(UPLOAD_DIR."big/".$category_field['picture']);
        unlink(UPLOAD_DIR."medium/".$category_field['picture']);
        unlink(UPLOAD_DIR."small/".$category_field['picture']);
        unlink(UPLOAD_DIR."tiny/".$category_field['picture']);
    }

    $sql  = "UPDATE `".TBL_PREFIX."categories` SET
	`status`          = ? ,
	`name`            = ? ,
	`keywords`        = ? ,
	`style`           = ? ,
	`picture`         = ? ,
	`picture_text`    = ? ,
	`changed`         = NOW() ,
	`visible_from`    = ? ,
	`visible_to`      = ?
	WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('isssssssi', $status, $name, $keywords, $style, $picture, $picture_text, $visible_from, $visible_to, $get_category);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    if($get_category != $category_target)
    {
        $Carticles->dir_remove_id($get_category, "C");
        if($category_target != 0)
        {
            $Carticles->dir_add($category_target, $get_category, "C");
        }
        else
        {
            $first_id = $Carticles->get_dir_root();
            $Carticles->dir_add($first_id, $get_category, "C");
        }
    }
    $script .= $Cpage->load_page("articles.php", "category=$get_category");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function clone_article()
{
    global $Cdb;
    global $Cpage;
    global $get_article;
    global $Carticles;
    global $script;

    // Füge neuen Eintrag hinzu mit Verweis auf den Original Artikel (clone_from = $get_article)
    $sql  = "INSERT INTO `".TBL_PREFIX."articles` (
    `status` ,
    `name` ,
    `clone_from` ,
    `changed` ,
    `created`
    )
    VALUES (
     '1', '(CLONE)', ?, NOW( ) , NOW( )
    );";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('i', $get_article);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    $article_insert_id = $Cdb->get_insert_id();
    // Bestimme ID der Kategorie in der der Original Artikel inne wohnt
    $category_id = $Carticles->get_category_id($get_article);
    // Füge Klon in Verzeichnis hinzu
    $Carticles->dir_add($category_id, $article_insert_id, "Ac");
    $script .= $Cpage->load_page("articles.php", "article=$article_insert_id");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


function copy_article()
{
    global $Cdb;
    global $Cpage;
    global $get_article;
    global $Carticles;
    global $script;

    $article_post = array();
    $article_post = $Carticles->get_article_post();
    foreach($article_post as $key => $value)
    {
        $$key = $value;
    }

    $picture_copy        = array();

    if(sizeof($picture) > 0)
    {
        foreach($picture as $value)
        {
            $filename = $Cpage->check_file($value, UPLOAD_DIR);
            copy(UPLOAD_DIR.$value, UPLOAD_DIR.$filename);
            copy(UPLOAD_DIR."big/".$value, UPLOAD_DIR."big/".$filename);
            copy(UPLOAD_DIR."medium/".$value, UPLOAD_DIR."medium/".$filename);
            copy(UPLOAD_DIR."small/".$value, UPLOAD_DIR."small/".$filename);
            copy(UPLOAD_DIR."tiny/".$value, UPLOAD_DIR."tiny/".$filename);
            array_push($picture_copy, $filename);
        }
    }

    // Wandle arrays in Text um
    $picture        = serialize($picture_copy);
    $picture_text   = serialize($picture_text);




    // Erstelle 1zu1 Kopie des Artikels
    $sql  = "INSERT INTO `".TBL_PREFIX."articles` (
	`id` ,
	`status` ,
	`name` ,
	`keywords` ,
	`isbn` ,
	`description` ,
	`style` ,
	`top_offer` ,
	`clone_from` ,
	`netto` ,
	`currency` ,
	`tax` ,
	`weight` ,
	`text` ,
	`picture` ,
	`picture_text` ,
	`number` ,
	`quantity` ,
	`quantity_warning` ,
	`condition` ,
	`guarantee` ,
	`changed` ,
	`created` ,
	`visible_from` ,
	`visible_to` ,
	`discount` ,
	`discount_from` ,
	`discount_to`
	)
	VALUES (
	NULL , '0', ?, ?, ?, ?, ?, ?, '', ?, '', ?, ?, ?, ?, ?,
	?, ?, ?, ?, ?, NOW( ) , NOW( ) , ?, ?, ?, ?, ?	);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $name = "Kopie - ".$name;
    $stmt->bind_param('sssssidiissssiiiississ', $name, $keywords, $isbn, $description, $style, $top_offer, $netto, $tax, $weight, $text, $picture, $picture_text, $number, $quantity, $quantity_warning, $condition, $guarantee, $visible_from, $visible_to, $discount, $discount_from, $discount_to);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    $article_insert_id = $Cdb->get_insert_id();
    $category_id       = $Carticles->get_category_id($get_article);
    $Carticles->dir_add($category_id, $article_insert_id, "A");



    // Attribute:

    $attribute['header'][0] = $Cpage->get_parameter("attribute_header");
    $attribute['type'] = $Cpage->get_parameter("attribute_type", NULL, NO_ESCAPE);
    $attribute['size'] = $Cpage->get_parameter("attribute_size", NULL, NO_ESCAPE);
    $attribute['warning'] = $Cpage->get_parameter("attribute_warning", NULL, NO_ESCAPE);
    $attribute['netto'] = $Cpage->get_parameter("attribute_netto", NULL, NO_ESCAPE);
    $attribute['brutto'] = $Cpage->get_parameter("attribute_brutto", NULL, NO_ESCAPE);
    $attribute['pos'] = $Cpage->get_parameter("attribute_pos", NULL, NO_ESCAPE);
    $attribute['number'] = $Cpage->get_parameter("attribute_number", NULL, NO_ESCAPE);

    foreach($attribute['type'] as $key => $value)
    {
        if(!empty($attribute['type'][$key]))
        {
            if(!empty($attribute['brutto'][$key]))
                $attribute['netto'][$key] = $Cpage->tax_calc($attribute['brutto'][$key], "netto", $Cpage->Aglobal['tax_percent'][$tax]);

            $sql = "INSERT INTO `".TBL_PREFIX."attributes`
                (`to_article`, `header`, `type`, `size`, `warning`, `netto`, `pos`, `number`)
                VALUES ('$article_insert_id', '".$attribute['header'][0]."', '".$attribute['type'][$key]."', '".$attribute['size'][$key]."', '".$attribute['warning'][$key]."', '".$attribute['netto'][$key]."', '".$attribute['pos'][$key]."', '".$attribute['number'][$key]."')
                ";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }



    $script .= $Cpage->load_page("articles.php", "article=$article_insert_id");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function delete_article()
{
    global $Cpage;
    global $get_article;
    global $get_category;
    global $script;

    func_delete_article($get_article);
    $script .= $Cpage->load_page("articles.php", "category=$get_category");

}

function func_delete_article($article_id)
{
    global $Cdb;
    global $Carticles;
    global $get_category;

    // Hole die eventuell bereits gespeicherten Bilder des Artikels
    $sql            = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$article_id;";
    $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $article_field  = $result->fetch_assoc();
    if($article_field['clone_from'] == 0)
    {
        // Lösche alle Klone
        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `clone_from` = $article_id;";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows > 0)
        {
            while($array = $result->fetch_assoc())
            {
                // Entferne Klone vom Verzeichnis
                $Carticles->dir_remove_id($array['id'], "Ac");
            }
            // Entgültiges Löschen des Klons
            $sql = "DELETE FROM `".TBL_PREFIX."articles` WHERE `clone_from` =$article_id;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }

        $picture        = array();
        // Wandle die gespeicherten Daten in arrays um
        $picture        = unserialize($article_field['picture']);
        // Lösche alle gespeicherten Bilder
        if(!empty($picture))
        {
            foreach($picture as $value)
            {
                unlink(UPLOAD_DIR.$value);
                unlink(UPLOAD_DIR."big/".$value);
                unlink(UPLOAD_DIR."medium/".$value);
                unlink(UPLOAD_DIR."small/".$value);
                unlink(UPLOAD_DIR."tiny/".$value);
            }
        }
    }
    // Lösche Original Artikel
    $sql = "DELETE FROM `".TBL_PREFIX."articles` WHERE `id` =$article_id;";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
    // Bestimme ob Hauptverzeichnis
    if(empty($get_category))
    {
        $category = $Carticles->get_dir_root();
    }
    else $category = $get_category;
    // Lösche aus Verzeichnis
    if($article_field['clone_from'] == 0)
        $Carticles->dir_remove($category, $article_id, "A");
    else
        $Carticles->dir_remove($category, $article_id, "Ac");

    // Lösche die Attribute, falls vorhanden
    $sql = "DELETE FROM `".TBL_PREFIX."attributes` WHERE to_article=".$article_id;
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function delete_category()
{
    global $Cdb;
    global $Cpage;
    global $get_category;
    global $Carticles;
    global $script;

    // Hole alle Elemente aus dem Verzeichnis der zu löschenden Kategorie
    $elements = $Carticles->dir_get_all_elements($get_category);

    // Gehe jedes Element durch
    foreach($elements as $key => $value)
    {
        // Wenn Artikel
        if($value['type'] == "A")
        {
            // Lösche Artikel
            func_delete_article($value['id']);
            continue;
        }
        // Wenn Klon
        if($value['type'] == "Ac")
        {
            // Lösche Klon
            $sql = "DELETE FROM `".TBL_PREFIX."articles` WHERE `id` =".$value['id']." ;";
            // Unterdrücke Fehlermeldungen, evtl. wurde ja Klon schon oben gelöscht
            @$Cdb->db_query($sql, __FILE__.":".__LINE__);
            continue;
        }
        // Wenn Kategorie
        if($value['type'] == "C")
        {
            // Bild löschen, falls vorhanden
            $sql            = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = ".$value['id'].";";
            $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $category_field = $result->fetch_assoc();
            if(!empty($category_field['picture']))
            {
                unlink(UPLOAD_DIR.$category_field['picture']);
                unlink(UPLOAD_DIR."big/".$category_field['picture']);
                unlink(UPLOAD_DIR."medium/".$category_field['picture']);
                unlink(UPLOAD_DIR."small/".$category_field['picture']);
                unlink(UPLOAD_DIR."tiny/".$category_field['picture']);
            }
            $sql = "DELETE FROM `".TBL_PREFIX."categories` WHERE `id` =".$value['id']." ;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }
    // Entferne die betreffende Kategorie aus dem Verzeichnis (Verzeichnis wird automatisch ermittelt)
    $Carticles->dir_remove_id($get_category, "C");
    $script .= $Cpage->load_page("articles.php");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function picture_article()
{
    global $Cpage;
    global $get_article;
    global $show_table;
    global $script;
    $picture_id = $Cpage->get_parameter("picture_id", -1);
    if($picture_id > -1)
    {
        $action = "modify_picture";
    }
    else $action = "";
    // Öffne Bilder Seite
    $script .= $Cpage->load_page("picture.php", "article=$get_article&show_table=$show_table&picture_id=$picture_id&do_action=$action");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function save_picture_article()
{
    global $Cpage;
    global $Cdb;
    global $get_article;
    global $show_table;
    global $script;

    $picture_id = $Cpage->get_parameter("picture_id", -1);

    // Hole eventuell bereits gespeicherte Bilder
    $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$get_article;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $article_field = $result->fetch_assoc();

    $Apicture        = array();
    $Apicture_text   = array();

    // Prüfe ob bereits Bilder gespeichert sind, erst dann die Arrays auslesen
    $tmp = unserialize($article_field['picture']);
    if(!empty($tmp))
    {
        $Apicture        = unserialize($article_field['picture']);
        $Apicture_text   = unserialize($article_field['picture_text']);
    }

    if($picture_id > -1)
    {
        $Apicture[$picture_id]        = $Cpage->get_parameter("picture");
        $Apicture_text[$picture_id]   = $Cpage->get_parameter("picture_text", "");
    }
    else
    {
        // Füge das neue Bild dem Array hinzu
        array_push($Apicture, $Cpage->get_parameter("picture"));
        array_push($Apicture_text, $Cpage->get_parameter("picture_text", ""));
    }

    // Speicher Array als Text
    $new_picture        = serialize($Apicture);
    $new_picture_text   = serialize($Apicture_text);

    // Speicher Artikel
    $sql  = "UPDATE `".TBL_PREFIX."articles` SET
		`picture`               =    ?,
		`picture_text`          =    ?,
		`changed`               =    NOW()
		WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ssi', $new_picture, $new_picture_text, $get_article);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    $script .= $Cpage->load_page("articles.php", "article=$get_article&show_table=$show_table");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function picture_category()
{
    global $Cpage;
    global $get_category;
    global $script;
    // Öffne Bilder Seite
    $script .= $Cpage->load_page("picture.php", "category=$get_category");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function save_picture_category()
{
    global $Cpage;
    global $Cdb;
    global $get_category;
    global $script;

    $picture        = $Cpage->get_parameter("picture");
    $picture_text   = $Cpage->get_parameter("picture_text", "");

    // Speicher Artikel
    $sql  = "UPDATE `".TBL_PREFIX."categories` SET
		`picture`               =    ?,
		`picture_text`          =    ?,
		`changed`               =    NOW()
		WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ssi', $picture, $picture_text, $get_category);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    $script .= $Cpage->load_page("articles.php", "category=$get_category");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function show_article()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $get_article;
    global $show_table;
    global $load_text_editor;
    global $load_box;
    global $Apath;
    global $script;
    global $script_footer;
    $content = "";

    $load_text_editor = TRUE;
    $load_box = TRUE;
    $uploaddir_picture = "upload/article_text/";
    $uploaddir_file = "download/";

    // Hole die Kategorien ID zur Artikel ID:
    $get_category = $Carticles->get_category_id($get_article);

    // Hole die Daten des Artikels
    $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$get_article;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $article_field = $result->fetch_assoc();
    $original_id = $article_field['id'];

    // Speichere Daten, falls es ein Klon ist
    $save_id       = $article_field['id'];
    $save_clone_id = $article_field['clone_from'];
    $save_created  = $article_field['created'];

    $is_clone = FALSE;
    // Wenn Klon
    if($article_field['clone_from'] != 0)
    {
        // Hole die Daten vom Klon
        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =".$article_field['clone_from'].";";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        // und überschreibe $article_field
        $article_field = $result->fetch_assoc();
        // Speichere gesichterte Daten zurück
        $original_id = $article_field['id'];
        $article_field['id']         = $save_id;
        $article_field['clone_from'] = $save_clone_id;
        $article_field['created']    = $save_created;
        $article_field['name'] .= " (Klon)";
        // Setze Schreibschutz auf Felder aktiv
        $is_clone = TRUE;
    }

    // Hole evtl. Verlinkungen

    $dont_show_link = array();
    $dont_show_link[] = $original_id;
    $sql = "SELECT * FROM `".TBL_PREFIX."articles_link` WHERE `main_article` =".$original_id." ORDER BY pos;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $article_links_array = NULL;
    if($result->num_rows > 0)
    {

        while($row = $result->fetch_assoc())
        {
            $article_links_array['status'][] = $Carticles->get_article_status($row['linked_article']);
            $article_links_array['linked_article'][] = $row['linked_article'];
            $dont_show_link[] = $row['linked_article'];
            $article_links_array['pos'][] = $row['pos'];
            $sql = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =".$row['linked_article'].";";
            $result2 = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $row2 = $result2->fetch_assoc();

                $article_links_array['name'][] = $row2['name'];

                $article_links_array['clone_from'][] = $row2['clone_from'];
                $article_links_array['netto'][] = $row2['netto'];
                $article_links_array['tax'][] = $row2['tax'];
                $article_links_array['number'][] = $row2['number'];

        }
    }


    // Hole evtl. Attribute

    $sql = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE `to_article` =".$original_id." ORDER BY pos ASC, type ASC;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($result->num_rows > 0)
    {
        if($result->num_rows == 1)
        {
            $attribute['id'][1] = "";
            $attribute['header'][1] = "";
            $attribute['type'][1] = "";
            $attribute['size'][1] = "";
            $attribute['warning'][1] = "";
            $attribute['netto'][1] = "0";
            $attribute['pos'][1] = "";
            $attribute['number'][1] = "";
            $row = $result->fetch_assoc();
            $attribute['id'][0] = $row['id'];
            $attribute['header'][0] = $row['header'];
            $attribute['type'][0] = $row['type'];
            $attribute['size'][0] = $row['size'];
            $attribute['warning'][0] = $row['warning'];
            $attribute['netto'][0] = $row['netto'];
            $attribute['pos'][0] = $row['pos'];
            $attribute['number'][0] = $row['number'];
        }
        else
        {
            $i = 0;
            while($row = $result->fetch_assoc())
            {
                $attribute['id'][$i] = $row['id'];
                $attribute['header'][$i] = $row['header'];
                $attribute['type'][$i] = $row['type'];
                $attribute['size'][$i] = $row['size'];
                $attribute['warning'][$i] = $row['warning'];
                $attribute['netto'][$i] = $row['netto'];
                $attribute['pos'][$i] = $row['pos'];
                $attribute['number'][$i] = $row['number'];
                $i++;
            }
        }

    }
    else
    {
        $attribute['id'][0] = "";
        $attribute['header'][0] = "";
        $attribute['type'][0] = "";
        $attribute['size'][0] = "";
        $attribute['warning'][0] = "";
        $attribute['netto'][0] = "0";
        $attribute['pos'][0] = "";
        $attribute['number'][0] = "";
        $attribute['id'][1] = "";
        $attribute['header'][1] = "";
        $attribute['type'][1] = "";
        $attribute['size'][1] = "";
        $attribute['warning'][1] = "";
        $attribute['netto'][1] = "0";
        $attribute['pos'][1] = "";
        $attribute['number'][1] = "";
    }



    $Apicture        = array();
    $Apicture_text   = array();

    // Hole Bilder
    if(!empty($article_field['picture']))
    {
        $Apicture        = unserialize($article_field['picture']);
        $Apicture_text   = unserialize($article_field['picture_text']);
    }

    $content .= "<div class='item_border'><div class='item_header'>
		<div class='item_point'><a href=\"javaScript:showTable('table_0');\">Artikel</a></div>
		<div class='item_point'><a href=\"javaScript:showTable('table_1');\">Beschreibung</a></div>
		<div class='item_point'><a href=\"javaScript:showTable('table_2');\">Bilder</a></div>
		<div class='item_point'><a href=\"javaScript:showTable('table_3');\">Attribute</a></div>
		<div class='item_point'><a href=\"javaScript:showTable('table_4');\">Verlinkungen</a></div>
		</div>
		<div class='item_content'>";

    $script .= "
    function showTable(table_id)
	{
		document.getElementById('table_0').style.display = 'none'; 
		document.getElementById('table_1').style.display = 'none'; 
		document.getElementById('table_2').style.display = 'none';
		document.getElementById('table_3').style.display = 'none';
		document.getElementById(table_id).style.display = 'block'; 
	}

	function delete_article(formID)
	{\n";

    if(!$is_clone)
    {
        $script .= "
	check = confirm('Wollen Sie diesen Artikel wirklich löschen?.');\n";
    }
    else
    {
        $script .= "
	check = confirm('Wollen Sie diesen Klon wirklich löschen?.');\n";
    }
    $script .= "
		if (check == true)
		{
			didChanges = false;
			formID.do_action.value = 'delete_article';
			formID.submit();
		}
	}

	var bild = new Array();
    bild[0] = 'Kein Bild';
	";

    // Bestimme die Bilder für Javascript
    if($is_clone)
    {
        $article_id_picture = $article_field['clone_from'];
    }
    else $article_id_picture = $get_article;
    if(sizeof($Apicture) > 0)
    {
        if(sizeof($Apicture) == 1)
        {
            $Cpage->load_image(UPLOAD_DIR."small/".$Apicture[0]);
            $script .= "   bild[0] = \"<span class='information'>Breite: ".$Cpage->get_image_width()." * Höhe: ".$Cpage->get_image_height()."<br>Typ: ".$Cpage->get_image_type()."<br><br><a href='picture_show.php?article=$article_id_picture&picture_id=0' target='_blank'><img src='".UPLOAD_DIR."small/".$Apicture[0]."'></a></span>\";\n";
        }
        else
        {
            for($i = 0; $i < sizeof($Apicture); $i++)
            {
                if($i == 0)
                {
                    $picture_back = (sizeof($Apicture)-1);
                }
                else $picture_back = $i-1;
                if($i == (sizeof($Apicture)-1))
                {
                    $picture_forward = 0;
                }
                else $picture_forward = $i+1;
                $Cpage->load_image(UPLOAD_DIR."small/".$Apicture[$i]);

                $script .= "   bild[$i] = \"<span class='big'><a href='#' onClick='document.getElementsByName(\\\"picture_show\\\")[0].innerHTML = bild[".$picture_back."];'>&#60;</a> Bild ".($i+1)." von ".(sizeof($Apicture))." <a href='#' onClick='document.getElementsByName(\\\"picture_show\\\")[0].innerHTML = bild[".$picture_forward."];'>></a></span><br><br><span class='information'>Breite: ".$Cpage->get_image_width()." * Höhe: ".$Cpage->get_image_height()."<br>Typ: ".$Cpage->get_image_type()."<br><br><a href='picture_show.php?article=$article_id_picture&picture_id=$i' target='_blank'><img src='".UPLOAD_DIR."small/".$Apicture[$i]."'></a></span>\";\n";
            }
        }
    }

    $script .= "
	// Verschiebe Bild
	function movePicture(picture, position, formID, tableID)
	{
		numpictures = ".sizeof($Apicture).";
		var bilder = new Array();
		for(var i=0; i < numpictures; i++)
		{
			bilder[i] = new Array();
		}
		var tempbild = new Array();
		for(var i=0; i < numpictures; i++)
		{
			bilder[i][0] = document.getElementsByName('bildA')[i].innerHTML; 
			bilder[i][1] = document.getElementsByName('bildB')[i].innerHTML;
		}

		if(position < 0) { position = numpictures-1; }
		else if(position >= numpictures) { position = 0; }

		tempbild = bilder[position];
		bilder[position] = bilder[picture];
		bilder[picture] = tempbild;

		for(var i=0; i < numpictures; i++)
		{
			document.getElementsByName('bildA')[i].innerHTML = bilder[i][0]; 
			document.getElementsByName('bildB')[i].innerHTML = bilder[i][1]; 
		}
		formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
	}

    // Lösche einzelnes Bild
    function deletePicture(selection, formID, tableID)
    {
        document.getElementsByName('bildA')[selection].innerHTML    = '';
        document.getElementsByName('bildB')[selection].innerHTML    = '';
		formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
    }

	function savePicture(formID, tableID)
	{
		formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}		
	}
	
    // Lösche einzelne Beschreibung
    function deleteText(selection, formID, tableID)
    {
        document.getElementsByName('textA')[selection].innerHTML    = '';
        document.getElementsByName('textB')[selection].innerHTML    = '';
		formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
    }	

	function saveDescription(formID, tableID)
	{
		formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
	}

	
	// Kopiere Artikel
	function copy_article(formID)
	{
		if(check_submit(formID))
		{
			formID.do_action.value = 'copy_article';
			formID.submit();
		}
	}
	
	// Füge Bilder hinzu
	function openPicturePage(formID, tableID, pictureID)
	{
		if(check_submit(formID))
		{
			formID.show_table.value = tableID;
			if(pictureID > -1)
			{
				formID.picture_id.value = pictureID;	
				formID.do_action.value = 'article_picture_modify';
			}
			else
			{
				formID.do_action.value = 'article_picture_add';
			}
			formID.submit();
		}
	}
	
	// Vorschau des Artikels
	function previewArticle(formID)
	{
		formID.target = '_blank';
		formID.target_page.value = 'article_preview.php';
		formID.submit();
		formID.target = '';
		formID.target_page.value = 'articles.php';
	}
	
	// Springe zum Original Artikel
	function gotoOriginal(formID)
	{
		formID.do_action.value='';
		formID.article.value='".$article_field['clone_from']."';
		formID.category.value = null;
		formID.submit();
	}

	function userDoNameChanges(formID)
	{
		count_length(formID.name, document.getElementsByName(\"name_count\")[0]);
		return true;
	}
	
	function count_length(elementID, targetID)
	{
		targetID.innerHTML = elementID.value.length;
		return true;
	}
	
    // Klone Artikel
	function clone_article(formID)
	{
		if(didChanges)
		{
			alert(\"Bitte vorher speichern! Sonst ist ein klonen nicht möglich.\");
			return false;
		}
		else
		{
			formID.do_action.value = 'clone_article';
			formID.submit();
		}
	}
	
	function check_submit(formID)
	{
         if ((formID.visible_from.value==null)||(formID.visible_from.value==\"\"))
         {
             formID.visible_from.value = \"0\";
         }
         if ((formID.visible_to.value==null)||(formID.visible_to.value==\"\"))
         {
             formID.visible_to.value = \"0\";
         }
         if ((formID.discount.value==null)||(formID.discount.value==\"\"))
         {
             formID.discount.value = \"0\";
         }
         if ((formID.discount_from.value==null)||(formID.discount_from.value==\"\"))
         {
             formID.discount_from.value = \"0\";
         }
         if ((formID.discount_to.value==null)||(formID.discount_to.value==\"\"))
         {
             formID.discount_to.value = \"0\";
         }
         if (isNaN(formID.discount.value))
         {
             alert(\"Bei Rabatt sind nur Zahlen erlaubt.\");
             formID.discount.focus();
             return false;
         }
  		if( ((formID.netto.value==null)||(formID.netto.value==\"\")) && ((formID.brutto.value==null)||(formID.brutto.value==\"\")) )
		{
			alert(\"Sie müssen einen Brutto oder Netto Preis angeben. Sie können auch 0 Euro eintragen.\");
			return false;
		}
		formID.netto.value = formID.netto.value.replace(/,/, \".\");
		formID.brutto.value = formID.brutto.value.replace(/,/, \".\");
		if(formID.name.value.length == 0)
		{
			alert(\"Sie müssen einen Artikelnamen vergeben.\");
			formID.name.focus();
			return false;
		}            

		return true;
	}    

	function reset_time(elementID)
	{
	    elementID.value = 0;
	}
	";

    $content .= $Cpage->form('article_edit_form', 'articles.php', 'update_article', 'return check_submit(this);').
        $Cpage->input_hidden("article", $get_article).
        $Cpage->input_hidden("category", $get_category).
        $Cpage->input_hidden("clone_from", $article_field['clone_from']).
        $Cpage->input_hidden("show_table", "table_0").
        $Cpage->input_hidden("picture_id", "-1").
        $Cpage->input_hidden("delete_attribute_id", "0").

        $Cpage->table('table_0', NO_CLASS, 0, 0, "display:block;")."
	<tr class='line_bottom'><td colspan='3'><strong>".$article_field['name']."</strong></td></tr>
	<tr class=''>
        <td colspan='3'>";
    if(!$is_clone)
    {
        $content .= $Cpage->input_submit("Änderungen speichern")." ".$Cpage->input_button("Kopie des Artikels anlegen", "copy_article(this.form);")." ".$Cpage->input_button("Artikel klonen", "clone_article(this.form);")." ".$Cpage->input_button("Artikel löschen", "delete_article(this.form)")."</td>\n";
    }
    if($is_clone)
    {
        $content .= $Cpage->input_submit("Änderungen speichern")." ".$Cpage->input_button("Gehe zu original Artikel", "gotoOriginal(this.form);")." ".$Cpage->input_button("Klon löschen", "delete_article(this.form);")."</td>\n";
    }
    $content .= "
	</tr>
	<tr class='line_bottom'>
        <td colspan='3'>Artikel verschieben nach: ".$Carticles->select_categories("category_target", $get_category)."</td>
	</tr>
	<tr class=''>
        <td>Artikelname:</td>
        <td>".$Cpage->input_text('name', $article_field['name'], 250, $is_clone, "onkeyup='userDoNameChanges(this.form);'")." <font class='information'><span name='name_count'></span> Zeichen lang</font></td>
        <td class='line_left td_line_bottom' rowspan='9' align='center' valign='top'><span name='picture_show'></span></td>
	</tr>
	<tr>
        <td>Such Wörter:</td>
        <td>".$Cpage->input_text('keywords', $article_field['keywords'], 250, $is_clone)."<br /><font class='information'>Mehrere durch Leerzeichen trennen</font></td>
	</tr>
	<tr>
        <td>ISBN:</td>
        <td>".$Cpage->input_text('isbn', $article_field['isbn'], 250, $is_clone)."</td>
	</tr>
	<tr>
        <td>Artikelnummer:</td>
        <td>".$Cpage->input_text('number', $article_field['number'], 150, $is_clone)."</td>
    </tr>
    <tr class='line_bottom'>
        <td><font class='information'>Datenbank ID:</font></td>
        <td><font class='information'>".$article_field['id']."</font></td>
	</tr>
	<tr class='line_bottom'>
        <td>Status:</td>";
    $inactive = FALSE;
    if(!$article_field['status'])
    {
        $inactive = TRUE;
    }
    $content .= "<td>".$Cpage->input_radio("status", 1, $article_field['status'])." Aktiv / ".$Cpage->input_radio("status", 0, $inactive)." Inaktiv</td>\n";
    $content .= "</tr>
	<tr>
        <td>Top Angebot:</td>
        <td>".$Cpage->select_multi('top_offer', $Cpage->Aglobal['answer'], $article_field['top_offer'], 0, 0, $is_clone)."</td>
	</tr>
	<tr class=''>
        <td>Angelegt:</td>
        <td>".$Cpage->input_text('created', $Cpage->format_time($article_field['created']), 180, READONLY)."</td>
	</tr>
	<tr class='line_bottom'>
        <td>Geändert:</td>";
    if($article_field['created'] == $article_field['changed'])
    {
        $changed = "";
    }
    else $changed = $article_field['changed'];
    $content .= "<td>".$Cpage->input_text('changed', $Cpage->format_time($changed), 180, READONLY)."</td>
	</tr>
	<tr class=''>
        <td>Preis Netto:</td>
        <td colspan='2'>".$Cpage->input_text('netto', $Cpage->money($article_field['netto'], NO_CURRENCY_SYMBOL), 60, $is_clone, "onkeyup='document.article_edit_form.brutto.value=\"\";'")." €</td>
	</tr>
	<tr>
        <td>MwSt.:</td>
        <td colspan='2'>".$Cpage->select_multi('tax', $Cpage->Aglobal['tax_percent'], $article_field['tax'], 0, 0, $is_clone)."</td>
	</tr>
	<tr class='line_bottom'>
        <td>Preis Brutto:</td>
        <td colspan='2'>".$Cpage->input_text('brutto', $Cpage->money($Cpage->tax_calc($article_field['netto'], "brutto", $Cpage->Aglobal['tax'][$article_field['tax']]), NO_CURRENCY_SYMBOL), 60, $is_clone, "onkeyup='document.article_edit_form.netto.value=\"\";'")." €</td>
	</tr>";

    $content .= "
		<tr";
    if($article_field['quantity'] < $article_field['quantity_warning'])
    {
        $content .= " class='low_quantity_warning_td'";
    }
    $content .= ">
	        <td class=''>Auf Lager:</td>
	        <td colspan='2' class=''>".$Cpage->input_text('quantity', $article_field['quantity'], 40, $is_clone)." Stück</td>
	    </tr>
		<tr";
    if($article_field['quantity'] < $article_field['quantity_warning'])
    {
        $content .= " class='low_quantity_warning_td'";
    }
    $content .= ">
	        <td class=''>Warnung unter:</td>
	        <td colspan='2' class=''>".$Cpage->input_text('quantity_warning', $article_field['quantity_warning'], 40, $is_clone)." Stück übrig <span class='information'>Der Wert 0 deaktiviert die Lagerbestandsverwaltung</span></td>
	    </tr>
	    <tr>
	    <tr class='line_bottom'>
	        <td";
    if(($article_field['weight'] == 0) && (DELIVERY_METHOD == "weight"))
    {
        $content .= " class='light_warning_td'";
    }
    $content .= ">Gewicht:</td>
	        <td";
    if(($article_field['weight'] == 0) && (DELIVERY_METHOD == "weight"))
    {
        $content .= " class='light_warning_td'";
    }
    $content .= " colspan='2'>".$Cpage->input_text('weight', $article_field['weight'], 100, $is_clone)." Gramm</td>
		</tr>";

    $content .= "
	<tr class=''>
        <td>Zustand:</td>
        <td colspan='2'>".$Cpage->select_multi('condition', $Cpage->Aglobal['condition'], $article_field['condition'], 0, 0, $is_clone)."</td>
    </tr>
    <tr class='line_bottom'>
        <td>Garantie:</td>
        <td colspan='2'>".$Cpage->select_multi('guarantee', $Cpage->Aglobal['guarantee'], $article_field['guarantee'], 0, 0, $is_clone)."</td>
	</tr>
	<tr class='line_bottom'>
        <td>Sichtbar:</td>
        <td colspan='2'>von ".$Cpage->input_text('visible_from', $Cpage->format_time($article_field['visible_from'], "sql"), 120, $is_clone, "id='visible_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_from);")."
        bis ".$Cpage->input_text('visible_to', $Cpage->format_time($article_field['visible_to'], "sql"), 120, $is_clone, "id='visible_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_to);")."<br /><font class='information'>0 = Immer, sonst Format: TT.MM.JJJJ HH:MM</font></td>
	</tr>
	<tr class='line_bottom";
	if($article_field['discount'] > 0) $content .= " warning_tr";
	$content .= "'>
        <td>Rabatt:</td>
        <td colspan='2'>".$Cpage->input_text('discount', $article_field['discount'], 40, $is_clone)."%
        von ".$Cpage->input_text('discount_from', $Cpage->format_time($article_field['discount_from'], "sql"), 120, $is_clone, "id='discount_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.discount_from);")."
        bis ".$Cpage->input_text('discount_to', $Cpage->format_time($article_field['discount_to'], "sql"), 120, $is_clone, "id='discount_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.discount_to);")."</td>
	</tr>
	<tr class=''>
        <td>Stil Artikelnamen:</td>
        <td colspan='2'>".$Cpage->input_text('style', $article_field['style'], 250, $is_clone)."<br /><font class='information'>CSS Font Style, siehe <a href='http://de.selfhtml.org/css/eigenschaften/schrift.htm' target='_blank'>de.selfhtml.org</a>, z.B. font-weight:bold;</font></td>
	</tr>
	</table>";

    // Beschreibungen:
    $content .= $Cpage->table('table_1', NO_CLASS, 0, 0, "display:none;")."
	<tr class='line_bottom'><td colspan='3'><strong>".$article_field['name']."</strong></td></tr>
	<tr class='line_bottom'>
        <td colspan='3'>";
    if(!$is_clone)
    {
        $content .= $Cpage->input_button("Änderungen speichern", "saveDescription(this.form, \"table_1\");")."</td>\n";
    }
    if($is_clone)
    {
        $content .= "</td>\n";
    }
    $content .= "
	</tr>
	<tr class='line_bottom'>
		<td valign='top'>Kurzbeschreibung:</td>
		<td>".$Cpage->textarea("description", 400, 120, $is_clone).$article_field['description']."</textarea></td>
		<td class='D2'><font class='information'>Jede Kurzbeschreibung ist durch<br />eine neue Zeile zu trennen</font></td>
	</tr>";
    $content .= "
		<tr class=''>
			<td valign='top'>Beschreibung:</td>
			<td colspan='2'>".$Cpage->textarea("text", 650, 400, $is_clone, "textarea", SUBMIT_CHANGE, "id='text'").$article_field['text']."</textarea><br />
            ".$Cpage->link("Bild einfügen", $Apath['root']."picture_upload.php", "KeepThis=true&uploaddir=$uploaddir_picture&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Datei einfügen", $Apath['root']."file_upload.php", "KeepThis=true&uploaddir=$uploaddir_file&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."
            ".$Cpage->link("Artikel Link einfügen", $Apath['root']."article_add.php", "KeepThis=true&TB_iframe=true&height=400&width=650", "thickbox link_button", NO_STYLE, DO_SID, NO_CHANGE)."

			</td>
		</tr>";

    $content .= "</table>";

    // Bilder:
    $content .= $Cpage->table('table_2', NO_CLASS, 0, 0, "display:none;")."<tr class='line_bottom'><td colspan='3'><strong>".$article_field['name']."</strong></td></tr>";
    if(!$is_clone)
    {
        $content .= "
    <tr class='R9 R10'>
    	<td colspan='3'>".$Cpage->input_button("Änderungen speichern", "savePicture(this.form, \"table_2\")")." ".$Cpage->input_button("Bild hinzufügen", "openPicturePage(this.form, \"table_2\", \"-1\");")."</td>
    </tr>";
    }

    for($i = 0; $i < sizeof($Apicture); $i++)
    {
        $content .= "
        <tr name='bildA' class='line_top'>
	        <td>Bild: ".$Cpage->input_text('picture[]', $Apicture[$i], 300, READONLY)."</td>
	        <td rowspan='2' align='center'><img src='".UPLOAD_DIR."tiny/".$Apicture[$i]."'></td>
	        <td>";
        if(!$is_clone)
        {
            if(sizeof($Apicture) > 1)
            {
                $content .= $Cpage->input_button("Nach oben", "movePicture($i, ($i-1), this.form, \"table_2\");")." ";
            }
            $content .= $Cpage->input_button("Ändern", "openPicturePage(this.form, \"table_2\", $i);");
        }
        $content .= "</td>
        </tr>
        <tr class='R10' name='bildB'>
	        <td>Text: ".$Cpage->input_text('picture_text[]', $Apicture_text[$i], 300, $is_clone)."</td>
	        <td>";
        if(!$is_clone)
        {
            if(sizeof($Apicture) > 1)
            {
                $content .= $Cpage->input_button("Nach unten", "movePicture($i, ($i+1), this.form, \"table_2\");")." ";
            }
            $content .= $Cpage->input_button("Löschen", "deletePicture($i, this.form, \"table_2\");");
        }
        $content .= "</td>
        </tr>
        ";
    }
    $content .= "</table>";


    // Attribute:
    $content .= $Cpage->table('table_3', NO_CLASS, 0, 0, "display:none;")."<tr class='line_bottom'><td colspan='9'><strong>".$article_field['name']."</strong></td></tr>";
    if(!$is_clone)
    {
        $content .= "
    <tr class='R9 R10'>
    	<td colspan='9'>".$Cpage->input_button("Änderungen speichern", "saveAttribute(this.form, \"table_3\")")." <span id='add_attribute_button'>".$Cpage->input_button("Attribut hinzufügen", "addAttribute(this.form);")."</span></td>
    </tr>";
    }

    $content .= "
    <tr>
     <td></td>
     <td colspan='8'><span class='information'>Geben Sie mindestens 2 Attribute an</span></td>
    </tr>

    <tr class='line_bottom'>
     <td>Überschrift:</td>
     <td colspan='8'>".$Cpage->input_text('attribute_header', $attribute['header'][0], 300, $is_clone)."</td>
    </tr>


    <tr>
     <td>Bezeichnung:</td>
     <td>".$Cpage->input_hidden('attribute_id[]', $attribute['id'][0]).$Cpage->input_text('attribute_type[]', $attribute['type'][0], 200, $is_clone)."</td>
     <td>Menge:</td>
     <td>".$Cpage->input_text('attribute_size[]', $attribute['size'][0], 40, $is_clone)."</td>
     <td>Netto Differenz:</td>
     <td>".$Cpage->input_text('attribute_netto[]', $Cpage->money($attribute['netto'][0], NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_brutto[]\"][0].value=\"\";'")." ".CURRENCY."</td>
     <td>Position:</td>
     <td>".$Cpage->input_text('attribute_pos[]', $attribute['pos'][0], 40, $is_clone)."</td>
     <td>";
        if(!$is_clone) $content .= $Cpage->input_button("Löschen", "deleteAttribute(\"".$attribute['id'][0]."\", this.form, \"table_3\")");
    $content .= "</td>
    </tr>
    <tr class='line_bottom'>
     <td></td>
     <td></td>
     <td>Warnung ab:</td>
     <td>".$Cpage->input_text('attribute_warning[]', $attribute['warning'][0], 40, $is_clone)."</td>
     <td>Brutto Differenz:</td>
     <td>".$Cpage->input_text('attribute_brutto[]', $Cpage->money($Cpage->tax_calc($attribute['netto'][0], "brutto", $Cpage->Aglobal['tax_percent'][$article_field['tax']]), NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_netto[]\"][0].value=\"\";'")." ".CURRENCY."</td>
     <td>";
    if(USE_ARTICLE_NUMBER)
    $content .= "Artikel Nummer:";
    $content .= "</td>
     <td>";
    if(USE_ARTICLE_NUMBER)
        $content .= $Cpage->input_text('attribute_number[]', $attribute['number'][0], 80, $is_clone);
    $content .= "</td>
     <td></td>
    </tr>

    <tr>
     <td>Bezeichnung:</td>
     <td>".$Cpage->input_hidden('attribute_id[]', $attribute['id'][1]).$Cpage->input_text('attribute_type[]', $attribute['type'][1], 200, $is_clone)."</td>
     <td>Menge:</td>
     <td>".$Cpage->input_text('attribute_size[]', $attribute['size'][1], 40, $is_clone)."</td>
     <td>Netto Differenz:</td>
     <td>".$Cpage->input_text('attribute_netto[]', $Cpage->money($attribute['netto'][1], NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_brutto[]\"][1].value=\"\";'")." ".CURRENCY."</td>
     <td>Position:</td>
     <td>".$Cpage->input_text('attribute_pos[]', $attribute['pos'][1], 40, $is_clone)."</td>
     <td>";
    if(!$is_clone) $content .= $Cpage->input_button('Löschen', "deleteAttribute(\"".$attribute['id'][1]."\", this.form, \"table_3\")");
     $content .= "</td>
    </tr>
    <tr class='line_bottom'>
     <td></td>
     <td></td>
     <td>Warnung ab:</td>
     <td>".$Cpage->input_text('attribute_warning[]', $attribute['warning'][1], 40, $is_clone)."</td>
     <td>Brutto Differenz:</td>
     <td>".$Cpage->input_text('attribute_brutto[]', $Cpage->money($Cpage->tax_calc($attribute['netto'][1], "brutto", $Cpage->Aglobal['tax_percent'][$article_field['tax']]), NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_netto[]\"][1].value=\"\";'")." ".CURRENCY."</td>
     <td>";
    if(USE_ARTICLE_NUMBER)
        $content .= "Artikel Nummer:";
    $content .= "</td>
     <td>";
    if(USE_ARTICLE_NUMBER)
        $content .= $Cpage->input_text('attribute_number[]', $attribute['number'][1], 80, $is_clone);
    $content .= "</td>
     <td></td>
    </tr>
   ";

    for($i=2; $i<sizeof($attribute['type']); $i++)
    {

        $content .= "
        <tr>
         <td>Bezeichnung:</td>
         <td>".$Cpage->input_hidden('attribute_id[]', $attribute['id'][$i]).$Cpage->input_text('attribute_type[]', $attribute['type'][$i], 200, $is_clone)."</td>
         <td>Menge:</td>
         <td>".$Cpage->input_text('attribute_size[]', $attribute['size'][$i], 40, $is_clone)."</td>
         <td>Netto Differenz:</td>
         <td>".$Cpage->input_text('attribute_netto[]', $Cpage->money($attribute['netto'][$i], NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_brutto[]\"][$i].value=\"\";'")." ".CURRENCY."</td>
         <td>Position:</td>
         <td>".$Cpage->input_text('attribute_pos[]', $attribute['pos'][$i], 40, $is_clone)."</td>
         <td>";
        if(!$is_clone) $content .= $Cpage->input_button('Löschen', "deleteAttribute(\"".$attribute['id'][$i]."\", this.form, \"table_3\")");
        $content .="</td>
        </tr>
        <tr class='line_bottom'>
         <td></td>
         <td></td>
         <td>Warnung ab:</td>
         <td>".$Cpage->input_text('attribute_warning[]', $attribute['warning'][$i], 40, $is_clone)."</td>
         <td>Brutto Differenz:</td>
         <td>".$Cpage->input_text('attribute_brutto[]', $Cpage->money($Cpage->tax_calc($attribute['netto'][$i], "brutto", $Cpage->Aglobal['tax_percent'][$article_field['tax']]), NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\"attribute_netto[]\"][$i].value=\"\";'")." ".CURRENCY."</td>
     <td>";
        if(USE_ARTICLE_NUMBER)
            $content .= "Artikel Nummer:";
        $content .= "</td>
     <td>";
        if(USE_ARTICLE_NUMBER)
            $content .= $Cpage->input_text('attribute_number[]', $attribute['number'][$i], 80, $is_clone);
        $content .= "</td>
         <td></td>
        </tr>
       ";
    }
    $content .= "<tr id='add_attribute1'><td colspan='9'></td></tr>";
    $content .= "<tr id='add_attribute2'><td colspan='9'></td></tr>";
    $content .= "</table>";






    // Verlinkungen:
    $content .=
        $Cpage->input_hidden("old_link_pos", "-2").$Cpage->input_hidden("new_link_pos", "-2").$Cpage->input_hidden("delete_link", "-1").
        $Cpage->input_hidden("max_link_pos", sizeof($article_links_array['linked_article'])).
        $Cpage->table('table_4', NO_CLASS, 0, 0, "display:none;")."<tr class='line_bottom'><td colspan='9'><strong>".$article_field['name']."</strong></td></tr>";
    if(!$is_clone)
    {
        $content .= "
    <tr class='R9 R10 line_bottom'>
    	<td colspan='8'>".$Carticles->get_articles_select("link_article_to_add", "0", $dont_show_link)."
    	<span id='add_attribute_button'>".$Cpage->input_button("Verlinkung hinzufügen", "addLink(this.form, \"table_4\");")."</span></td>
    </tr>";
    }

    if(isset($article_links_array))
    {
        for($i=0; $i<sizeof($article_links_array['linked_article']); $i++)
        {

            $content .= "
            <tr class='line_bottom'>
             <td>Verlinkter Artikel:</td>
             <td>".$Cpage->input_text('dummy1', $article_links_array['name'][$i], 200, READONLY)."</td>
             <td>Status:</td>
             <td>";

            switch($article_links_array['status'][$i])
            {
                case "0": $content .= "Sichtbar";
                    break;
                case "1": $content .= "Artikel deaktiviert";
                    break;
                case "2": $content .= "Artikel nicht sichtbar";
                    break;
                case "3": $content .= "Kategorie unsichtbar";
                    break;
                default: $content .= "Fehler";
                    break;
            }

             $content .= "</td>
             <td>Brutto:</td>
             <td>".$Cpage->input_text('dummy2', $Cpage->money($Cpage->tax_calc($article_links_array['netto'][$i], "brutto", $article_links_array['tax'][$i])) , 60, READONLY, "", "", "input_right")."</td>

             <td>";
            if(!$is_clone) $content .= $Cpage->input_button('Löschen', "deleteLink(\"".$article_links_array['linked_article'][$i]."\", this.form, \"table_4\")");
            $content .="</td><td>";
            if(!$is_clone)
            {
                if(sizeof($article_links_array['linked_article']) > 1)
                {
                    $content .= $Cpage->input_button("Nach oben", "moveLink($i, ($i-1), this.form, \"table_4\");")." ";
                    $content .= $Cpage->input_button("Nach unten", "moveLink($i, ($i+1), this.form, \"table_4\");");
                }
            }
            $content .= "</td>
            </tr>
           ";
        }
    }
    $content .= "</table></form>";









    if(sizeof($attribute['type']) == 0)
        $i=2;
    else
        $i=sizeof($attribute['type']);

    $delete_attribute_text = "";
    if(USE_ARTICLE_NUMBER)
    {
        $number_attribute_text1 = "Artikel Nummer:";
        $number_attribute_text2 = $Cpage->input_text('attribute_number[]', "", 80, $is_clone);
    }
    else
    {
        $number_attribute_text1 = "";
        $number_attribute_text2 = "";
    }

    $script_footer .= $Cpage->datepicker("visible_from");
    $script_footer .= $Cpage->datepicker("visible_to");
    $script_footer .= $Cpage->datepicker("discount_from");
    $script_footer .= $Cpage->datepicker("discount_to");

    $script_footer .= "

    function deleteAttribute(attributeID, formID, tableID)
    {
        formID.elements[\"delete_attribute_id\"].value = attributeID;
        formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
    }

    function saveAttribute(formID, tableID)
    {
        formID.show_table.value = tableID;
		if(check_submit(formID))
		{
			formID.submit();
		}
    }

    function addLink(formID, tableID)
    {

		if(check_submit(formID))
		{
			formID.show_table.value = tableID;
			formID.do_action.value = 'add_link';
			formID.submit();
		}
    }

    function deleteLink(linkID, formID, tableID)
    {

		if(check_submit(formID))
		{
			formID.show_table.value = tableID;
			formID.do_action.value = 'delete_link';
			formID.delete_link.value = linkID;
			formID.submit();
		}
    }

    function moveLink(old_position, new_position, formID, tableID)
    {
		if(check_submit(formID))
		{
			formID.show_table.value = tableID;
			formID.do_action.value = 'move_link';
			formID.old_link_pos.value = old_position;
			formID.new_link_pos.value = new_position;
			formID.submit();
		}
    }

    function addAttribute(formID)
    {
        document.getElementById('add_attribute_button').innerHTML = '<span class=\"information\">Speichern Sie, um ein weiteres Attribut hinzuzufügen</span>';
        document.getElementById('add_attribute1').innerHTML = \"<tr>\" +
         \"<td>Bezeichnung:</td>\" +
         \"<td>".$Cpage->input_text('attribute_type[]', "", 200, $is_clone)."</td>\" +
         \"<td>Menge:</td>\" +
         \"<td>".$Cpage->input_text('attribute_size[]', "", 40, $is_clone)."</td>\" +
         \"<td>Netto Differenz:</td>\" +
         \"<td>".$Cpage->input_text('attribute_netto[]', $Cpage->money("0", NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\\\"attribute_brutto[]\\\"][$i].value=\\\"\\\";'")." ".CURRENCY."</td>\" +
         \"<td>Position:</td>\" +
         \"<td>".$Cpage->input_text('attribute_pos[]', "", 40, $is_clone)."</td>\" +
         \"<td>".$delete_attribute_text."</td>\" +
        \"</tr>\";
        document.getElementById('add_attribute2').innerHTML = \"<tr class='line_bottom'>\" +
         \"<td>".$Cpage->input_hidden('attribute_id[]', "0")."</td>\" +
         \"<td></td>\" +
         \"<td>Warnung ab:</td>\" +
         \"<td>".$Cpage->input_text('attribute_warning[]', "", 40, $is_clone)."</td>\" +
         \"<td>Brutto Differenz:</td>\" +
         \"<td>".$Cpage->input_text('attribute_brutto[]', $Cpage->money("0", NO_CURRENCY_SYMBOL), 65, $is_clone, "onkeyup='document.article_edit_form.elements[\\\"attribute_netto[]\\\"][$i].value=\\\"\\\";'")." ".CURRENCY."</td>\" +
         \"<td>$number_attribute_text1</td>\" +
         \"<td>$number_attribute_text2</td>\" +
         \"<td></td>\" +
        \"</tr>\";

    }


	count_length(document.article_edit_form.name, document.getElementsByName('name_count')[0]);
    document.getElementsByName('picture_show')[0].innerHTML = bild[0];
	document.getElementById('table_0').style.display = 'none'; 
	document.getElementById('table_1').style.display = 'none'; 
	document.getElementById('table_2').style.display = 'none';
	document.getElementById('table_3').style.display = 'none';
	document.getElementById('table_4').style.display = 'none';
	document.getElementById('".$show_table."').style.display = 'block';
	
	";

    $content .= "</div></div>\n";
    return $content;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

function show_category()
{
    global $Cdb;
    global $Cpage;
    global $Carticles;
    global $get_category;
    global $script;
    global $script_footer;

    $sql            = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` = ".$get_category.";";
    $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $category_field = $result->fetch_assoc();

    $content = "<div class='item_border'><div class='item_header'>Kategorie</div>
		<div class='item_content'>\n";

    $script .= "
	function delete_category(formID)
	{
		check = confirm('Wollen Sie diese Kategorie wirklich löschen? Diese, betreffende Artikel, evtl. Unterkategorien und Bilder werden dann auch gelöscht. Alle Artikel dieser Kategorie werden ebenfalls gelöscht.');
		if (check == true)
		{
			check = prompt('Geben Sie bitte den Sicherheitscode 123 ein', '');
			if (check == '".SECURITY_CODE."')
			{
				didChanges = false;
				formID.do_action.value = 'delete_category';
				formID.submit();
			}
			else alert('Falscher Sicherheitscode.');
		}
	}
	
	function openPicturePage(formID)
	{
		if(check_submit(formID))
		{
			formID.do_action.value = 'category_picture_modify';
			formID.submit();
		}
	}
	
	function deletePicture(formID)
	{
		formID.picture.value = '';
		formID.picture_text.value = '';
		document.getElementsByName('picture_show')[0].innerHTML = '';
		formID.use_categoryname_for_picture.checked = false;
		changesDone();
	}
	
	function userDoNameChanges(formID)
	{
		count_length(formID.name, document.getElementsByName(\"name_count\")[0]);
		return true;
	}
	
	function count_length(elementID, targetID)
	{
		targetID.innerHTML = elementID.value.length;
		return true;
	}
	
	function check_submit(formID)
	{
         if ((formID.visible_from.value==null)||(formID.visible_from.value==\"\"))
         {
             formID.visible_from.value = 0;
         }
         if ((formID.visible_to.value==null)||(formID.visible_to.value==\"\"))
         {
             formID.visible_to.value = 0;
         }
		if(formID.name.value.length == 0)
		{
			alert(\"Sie müssen einen Kategoriennamen vergeben.\");
			formID.name.focus();
			return false;
		}
		
		return true;
	}

	function reset_time(elementID)
	{
	    elementID.value = 0;
	}
	";

    if(!empty($category_field['picture']))
    {
        $Cpage->load_image(UPLOAD_DIR."small/".$category_field['picture']);
        $image_info = "<font class='information'>Breite: ".$Cpage->get_image_width()." * Höhe: ".$Cpage->get_image_height()."<br>Typ: ".$Cpage->get_image_type()."<br><br><a href='picture_show.php?category=".$get_category."' target='_blank'><img src='".UPLOAD_DIR."small/".$category_field['picture']."'></a></font>";
    }
    else $image_info = "Kein Bild";

    $content .= $Cpage->form('category_edit_form', 'articles.php', 'update_category', 'return check_submit(this);').$Cpage->input_hidden("category", $get_category).$Cpage->table()."
	<tr class='line_bottom'>
		<td colspan='3'><strong>".$category_field['name']."</strong></td>
	</tr>
	<tr class=''>
		<td colspan='3'>".$Cpage->input_submit("Änderungen speichern")." ".$Cpage->input_button("Kategorie löschen (!)", "delete_category(this.form);")."</td>
	</tr>";

    $content .= "<tr class='line_bottom'>
		<td>Kategorie verschieben nach:</td><td colspan='4'>".$Carticles->select_categories("category_target", $get_category, REMOVE_SUB)."</td>
	</tr>";
    $content .= "
	<tr class=''>
		<td>Kategorienname:</td>
		<td>".$Cpage->input_text('name', $category_field['name'], 250, NOT_READONLY, "onkeyup='userDoNameChanges(document.category_edit_form);'")." <font class='information'><span name='name_count'></span> Zeichen lang</font></td>
		<td class='line_left td_line_bottom' rowspan='8' valign='top' align='center'><span name='picture_show'>".$image_info."</span></td>
	</tr>
	<tr class='line_bottom'>
		<td>Such Wörter:</td>
		<td>".$Cpage->input_text('keywords', $category_field['keywords'], 150)." <font class='information'><span name='name_count'></span> Mehrere durch Leerzeichen trennen</font></td>
	</tr>        
	<tr>
		<td>Status:</td>";
    $inaktive = 1;
    if($category_field['status'])
    {
        $inaktive = 0;
    }
    $content .= "
	    <td>".$Cpage->input_radio("status", 1, $category_field['status'])." Aktiv / ".$Cpage->input_radio("status", 0, $inaktive)." Inaktiv</td>
	</tr>
	<tr>
		<td>Angelegt:</td>
		<td>".$Cpage->input_text('created', $Cpage->format_time($category_field['created']), 200, READONLY)."</td>
	</tr>
	<tr>
		<td>Geändert:</td>";
    if($category_field['created'] == $category_field['changed'])
    {
        $changed = "";
    }
    else $changed = $category_field['changed'];
    $content .= "
		<td>".$Cpage->input_text('changed', $Cpage->format_time($changed), 200, READONLY)."</td>
	</tr>
	<tr class='line_bottom'>
		<td><font class='information'>Datenbank ID:</font></td>
		<td><font class='information'>".$category_field['id']."</font></td>
	</tr>
	<tr class=''>
		<td>Bild:</td>
		<td>".$Cpage->input_text('picture', $category_field['picture'], 150, READONLY)."
		".$Cpage->input_button("Ändern", "openPicturePage(this.form);")." ".$Cpage->input_button("Löschen", "deletePicture(this.form);")."</td>
	</tr>
	<tr class='line_bottom'>
		<td>Bild Text:</td>
		<td>".$Cpage->input_text('picture_text', $category_field['picture_text'], 150)."</td>";
    $content .= "
	</tr>
     <tr class=''>
         <td>Sichtbar:</td>
         <td colspan='2'>von ".$Cpage->input_text('visible_from', $Cpage->format_time($category_field['visible_from'], "sql"), 120, NOT_READONLY, "id='visible_from'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_from);")."
         bis ".$Cpage->input_text('visible_to', $Cpage->format_time($category_field['visible_to'], "sql"), 120, NOT_READONLY, "id='visible_to'")." ".$Cpage->input_button("Reset", "reset_time(this.form.visible_to);")."
         </td>
     </tr>
     <tr>
         <td></td>
         <td colspan='2'><font class='information'>0 = Immer, sonst Format: TT.MM.JJJJ HH:MM</font></td>
     </tr>
     <tr>
		<td>Stil Kategorienname:</td>
		<td colspan='2'>".$Cpage->input_text('style', $category_field['style'], 250)."</td>
	</tr>
	<tr>
		<td></td>
		<td colspan='2'><font class='information'>= CSS Font Style, siehe <a href='http://de.selfhtml.org/css/eigenschaften/schrift.htm' target='_blank'>de.selfhtml.org</a>, z.B. font-weight:bold;</font></td>
	</tr>
	</table>
	</form>\n</div></div>";

    $script_footer .= "
	count_length(document.category_edit_form.name, document.getElementsByName('name_count')[0]);
	";

    $script_footer .= $Cpage->datepicker("visible_from");
    $script_footer .= $Cpage->datepicker("visible_to");


    return $content;
}