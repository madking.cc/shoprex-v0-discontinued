<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action  = $Cpage->get_parameter("do_action");
$subject = $Cpage->get_parameter("subject", "yes");
switch($action)
{
    case "update_invoice_template":
        $text        = $Cpage->get_parameter("text", "", NO_ESCAPE);
        $settings_id = $Cpage->get_parameter("settings_id");
        $id          = $Cpage->get_parameter("id", "", NO_ESCAPE);
        $t           = sizeof($id);
        for($i = 0; $i < $t; $i++)
        {
            if($settings_id == $id[$i])
            {
                $Atext    = array("top" => $text[$i], "left" => $text[$i+1]);
                $text[$i] = serialize($Atext);
            }
            $sql  = "UPDATE `".TBL_PREFIX."adm_templates_".LANGUAGE."` SET `text` = ? WHERE `id` =?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('si', $text[$i], $id[$i]);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            if($settings_id == $id[$i])
            {
                $i += 5;
            }
        }
    case "show_admin_template":
        $item = $Cpage->get_parameter("item");
        $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Vorlagenmenü</div></div><div class='item_content'>";
        $content .= load_menu();
        $content .= "</div></div>\n";
        $content .= "<div class='item_border'><div class='item_header'>Vorlage</div><div class='item_content'>\n";
        $content .= show_invoice($item);
        $content .= "</div></div>\n";
        break;
    case "update_mail_template":
        $text    = $Cpage->get_parameter("text", "", NO_ESCAPE);
        $subject_text = stripslashes($Cpage->get_parameter("subject_text", "", NO_ESCAPE));
        $id      = $Cpage->get_parameter("id", "", NO_ESCAPE);
        $t       = sizeof($id);
        for($i = 0; $i < $t; $i++)
        {
            if($i > 0)
            {
                $subject_text = NULL;
            }
            $sql  = "UPDATE `".TBL_PREFIX."email_".LANGUAGE."` SET `subject` = ?, `text` = ? WHERE `id` =?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $tmp  = stripslashes($text[$i]);
            $stmt->bind_param('ssi', $subject_text, $tmp, $id[$i]);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
    case "show_mail_template":
        $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Vorlagenmenü</div></div><div class='item_content'>";
        $content .= load_menu();
        $content .= "</div></div>\n";
        $is_array = $Cpage->get_parameter("is_array", FALSE);
        $item     = $Cpage->get_parameter("item");
        if($is_array)
        {
            $item = explode(",", $item);
        }
        $content .= "<div class='item_border'><div class='item_header'>Vorlage</div><div class='item_content'>\n";
        $content .= show_item($item, $subject);
        $content .= "</div></div>\n";
        break;
    default:
        $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Vorlagenmenü</div></div><div class='item_content'>";
        $content .= load_menu();
        $content .= "</div></div>\n";
        $content .= "<div class='item_border'><div class='item_header'>Vorlage</div><div class='item_content'>\n";
        $Aitem = array("mail_header", "mail_footer");
        $content .= show_item($Aitem, $subject);
        $content .= "</div></div>\n";
        break;
}

function load_menu()
{
    global $Cpage;
    $content = "";

    $content .= "<ul class='sub_menu'>\n";
    $Aitem = array("mail_header", "mail_footer");
    $item  = implode(",", $Aitem);
    $content .= "<li>".$Cpage->link("Mail Vorlage (Kopf/Fuß)", "templates.php", "do_action=show_mail_template&item=$item&is_array=1");
    unset ($Aitem);
    $content .= "<ul>\n";
    $Aitem = array("order_weight", "order_weight_cart", "order_weight_tax");
    $item  = implode(",", $Aitem);
    $content .= "<li>".$Cpage->link("Bestellung (Versand nach Gewicht)", "templates.php", "do_action=show_mail_template&item=$item&is_array=1");
    unset ($Aitem);
    $Aitem = array("order_flatrate", "order_flatrate_cart", "order_flatrate_tax");
    $item  = implode(",", $Aitem);
    $content .= "<li>".$Cpage->link("Bestellung (Flatrate Versand)", "templates.php", "do_action=show_mail_template&item=$item&is_array=1");
    unset ($Aitem);
    $Aitem = array("order_free", "order_free_cart", "order_free_tax");
    $item  = implode(",", $Aitem);
    $content .= "<li>".$Cpage->link("Bestellung (Freier Versand)", "templates.php", "do_action=show_mail_template&item=$item&is_array=1");
    unset ($Aitem);
    $content .= "<li>".$Cpage->link("Kunden storno", "templates.php", "do_action=show_mail_template&item=order_cancel");
    $content .= "<li>".$Cpage->link("Mail Änderung", "templates.php", "do_action=show_mail_template&item=mail_change");
    $content .= "<li>".$Cpage->link("Mail Artikelfrage", "templates.php", "do_action=show_mail_template&item=question");
    $content .= "<li>".$Cpage->link("Neues Passwort", "templates.php", "do_action=show_mail_template&item=new_password");
    $content .= "<li>".$Cpage->link("Newsletter Eintrag", "templates.php", "do_action=show_mail_template&item=newsletter_sign_in");
    $content .= "<li>".$Cpage->link("Newsletter Abmelden", "templates.php", "do_action=show_mail_template&item=newsletter_sign_off&subject=none");
    $content .= "<li>".$Cpage->link("Passwort ändern", "templates.php", "do_action=show_mail_template&item=password_change");
    $content .= "<li>".$Cpage->link("Passwort reset", "templates.php", "do_action=show_mail_template&item=password_reset");
    $content .= "<li>".$Cpage->link("Registrieren", "templates.php", "do_action=show_mail_template&item=register");
    $content .= "</ul>\n";
    $content .= "<li>".$Cpage->link("Paketschein Vorlage", "templates.php", "do_action=show_admin_template&item=parcel");
    $content .= "<li>".$Cpage->link("Rechnungs Vorlage (Versand nach Gewicht)", "templates.php", "do_action=show_admin_template&item=invoice_weight");
    $content .= "<li>".$Cpage->link("Rechnungs Vorlage (Flatrate Versand)", "templates.php", "do_action=show_admin_template&item=invoice_flatrate");
    $content .= "<li>".$Cpage->link("Rechnungs Vorlage (Freier Versand)", "templates.php", "do_action=show_admin_template&item=invoice_free");
    $content .= "</ul>\n";
    return $content;
}

function show_item($item, $subject)
{
    global $Cpage;
    global $Cdb;
    global $load_syntax_highlighter;
    $load_syntax_highlighter = TRUE;

    $content = "";

    if($load_syntax_highlighter)
    {
        $content .= "<script>/* <![CDATA[ */
				editAreaLoader.init({
				id: 'subject_text'
				,start_highlight: true
				,allow_resize: 'both'
				,allow_toggle: true
				,word_wrap: true
				,language: 'de'
				,syntax: 'php'
				,show_line_colors: true
				,font_size: 8
			});
			/* ]]> */</script>";
    }

    if(is_array($item))
    {
        $implode_item = implode(",", $item);
        $content .= $Cpage->form("page", "templates.php", "update_mail_template").$Cpage->input_hidden("is_array", "1").$Cpage->input_hidden("item", $implode_item).$Cpage->table();

        $i = 0;
        foreach($item as $key => $value)
        {
            $sql       = "SELECT * FROM `".TBL_PREFIX."email_".LANGUAGE."` WHERE `name` like '".$value."' LIMIT 1;";
            $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Atemplate = $result->fetch_assoc();

            if(!empty($Atemplate['subject']))
            {
                $content .= "<tr><td width='90' align='right'>Subject: </td><td>".$Cpage->textarea_id('subject_text', "800", "20", "subject_text").$Atemplate['subject']."</textarea></td></tr>";
            }
            $content .= "<tr><td width='90' valign='top' align='right'>".$Atemplate['description'].": </td><td>".$Cpage->textarea_id('text[]', "800", "200", "text_".$i).$Atemplate['text']."</textarea></td></tr>".$Cpage->input_hidden("id[]", $Atemplate['id']);

            if($load_syntax_highlighter)
            {
                $content .= "<script>/* <![CDATA[ */
						editAreaLoader.init({
							id: 'text_$i'
							,start_highlight: true
							,allow_resize: 'both'
							,allow_toggle: true
							,word_wrap: true
							,language: 'de'
							,syntax: 'php'
							,show_line_colors: true
							,font_size: 8
						});
					/* ]]> */</script>";
            }
            $i++;

        }
        $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table></form>";
    }
    else
    {
        $content .= $Cpage->form("page", "templates.php", "update_mail_template").$Cpage->input_hidden("item", $item).$Cpage->table();
        $sql       = "SELECT * FROM `".TBL_PREFIX."email_".LANGUAGE."` WHERE `name` like '".$item."' LIMIT 1;";
        $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $Atemplate = $result->fetch_assoc();

        if($subject == "yes")
        {
            $content .= "<tr><td width='90' align='right'>Subject: </td><td>".$Cpage->textarea_id('subject_text', "800", "20", "subject_text").$Atemplate['subject']."</textarea></td></tr>";
        }
        $content .= "<tr><td valign='top' align='right'>".$Atemplate['description'].": </td><td>".$Cpage->textarea_id('text[]', "800", "500", "text_0").$Atemplate['text']."</textarea></td></tr>".$Cpage->input_hidden("id[]", $Atemplate['id']);

        if($load_syntax_highlighter)
        {
            $content .= "<script>/* <![CDATA[ */
						editAreaLoader.init({
							id: 'text_0'
							,start_highlight: true
							,allow_resize: 'both'
							,allow_toggle: true
							,word_wrap: true
							,language: 'de'
							,syntax: 'php'
							,show_line_colors: true
							,font_size: 8
						});
					/* ]]> */</script>";
        }

        $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table></form>";
    }
    return $content;
}

function show_invoice($item)
{
    global $Cpage;
    global $Cdb;
    global $load_syntax_highlighter;
    $load_syntax_highlighter = TRUE;

    $content = "";
    $content .= $Cpage->form("page", "templates.php", "update_invoice_template").$Cpage->input_hidden("item", $item).$Cpage->table();

    $sql    = "SELECT * FROM `".TBL_PREFIX."adm_templates_".LANGUAGE."` WHERE `name` like '".$item."%' ORDER BY `id` ASC ;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $count  = $result->num_rows;
    if($count == 2)
    {
        $size_height = 400;
    }
    else $size_height = 200;

    $i = 0;
    while($Atemplate = $result->fetch_assoc())
    {
        if($Atemplate['name'] == $item."_settings")
        {
            $Asetting = unserialize($Atemplate['text']);
            $content .= "<tr><td width='90' valign='top' align='right'>".$Atemplate['description'].": </td><td>".$Cpage->table()."
					<tr><td>Abstand oben:</td><td>".$Cpage->input_text('text[]', $Asetting['top'], "80")." mm </td></tr>
					<tr><td>Abstand links:</td><td>".$Cpage->input_text('text[]', $Asetting['left'], "80")." mm </td></tr>
					</table>".$Cpage->input_hidden("settings_id", $Atemplate['id']);
        }
        else
        {
            $content .= "<tr><td width='90' valign='top' align='right'>".$Atemplate['description'].": </td><td>".$Cpage->textarea_id('text[]', "800", $size_height, "text_".$i).$Atemplate['text']."</textarea></td></tr>";
            if($load_syntax_highlighter)
            {
                $content .= "<script>/* <![CDATA[ */
						editAreaLoader.init({
						id: 'text_$i'
						,start_highlight: true
						,allow_resize: 'both'
						,allow_toggle: true
						,word_wrap: true
						,language: 'de'
						,syntax: 'php'
						,show_line_colors: true
						,font_size: 8
					});
					/* ]]> */</script>";
            }
            $i++;
        }
        $content .= $Cpage->input_hidden("id[]", $Atemplate['id']);
    }
    $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table></form>";
    return $content;
}