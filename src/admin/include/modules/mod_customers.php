<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "store_document":
        $content .= store_document();
    case "delete_document":
        $document_id = $Cpage->get_parameter("document_id", -1);
        if($document_id > -1)
        {
            delete_document($document_id);
        }
    case "edit":
        $id = $Cpage->get_parameter("customer_id");
        $content .= edit_customer($id);
        break;
    case "update":
        $start = $Cpage->get_parameter("start");
        $id    = $Cpage->get_parameter("customer_id");
        update_customer($id);
        $content .= edit_customer($id);
        break;
    case "page_back":
        $start  = $Cpage->get_parameter("back");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_customers($start, $search);
        break;
    case "page_forward":
        $start  = $Cpage->get_parameter("forward");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_customers($start, $search);
        break;
    case "delete_customer":
        $start  = $Cpage->get_parameter("start");
        $search = $Cpage->get_parameter("search", "");
        $id     = $Cpage->get_parameter("customer_id");
        $content .= delete_customer($id);
        $content .= show_customers($start, $search);
        break;
    default:
        $start  = $Cpage->get_parameter("start", 0);
        $search = $Cpage->get_parameter("search", "");
        $content .= show_customers($start, $search);
        break;
}

function show_customers($start, $search)
{
    global $Cpage;
    global $Cdb;
    global $script;

    $amount  = $Cpage->get_parameter("amount", 20);
    $content = "";
    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."customers`;";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];

    $item      = $Cpage->get_parameter("item", "lastname");
    $direction = $Cpage->get_parameter("direction", "asc");

    $_SESSION['amount'] = $amount;

    if(empty($search))
    {
        $sql = "SELECT * FROM `".TBL_PREFIX."customers` ORDER BY `$item` $direction LIMIT $start, $amount;";
    }
    else
    {
        $sql = "	SELECT  * FROM `".TBL_PREFIX."customers`
				WHERE  ((`customer` LIKE '%$search%') OR (`firstname` LIKE '%$search%') OR (`lastname` LIKE '%$search%') OR (`address1` LIKE '%$search%') OR (`address2` LIKE '%$search%') OR (`zip` LIKE '%$search%') OR (`city` LIKE '%$search%') OR (`country` LIKE '%$search%') OR (`phone` LIKE '%$search%') OR (`company` LIKE '%$search%') OR (`del_name` LIKE '%$search%') OR (`del_address1` LIKE '%$search%') OR (`del_address2` LIKE '%$search%') OR (`del_zip` LIKE '%$search%') OR (`del_city` LIKE '%$search%') OR (`del_country` LIKE '%$search%') OR (`del_phone` LIKE '%$search%') OR (`mail` LIKE '%$search%'))
		  		ORDER 	BY `$item` $direction LIMIT $start, $amount;";
    }

    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= "<div class='item_border'><div class='item_header'>Kundenverwaltung</div><div class='item_content'>\n";
    $content .= $Cpage->form("customer_search", "customers.php", "search").$Cpage->input_hidden("amount", $amount).$Cpage->table()."<tr><td>Suche:</td><td>".$Cpage->input_text("search", $search, FORM_TEXT_SIZE, NOT_READONLY, "", NO_MAXLENGTH, "input", NO_ON_SUBMIT)."</td><td>".$Cpage->input_submit("Suchen")." ".$Cpage->input_button("Suche löschen", "delete_search(this.form);");

    $content .= "</td></tr></table></form><br />";

    $script .= "function back_page(form) 						{ form.do_action.value='page_back'; form.submit(); }";
    $script .= "function forward_page(form) 					{ form.do_action.value='page_forward'; form.submit(); }";
    $script .= "function delete_search(form) 					{ form.search.value=''; form.submit(); }";
    $script .= "function edit_customer(form, customer_id) 		{ form.customer_id.value=customer_id; form.submit(); }";
    $script .= "function delete_customer(form, customer_id)
					{ 
						check = confirm('Wollen Sie den Kunden wirklich löschen?');
						if (check == true)
						{
							form.do_action.value='delete_customer';
							form.customer_id.value=customer_id; 
							form.submit(); 
						}
					}";

    $content .= $Cpage->form("customers", "customers.php", "edit").$Cpage->input_hidden("back", $start-$amount).$Cpage->input_hidden("forward", $start+$amount).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("direction", $direction).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("customer_id").$Cpage->table()."<tr><td>";
    if($item == "id" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Kundennr.:", "customers.php", "item=id&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "lastname" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Name:", "customers.php", "item=lastname&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "type" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Typ:", "customers.php", "item=type&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "company" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Firma:", "customers.php", "item=company&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "address1" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Adresse:", "customers.php", "item=address1&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "zip" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("PLZ:", "customers.php", "item=zip&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "city" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Ort:", "customers.php", "item=city&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "country" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Land:", "customers.php", "item=country&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "mail" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("E-Mail:", "customers.php", "item=mail&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "created" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Erstellt:", "customers.php", "item=created&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "last_login" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Letzter Login:", "customers.php", "item=last_login&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>";
    if($item == "banned" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Status:", "customers.php", "item=banned&direction=$direction_item&start=$start&amount=$amount")."</td>
						 <td>".$Cpage->select("amount", 1, FALSE, FALSE, "", "select_form", "", "this.form.do_action.value=\"\";this.form.submit();")."
						 <option value='5'";
    if($amount == 5)
    {
        $content .= " selected";
    }
    $content .= ">5 anzeigen</option>
						 <option value='20'";
    if($amount == 20)
    {
        $content .= " selected";
    }
    $content .= ">20 anzeigen</option>
						 <option value='50'";
    if($amount == 50)
    {
        $content .= " selected";
    }
    $content .= ">50 anzeigen</option>
						 <option value='100'";
    if($amount == 100)
    {
        $content .= " selected";
    }
    $content .= ">100 anzeigen</option>
						 <option value='999999'";
    if($amount == 999999)
    {
        $content .= " selected";
    }
    $content .= ">Alle anzeigen</option>
						 </select>
						 </td></tr>";

    if($result->num_rows > 0)
    {
        while($Acustomer = $result->fetch_assoc())
        {
            $content .= "<tr class='line_top' onmouseover=\"style.backgroundColor='yellow'\" onmouseout=\"style.backgroundColor='transparent'\">
			<td><nobr>".$Acustomer['customer']."</nobr></td>
			<td><nobr>";
            $content .= $Acustomer['lastname'].", ".$Acustomer['firstname'];
            $content .= "</nobr></td><td><nobr>".($Cpage->Aglobal['customer_type'][$Acustomer['type']])."</nobr></td>
			<td><nobr>".$Acustomer['company']."</td>
			<td><nobr>".$Acustomer['address1'];
            if(!empty($Acustomer['address2']))
            {
                $content .= "<br />".$Acustomer['address2'];
            }
            $content .= "</nobr></td><td><nobr>".$Acustomer['zip']."</nobr></td><td><nobr>".$Acustomer['city']."</nobr></td><td><nobr>".$Acustomer['country']."</nobr></td>
			<td><nobr>".$Acustomer['mail']."</nobr></td><td><nobr>".$Cpage->format_time($Acustomer['created'], "sql")."</nobr></td><td><nobr>";

            if(!empty($Acustomer['last_login']))
            {
                $content .= $Cpage->format_time($Acustomer['last_login']);
            }
            else $content .= "Nie";
            $content .= "
			</nobr></td>
			<td>";
            if($Acustomer['banned'])
            {
                $content .= "<span class='status_1'>Banned</span>";
            }
            else $content .= "<span class='status_3'>Ok</span>";
            $content .= " </td>
			<td><nobr>".$Cpage->input_button("editieren", "edit_customer(this.form, \"".$Acustomer['id']."\");").$Cpage->input_button("löschen", "delete_customer(this.form, \"".$Acustomer['id']."\");")."</nobr></td></tr>";
        }
    }
    else
    {
        $content .= "<tr class='line_top'><td colspan='13' align='center'><span class='information'>Keine Ergebnisse</span></td></tr>\n";
    }

    if($result->num_rows > 0)
    {
        $content .= "<tr><td colspan='6'>";
        if($start > 0)
        {
            $content .= $Cpage->input_button("Zurück", "back_page(this.form);");
        }
        $content .= "</td>";
        $content .= "<td colspan='3' align='center'>Seite ".(($start/$amount)+1)." von ".ceil($entries/$amount);
        $content .= "</td>";
        $content .= "<td align='right' colspan='4'>";
        if($start < ($entries-($amount)))
        {
            $content .= $Cpage->input_button("Vor", "forward_page(this.form);");
        }
        $content .= "</td></tr>";
    }
    $content .= "</table></form>";
    $content .= "</div></div>\n";
    return $content;
}

function edit_customer($id)
{
    global $Cpage;
    global $Cdb;
    global $script;
    $order_id  = $Cpage->get_parameter("order_id", 0);
    $start     = $Cpage->get_parameter("start");
    $amount    = $Cpage->get_parameter("amount", 20);
    $item      = $Cpage->get_parameter("item", "lastname");
    $search    = $Cpage->get_parameter("search");
    $direction = $Cpage->get_parameter("direction", "ASC");
    $content   = "";

    $script .= "
				 function cancel(form) 						{ form.do_action.value=''; form.submit(); }
				";

    $sql       = "SELECT * FROM `".TBL_PREFIX."customers` WHERE id=$id;";
    $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acustomer = $result->fetch_assoc();

    $content .= $Cpage->form("custumer_edit", "customers.php", "update");
    $content .= $Cpage->input_hidden("order_id", $order_id);
    $content .= $Cpage->input_hidden("start", $start);
    $content .= $Cpage->input_hidden("direction", $direction);
    $content .= $Cpage->input_hidden("item", $item);
    $content .= $Cpage->input_hidden("amount", $amount);
    $content .= $Cpage->input_hidden("search", $search);
    $content .= $Cpage->input_hidden("customer_id", $id);

    $content .= "<div class='item_border'><div class='item_header'>Kunde ".CUSTOMER_PREFIX.($Acustomer['id']+CUSTOMER_START).CUSTOMER_SUFFIX."</div><div class='item_content'>\n";
    $content .= $Cpage->table();
    $content .= "<tr><td>E-Mail:</td><td>".$Cpage->input_text("mail", $Acustomer['mail'])."</td></tr>\n";
    $content .= "<tr><td valign='top'>Neues Passwort:</td><td>".$Cpage->input_text("password")."
	<br /><span class='information'>Das neue Passwort wird dem User<br />sofort zugesendet.</span></td></tr>\n";
    $content .= "<tr><td>Typ:</td><td>".$Cpage->select_multi("customer_type", $Cpage->Aglobal['customer_type'], $Acustomer['type'])."</td></tr>\n";
    $content .= "<tr><td>Firma:</td><td>".$Cpage->input_text("company", $Acustomer['company'])."</td></tr>\n";
    $content .= "<tr><td>Vorname:</td><td>".$Cpage->input_text("firstname", $Acustomer['firstname'])."</td></tr>\n";
    $content .= "<tr><td>Nachname:</td><td>".$Cpage->input_text("lastname", $Acustomer['lastname'])."</td></tr>\n";
    $content .= "<tr><td>Adresse 1:</td><td>".$Cpage->input_text("address1", $Acustomer['address1'])."</td></tr>\n";
    $content .= "<tr><td>Adresse 2:</td><td>".$Cpage->input_text("address2", $Acustomer['address2'])."</td></tr>\n";
    $content .= "<tr><td>Postleitzahl:</td><td>".$Cpage->input_text("zip", $Acustomer['zip'])."</td></tr>\n";
    $content .= "<tr><td>Stadt:</td><td>".$Cpage->input_text("city", $Acustomer['city'])."</td></tr>\n";
    $content .= "<tr><td>Land:</td><td>".$Cpage->select_countries("country", $Acustomer['country'])."</td></tr>\n";
    $content .= "<tr><td>Telefon:</td><td>".$Cpage->input_text("phone", $Acustomer['phone'])."</td></tr>\n";
    $content .= "<tr><td>Liefername:</td><td>".$Cpage->input_text("del_name", $Acustomer['del_name'])."</td></tr>\n";
    $content .= "<tr><td>Lieferadresse 1:</td><td>".$Cpage->input_text("del_address1", $Acustomer['del_address1'])."</td></tr>\n";
    $content .= "<tr><td>Lieferadresse 2:</td><td>".$Cpage->input_text("del_address2", $Acustomer['del_address2'])."</td></tr>\n";
    $content .= "<tr><td>Liefer-PLZ:</td><td>".$Cpage->input_text("del_zip", $Acustomer['del_zip'])."</td></tr>\n";
    $content .= "<tr><td>Lieferland:</td><td>".$Cpage->select_countries("del_country", $Acustomer['del_country'])."</td></tr>\n";
    $content .= "<tr><td>Liefer-Telefon:</td><td>".$Cpage->input_text("del_phone", $Acustomer['del_phone'])."</td></tr>\n";
    $content .= "<tr><td>Newsletter?:</td><td>".$Cpage->select_multi("newsletter", $Cpage->Aglobal['answer'], $Acustomer['newsletter'])."</td></tr>\n";
    $content .= "<tr><td>Angelegt:</td><td>".$Cpage->input_text("created", $Cpage->format_time($Acustomer['created']), FORM_TEXT_SIZE, READONLY)."</td></tr>\n";
    $content .= "<tr><td>Letzter Login:</td><td>".$Cpage->input_text("last_login", $Cpage->format_time($Acustomer['last_login']), FORM_TEXT_SIZE, READONLY)."</td></tr>\n";
    $content .= "<tr><td>Letzte Änderung:</td><td>".$Cpage->input_text("changed", $Cpage->format_time($Acustomer['changed']), FORM_TEXT_SIZE, READONLY)."</td></tr>\n";
    $content .= "<tr><td>Gebannt?:</td><td>".$Cpage->select_multi("banned", $Cpage->Aglobal['answer'], $Acustomer['banned'])."</td></tr>\n";
    $content .= "<tr><td colspan='2'>".$Cpage->input_submit("Speichern")." ";
    if($order_id == 0)
    {
        $content .= $Cpage->input_button("Zurück", "cancel(this.form);");
    }
    else $content .= $Cpage->input_button("Fenster schliessen", "window.close();");

    $content .= "</td></tr>\n";
    $content .= "</table>\n";
    $content .= "</div></div></form>\n";

    $Adocuments = unserialize($Acustomer['documents']);

    $script .= "
				 function deleteDocument(formID, i)	{ formID.document_id.value=i; formID.submit(); }
				";

    $content .= $Cpage->form('user_documents', "customers.php", "delete_document");
    $content .= $Cpage->input_hidden("order_id", $order_id);
    $content .= $Cpage->input_hidden("start", $start);
    $content .= $Cpage->input_hidden("direction", $direction);
    $content .= $Cpage->input_hidden("item", $item);
    $content .= $Cpage->input_hidden("amount", $amount);
    $content .= $Cpage->input_hidden("search", $search);
    $content .= $Cpage->input_hidden("customer_id", $id);
    $content .= $Cpage->input_hidden("document_id");
    $content .= "<div class='item_border'><div class='item_header'>Dokumente</div><div class='item_content'>\n";

    if(!empty($Adocuments))
    {
        for($i = 0; $i < sizeof($Adocuments); $i++)
        {
            $content .= "<div class='document_wrapper'><a href='".UPLOAD_DIR_ADMIN.$Adocuments[$i]."' target='_blank'>\n<img src='".UPLOAD_DIR_ADMIN."/file_preview_symbol.jpg' alt='".$Adocuments[$i]."'></a><br />\n".$Adocuments[$i]."<br />".$Cpage->input_button("Löschen", "deleteDocument(this.form, \"$i\");")."</div>\n";
        }
    }
    $content .= "</form>";
    $content .= $Cpage->form_file('user_documents', "customers.php", "store_document");
    $content .= $Cpage->input_hidden("order_id", $order_id);
    $content .= $Cpage->input_hidden("start", $start);
    $content .= $Cpage->input_hidden("direction", $direction);
    $content .= $Cpage->input_hidden("item", $item);
    $content .= $Cpage->input_hidden("amount", $amount);
    $content .= $Cpage->input_hidden("search", $search);
    $content .= $Cpage->input_hidden("customer_id", $id);
    $content .= "<br />".$Cpage->input_file('file_to_upload')."<br />".$Cpage->input_submit("Dokument hinzufügen");
    $content .= "</div></div></form>";
    return $content;
}

function store_document()
{
    global $Cpage;
    global $Cdb;
    $content = "";

    $id        = $Cpage->get_parameter("customer_id");
    $uploaddir = UPLOAD_DIR_ADMIN;
    if(isset($_FILES['file_to_upload']) && !empty($_FILES['file_to_upload']['name']))
    {
        $file_parts = pathinfo($_FILES['file_to_upload']['name']);
        $file_name  = $file_parts['basename'];
        $last_dot   = strrpos($file_name, ".");
        $file_name  = substr($file_name, 0, $last_dot);
        $file_type  = $file_parts['extension'];

        $file_name_original      = $Cpage->check_filename($file_name, $file_type, $uploaddir);
        $upload_file_destination = $uploaddir.$file_name_original;

        if(!move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $upload_file_destination))
        {
            // Upload war nicht erfolgreich:
            $content .= "<div class='item_border'><div class='item_header'>Fehler</div><div class='item_content'>\n";
            $content .= "<font class='error'>Fehler!</font>
	        Die Datei konnte aus unbekannten Gründen nicht hochgeladen werden. Wahrscheinlich hat das upload Verzeichnis \"".UPLOAD_DIR_ADMIN."\" keine Schreibrechte.<br />
	        <br />Debug Informationen:<br />".print_r($_FILES);
            $content .= "</div></div>";
        }
        else
        {
            // Hole eventuell bereits gespeicherte Dokumente
            $sql       = "SELECT * FROM `".TBL_PREFIX."customers` WHERE `id` =$id;";
            $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Acustomer = $result->fetch_assoc();

            $Adocuments = array();

            $tmp = unserialize($Acustomer['documents']);
            if(!empty($tmp))
            {
                $Adocuments = unserialize($Acustomer['documents']);
            }

            array_push($Adocuments, $file_name_original);

            $documents = serialize($Adocuments);

            // Speicher
            $sql  = "UPDATE `".TBL_PREFIX."customers` SET
				`documents` = ?
				WHERE `id` = ?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('si', $documents, $id);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
    }
    return $content;
}

function delete_document($document_id)
{
    global $Cpage;
    global $Cdb;

    $id        = $Cpage->get_parameter("customer_id");
    $sql       = "SELECT * FROM `".TBL_PREFIX."customers` WHERE id=$id;";
    $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acustomer = $result->fetch_assoc();

    $Adocuments = unserialize($Acustomer['documents']);

    unlink(UPLOAD_DIR_ADMIN.$Adocuments[$document_id]);

    array_splice($Adocuments, $document_id, 1);

    $documents = serialize($Adocuments);

    // Speicher
    $sql  = "UPDATE `".TBL_PREFIX."customers` SET
		`documents` = ?
		WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('si', $documents, $id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function update_customer($id)
{
    global $Cdb;
    global $Cpage;

    $mail          = $Cpage->get_parameter("mail");
    $password      = $Cpage->get_parameter("password", "");
    $customer_type = $Cpage->get_parameter("customer_type");
    $company       = $Cpage->get_parameter("company");
    $firstname     = $Cpage->get_parameter("firstname");
    $lastname      = $Cpage->get_parameter("lastname");
    $address1      = $Cpage->get_parameter("address1");
    $address2      = $Cpage->get_parameter("address2");
    $zip           = $Cpage->get_parameter("zip");
    $city          = $Cpage->get_parameter("city");
    $country       = $Cpage->get_parameter("country");
    $phone         = $Cpage->get_parameter("phone");
    $del_name      = $Cpage->get_parameter("del_name");
    $del_address1  = $Cpage->get_parameter("del_address1");
    $del_address2  = $Cpage->get_parameter("del_address2");
    $del_zip       = $Cpage->get_parameter("del_zip");
    $del_country   = $Cpage->get_parameter("del_country");
    $del_phone     = $Cpage->get_parameter("del_phone");
    $newsletter    = $Cpage->get_parameter("newsletter");
    $banned        = $Cpage->get_parameter("banned");

    if(!empty($password))
    {
        $Acustomer['password'] = $password;
        require ($Cpage->Apath['mail']."new_password.php");

        $password = md5($password);
        $password = $Cpage->salt_password($password);
        // Speicher
        $sql  = "UPDATE `".TBL_PREFIX."customers` SET
		`mail` = ?,
		`password` = ?,
		`type` = ?,
		`company` = ?,
		`firstname` = ?,
		`lastname` = ?,
		`address1` = ?,
		`address2` = ?,
		`zip` = ?,
		`city` = ?,
		`country` = ?,
		`phone` = ?,
		`del_name` = ?,
		`del_address1` = ?,
		`del_address2` = ?,
		`del_zip` = ?,
		`del_country` = ?,
		`del_phone` = ?,
		`newsletter` = ?,
		`banned` = ?
		WHERE `id` = ?;";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('ssisssssssssssssssiii', $mail, $password, $customer_type, $company, $firstname, $lastname, $address1, $address2, $zip, $city, $country, $phone, $del_name, $del_address1, $del_address2, $del_zip, $del_country, $del_phone, $newsletter, $banned, $id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }
    else
    {
        $sql  = "UPDATE `".TBL_PREFIX."customers` SET
		`mail` = ?,
		`type` = ?,
		`company` = ?,
		`firstname` = ?,
		`lastname` = ?,
		`address1` = ?,
		`address2` = ?,
		`zip` = ?,
		`city` = ?,
		`country` = ?,
		`phone` = ?,
		`del_name` = ?,
		`del_address1` = ?,
		`del_address2` = ?,
		`del_zip` = ?,
		`del_country` = ?,
		`del_phone` = ?,
		`newsletter` = ?,
		`banned` = ?
		WHERE `id` = ?;";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        $stmt->bind_param('sisssssssssssssssiii', $mail, $customer_type, $company, $firstname, $lastname, $address1, $address2, $zip, $city, $country, $phone, $del_name, $del_address1, $del_address2, $del_zip, $del_country, $del_phone, $newsletter, $banned, $id);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }
}

function delete_customer($id)
{
    global $Cdb;

    $sql       = "SELECT * FROM `".TBL_PREFIX."customers` WHERE id=$id;";
    $result    = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acustomer = $result->fetch_assoc();

    $Adocuments = unserialize($Acustomer['documents']);

    if(!empty($Adocuments))
    {
        foreach($Adocuments as $value)
        {
            unlink(UPLOAD_DIR_ADMIN.$value);
        }
    }

    $sql = "DELETE FROM `".TBL_PREFIX."customers` WHERE `id`=".$id.";";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}