<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
$content .= "<div class='item_border'><div class='item_header'>Login</div><div class='item_content'>\n";
switch($action)
{
    case "login":
        login_check();
    default:
        $content .= login();
        break;
}
$content .= "</div></div>";

function login_check()
{
    ////////////////////////////////
    /// Funktionsheader:
    global $Cpage;
    global $Cdb;
    global $script;
    ////////////////////////////////

    $_SESSION['admin_name']     = $Cpage->get_parameter("name");
    $_SESSION['admin_password'] = $Cpage->get_parameter("password");
    if((empty($_SESSION['admin_name'])) || (empty($_SESSION['admin_password']))) // Security
    {
        if(time_nanosleep(2, 0))
        {
            $script .= $Cpage->load_page("login.php", "login_error=1");
        } // Lade Login Seite
    }
    else
    {
        $name                       = $_SESSION['admin_name'];
        $password                   = $Cpage->salt_password($_SESSION['admin_password']);
        $sql                        = "SELECT * FROM `".TBL_PREFIX."adm_access`
              WHERE `name` = '".$name."'
                    AND
                    BINARY `password` = '".$password."'
                    LIMIT 1;";
        $_SESSION['admin_name']     = NULL;
        $_SESSION['admin_password'] = NULL;
        $password                   = NULL;
        $result                     = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        // Wenn Passwort und Name richtig:
        if($result->num_rows == 1)
        {
            $_SESSION['login'] = $name;
            // Speichere Login Zeit
            $sql = "UPDATE `".TBL_PREFIX."adm_access`
                SET
                `last_login` = NOW()
                WHERE
                `id` = 1;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);

            if(time_nanosleep(2, 0))
            {
                $script .= $Cpage->load_page();
            }
        } // Wenn Passwort oder Name falsch:
        else
        {
            if(time_nanosleep(2, 0))
            {
                $script .= $Cpage->load_page("login.php", "login_error=2");
            } // Lade Startseite
        }
    }
}

function login()
{
    global $Cpage;
    global $load_md5;
    global $script;
    $content = "";

    $load_md5 = TRUE;
    $login_error = $Cpage->get_parameter("login_error", 0);

    $script .= "
                     function checkSubmit(formID)
                     {
                            formID.password.value = MD5(formID.password.value);
                            return true;
                     }

				";

    $content .= $Cpage->form("login_form", "login.php", "login", "return checkSubmit(this);").$Cpage->table()."<tr>
         <td>Login Name:</td><td>".$Cpage->input_text("name")."</td>
        </tr>
        <tr>
         <td>Passwort:</td><td>".$Cpage->input_password("password")."</td>
        </tr>
        <tr>
         <td></td><td>".$Cpage->input_submit("Einloggen")."</td>
        </tr>";
    if($login_error == 1)
    {
        $content .= "<tr><td></td><td><span class='error'>Es darf kein Feld leer sein!</span></td></tr>";
    }
    if($login_error == 2)
    {
        $content .= "<tr><td></td><td><span class='error'>Falscher Login Name oder Passwort!</span></td></tr>";
    }
    $content .= "</table>";

    return $content;
}