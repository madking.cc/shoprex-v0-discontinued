<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
$content .= "<div class='item_border'><div class='item_header'>Sprache</div><div class='item_content'>\n";
switch($action)
{
    case "page_back":
        $start = $Cpage->get_parameter("back");
        $content .= show_language($start);
        break;
    case "page_forward":
        $start = $Cpage->get_parameter("forward");
        $content .= show_language($start);
        break;
    case "store":
        $start = $Cpage->get_parameter("start");
        store_language();
        $content .= show_language($start);
        break;
    case "search_store":
        $start = $Cpage->get_parameter("start");
        $text  = $Cpage->get_parameter("text");
        store_language();
        $content .= show_search($text, $start);
        break;
    case "search":
        $text  = $Cpage->get_parameter("text");
        $start = 0;
        $content .= show_search($text, $start);
        break;
    case "search_page_back":
        $text  = $Cpage->get_parameter("text");
        $start = $Cpage->get_parameter("back");
        $content .= show_search($text, $start);
        break;
    case "search_page_forward":
        $text  = $Cpage->get_parameter("text");
        $start = $Cpage->get_parameter("forward");
        $content .= show_search($text, $start);
        break;
    case "add_item":
        $start = $Cpage->get_parameter("start");
        $content .= add_item();
        $content .= show_language($start);
        break;
    case "delete_item":
        $start = $Cpage->get_parameter("start");
        $content .= delete_item();
        $content .= show_language($start);
        break;
    case "delete_item_search":
        $start = $Cpage->get_parameter("start");
        $text  = $Cpage->get_parameter("text");
        $content .= delete_item();
        $content .= show_search($text, $start);
        break;
    default:
        $start = 0;
        $content .= show_language($start);
        break;
}
$content .= "</div></div>\n";

function show_language($start)
{
    global $Cpage;
    global $Cdb;
    global $script;
    $amount  = 20;
    $content = "";
    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `type`='language';";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];
    $sql     = "SELECT * FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `type`='language' ORDER BY `name` ASC LIMIT $start, $amount;";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= $Cpage->form("language_search", "language.php", "search").$Cpage->table()."<tr><td width='300' align='right'>Suche:</td><td>".$Cpage->input_text("text", NO_VALUE, FORM_TEXT_SIZE, NOT_READONLY, "", NO_MAXLENGTH, "input", NO_ON_SUBMIT)."</td><td>".$Cpage->input_submit("Suchen")."</td></table></form><br />";

    $content .= $Cpage->form("add_item_form", "language.php", "add_item").$Cpage->table().$Cpage->input_hidden("start", $start)."<tr><td width='300' align='right'>".$Cpage->input_text("name", NO_VALUE, "300")."</td><td>".$Cpage->input_text("value", NO_VALUE, "600")."</td><td>".$Cpage->input_submit("Hinzufügen")."</td></table></form><br />";

    $script .= "function back_page(form) { form.do_action.value='page_back'; form.submit(); }
    ";
    $script .= "function forward_page(form) { form.do_action.value='page_forward'; form.submit(); }
    ";
    $script .= "function delete_item(form, delete_it) { form.do_action.value='delete_item'; form.item_to_delete.value=delete_it; form.submit(); }
    ";
    $content .= $Cpage->form("language", "language.php", "store").$Cpage->input_hidden("back", $start-$amount).$Cpage->input_hidden("forward", $start+$amount).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("item_to_delete").$Cpage->table()."<tr><td align='right'>Name:</td><td>Text:</td><td align='right'>Seite ".(($start/$amount)+1)." von ".ceil($entries/$amount)."</td></tr>";

    while($Alanguage = $result->fetch_assoc())
    {
        $content .= "<tr><td width='300' align='right'>
			".$Alanguage['name']."</td><td colspan='2'>".$Cpage->input_text("entry[".$Alanguage['id']."]", $Alanguage['value'], "600")." ".$Cpage->input_button("löschen", "delete_item(document.language, \"".$Alanguage['id']."\");")."</td></tr>";
    }

    $content .= "<tr><td align='right'>";
    if($start > 0)
    {
        $content .= $Cpage->input_button("Zurück", "back_page(document.language);");
    }
    $content .= "</td><td>";
    $content .= $Cpage->input_submit("Speichern");
    $content .= "</td><td align='right'>";
    if($start < ($entries-($amount)))
    {
        $content .= $Cpage->input_button("Vor", "forward_page(document.language);");
    }
    $content .= "</td></tr></table></form>";
    return $content;
}

function show_search($text, $start)
{
    global $Cpage;
    global $Cdb;
    global $script;
    $amount  = 20;
    $content = "";
    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `type`='language' AND (`name` like '%".$text."%' OR `value` like '%".$text."%');";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];

    $sql    = "SELECT * FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `type`='language' AND (`name` like '%".$text."%' OR `value` like '%".$text."%') ORDER BY `name` ASC LIMIT $start, $amount;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= $Cpage->form("language_search", "language.php", "search").$Cpage->table()."<tr><td width='300' align='right'>Suche:</td><td>".$Cpage->input_text("text", $text, FORM_TEXT_SIZE, NOT_READONLY, "", NO_MAXLENGTH, "input", NO_ON_SUBMIT)."</td><td>".$Cpage->input_submit("Suchen")." ".$Cpage->input_button("Suche löschen", "delete_search(document.language_search);")."</td></table></form><br /><br />";
    $script .= "function delete_search(form) { form.do_action.value='default'; form.submit(); }
    ";

    if($entries == 0)
    {
        return $content."<span class='information'>Kein Ergebnis</span>";
    }
    $script .= "function back_page(form) { form.do_action.value='search_page_back'; form.submit(); }
    ";
    $script .= "function forward_page(form) { form.do_action.value='search_page_forward'; form.submit(); }
    ";
    $script .= "function delete_item(form, delete_it) { form.do_action.value='delete_item_search'; form.item_to_delete.value=delete_it; form.submit(); }
    ";

    $content .= $Cpage->form("language", "language.php", "search_store").$Cpage->input_hidden("back", $start-$amount).$Cpage->input_hidden("forward", $start+$amount).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("text", $text).$Cpage->input_hidden("item_to_delete").$Cpage->table()."<tr><td align='right'>Name:</td><td>Text:</td><td align='right'>Seite ".(($start/$amount)+1)." von ".ceil($entries/$amount)."</td></tr>";

    while($Alanguage = $result->fetch_assoc())
    {
        $content .= "<tr><td width='300' align='right'>
			".$Alanguage['name']."</td><td colspan='2'>".$Cpage->input_text("entry[".$Alanguage['id']."]", $Alanguage['value'], "600")." ".$Cpage->input_button("löschen", "delete_item(document.language, \"".$Alanguage['id']."\");")."</td></tr>";
    }

    $content .= "<tr><td align='right'>";
    if($start > 0)
    {
        $content .= $Cpage->input_button("Zurück", "back_page(document.language);");
    }
    $content .= "</td><td>";
    $content .= $Cpage->input_submit("Speichern");
    $content .= "</td><td align='right'>";
    if($start < ($entries-($amount)))
    {
        $content .= $Cpage->input_button("Vor", "forward_page(document.language);");
    }
    $content .= "</td></tr></table></form>";
    return $content;
}

function store_language()
{
    global $Cdb;
    global $Cpage;
    $Aentry = $Cpage->get_parameter("entry", "", NO_ESCAPE);

    $sql  = "UPDATE `".TBL_PREFIX."language_".LANGUAGE."` SET `value` = ? WHERE `id` =?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);

    foreach($Aentry as $key => $value)
    {
        $stmt->bind_param('si', $value, $key);
        $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    }
}

function add_item()
{
    global $Cdb;
    global $Cpage;

    $name    = strtoupper($Cpage->get_parameter("name"));
    $value   = $Cpage->get_parameter("value");
    $type    = "language";
    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `name`='".$name."';";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];
    if($entries > 0)
    {
        return "<span class='error'>Fehler: Der Name $name der Konstante ist bereits vorhanden. Speichern nicht möglich.</span>";
    }
    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."settings` WHERE `name`='".$name."';";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];
    if($entries > 0)
    {
        return "<span class='error'>Fehler: Der Name $name der Konstante ist bereits als Einstellungsname vorhanden. Speichern nicht möglich.</span>";
    }

    $sql  = "INSERT INTO `".TBL_PREFIX."language_".LANGUAGE."` (`name`, `value`, `type`) VALUES (?, ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('sss', $name, $value, $type);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    return FALSE;
}

function delete_item()
{
    global $Cpage;
    global $Cdb;
    $item_to_delete = $Cpage->get_parameter("item_to_delete");
    $sql            = "DELETE FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE `id`=".$item_to_delete.";";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}