<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "change_invoice_into_order":
        change_invoice_into_order();
        $content .= show_order();
        break;
    case "delete_order":
        delete_order();
        $content .= back();
        break;
    case "create_invoice":
        $content .= create_invoice();
    case "delete_document":
        $content .= delete_document();
    case "update":
        $content .= update_order();
    default:
        $content .= show_order();
        break;
}

function show_order()
{
    global $Cpage;
    global $Cdb;
    global $script;
    $content = "";

    $start     = $Cpage->get_parameter("start", 0);
    $search    = $Cpage->get_parameter("search", "");
    $amount    = $Cpage->get_parameter("amount", 20);
    $item      = $Cpage->get_parameter("item", "id");
    $direction = $Cpage->get_parameter("direction", "desc");
    $status2   = $Cpage->get_parameter("status2", -1);

    $id = $Cpage->get_parameter("id");
    if(empty($id))
    {
        return FALSE;
    }

    $sql         = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =".$id.";";
    $result      = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $order_field = $result->fetch_assoc();
    // Hole den kompletten alten Warenkorb
    $ar_cart = unserialize($order_field['cart']);

    $Adocuments = unserialize($order_field['documents']);

    if(empty($order_field['del_country']))
    {
        $del_country_select = $order_field['country'];
    }
    else $del_country_select = $order_field['del_country'];

    // Damit die letzte Rechnung gelöscht werden kann:
    $sql             = "SELECT * FROM `".TBL_PREFIX."counter` WHERE `type` ='INVOICE_COUNTER';";
    $result          = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acounter        = $result->fetch_assoc();
    $invoice_counter = $Acounter['count'];

    $script .= "
				     function deleteInvoice(formID) { check = confirm('Wollen Sie die Rechnung wirklich löschen? OK für Ja');
				                                      if(check)
				                                      { formID.do_action.value='change_invoice_into_order'; formID.submit(); }
				                                    }
				     function deleteOrder(formID)   { check = confirm('Wollen Sie den Auftrag wirklich löschen? OK für Ja');
				                                      if(check)
				                                      { formID.do_action.value='delete_order'; formID.submit(); }
				                                    }
				     function Back(formID) 			{ formID.action='correspondence.php'; formID.submit(); }
					 function editCart(formID) 		{ formID.action='correspondence_cart.php'; formID.do_action.value='dummy'; formID.submit(); }
					 function deleteDocument(formID, id) 			{ formID.do_action.value='delete_document'; formID.file.value=id; formID.submit(); }
					 function orderToInvoice(formID) 			    { formID.do_action.value='create_invoice'; formID.submit(); }
					 function removeDel(formID) 			        { formID.del_name.value=''; formID.submit(); }
					 function OpenCustomer()		{   KundenFenster = window.open('".$Cpage->Apath['root']."customers.php?do_action=edit&customer_id=".$order_field['customer_id']."&order_id=".$id."', '_blank');
  														KundenFenster.focus(); }
					 function printInvoice()		{   RechnungsFenster = window.open('".$Cpage->Apath['root']."print_invoice.php?id=".$id."', '_blank');
  														RechnungsFenster.focus(); }	
					 function printParcel()		    {   PaketscheinFenster = window.open('".$Cpage->Apath['root']."print_parcel.php?id=".$id."', '_blank');
  														PaketscheinFenster.focus(); }	
					 		
				";

    $content .= $Cpage->form_file('address_edit', 'correspondence_edit.php', 'update').$Cpage->input_hidden("id", $id).$Cpage->input_hidden("file", -1).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("status2", $status2).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("amount", $amount).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("direction", $direction);

    if(empty($order_field['invoice_id']))
    {
        $content .= "<div class='item_border'><div class='item_header'>Auftrag ".$order_field['order_id']." vom ".$Cpage->format_time($order_field['order_time'])."</div><div class='item_content'>\n";
    }
    else
    {
        $content .= "<div class='item_border'><div class='item_header'>Rechnung ".$order_field['invoice_id']." vom ".$Cpage->format_time($order_field['invoice_time'])."</div><div class='item_content'>\n";
    }

    $content .= "<div class='content_wrapper'>".$Cpage->input_submit("Speichern")." ".$Cpage->input_reset("Eingaben zurücksetzen", "didChanges = false;")." ".$Cpage->input_button("Zurück", "Back(this.form);")."</div><div class='clear_float'></div>";

    $target_mismatch = FALSE;
    if(empty($order_field['del_country']))
    {
        if($order_field['country'] != $ar_cart['delivery_country_target'])
        {
            $target_mismatch = TRUE;
        }
    }
    else
    {
        if($order_field['del_country'] != $ar_cart['delivery_country_target'])
        {
            $target_mismatch = TRUE;
        }
    }
    if($target_mismatch && ($order_field['delivery_type'] < 2))
    {
        $content .= "<div class='content_wrapper'><span class='error'>Das Zielland des Warenkorbes stimmt nicht mit dem Lieferland des Autrages / der Rechnung überein!<br />Bitte Warenkorb bearbeiten!</span></div>";
    }

    $content .= "<div class='item_border'><div class='item_header'>Aktionen:</div><div class='item_content'><div class='content_wrapper'>\n".$Cpage->table()."
			<tr>
			 <td>Status ändern auf ".$Cpage->select_multi("status", $Cpage->Aglobal['status'], $order_field['status']);
    if(!empty($order_field['invoice_id']))
        $content .= "<br />
			 <span class='information'>Passen Sie ggf. die Lagermenge an, wenn Sie den Status z.B. auf Storniert setzen oder z.B. einzelne Artikel zurückgegeben wurden.</span>";
	$content .= "</td><td>";
    if(($ar_cart['shipping_error'] == FALSE) && (empty($order_field['invoice_id'])))
    {
        $content .= $Cpage->input_button("Auftrag in Rechnung umwandeln", "orderToInvoice(this.form);");
    }
    if(empty($order_field['invoice_id']))
    {
        $content .= " ".$Cpage->input_button("Auftrag löschen", "deleteOrder(this.form);");
    }
    $content .= "</td>
			</tr>";
    if(!empty($order_field['invoice_id']))
    {
        $content .= "<tr><td>".$Cpage->input_button("Rechnung drucken", "printInvoice(this.form)")." ".$Cpage->input_button("Paketschein drucken", "printParcel(this.form)");
        if(($invoice_counter-1) == $order_field['invoice_counter'])
            $content .= " ".$Cpage->input_button("Rechnung löschen und in Auftrag umwandeln", "deleteInvoice(this.form)")."<br /><span class='information'>Es kann immer nur die letzte Rechnung gelöscht werden, damit keine Löcher in den Rechnungsnummern entstehen.</span>";
        $content .= "</td></tr>";
    }

    $content .= "</table></div>";
    $content .= "</div></div><div class='clear_float'></div>";

    $content .= "<div class='item_border'><div class='item_header'>Kundenanschrift:</div><div class='item_content'><div class='content_wrapper'>\n".$Cpage->table()."
              <tr class='R2'>
               <td>Kunden Typ:</td>
               <td>".$Cpage->select_multi('customer_type', $Cpage->Aglobal['customer_type'], $order_field['customer_type'])."</td>
              </tr>
              <tr>
               <td>Firma:</td>
               <td>".$Cpage->input_text('company', $order_field['company'], 200)."</td>
              </tr>
              <tr>
               <td>Vor-, Nachname:</td>
               <td>".$Cpage->input_text('firstname', $order_field['firstname'], 90)." ".$Cpage->input_text('lastname', $order_field['lastname'], 103)."</td>
              </tr>
              <tr>
               <td>Adresse 1:</td>
               <td>".$Cpage->input_text('address1', $order_field['address1'], 200)."</td>
              </tr>
              <tr>
               <td>Adresse 2:</td>
               <td>".$Cpage->input_text('address2', $order_field['address2'], 200)."</td>
              </tr>
              <tr>
               <td>PLZ, Ort:</td>
               <td>".$Cpage->input_text('zip', $order_field['zip'], 50)." ".$Cpage->input_text('city', $order_field['city'], 143)."</td>
              </tr>
              <tr>
               <td>Land:</td>
               <td>".$Cpage->select_countries('country', $order_field['country'], 204)."</td>
              </tr>
              <tr>
               <td>Telefon:</td>
               <td>".$Cpage->input_text('phone', $order_field['phone'], 200)."</td>
              </tr>
              <tr>
               <td>E-Mail:</td>
               <td>".$Cpage->input_text('email', $order_field['email'], 200)."</td>
              </tr>
	         </table>".$Cpage->input_button("In Kundenverwaltung öffnen", "OpenCustomer();");
    $content .= "</div></div></div>";

    $content .= "<div class='item_border'><div class='item_header'>Lieferadresse:";
    if($order_field['del_country'] == NULL)
    {
        $content .= " <span class='info'>-Leer-</span>";
    }
    $content .= "</div><div class='item_content'><div class='content_wrapper'>\n".$Cpage->table()."
			<tr class='R2'>
			<td>Name:</td>
			<td>".$Cpage->input_text('del_name', $order_field['del_name'], 200)."</td>
			</tr>
			<tr>
			<td>Adresse 1:</td>
			<td>".$Cpage->input_text('del_address1', $order_field['del_address1'], 200)."</td>
			</tr>
			<tr>
			<td>Adresse 2:</td>
			<td>".$Cpage->input_text('del_address2', $order_field['del_address2'], 200)."</td>
			</tr>
			<tr>
			<td>PLZ, Ort:</td>
			<td>".$Cpage->input_text('del_zip', $order_field['del_zip'], 50)." ".$Cpage->input_text('del_city', $order_field['del_city'], 143)."</td>
			</tr>
			<tr>
			<td>Land:</td>
			<td>".$Cpage->select_countries('del_country', $del_country_select, 204)."</td>
			</tr>
			<tr>
			<td>Telefon:</td>
			<td>".$Cpage->input_text('del_phone', $order_field['del_phone'], 200)."</td>
			</tr>";
    if($order_field['del_country'] != NULL)
    {
        $content .= "
			<tr class='RL'>
			<td></td>
			<td>".$Cpage->input_button("Lieferadresse Entfernen", "removeDel(this.form);")."</td>
			</tr>";
    }
    $content .= "</table>";
    $content .= "</div></div></div><div class='clear_float'></div>";

    if($order_field['delivery_method'] == "weight")
    {
        $z = 0;
    }
    else
    {
        $z = -1;
    }
    if(ENABLE_TAX)
    {
        $zz = 0;
    }
    else
    {
        $zz = -2;
    }
    if(USE_ARTICLE_NUMBER)
    {
        $zzz = 0;
    }
    else
    {
        $zzz = -1;
    }

    $content .= "<div class='item_border'><div class='item_header'>Warenkorb:</div><div class='item_content'>\n".$Cpage->table()."
	<tr class='R1'>
		<td colspan='".(10+$z+$zz+$zzz)."' align='right'>Status: <span class='status_".$order_field['status']."'>".$Cpage->Aglobal['status'][$order_field['status']]."</span></td>
	</tr>
		<tr class='line_top line_bottom'>
		<th>Pos.:</th>
		<th>Artikel:</th>";
		if(USE_ARTICLE_NUMBER) $content .= "<th>Art.-Nr.:</th>";
    if($order_field['delivery_method'] == "weight")
    {
        $content .= "<th>Gewicht:</th>";
    }
    if(ENABLE_TAX)
        $content .= "<th>Netto:</th>
		    <th>MwSt.:</th>";
	$content .= "
        <th>Brutto:</th>
		<th>Menge:</th>
		<th class='darker_background'>Lager:</th>
		<th>Summen:</th>
	</tr>\n";

    foreach($ar_cart['cart'] as $cart_pos => $ar_cart_values)
    {
        if($ar_cart_values['id'] != 0)
        {
            if(!isset($ar_cart_values['attribute_id'])) $ar_cart_values['attribute_id'] = 0;
            if($ar_cart_values['attribute_id'] > 0)
            {
                $sql           = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE `id` =".$ar_cart_values['attribute_id'].";";
                $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                    $instock = "k.A.";
                else
                {
                    $Aattribute      = $result->fetch_assoc();
                    $instock         = $Aattribute['size'];
                }
            }
            else
            {
                $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =".$ar_cart_values['id'].";";
                $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
                if($result->num_rows == 0)
                    $instock = "k.A.";
                else
                {
                    $Aarticle      = $result->fetch_assoc();
                    $instock       = $Aarticle['quantity'];
                }
            }
        }
        else
            $instock = "k.A.";

        if(empty($ar_cart_values['description']))
            $content .= "<tr class='line_bottom'>";
        $content .= "
		<td>".($cart_pos+1)."</td>
		<td>".$ar_cart_values['name']."</td>";
		if(USE_ARTICLE_NUMBER) $content .= "<td>".$ar_cart_values['number']."</td>";
        if($order_field['delivery_method'] == "weight")
        {
            if(isset($ar_cart_values['weight']))
                $content .= "<td align='right'><nobr>".$Cpage->weight_kilogram($ar_cart_values['weight'])."</nobr></td>";
            else
                $content .= "<td></td>";
        }
        if(ENABLE_TAX)
            $content .= "<td align='right'><nobr>".$Cpage->money($ar_cart_values['netto'])."</nobr></td>
		    <td align='right'>".$ar_cart_values['tax']."%</td>\n";

		$content .= "<td align='right'><nobr>".$Cpage->money($Cpage->tax_calc($ar_cart_values['netto'], "brutto", $ar_cart_values['tax']))."</nobr></td>
		<td align='center'>".$ar_cart_values['quantity']."</td>
		<td class='";

        if($instock != "k.A.")
            if(($instock-$ar_cart_values['quantity']) <= 0) $content .= "important";
            else $content .= "darker";

        $content .= "_background'>$instock</td>
		<td align='right'><nobr>".$Cpage->money($Cpage->tax_calc($ar_cart_values['total_netto'], "brutto", $ar_cart_values['tax']))."</nobr></td>
		</tr>\n";
        $max_cart_pos = $cart_pos;
        if(!empty($ar_cart_values['description']))
            $content .= "<tr class='line_bottom'><td></td><td colspan='".(9+$z+$zz+$zzz)."'>".$ar_cart_values['description']."</td></tr>\n";
    }

    // Versandkosten anzeigen:

    // Falls Versand (Mit und ohne Nachnahme):
    if($order_field['delivery_type'] < 2)
    {

        $content .= "<tr class='line_bottom'><td>".($max_cart_pos+2)."</td><td colspan='".(3+$z+$zzz)."'>Versandkosten nach ";
        // Falls Zielland

        $content .= $Cpage->get_country_name($ar_cart['delivery_country_target']).":";

        // Gewicht:
        if($order_field['delivery_method'] == "weight")
        {
            $content .= "<br>(Gewicht ca. <nobr>".$Cpage->weight_kilogram($ar_cart['total_weight']).")</nobr>";
        }
        // Wenn per Nachnahme:
        $content .= "</td>\n";
        // Versandkosten, Tax, Netto und Brutto:
        // Wenn Fehler in Versandkosten:
        if($ar_cart['shipping_error'] == TRUE)
        {
            $content .= "<td align='right'><nobr></nobr></td>
					<td align='right'></td>
					<td colspan='3' align='right'><span class='error'>Versandkosten können nicht berechnet werden!</span></td>\n";
        }
        else
        {
            // Wenn alles OK:
            if(ENABLE_TAX)
                $content .= "<td align='right'><nobr>".$Cpage->money($ar_cart['shipping_cost_netto'])."</nobr></td>
	          				 <td align='right'>".$ar_cart['shipping_tax_percent']."%</td>";
            $content .= "<td colspan='4' align='right'><nobr>".$Cpage->money($Cpage->tax_calc($ar_cart['shipping_cost_netto'], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]))."</nobr></td>\n";
            $content .= "</tr>";
            // Wenn Nachnahmekosten
            if($order_field['delivery_type'] == 1)
            {
                $max_cart_pos++;
                $content .= "<tr class='line_bottom'><td>".($max_cart_pos+2)."</td><td colspan='".(3+$z+$zzz)."'>Nachnahmekosten</td>";
                if(ENABLE_TAX)
                    $content .= "<td align='right'><nobr>".$Cpage->money($ar_cart['cod_shipping_cost_netto'])."</nobr></td>
							     <td align='right'>".$ar_cart['cod_shipping_tax_percent']."%</td>";
				$content .= "<td colspan='4' align='right'><nobr>".$Cpage->money($Cpage->tax_calc($ar_cart['cod_shipping_cost_netto'], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]))."</nobr></td>\n";
                $content .= "</tr>";
            }
        }
    }
    else
    {
        // Falls Abholung
        if(!empty($order_field['delivery_text']))
        {
            $content .= "<tr class='line_bottom'><td>".($max_cart_pos+2)."</td><td colspan='".(3+$z+$zzz)."'>";
            if(empty($order_field['delivery_text']))
            {
                $content .= $Cpage->Aglobal['delivery'][$order_field['delivery_type']]['value'];
            }
            else $content .= $order_field['delivery_text'];
            $content .= "</td>\n";
            if(ENABLE_TAX)
                $content .= "<td align='right'><nobr>0,00 €</nobr></td>
					         <td align='right'></td>";
            $content .= "<td colspan='4' align='right'><nobr>0,00 €</nobr></td>
					</tr>";
        }
    }

    $content .= "<tr class=''>";

    // Brutto Preis des Warenkorbes:
    $content .= "\n
					<td colspan='".(7+$z+$zz+$zzz)."'></td><td colspan='2' class='td_line_bottom' align='right'><strong>Brutto:</strong></td><td class='td_line_bottom' align='right'><nobr><strong>".$Cpage->money($ar_cart['cart_price_brutto']+$ar_cart['shipping_cost_brutto']+$ar_cart['cod_shipping_cost_brutto'])."</strong></nobr></td>
					</tr>\n";

    // Mehrwertsteuer, falls vorhanden:
    if(!empty($ar_cart['tax']) && ENABLE_TAX)
    {
        foreach($ar_cart['tax'] as $tax_percent => $tax)
        {
            if(!empty($tax))
            {
                $content .= "
								<tr class=''>
								<td colspan='".(7+$z+$zzz)."'></td><td colspan='2' class='td_line_bottom' align='right'><nobr>".$tax_percent."% MwSt.:</nobr></td><td class='td_line_bottom' align='right'><nobr>".$Cpage->money($tax)."</nobr></td>
								</tr>\n";
            }
        }
    }

    // Nettopreis + Aktionsbuttons
    if(ENABLE_TAX)
        $content .= "
		    	<tr class='R_netto'>
			    <td colspan='".(7+$z+$zzz)."'></td><td colspan='2' class='td_line_bottom' align='right'>Netto:</td><td class='td_line_bottom' align='right'><nobr>".$Cpage->money($ar_cart['cart_price_netto']+$ar_cart['shipping_cost_netto']+$ar_cart['cod_shipping_cost_netto'])."</nobr></td>
			    </tr>";
    $content .= "
			<tr class='RL'>
			<td colspan='".(10+$z+$zz+$zzz)."'>".

        $Cpage->input_button("Warenkorb bearbeiten", "editCart(this.form);")."

					</td>
				</tr>
			</table>";
    $content .= "</div></div><div class='clear_float'></div>";

    $content .= "<div class='item_border'><div class='item_header'>Nachrichten:</div><div class='item_content'>\n".$Cpage->table()."
			<tr>
			 <td>\n";
    // User Message + Interne Message + Rechnungs Vermerk
    $content .= $Cpage->table()."
					<tr>
						<td>Hinweise vom Kunden</td>
					</tr>
					<tr>
						<td>".$Cpage->textarea("message", 250, 100).stripslashes($order_field['message'])."</textarea></td>
					</tr>
			</table>
  			</td>
			<td>".$Cpage->table()."
							<tr>
								<td>Interne Vermerke</td>
							</tr>
							<tr>
								<td>".$Cpage->textarea("internal", 250, 100).stripslashes($order_field['internal'])."</textarea></td>
							</tr>
					</table>
			</td>
			</tr>			
            <tr>
			 <td colspan='2'>".$Cpage->table()."
							<tr>
								<td>Rechnungs-Hinweis</td>
							</tr>
							<tr>
								<td>".$Cpage->textarea("invoice_text", 500, 150).stripslashes($order_field['invoice_text'])."</textarea></td>
			 								</tr>
			 								</table>
			 </td>
			 </tr>
			</table>";
    $content .= "</div></div><div class='clear_float'></div>";

    // Dokumente
    $content .= "<div class='item_border'><div class='item_header'>Dokumente:</div><div class='item_content'>\n".$Cpage->table()."
            <tr>
            <td>";

    if(!empty($Adocuments))
    {
        for($i = 0; $i < sizeof($Adocuments); $i++)
        {
            $content .= "<div class='document_wrapper'><a href='".UPLOAD_DIR_ADMIN.$Adocuments[$i]."' target='_blank'>\n<img src='".UPLOAD_DIR_ADMIN."/file_preview_symbol.jpg' alt='".$Adocuments[$i]."'></a><br />\n".$Adocuments[$i]."<br />".$Cpage->input_button("Löschen", "deleteDocument(this.form, \"$i\");")."</div>\n";
        }
    }
    $content .= "
           </td>
          </tr>
          <tr class='RL'>
           <td>Datei hinzufügen: ".$Cpage->input_file("file_to_upload")."</td>
          </tr>
          </table>";
    $content .= "</div></div><div class='clear_float'></div><br />";

    $content .= "<div class='content_wrapper'>".$Cpage->input_submit("Speichern")." ".$Cpage->input_reset("Eingaben zurücksetzen", "didChanges = false;")." ".$Cpage->input_button("Zurück", "Back(this.form);")."</div><div class='clear_float'></div>";

    $content .= "</div></div></form>";
    return $content;
}

function update_order()
{
    global $Cpage;
    global $Cdb;
    $content = "";

    $id = $Cpage->get_parameter("id");
    if(empty($id))
    {
        return FALSE;
    }

    $status        = $Cpage->get_parameter("status");
    $customer_type = $Cpage->get_parameter("customer_type");
    $company       = $Cpage->get_parameter("company");
    $firstname     = $Cpage->get_parameter("firstname");
    $lastname      = $Cpage->get_parameter("lastname");
    $address1      = $Cpage->get_parameter("address1");
    $address2      = $Cpage->get_parameter("address2");
    $zip           = $Cpage->get_parameter("zip");
    $city          = $Cpage->get_parameter("city");
    $phone         = $Cpage->get_parameter("phone");
    $email         = $Cpage->get_parameter("email");
    $country       = $Cpage->get_parameter("country");
    $del_name      = $Cpage->get_parameter("del_name");
    $del_address1  = $Cpage->get_parameter("del_address1");
    $del_address2  = $Cpage->get_parameter("del_address2");
    $del_zip       = $Cpage->get_parameter("del_zip");
    $del_city      = $Cpage->get_parameter("del_city");
    $del_phone     = $Cpage->get_parameter("del_phone");
    $del_country   = $Cpage->get_parameter("del_country");
    $message       = $Cpage->get_parameter("message", "", NO_ESCAPE, DO_HTMLSPECIALCHARS);
    $internal      = $Cpage->get_parameter("internal", "", NO_ESCAPE, DO_HTMLSPECIALCHARS);
    $invoice_text  = $Cpage->get_parameter("invoice_text", "", NO_ESCAPE, DO_HTMLSPECIALCHARS);

    if(empty($del_name))
    {
        $del_country  = NULL;
        $del_address1 = NULL;
        $del_address2 = NULL;
        $del_zip      = NULL;
        $del_city     = NULL;
        $del_phone    = NULL;
    }

    // Überprüfe, ob Datei übertragen wurde:
    if(isset($_FILES['file_to_upload']) && !empty($_FILES['file_to_upload']['name']))
    {
        $uploaddir = UPLOAD_DIR_ADMIN;

        $file_parts = pathinfo($_FILES['file_to_upload']['name']);
        $file_name  = $file_parts['basename'];
        $last_dot   = strrpos($file_name, ".");
        $file_name  = substr($file_name, 0, $last_dot);
        $file_type  = $file_parts['extension'];

        $file_name_original      = $Cpage->check_filename($file_name, $file_type, $uploaddir);
        $upload_file_destination = $uploaddir.$file_name_original;

        if(!move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $upload_file_destination))
        {
            // Upload war nicht erfolgreich:
            $content .= "<div class='item_border'><div class='item_header'>Fehler</div><div class='item_content'>\n";
            $content .= "<font class='error'>Fehler!</font>
	        Die Datei konnte aus unbekannten Gründen nicht hochgeladen werden. Wahrscheinlich hat das upload Verzeichnis \"".UPLOAD_DIR_ADMIN."\" keine Schreibrechte.<br />
	        <br />Debug Informationen:<br />".print_r($_FILES);
            $content .= "</div></div>";
        }
        else
        {
            // Hole eventuell bereits gespeicherte Dokumente
            $sql    = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $Aorder = $result->fetch_assoc();

            $Adocuments = array();

            $tmp = unserialize($Aorder['documents']);
            if(!empty($tmp))
            {
                $Adocuments = unserialize($Aorder['documents']);
            }

            array_push($Adocuments, $file_name_original);

            $documents = serialize($Adocuments);

            // Speicher
            $sql  = "UPDATE `".TBL_PREFIX."orders` SET
				`documents` = ?
				WHERE `id` = ?;";
            $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
            $stmt->bind_param('si', $documents, $id);
            $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
        }
    }

    $sql  = "UPDATE `".TBL_PREFIX."orders` SET
              `changed` = NOW(),
              `status` =?,
              `customer_type` =?,
              `company` =?,
              `firstname` =?,
              `lastname` =?,
              `address1` =?,
              `address2` =?,
              `zip` =?,
              `city` =?,
              `country` =?,
              `phone` =?,
              `email` =?,
              `del_name` =?,
              `del_address1` =?,
              `del_address2` =?,
              `del_zip` =?,
              `del_city` =?,
              `del_country` =?,
              `del_phone` =?,
              `message` =?,
              `internal` =?,
			  `invoice_text` =?
			  WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('iissssssssssssssssssssi', $status, $customer_type, $company, $firstname, $lastname, $address1, $address2, $zip, $city, $country, $phone, $email, $del_name, $del_address1, $del_address2, $del_zip, $del_city, $del_country, $del_phone, $message, $internal, $invoice_text, $id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    return $content;
}

function delete_document()
{
    global $Cpage;
    global $Cdb;

    $document_id = $Cpage->get_parameter("file", -1);
    if($document_id == -1)
    {
        return FALSE;
    }

    $id = $Cpage->get_parameter("id");
    if(empty($id))
    {
        return FALSE;
    }

    $sql    = "SELECT * FROM `".TBL_PREFIX."orders` WHERE id=$id;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Aorder = $result->fetch_assoc();

    $Adocuments = unserialize($Aorder['documents']);

    unlink(UPLOAD_DIR_ADMIN.$Adocuments[$document_id]);

    array_splice($Adocuments, $document_id, 1);

    $documents = serialize($Adocuments);

    // Speicher
    $sql  = "UPDATE `".TBL_PREFIX."orders` SET
		`documents` = ?
		WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('si', $documents, $id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
    return TRUE;
}

function create_invoice()
{
    global $Cpage;
    global $Cdb;
    $content = "";

    $id = $Cpage->get_parameter("id");
    if(empty($id))
    {
        return FALSE;
    }

    $sql             = "SELECT * FROM `".TBL_PREFIX."counter` WHERE `type` ='INVOICE_COUNTER';";
    $result          = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acounter        = $result->fetch_assoc();
    $invoice_counter = $Acounter['count'];

    $invoice_id = INVOICE_PREFIX.($invoice_counter+INVOICE_START).INVOICE_SUFFIX;

    $sql           = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Ainvoice      = $result->fetch_assoc();
    $delivery_text = $Cpage->Aglobal['delivery'][$Ainvoice['delivery_type']]['value'];
    if(!empty($Ainvoice['invoice_id']))
    {
        return FALSE;
    }

    $Acart = unserialize($Ainvoice['cart']);

    $sql  = "UPDATE `".TBL_PREFIX."orders` SET
              `changed` = NOW(),
              `invoice_id` =?,
              `invoice_time` =NOW(),
              `invoice_counter` =?,
			  `delivery_text` =?
			  WHERE `id` = ?;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('sisi', $invoice_id, $invoice_counter, $delivery_text, $id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    $sql  = "UPDATE `".TBL_PREFIX."counter` SET
				`count` = `count`+1
				WHERE `type` = 'INVOICE_COUNTER';";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);


    $difference = array();
    $difference_attribute = array();
    if(sizeof($Acart['cart']) > 0)
    {
        foreach($Acart['cart'] as $key => $value)
        {
            if($value['id'] != 0)
            {
                if($value['attribute_id'] >0 )
                {
                    $difference_attribute[$value['attribute_id']] = -$value['quantity'];
                }
                else
                    $difference[$value['id']] = -$value['quantity'];
            }
        }
    }

    if(sizeof($difference) > 0)
    {
        foreach($difference as $article_id => $value)
        {
            $sql = "UPDATE ".TBL_PREFIX."articles SET `quantity` = `quantity` + $value WHERE `id` =$article_id AND `quantity_warning` > 0;";
            $Cdb->db_query($sql);
            $sql = "UPDATE ".TBL_PREFIX."articles SET `bought` = `bought` - $value WHERE `id` =$article_id;";
            $Cdb->db_query($sql);
        }
    }
    if(sizeof($difference_attribute) > 0)
    {
        foreach($difference_attribute as $attribute_id => $value)
        {
            $sql = "UPDATE ".TBL_PREFIX."attributes SET `size` = `size` + $value WHERE `id` =$attribute_id AND `warning` > 0;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $sql = "UPDATE ".TBL_PREFIX."attributes SET `bought` = `bought` - $value WHERE `id` =$attribute_id;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }

    // Lösche die reservierten Bestellungen
    $sql = "DELETE FROM `".TBL_PREFIX."reserved_orders` WHERE `order_id` =$id";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    return $content;
}


function delete_order()
{
    global $Cpage;
    global $Cdb;

    $id     = $Cpage->get_parameter("id", 0);
    if($id==0) return false;

    $sql             = "DELETE FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
    return true;
}

function change_invoice_into_order()
{
    global $Cpage;
    global $Cdb;

    $id     = $Cpage->get_parameter("id", 0);
    if($id==0) return false;

    $sql  = "UPDATE `".TBL_PREFIX."orders` SET
              `changed` = NOW(),
              `invoice_id` = NULL,
              `invoice_time` = NULL,
              `invoice_counter` = NULL,
			  `delivery_text` = NULL
			  WHERE `id` = $id;";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $sql  = "UPDATE `".TBL_PREFIX."counter` SET
				`count` = `count`-1
				WHERE `type` = 'INVOICE_COUNTER';";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);


    $sql           = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Ainvoice      = $result->fetch_assoc();

    $Acart = unserialize($Ainvoice['cart']);

    $difference = array();
    $difference_attribute = array();
    if(sizeof($Acart['cart']) > 0)
    {
        foreach($Acart['cart'] as $key => $value)
        {
            if($value['id'] != 0)
            {
                if($value['attribute_id'] >0 )
                {
                    $difference_attribute[$value['attribute_id']] = -$value['quantity'];
                }
                else
                    $difference[$value['id']] = -$value['quantity'];
            }
        }
    }

    if(sizeof($difference) > 0)
    {
        foreach($difference as $article_id => $value)
        {
            $sql = "UPDATE ".TBL_PREFIX."articles SET `quantity` = `quantity` - $value WHERE `id` =$article_id AND `quantity_warning` > 0;";
            $Cdb->db_query($sql);
            $sql = "UPDATE ".TBL_PREFIX."articles SET `bought` = `bought` + $value WHERE `id` =$article_id;";
            $Cdb->db_query($sql);
        }
    }
    if(sizeof($difference_attribute) > 0)
    {
        foreach($difference_attribute as $attribute_id => $value)
        {
            $sql = "UPDATE ".TBL_PREFIX."attributes SET `size` = `size` - $value WHERE `id` =$attribute_id AND `warning` > 0;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $sql = "UPDATE ".TBL_PREFIX."attributes SET `bought` = `bought` + $value WHERE `id` =$attribute_id;";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);
        }
    }
}

function back()
{
    global $Cpage;
    global $script_footer;
    $content = "";

    $start     = $Cpage->get_parameter("start", 0);
    $search    = $Cpage->get_parameter("search", "");
    $amount    = $Cpage->get_parameter("amount", 20);
    $item      = $Cpage->get_parameter("item", "id");
    $direction = $Cpage->get_parameter("direction", "desc");
    $status2   = $Cpage->get_parameter("status2", -1);

    $content .= $Cpage->form("back_form", "correspondence.php").$Cpage->input_hidden("start", $start).$Cpage->input_hidden("status2", $status2).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("amount", $amount).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("direction", $direction)."</form>";
    $script_footer .= "
                 document.back_form.submit();
				";

    return $content;
}