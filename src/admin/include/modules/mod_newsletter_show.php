<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$show_page = FALSE;


$text   = $Cpage->get_parameter("text", "", NO_ESCAPE);
$sql    = "SELECT * FROM `".TBL_PREFIX."email_".LANGUAGE."`
          WHERE
          `name` = 'mail_header'";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$header = $result->fetch_assoc();

$sql    = "SELECT * FROM `".TBL_PREFIX."email_".LANGUAGE."`
          WHERE
          `name` = 'mail_footer'";
$result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$footer = $result->fetch_assoc();


$content = "<!DOCTYPE html>\n".$header['text'].$text."<br /><br />
		<p class='p_line'></p>		
    	<div class='content_wrapper'><a href='#' onClick='window.close();' class='link_button'>Fenster schließen</a></div><br /><br />
		".$footer['text'];

 