<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "change_type":  // and Name
        change_type();
        $content .= start();
        break;

    case "add_file":

        $content .= add_file();
        $content .= start();

        break;
    case "delete_file":

        delete_file();
        $content .= start();

        break;
    default:

        $content .= start();
        break;
}

function start()
{
    global $Cdb;
    global $Cpage;
    $content = "";

    $sql    = "SELECT * FROM `".TBL_PREFIX."email_attachements_".LANGUAGE."` ORDER BY `file` ASC, `type` DESC;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= $Cpage->form_file("email_attachement_form", "email_attachement.php", "add_file");
    $content .= "<div class='item_border'><div class='item_header'>Neue Datei hinzufügen</div>
    <div class='item_content content_wrapper'>\n".$Cpage->table()."<tr><td>".$Cpage->input_file("file_to_upload")."</td>
    <td>Anzeigename (mit .Dateityp am Ende!) ".$Cpage->input_text("name")."</td><td>Typ: ".$Cpage->select("type")."
    <option value='1'>Nur bei Bestellungen</option><option value='2'>Bei allen E-Mails</option></select><td>".
        $Cpage->input_submit("Hochladen")."</td><td>Maximale Datei Größe: ".$Cpage->return_max_upload()." Megabyte</td></tr></table>
        </div></div></form><div class='clear_float'></div>\n";
    if($result->num_rows != 0)
    {
        $content .= "<div class='item_border'><div class='item_header'>Vorhandene Dateien:</div>
    <div class='item_content content_wrapper'>\n".
            $Cpage->table()."<tr><th>Datei:</th><th>Name + Typ:</th><th>Aktion:</th></tr>\n";
        
        while($row = $result->fetch_assoc())
        {
            $file = $row['file'];
            $slash_pos = strrpos($file, "/");
            if($slash_pos !== FALSE)
            {
                $file = substr($file, $slash_pos+1);
            }
            $slash_pos = strrpos($file, "\\");
            if($slash_pos !== FALSE)
            {
                $file = substr($file, $slash_pos+1);
            }
            
            $content .= "<tr><td>$file</td><td>".$Cpage->form("email_attachement_form_type", "email_attachement.php", "change_type").
                $Cpage->input_hidden("id", $row['id']).$Cpage->input_text("name", $row['name'])." ".$Cpage->select("type")."
                <option value='1'"; if($row["type"] == 1) $content .= " selected=selected"; $content .= ">Nur bei Bestellungen</option>
                <option value='2'"; if($row["type"] == 2) $content .= " selected=selected"; $content .= ">Bei allen Bestellungen</option>
                </select> ".$Cpage->input_submit("Speichern")."</form></td><td>".$Cpage->form("email_attachement_form_file", "email_attachement.php", "delete_file").$Cpage->input_hidden("id", $row['id']).
                $Cpage->input_submit("Datei löschen")."</form></td></tr>\n";
        }

        $content .= "</table></div></div></form><div class='clear_float'></div>\n";
    }

    return $content;
}

function add_file()
{
    global $dir_root;
    global $Cpage;
    global $Cdb;
    $uploaddir = "download/";
    $content = "";

    $type = $Cpage->get_parameter("type");
    $name = $Cpage->get_parameter("name");
    //$not_allowed_filetypes = array ("php", "exe", "com", "bat", "cgi", "cmd", "scr", "pif", "hta", "scf", "sh", "msi", "html", "htm",
    //    "js", "mde", "nws", "reg", "url", "ws.bas", "bas", "cpl", "folder", "inf", "jse", "msc", "scf", "vb", "shs");

    // Überprüfe, ob das Bild vom Formular richtig übertragen wurde:
    if(isset($_FILES['file_to_upload']) && !empty($_FILES['file_to_upload']['name']))
    {
        // Bestimme die Formatierung des Dateinamens:
        $file_parts = pathinfo($_FILES['file_to_upload']['name']);
        $file_name  = $file_parts['basename'];
        $last_dot   = strrpos($file_name, ".");
        $file_name  = substr($file_name, 0, $last_dot);
        $file_type  = $file_parts['extension'];

        /*
        if(in_array($file_type, $not_allowed_filetypes))
        {
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Es gab ein Problem beim Upload, dieser Dateityp ist nicht erlaubt: ".$file_type.".<br/>
                </p><p class='bottom'>".$Cpage->input_button("Zurück", "history.back();")."</p></fieldset>";
            return $content;
        }
        */

        $file_name_original = $Cpage->check_filename($file_name, $file_type, $dir_root.$uploaddir);

        // Bestimme Zieldatei mit Pfad:
        $upload_file_destination        = $dir_root.$uploaddir.$file_name_original;

        // Die Original Datei hochladen:
        if(move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $upload_file_destination))
        {
            $file = $file_name_original;

            $sql    = "INSERT INTO `".TBL_PREFIX."email_attachements_".LANGUAGE."` (`file`, `type`, `name`) VALUES ('$file', '$type', '$name'');";
            $Cdb->db_query($sql, __FILE__.":".__LINE__);

        }
        else
        {
            $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Es gab ein Problem beim Upload, evtl. sind keine Schreibrechte vorhanden, oder die Datei ist zu groß.<br/>
                Quellpfad: ".$_FILES['file_to_upload']['tmp_name']."<br />
                Zielpfad: ".$upload_file_destination."</p></fieldset>";

            return $content;
        }
    }
    else
    {
        $content .= "<fieldset class='info'><legend class='error'>Fehler!</legend>
                <p class='top'>Die Datei wurde nicht korrekt vom Formular übertragen.</p>
                </fieldset>";
        return $content;

    }



    return $content;
}

function change_type()
{
    global $Cpage;
    global $Cdb;

    $type = $Cpage->get_parameter("type");
    $name = $Cpage->get_parameter("name");
    $id = $Cpage->get_parameter("id");

    $sql = "UPDATE `".TBL_PREFIX."email_attachements_".LANGUAGE."`  SET `type` = '$type', `name` = '$name' WHERE `id`='$id';";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}

function delete_file()
{
    global $Cpage;
    global $Cdb;
    global $dir_root;
    $uploaddir = "download/";

    $id = $Cpage->get_parameter("id");

    $sql    = "SELECT * FROM `".TBL_PREFIX."email_attachements_".LANGUAGE."` WHERE `id`='$id';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $row = $result->fetch_assoc();

    $file =$dir_root.$uploaddir.$row['file'];
    unlink($file);

    $sql    = "DELETE FROM `".TBL_PREFIX."email_attachements_".LANGUAGE."` WHERE `id`='$id';";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

}