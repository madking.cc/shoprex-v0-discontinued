<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$id = $Cpage->get_parameter("id");
if(empty($id))
{
    return;
}

$sql      = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =$id;";
$result   = $Cdb->db_query($sql, __FILE__.":".__LINE__);
$Ainvoice = $result->fetch_assoc();

if(empty($Ainvoice['invoice_id']))
{
    return FALSE;
}

$Ainvoice['use_delivery_data'] = FALSE;
if(!empty($Ainvoice['del_name']))
{
    $Ainvoice['use_delivery_data'] = TRUE;
}

$Ainvoice['country']     = $Cpage->get_country_name($Ainvoice['country']);
$Ainvoice['del_country'] = $Cpage->get_country_name($Ainvoice['del_country']);

$Ainvoice['address']     = $Ainvoice['address1'];
$Ainvoice['del_address'] = $Ainvoice['del_address1'];
if(!empty($Ainvoice['address2']))
{
    $Ainvoice['address'] .= "<br />".$Ainvoice['address2'];
}
if(!empty($Ainvoice['del_address2']))
{
    $Ainvoice['del_address'] .= "<br />".$Ainvoice['del_address2'];
}

$Ainvoice['name'] = $Ainvoice['firstname']." ".$Ainvoice['lastname'];
if(!empty($Ainvoice['company']))
{
    $Ainvoice['name'] = $Ainvoice['company']."<br />".$Ainvoice['name'];
}

$parcel_draft = $Cpage->get_invoice_draft("parcel");

if(!$Ainvoice['use_delivery_data'])
{
    $parcel_draft['text'] = preg_replace("/\[!if_delivery\](.*?)\[!end_if_delivery\]/is", "", $parcel_draft['text']);
    $parcel_draft['text'] = preg_replace("/\[!if_invoice\]/is", "", $parcel_draft['text']);
    $parcel_draft['text'] = preg_replace("/\[!end_if_invoice\]/is", "", $parcel_draft['text']);
}
else
{
    $parcel_draft['text'] = preg_replace("/\[!if_invoice\](.*?)\[!end_if_invoice\]/is", "", $parcel_draft['text']);
    $parcel_draft['text'] = preg_replace("/\[!if_delivery\]/is", "", $parcel_draft['text']);
    $parcel_draft['text'] = preg_replace("/\[!end_if_delivery\]/is", "", $parcel_draft['text']);
}

foreach($Ainvoice as $key => $value)
{
    $parcel_draft['text'] = preg_replace("/\\\$invoice\['".$key."'\]/i", $value, $parcel_draft['text']);
}

$parcel_settings = $Cpage->get_invoice_draft("parcel_settings");
$parcel_settings = unserialize($parcel_settings['text']);

$head = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
        \"http://www.w3.org/TR/html4/strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\"
  xml:lang=\"de\" lang=\"de\">
<head>
<title>".PAGE_TITLE.$subtitle."</title>
<meta http-equiv='content-type' content='text/html; charset=utf-8' />
<link rel='stylesheet' href='layout/css/screen_template.css' type='text/css' media='screen' />
<link rel='stylesheet' href='layout/css/print_template.css' type='text/css' media='print' />
<link rel='shortcut icon' href='layout/favicon.ico' type='image/x-icon' />
</head>
<body>
";
$parcel_draft['text'] = $head.$Cpage->input_button("Drucken", "window.print();")." ".$Cpage->input_button("Fenster schliessen", "window.close()")."<br />"."<div style='padding-top:".$parcel_settings['top']."mm;padding-left:".$parcel_settings['left']."mm;'>\n".$parcel_draft['text']."</div>";
$parcel_draft['text'] .= "</body></html>";

$content .= $parcel_draft['text'];
