<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "save_and_quit":
        update_cart();
        $content .= save_cart();
        break;
    case "update":
        update_cart();
    default:
        $content .= edit_cart();
        break;
}

function save_cart()
{
    global $Cpage;
    global $Cdb;
    global $AnewCart;
    global $script_footer;

    $content   = "";
    $id        = $Cpage->get_parameter("id", 0);
    $start     = $Cpage->get_parameter("start", 0);
    $search    = $Cpage->get_parameter("search", "");
    $amount    = $Cpage->get_parameter("amount", 20);
    $item      = $Cpage->get_parameter("item", "id");
    $direction = $Cpage->get_parameter("direction", "desc");
    $status2   = $Cpage->get_parameter("status2", -1);

    $sql         = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =".$id.";";
    $result      = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $order_field = $result->fetch_assoc();


    // Falls eine Rechnung bearbeitet wird:
    if(!empty($order_field['invoice_id']))
    {
        $Acart = unserialize($order_field['cart']);

        $difference = array();
        $cart_tmp   = $AnewCart['cart'];
        if(sizeof($Acart['cart']) > 0)
        {
            if(sizeof($cart_tmp) > 0)
            {
                // $value = Alter gespeicherter Warenkorb
                // $value2 = Neuer online geänderter Warenkorb

                foreach($Acart['cart'] as $key => $value)
                {
                    $match_found = FALSE;
                    foreach($cart_tmp as $key2 => $value2)
                    {
                        if($value2['id'] == $value['id'])
                        {
                            $match_found               = TRUE;
                            if($value['attribute_id'] > 0 && $value2['attribute_id'] == 0)
                            {
                                $difference_attribute[$value['attribute_id']] = $value['quanitity'];
                                $difference[$value2['id']] = -$value2['quantity'];
                            }
                            elseif($value['attribute_id'] = 0 && $value2['attribute_id'] > 0)
                            {
                                $difference_attribute[$value2['attribute_id']] = -$value2['quanitity'];
                                $difference[$value['id']] = $value['quantity'];
                            }
                            elseif(($value['attribute_id'] > 0 && $value2['attribute_id'] > 0) && ($value['attribute_id'] != $value2['attribute_id']))
                            {
                                $difference_attribute[$value['attribute_id']] = $value['quanitity'];
                                $difference_attribute[$value2['attribute_id']] = -$value2['quanitity'];
                            }
                            else
                            {
                                $difference[$value2['id']] = $value['quantity']-$value2['quantity'];
                            }
                            unset($cart_tmp[$key2]);
                        }
                    }
                    if(!$match_found)
                    {
                        $difference[$value['id']] = $value['quantity'];
                    }
                }
                if(sizeof($cart_tmp) > 0)
                {
                    foreach($cart_tmp as $key => $value)
                        if($value['id'] != 0)
                        {
                            if($value['attribute_id'] > 0)
                                $difference_attribute[$value['attribute_id']] = -$value['quanitity'];
                            else
                                $difference[$value['id']] = -$value['quantity'];
                        }
                }
            }
        }
        else
        {
            if(sizeof($AnewCart['cart']) > 0)
            {
                foreach($AnewCart['cart'] as $key2 => $value2)
                    if($value2['id'] != 0)
                    {
                        if($value2['attribute_id'] > 0)
                            $difference_attribute[$value2['attribute_id']] = -$value2['quanitity'];
                        else
                            $difference[$value2['id']] = -$value2['quanitity'];
                    }
            }
        }

        if(sizeof($difference) > 0)
        {
            foreach($difference as $article_id => $value)
            {
                $sql = "UPDATE ".TBL_PREFIX."articles SET `quantity` = `quantity` + $value WHERE `id` =$article_id AND `quantity_warning` > 0;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
                $sql = "UPDATE ".TBL_PREFIX."articles SET `bought` = `bought` - $value WHERE `id` =$article_id;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
            }
        }

        if(sizeof($difference_attribute) > 0)
        {
            foreach($difference_attribute as $attribute_id => $value)
            {
                $sql = "UPDATE ".TBL_PREFIX."attributes SET `size` = `size` + $value WHERE `id` =$attribute_id AND `warning` > 0;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
                $sql = "UPDATE ".TBL_PREFIX."attributes SET `bought` = `bought` - $value WHERE `id` =$attribute_id;";
                $Cdb->db_query($sql, __FILE__.":".__LINE__);
            }
        }
    }

    $newCart       = serialize($AnewCart);
    $action_needed = 0;
    $delivery_type = $Cpage->get_parameter("delivery_type", NULL);
    $pickup        = $Cpage->get_parameter("pickup", NULL);

    if($delivery_type >= 2)
    {
        if($pickup == -1)
        {
            $delivery_text = "";
        }
        else
        {
            $delivery_text = $Cpage->Aglobal['delivery'][$pickup]['value'];
        }
    }
    else
    {
        $delivery_text = $Cpage->Aglobal['delivery'][$delivery_type]['value'];
    }

    $sql  = "UPDATE `".TBL_PREFIX."orders` SET
	      `cart`             = ?,
	      `delivery_text`    = ?,
	      `delivery_type`    = ?,
	      `action_needed`    = ?
	      WHERE
	      `id` = ?
	      LIMIT 1 ;";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ssiii', $newCart, $delivery_text, $delivery_type, $action_needed, $id);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    $content .= $Cpage->form("back_form", "correspondence_edit.php").$Cpage->input_hidden("id", $id).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("status2", $status2).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("amount", $amount).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("direction", $direction)."</form>";

    $script_footer .= "
				    document.back_form.submit();
				    ";
    return $content;
}

function update_cart()
{
    global $Cpage;
    global $Cdb;
    global $AnewCart;

    if(isset($AnewCart))
    {
        unset($AnewCart);
    }
    $delete_pos               = $Cpage->get_parameter("delete_pos", NULL, NO_ESCAPE);
    $article_id               = $Cpage->get_parameter("article_id", NULL, NO_ESCAPE);
    $name                     = $Cpage->get_parameter("name", NULL, NO_ESCAPE);
    $number                   = $Cpage->get_parameter("number", NULL, NO_ESCAPE);
    $weight                   = $Cpage->get_parameter("weight", NULL, NO_ESCAPE);
    $netto                    = $Cpage->get_parameter("netto", NULL, NO_ESCAPE);
    $tax                      = $Cpage->get_parameter("tax", NULL, NO_ESCAPE);
    $brutto                   = $Cpage->get_parameter("brutto", NULL, NO_ESCAPE);
    $quantity                 = $Cpage->get_parameter("quantity", NULL, NO_ESCAPE);
    $description              = $Cpage->get_parameter("description", NULL, NO_ESCAPE);
    $attribute_id             = $Cpage->get_parameter("attribute_id", NULL, NO_ESCAPE);
    $delete_shipping          = $Cpage->get_parameter("delete_shipping", NULL);
    $delivery_type            = $Cpage->get_parameter("delivery_type", NULL);
    $delivery_country_target  = $Cpage->get_parameter("delivery_country_target", NULL);
    $shipping_cost_netto      = $Cpage->get_parameter("shipping_cost_netto", NULL);
    $shipping_tax_percent     = $Cpage->get_parameter("shipping_tax_percent", 0);
    $shipping_cost_brutto     = $Cpage->get_parameter("shipping_cost_brutto", NULL);
    $delete_cod_shipping      = $Cpage->get_parameter("delete_cod_shipping", NULL);
    $cod_shipping_cost_netto  = $Cpage->get_parameter("cod_shipping_cost_netto", NULL);
    $cod_shipping_tax_percent = $Cpage->get_parameter("cod_shipping_tax_percent", 0);
    $cod_shipping_cost_brutto = $Cpage->get_parameter("cod_shipping_cost_brutto", NULL);
    $delete_pickup            = $Cpage->get_parameter("delete_pickup", NULL);
    $pickup                   = $Cpage->get_parameter("pickup", NULL);
    $add_name                 = $Cpage->get_parameter("add_name", NULL);
    $add_number               = $Cpage->get_parameter("add_number", 0);
    $add_weight               = $Cpage->get_parameter("add_weight", 0);
    $add_netto                = $Cpage->get_parameter("add_netto", NULL);
    $add_tax                  = $Cpage->get_parameter("add_tax", NULL);
    $add_brutto               = $Cpage->get_parameter("add_brutto", NULL);
    $add_quantity             = $Cpage->get_parameter("add_quantity", NULL);
    $add_description          = $Cpage->get_parameter("add_description", NULL);
    $add_article              = $Cpage->get_parameter("add_article", NULL);
    $add_quantity2            = $Cpage->get_parameter("add_quantity2", NULL);

    array_shift($brutto);
    array_shift($netto);
    array_shift($weight);
    array_shift($quantity);
    array_shift($tax);
    array_shift($name);
    array_shift($number);
    array_shift($description);
    array_shift($attribute_id);

    if(!isset($article_id))
    {
        $article_id = array();
    }
    if(!isset($name))
    {
        $name = array();
    }
    if(!isset($number))
    {
        $number = array();
    }
    if(!isset($weight))
    {
        $weight = array();
    }
    if(!isset($netto))
    {
        $netto = array();
    }
    if(!isset($tax))
    {
        $tax = array();
    }
    if(!isset($brutto))
    {
        $brutto = array();
    }
    if(!isset($quantity))
    {
        $quantity = array();
    }
    if(!isset($name))
    {
        $name = array();
    }
    if(!isset($number))
    {
        $number = array();
    }
    if(!isset($description))
    {
        $description = array();
    }
    if(!isset($attribute_id))
    {
        $attribute_id = array();
    }


    $Atax     = array();
    $AnewCart = array();

    $AnewCart['delivery_country_target'] = $delivery_country_target;
    $AnewCart['delivery_type']           = $delivery_type;
    if(empty($delete_pickup))
    {
        $AnewCart['pickup'] = $pickup;
    }
    else
    {
        $AnewCart['pickup'] = "";
    }

    if(!empty($add_name))
    {
        array_push($article_id, "0");
        array_push($number, $add_number);
        array_push($weight, $add_weight);
        array_push($tax, $add_tax);
        array_push($quantity, $add_quantity);
        array_push($name, $add_name);
        array_push($description, $add_description);
        if(!empty($add_netto))
        {
            array_push($netto, $add_netto);
        }
        else
        {
            $add_netto = $Cpage->tax_calc($add_brutto, "netto", $add_tax);
            array_push($netto, $add_netto);
        }
        if(!empty($add_brutto))
        {
            array_push($brutto, $add_brutto);
        }
        else
        {
            $add_brutto = $Cpage->tax_calc($add_netto, "brutto", $add_tax);
            array_push($brutto, $add_brutto);
        }
    }

    if(!empty($add_article))
    {
        $sql    = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$add_article;";
        $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        if($result->num_rows == 1)
        {
            $Aarticle = $result->fetch_assoc();
            array_push($article_id, $add_article);
            array_push($number, $Aarticle['number']);
            array_push($weight, $Aarticle['weight']);
            array_push($tax, $Cpage->Aglobal['tax'][$Aarticle['tax']]);
            array_push($quantity, $add_quantity2);
            array_push($name, $Aarticle['name']);
            array_push($netto, $Aarticle['netto']);
            $add_brutto = $Cpage->tax_calc($Aarticle['netto'], "brutto", $Cpage->Aglobal['tax'][$Aarticle['tax']]);
            array_push($brutto, $add_brutto);
            array_push($description, "");
            array_push($attribute_id, 0);
        }
    }

    if(empty($delete_shipping) && ($delivery_type <= 1))
    {
        if(empty($shipping_cost_netto))
        {
            $shipping_cost_netto = $Cpage->tax_calc($shipping_cost_brutto, "netto", $shipping_tax_percent);
        }
        if(empty($shipping_cost_brutto))
        {
            $shipping_cost_brutto = $Cpage->tax_calc($shipping_cost_netto, "brutto", $shipping_tax_percent);
        }
        $AnewCart['shipping_cost_netto']  = $shipping_cost_netto;
        $AnewCart['shipping_cost_brutto'] = $shipping_cost_brutto;
        $AnewCart['shipping_tax_percent'] = $shipping_tax_percent;
        $dummy                            = $Cpage->tax_calc($shipping_cost_netto, "tax_of_netto", $shipping_tax_percent);

        if(!empty($dummy) && !empty($shipping_tax_percent))
        {
            @$Atax[$shipping_tax_percent] += $dummy;
        }
    }
    else
    {
        $AnewCart['shipping_cost_netto']  = "";
        $AnewCart['shipping_cost_brutto'] = "";
        $AnewCart['shipping_tax_percent'] = "";
    }

    $AnewCart['shipping_tax_key'] = SHIPPING_TAX_KEY;
    $AnewCart['cod_tax_key']      = COD_TAX_KEY;
    if(empty($delete_cod_shipping) && ($delivery_type == 1))
    {
        if(empty($cod_shipping_cost_netto))
        {
            $cod_shipping_cost_netto = $Cpage->tax_calc($cod_shipping_cost_brutto, "netto", $cod_shipping_tax_percent);
        }
        if(empty($cod_shipping_cost_brutto))
        {
            $cod_shipping_cost_brutto = $Cpage->tax_calc($cod_shipping_cost_netto, "brutto", $cod_shipping_tax_percent);
        }
        $AnewCart['cod_shipping_cost_netto']  = $cod_shipping_cost_netto;
        $AnewCart['cod_shipping_cost_brutto'] = $cod_shipping_cost_brutto;
        $AnewCart['cod_shipping_tax_percent'] = $cod_shipping_tax_percent;
        $dummy                                = $Cpage->tax_calc($cod_shipping_cost_netto, "tax_of_netto", $cod_shipping_tax_percent);
        if(!empty($dummy) && !empty($cod_shipping_tax_percent))
        {
            $Atax[$cod_shipping_tax_percent] += $dummy;
        }
    }
    else
    {
        $AnewCart['cod_shipping_cost_netto']  = "";
        $AnewCart['cod_shipping_cost_brutto'] = "";
        $AnewCart['cod_shipping_tax_percent'] = "";
    }

    if(is_array($delete_pos))
    {
        foreach($delete_pos as $value)
        {
            unset($article_id[$value]);
            unset($name[$value]);
            unset($number[$value]);
            unset($weight[$value]);
            unset($netto[$value]);
            unset($tax[$value]);
            unset($brutto[$value]);
            unset($quantity[$value]);
            unset($description[$value]);
            unset($attribute_id[$value]);
        }
        if(is_array($article_id))
        {
            array_merge($article_id);
            array_merge($name);
            array_merge($number);
            array_merge($weight);
            array_merge($netto);
            array_merge($tax);
            array_merge($brutto);
            array_merge($quantity);
            array_merge($description);
            array_merge($attribute_id);
        }
    }

    $AnewCart['cart']  = array();
    $total_weight      = 0;
    $entire_quantity   = 0;
    $cart_price_netto  = 0;
    $cart_price_brutto = 0;
    $i=0;
    if(is_array($article_id))
    {
        foreach($article_id as $key => $value)
        {
            $AnewCart['cart'][$i]                 = array();
            $AnewCart['cart'][$i]['id']           = $article_id[$key];
            $AnewCart['cart'][$i]['name']         = $name[$key];
            $AnewCart['cart'][$i]['number']       = $number[$key];
            $AnewCart['cart'][$i]['weight']       = $weight[$key];
            $AnewCart['cart'][$i]['total_weight'] = ($quantity[$key]*$weight[$key]);
            $total_weight += $AnewCart['cart'][$i]['total_weight'];
            $AnewCart['cart'][$i]['tax']      = $tax[$key];
            $AnewCart['cart'][$i]['quantity'] = $quantity[$key];
            $AnewCart['cart'][$i]['description'] = $description[$key];
            if(!isset($attribute_id[$key]))
                $attribute_id[$key] = 0;
            $AnewCart['cart'][$i]['attribute_id'] = $attribute_id[$key];
            $entire_quantity += $quantity[$key];
            if(empty($netto[$key]))
            {
                $AnewCart['cart'][$i]['netto'] = $Cpage->tax_calc($brutto[$key], "netto", $tax[$key]);
                $dummy                           = $brutto[$key];
            }
            else
            {
                $AnewCart['cart'][$i]['netto'] = $netto[$key];
                $dummy                           = $Cpage->tax_calc($netto[$key], "brutto", $tax[$key]);
            }
            $cart_price_netto += ($AnewCart['cart'][$i]['netto']*$quantity[$key]);
            $cart_price_brutto += ($dummy*$quantity[$key]);
            $AnewCart['cart'][$i]['total_netto'] = $quantity[$key]*$AnewCart['cart'][$i]['netto'];
            $dummy                                 = $Cpage->tax_calc($AnewCart['cart'][$i]['total_netto'], "tax_of_netto", $tax[$key]);

            if(!empty($tax[$key]) && !empty($dummy))
            {
                if(isset($Atax[$tax[$key]]))
                {
                    $Atax[$tax[$key]] += $dummy;
                }
                else $Atax[$tax[$key]] = $dummy;
            }
            $i++;
        }
    }

    $AnewCart['positions']         = $i;
    $AnewCart['total_weight']      = $total_weight;
    $AnewCart['cart_price_netto']  = $cart_price_netto;
    $AnewCart['cart_price_brutto'] = $cart_price_brutto;

    $AnewCart['shipping_error']     = FALSE;
    $AnewCart['cod_shipping_error'] = FALSE;
    $AnewCart['entire_quantity']    = $entire_quantity;
    $AnewCart['tax']                = $Atax;
}

function edit_cart()
{
    global $Cpage;
    global $Cdb;
    global $Carticles;
    global $AnewCart;
    global $script;
    $content = "";

    $id = $Cpage->get_parameter("id");
    if(empty($id))
    {
        return FALSE;
    }

    $start     = $Cpage->get_parameter("start", 0);
    $search    = $Cpage->get_parameter("search", "");
    $amount    = $Cpage->get_parameter("amount", 20);
    $item      = $Cpage->get_parameter("item", "id");
    $direction = $Cpage->get_parameter("direction", "desc");
    $status2   = $Cpage->get_parameter("status2", -1);

    $sql         = "SELECT * FROM `".TBL_PREFIX."orders` WHERE `id` =".$id.";";
    $result      = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $order_field = $result->fetch_assoc();

    if(isset($AnewCart))
    {
        $ar_cart                  = $AnewCart;
        $shipping_tax_percent     = $AnewCart['shipping_tax_percent'];
        $cod_shipping_tax_percent = $AnewCart['cod_shipping_tax_percent'];
        $delivery_type            = $AnewCart['delivery_type'];
        $delivery_type2           = $AnewCart['pickup'];
    }
    else
    {

        // Hole den kompletten alten Warenkorb
        $ar_cart_temp             = unserialize($order_field['cart']);
        $ar_cart                  = $ar_cart_temp;
        $shipping_tax_percent     = $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY];
        $cod_shipping_tax_percent = $Cpage->Aglobal['tax'][COD_TAX_KEY];
        $delivery_type            = $order_field['delivery_type'];
        $delivery_type2           = $delivery_type;
    }

    if($order_field['delivery_method'] == "weight")
    {
        $z = 0;
    }
    else
    {
        $z = -1;
    }

    $content .= $Cpage->form("back_form", "correspondence_edit.php").$Cpage->input_hidden("id", $id).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("status2", $status2).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("amount", $amount).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("direction", $direction)."</form>";

    $script .= "     function cancelCart() 			{ document.back_form.submit(); }
					 function saveCart(formID) 		{ if(checkSubmit(formID)) {formID.do_action.value='save_and_quit'; formID.submit(); }}

                     function checkSubmit(formID)
                     {
                        	for (var i=formID.elements[\"netto[]\"].length-1; i>=0; i--)
                            {
                                if(formID.elements[\"netto[]\"][i].value != '')
                                {
                                    formID.elements[\"netto[]\"][i].value = formID.elements[\"netto[]\"][i].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"netto[]\"][i].value))
                                    {
                                        alert('Es sind nur Zahlen bei Netto erlaubt. Abbruch.');
                                        formID.elements[\"netto[]\"][i].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"brutto[]\"][i].value != '')
                                {
                                    formID.elements[\"brutto[]\"][i].value = formID.elements[\"brutto[]\"][i].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"brutto[]\"][i].value))
                                    {
                                        alert('Es sind nur Zahlen bei Brutto erlaubt. Abbruch.');
                                        formID.elements[\"brutto[]\"][i].focus();
                                        return false;
                                    }
                                }
                                if(isNaN(formID.elements[\"tax[]\"][i].value))
                                {
                                    alert('Es sind nur Zahlen bei Mehrwertsteuer erlaubt. Abbruch.');
                                    formID.elements[\"tax[]\"][i].focus();
                                    return false;
                                }
                                if(isNaN(formID.elements[\"quantity[]\"][i].value))
                                {
                                    alert('Es sind nur Zahlen bei Anzahl erlaubt. Abbruch.');
                                    formID.elements[\"quantity[]\"][i].focus();
                                    return false;
                                }";
					            if($order_field['delivery_method'] == "weight")
								{
                                    $script .= "
										formID.elements[\"weight[]\"][i].value = formID.elements[\"weight[]\"][i].value.replace(/,/, \".\");
										if(isNaN(formID.elements[\"weight[]\"][i].value))
										{
											alert('Es sind nur Zahlen bei Gewicht erlaubt. Abbruch.');
											formID.elements[\"weight[]\"][i].focus();
											return false;
										}";
								}
    $script .= "

                            }
                            if(formID.elements[\"shipping_cost_brutto\"])
                            {
                                if(formID.elements[\"shipping_cost_brutto\"].value != '')
                                {
                                    formID.elements[\"shipping_cost_brutto\"].value = formID.elements[\"shipping_cost_brutto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"shipping_cost_brutto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Brutto erlaubt. Abbruch.');
                                        formID.elements[\"shipping_cost_brutto\"].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"shipping_cost_netto\"].value != '')
                                {
                                    formID.elements[\"shipping_cost_netto\"].value = formID.elements[\"shipping_cost_netto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"shipping_cost_netto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Netto erlaubt. Abbruch.');
                                        formID.elements[\"shipping_cost_netto\"].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"shipping_tax_percent\"].value != '')
                                {
                                    formID.elements[\"shipping_tax_percent\"].value = formID.elements[\"shipping_tax_percent\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"shipping_tax_percent\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Mehrwertsteuer erlaubt. Abbruch.');
                                        formID.elements[\"shipping_tax_percent\"].focus();
                                        return false;
                                    }
                                }
                            }
                            if(formID.elements[\"cod_shipping_cost_brutto\"])
                            {
                                if(formID.elements[\"cod_shipping_cost_brutto\"].value != '')
                                {
                                    formID.elements[\"cod_shipping_cost_brutto\"].value = formID.elements[\"cod_shipping_cost_brutto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"cod_shipping_cost_brutto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Brutto erlaubt. Abbruch.');
                                        formID.elements[\"cod_shipping_cost_brutto\"].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"cod_shipping_cost_netto\"].value != '')
                                {
                                    formID.elements[\"cod_shipping_cost_netto\"].value = formID.elements[\"cod_shipping_cost_netto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"cod_shipping_cost_netto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Netto erlaubt. Abbruch.');
                                        formID.elements[\"cod_shipping_cost_netto\"].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"cod_shipping_tax_percent\"].value != '')
                                {
                                    formID.elements[\"cod_shipping_tax_percent\"].value = formID.elements[\"cod_shipping_tax_percent\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"cod_shipping_tax_percent\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Mehrwertsteuer erlaubt. Abbruch.');
                                        formID.elements[\"cod_shipping_tax_percent\"].focus();
                                        return false;
                                    }
                                }
                            }
                             if(formID.elements[\"add_netto\"].value != '')
                                {
                                    formID.elements[\"add_netto\"].value = formID.elements[\"add_netto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"add_netto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Netto erlaubt. Abbruch.');
                                        formID.elements[\"add_netto\"].focus();
                                        return false;
                                    }
                                }
                                if(formID.elements[\"add_brutto\"].value != '')
                                {
                                    formID.elements[\"add_brutto\"].value = formID.elements[\"add_brutto\"].value.replace(/,/, \".\");
                                    if(isNaN(formID.elements[\"add_brutto\"].value))
                                    {
                                        alert('Es sind nur Zahlen bei Brutto erlaubt. Abbruch.');
                                        formID.elements[\"add_brutto\"].focus();
                                        return false;
                                    }
                                }
                                if(isNaN(formID.elements[\"add_tax\"].value))
                                {
                                    alert('Es sind nur Zahlen bei Mehrwertsteuer erlaubt. Abbruch.');
                                    formID.elements[\"add_tax\"].focus();
                                    return false;
                                }
                                if(isNaN(formID.elements[\"add_quantity\"].value))
                                {
                                    alert('Es sind nur Zahlen bei Anzahl erlaubt. Abbruch.');
                                    formID.elements[\"add_quantity\"].focus();
                                    return false;
                                }";
					            if($order_field['delivery_method'] == "weight")
								{
                                    $script .= "
										formID.elements[\"add_weight\"].value = formID.elements[\"add_weight\"].value.replace(/,/, \".\");
										if(isNaN(formID.elements[\"add_weight\"].value))
										{
											alert('Es sind nur Zahlen bei Gewicht erlaubt. Abbruch.');
											formID.elements[\"add_weight\"].focus();
											return false;
										}";
								}
    $script .= "
                            return true;
                     }

				";

    $content .= "<div class='item_border'><div class='item_header'>Editiere Warenkorb:</div><div class='item_content'>".$Cpage->form("edit_cart_form", "correspondence_cart.php", "update", "return checkSubmit(this);")."<div class='content_wrapper'>
			Lieferart ändern auf: ".$Cpage->select_multi("delivery_type", $Cpage->Aglobal['delivery'], $delivery_type)."</div><br />".
        $Cpage->table().$Cpage->input_hidden("id", $id).
        $Cpage->input_hidden("start", $start).
        $Cpage->input_hidden("status2", $status2).
        $Cpage->input_hidden("search", $search).
        $Cpage->input_hidden("amount", $amount).
        $Cpage->input_hidden("item", $item).
        $Cpage->input_hidden("direction", $direction).
        $Cpage->input_hidden("name[]", 0).
        $Cpage->input_hidden("number[]", 0).
        $Cpage->input_hidden("weight[]", 0).
        $Cpage->input_hidden("tax[]", 0).
        $Cpage->input_hidden("quantity[]", 0).
        $Cpage->input_hidden("brutto[]", 0).
        $Cpage->input_hidden("netto[]", 0).
        $Cpage->input_hidden("attribute_id[]", 0).
        $Cpage->input_hidden("description[]", 0)."
		<tr class='line_bottom'>
		<th>Entfernen:</th>
		<th>Pos.:</th>
		<th>Artikel:</th>
		<th>Art.-Nr.:</th>";
    if($order_field['delivery_method'] == "weight")
    {
        $content .= "<th>Gewicht:</th>";
    }
    $content .= "<th>Netto:</th>
		<th>MwSt.:</th>
		<th>Brutto:</th>
		<th>Menge:</th>
		<th>Summen:</th>
	</tr>\n";

    if(sizeof($ar_cart['cart']) > 0)
    {
        foreach($ar_cart['cart'] as $cart_pos => $ar_cart_values)
        {
            $sql = "SELECT * FROM `".TBL_PREFIX."attributes` WHERE `to_article` =".$ar_cart_values['id'];
            $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $has_attributes = FALSE;

            if($result->num_rows > 0)
            {
                $has_attributes = TRUE;
                $attribute_box = $Cpage->select("attribute_id[]")."\n";
                $attribute_box .= "<option value='0'>Kein Attribut</option>\n";
                if(empty($ar_cart_values['attribute_id'])) $ar_cart_values['attribute_id'] = 0;
                while($row = $result->fetch_assoc())
                {
                    $attribute_box .= "<option value='".$row['id']."'";
                    if($row['id'] == $ar_cart_values['attribute_id']) $attribute_box .= " selected='selected'";
                    if(!empty($row['number'])) $number = " - Art.-Nr.: ".$row['number'];
                    else $number = "";
                    $attribute_box .= ">".$row['type'].$number."</option>\n";
                }
                $attribute_box .= "</select></p>\n";

            }
            $content .= "<tr>
            <td>".$Cpage->input_checkbox("delete_pos[]", $cart_pos)."</td>
            <td>".($cart_pos+1)."</td>
            <td>".$Cpage->input_text("name[]", $ar_cart_values['name']).$Cpage->input_hidden("article_id[]", $ar_cart_values['id'])."</td>
            <td>".$Cpage->input_text("number[]", $ar_cart_values['number'], 170)."</td>\n";
            if($order_field['delivery_method'] == "weight")
            {
                $content .= "<td align='right'><nobr>".$Cpage->input_text("weight[]", $ar_cart_values['weight'], 50, NOT_READONLY, "style='text-align:right;'")." gramm</nobr></td>\n";
            }
            $content .= "<td align='right'><nobr>".$Cpage->input_text("netto[]", $ar_cart_values['netto'], FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"brutto[]\"][$cart_pos+1].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>
            <td align='right'><nobr>".$Cpage->input_text("tax[]", $ar_cart_values['tax'], 30, NOT_READONLY, "style='text-align:right;'")."%</nobr></td>
            <td align='right'><nobr>".$Cpage->input_text("brutto[]", $Cpage->tax_calc($ar_cart_values['netto'], "brutto", $ar_cart_values['tax']), FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"netto[]\"][$cart_pos+1].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>
            <td align='center'>".$Cpage->input_text("quantity[]", $ar_cart_values['quantity'], 30, NOT_READONLY, "style='text-align:right;'")."</td>
            <td align='right'><nobr>".$Cpage->money($Cpage->tax_calc($ar_cart_values['total_netto'], "brutto", $ar_cart_values['tax']))."</nobr></td>
            </tr>
            <tr";
            if(!$has_attributes) $content .= " class='line_bottom'";
            $content .= ">
             <td>";
            if(!$has_attributes) $content .= $Cpage->input_hidden("attribute_id[]", 0);
            $content.="</td>
             <td></td>
             <td colspan='7'>Beschreibung: ".$Cpage->input_text("description[]", $ar_cart_values['description'], 700)."</td>
             <td></td>
            </tr>";
            if($has_attributes)
            {
                $content .= "
            <tr class='line_bottom'>
             <td></td>
             <td></td>
             <td colspan='7'>Attribut: ".$attribute_box."</td>
             <td></td>
            </tr>
                ";
            }
            $content .= "\n";
            $max_cart_pos = $cart_pos;
        }
    }
    else
    {
        $max_cart_pos = -1;
    }

    // Versandkosten anzeigen:

    // Falls Versand (Mit und ohne Nachnahme):

    if($ar_cart['shipping_error'] == TRUE)
    {
        $content .= "<tr><td align='right'><td>
					<td colspan='9' align='right'><span class='error'>Versandkosten können nicht automatisch berechnet werden!</span></td></tr>\n";
    }

    if($delivery_type <= 1)
    {
        $content .= "<tr class='line_bottom'><td>".$Cpage->input_checkbox("delete_shipping", $max_cart_pos+2)."</td><td>".($max_cart_pos+2)."</td><td colspan='".(3+$z)."'>Versandkosten nach ";
        // Falls Zielland
        $content .= $Cpage->select_countries('delivery_country_target', $ar_cart['delivery_country_target']).":";
        // Gewicht:
        if($order_field['delivery_method'] == "weight")
        {
            $content .= "<br>(Gewicht ca. <nobr>".$Cpage->weight_kilogram($ar_cart['total_weight']).")</nobr>";
        }
        $content .= "</td>\n";
        // Versandkosten, Tax, Netto und Brutto:
        // Wenn Fehler in Versandkosten:
        // Wenn alles OK:
        $content .= "<td align='right'><nobr>".$Cpage->input_text("shipping_cost_netto", $ar_cart['shipping_cost_netto'], FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"shipping_cost_brutto\"].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>
                        <td align='right'>".$Cpage->input_text("shipping_tax_percent", $shipping_tax_percent, 30, NOT_READONLY, "style='text-align:right;'")."%</td>
                        <td colspan='3' align='right'><nobr>".$Cpage->input_text("shipping_cost_brutto", $Cpage->tax_calc($ar_cart['shipping_cost_netto'], "brutto", $shipping_tax_percent), FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"shipping_cost_netto\"].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>\n";
        $content .= "</tr>";
    }
    if($delivery_type == 1)
    {
        // Nachnahme
        $max_cart_pos++;
        if($ar_cart['cod_shipping_error'] == TRUE)
        {
            $content .= "<tr><td align='right'><td>
                <td colspan='9' align='right'><span class='error'>Nachnahmekosten können nicht automatisch berechnet werden!</span></td></tr>\n";
        }
        $content .= "<tr class='line_bottom'><td>".$Cpage->input_checkbox("delete_cod_shipping", $max_cart_pos+2)."</td><td>".($max_cart_pos+2)."</td><td colspan='".(3+$z)."'>Nachnahmekosten</td><td align='right'><nobr>".$Cpage->input_text("cod_shipping_cost_netto", $ar_cart['cod_shipping_cost_netto'], FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"cod_shipping_cost_brutto\"].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>
                <td align='right'>".$Cpage->input_text("cod_shipping_tax_percent", $cod_shipping_tax_percent, 30, NOT_READONLY, "style='text-align:right;'")."%</td>
                <td colspan='3' align='right'><nobr>".$Cpage->input_text("cod_shipping_cost_brutto", $Cpage->tax_calc($ar_cart['cod_shipping_cost_netto'], "brutto", $cod_shipping_tax_percent), FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;' onkeyup='this.form.elements[\"cod_shipping_cost_netto\"].value=\"\";'")." ".CURRENCY_SYMBOL."</nobr></td>\n";
        $content .= "</tr>";
    }
    if($delivery_type >= 2)
    {
        // Falls Abholung
        $content .= "<tr class='line_bottom'><td>".$Cpage->input_checkbox("delete_pickup", $max_cart_pos+2)."</td><td>".($max_cart_pos+3)."</td><td colspan='".(3+$z)."'>";
        $content .= $Cpage->select_multi("pickup", $Cpage->Aglobal['pickup'], $delivery_type2);
        $content .= "</td>\n
            <td align='right'><nobr>0,00 €</nobr></td>
            <td align='right'></td>
            <td colspan='3' align='right'><nobr>0,00 €</nobr></td>
            </tr>";
    }

    $content .= "<tr class=''>";

    // Brutto Preis des Warenkorbes:
    $content .= "\n
            <td colspan='".(8+$z)."'></td><td class='td_line_bottom' align='right'><strong>Brutto:</strong></td><td class='td_line_bottom' align='right'><nobr><strong>".$Cpage->money($ar_cart['cart_price_brutto']+$ar_cart['shipping_cost_brutto']+$ar_cart['cod_shipping_cost_brutto'])."</strong></nobr></td>
            </tr>\n";

    // Mehrwertsteuer, falls vorhanden:
    if(!empty($ar_cart['tax']))
    {
        foreach($ar_cart['tax'] as $tax_percent => $tax)
        {
            if(!empty($tax))
            {
                $content .= "
                        <tr class=''>
                        <td colspan='".(8+$z)."'></td><td class='td_line_bottom' align='right'><nobr>".$tax_percent."% MwSt.:</nobr></td><td class='td_line_bottom' align='right'><nobr>".$Cpage->money($tax)."</nobr></td>
                        </tr>\n";
            }
        }
    }

    // Nettopreis + Aktionsbuttons
    $content .= "
			<tr class='R_netto'>
			<td colspan='".(8+$z)."'></td><td class='td_line_bottom' align='right'>Netto:</td><td class='td_line_bottom' align='right'><nobr>".$Cpage->money($ar_cart['cart_price_netto']+$ar_cart['shipping_cost_netto']+$ar_cart['cod_shipping_cost_netto'])."</nobr></td>
			</tr>
			<tr class='RL'>
			<td colspan='".(10+$z)."'>".

        $Cpage->input_submit("Warenkorb aktualisieren")." ".$Cpage->input_button("Warenkorb speichern", "saveCart(this.form);")." ".$Cpage->input_button("Abbruch", "cancelCart();")."

					</td>
				</tr>
			</table>
					
			<div class='item_border'><div class='item_header'>Artikel hinzufügen:</div><div class='item_content'>".$Cpage->table()."
			<tr class='line_bottom'>
				<th>Artikel:</th>
				<th>Art.-Nr.:</th>";
    if($order_field['delivery_method'] == "weight")
    {
        $content .= "<th>Gewicht:</th>";
    }
    $content .= "<th>Netto:</th>
				<th>MwSt.:</th>
				<th>Brutto:</th>
				<th>Menge:</th>
			</tr>
			<tr><td colspan='".(7+$z)."'><br /></td></tr>
			<tr>
				<td>".$Cpage->input_text("add_name")."</td>
				<td>".$Cpage->input_text("add_number", NO_VALUE, 170)."</td>";
    if($order_field['delivery_method'] == "weight")
    {
        $content .= "<td align='right'><nobr>".$Cpage->input_text("add_weight", NO_VALUE, 50, NOT_READONLY, "style='text-align:right;'")." gramm</nobr></td>";
    }
    $content .= "<td align='right'><nobr>".$Cpage->input_text("add_netto", NO_VALUE, FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;'")." ".CURRENCY_SYMBOL."</nobr></td>
				<td align='right'><nobr>".$Cpage->input_text("add_tax", NO_VALUE, 30, NOT_READONLY, "style='text-align:right;'")."%</nobr></td>
				<td align='right'><nobr>".$Cpage->input_text("add_brutto", NO_VALUE, FORM_TEXT_SIZE_HALF, NOT_READONLY, "style='text-align:right;'")." ".CURRENCY_SYMBOL."</nobr></td>
				<td align='center'>".$Cpage->input_text("add_quantity", NO_VALUE, 30, NOT_READONLY, "style='text-align:right;'")."</td>
			</tr>
			<tr>
			 <td>Beschreibung:</td><td colspan='".(6+$z)."'>".$Cpage->input_text("add_description", NO_VALUE, 500)."</td>
			</tr>
			<tr class='line_bottom'><td colspan='".(7+$z)."'><br /></td></tr>
			<tr><td colspan='".(7+$z)."'><br /></td></tr>
			<tr class=''>
				<td colspan='".(6+$z)."'>".$Carticles->get_articles_select("add_article")."</td>
				<td align='center'>".$Cpage->input_text("add_quantity2", NO_VALUE, 30, NOT_READONLY, "style='text-align:right;'")."</td>
			</tr>
			<tr><td colspan='".(7+$z)."'><br /></td></tr>
			</table>
			</div></div><div class='clear_float'></div><br />				
						
					
					
					</form>";
    $content .= "</div></div>";

    return $content;
}
