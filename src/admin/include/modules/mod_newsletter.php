<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "send":
        $content .= send_newsletter();
    default:
        $template_id = $Cpage->get_parameter("template_id", 0);
        $content .= start_site($template_id);
        break;
}

function start_site($template_id)
{
    global $Cpage;
    global $Cdb;
    global $load_text_editor;
    global $script;
    $content          = "";
    $load_text_editor = TRUE;

    $sql             = "SELECT * FROM `".TBL_PREFIX."newsletter_templates` ORDER BY `id` ASC ;";
    $template_result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    if($template_result->num_rows > 0)
    {
        $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Gesendete Newsletter</div></div><div class='item_content'><div class='content_wrapper'>";
        $content .= $Cpage->form("template_select", "newsletter.php").$Cpage->select("template_id", 1, FALSE, NOT_READONLY, NO_ON_SUBMIT);
        while($Atemplate = $template_result->fetch_assoc())
        {
            $content .= "<option value='".$Atemplate['id']."'";
            if($template_id == $Atemplate['id'])
            {
                $content .= " selected";
            }
            $content .= ">Am ".$Cpage->format_time($Atemplate['sent'])." an ".$Cpage->Aglobal['customer_types'][$Atemplate['type']]." gesendet: ".$Atemplate['subject']."</option>\n";
        }
        $content .= "</select><br />".$Cpage->input_submit("Newsletter holen")."
		</form></div>
		</div></div><div class='clear_float'></div>\n";
    }

    if($template_id != 0)
    {
        $sql             = "SELECT * FROM `".TBL_PREFIX."newsletter_templates` WHERE `id`=$template_id ;";
        $template_result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
        $Atemplate       = $template_result->fetch_assoc();
    }
    else
    {
        $Atemplate['subject'] = "";
        $Atemplate['text']    = "";
        $Atemplate['type']    = -1;
    }
    $script .= "function preview(formID)
						 { 
						 	formID.action='newsletter_show.php';
							formID.target='_blank'; 
							formID.submit(); 
							formID.action='newsletter.php';
							formID.target=''; 
						 }
				 ";

    $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Newsletter senden</div></div><div class='item_content'>";
    $content .= $Cpage->form("send_newsletter", "newsletter.php", "send").$Cpage->table()."
		<tr>
		 <td>Versenden an:</td>
		 <td>".$Cpage->select("consignee", 1, FALSE, FALSE, "");
    for($i = 0; $i < sizeof($Cpage->Aglobal['customer_types']); $i++)
    {
        $content .= "<option value='$i'";
        if($Atemplate['type'] == $i)
        {
            $content .= " selected";
        }
        $content .= ">".$Cpage->Aglobal['customer_types'][$i]."</option>\n";
    }
    $content .= "
			</select></td>
		</tr>
		<tr>
		 <td>Subjekt:</td>
		 <td>".$Cpage->input_text("subject", $Atemplate['subject'], 800)."</td>
		</tr>
		<tr>
		 <td valign='top'>Text:</td>
		 <td>".$Cpage->textarea("text", 800, 600).$Atemplate['text']."</textarea></td>
		</tr>
		<tr>
		 <td colspan='2'>".$Cpage->input_button("Vorschau", "preview(this.form);")." ".$Cpage->input_submit("Newsletter senden und speichern")."
		 </td>
		</tr>
		</table>
		</form>
		</div></div>\n";

    return $content;
}

function send_newsletter()
{
    global $Cpage;
    global $Cdb;
    $content = "";

    $text    = $Cpage->get_parameter("text", "", NO_ESCAPE);
    $subject = $Cpage->get_parameter("subject");
    $type    = $Cpage->get_parameter("consignee", 0);

    $sql  = "INSERT INTO `".TBL_PREFIX."newsletter_templates` (`subject`, `text`, `type`) VALUES (?, ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ssi', $subject, $text, $type);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);




    $i = 0;

    if($type == 0)
    {
        $where = "WHERE (`type`=1 OR `type`=2)";
    }
    else
    {
        $where = "WHERE `type`=$type";
    }

    $header = $Cpage->get_mail_header();
    $footer = $Cpage->get_mail_footer();

    $sql             = "SELECT * FROM `".TBL_PREFIX."customers` $where AND `newsletter`=1;";
    $customer_result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Acustomer = $customer_result->fetch_assoc())
    {

        $mail_sign_off         = $Cpage->get_mail_draft("newsletter_sign_off", FALSE);
        $mail_sign_off['text'] = preg_replace("/\\\$newsletter\['http'\]/i", $Cpage->Aglobal['domain']."newsletter.php", $mail_sign_off['text']);
        $mail_sign_off['text'] = preg_replace("/\\\$newsletter\['sign_off_key'\]/i", $Acustomer['hash'], $mail_sign_off['text']);

        $Acustomer['time']     = $Cpage->Aglobal['now'];
        //$Acustomer['ip']       = $Cpage->Aglobal['ip'];
        $Acustomer['homepage'] = $Cpage->Aglobal['www'];
        $Acustomer['theme']    = THEME;

        $text_user = $text.$mail_sign_off['text'];
        $text_user = $header.$text_user.$footer;
        foreach($Acustomer as $key => $value)
        {
            $text_user    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $text_user);
        }





        if(time_nanosleep(0, 5000))
        {
            $Cpage->send_mail(SHOP_MAIL, $text_user, $subject, $Acustomer['mail'], NO_BCC_MAIL);
        }
        $i++;
    }
    $sql               = "SELECT * FROM `".TBL_PREFIX."newsletter` $where;";
    $newsletter_result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Anewsletter = $newsletter_result->fetch_assoc())
    {
        $mail_sign_off         = $Cpage->get_mail_draft("newsletter_sign_off", FALSE);
        $mail_sign_off['text'] = preg_replace("/\\\$newsletter\['http'\]/i", $Cpage->Aglobal['domain']."newsletter.php", $mail_sign_off['text']);
        $mail_sign_off['text'] = preg_replace("/\\\$newsletter\['sign_off_key'\]/i", $Acustomer['hash'], $mail_sign_off['text']);

        $Acustomer['time']     = $Cpage->Aglobal['now'];
        //$Acustomer['ip']       = $Cpage->Aglobal['ip'];
        $Acustomer['homepage'] = $Cpage->Aglobal['www'];
        $Acustomer['theme']    = THEME;

        $text_user = $text.$mail_sign_off['text'];
        $text_user = $header.$text_user.$footer;
        foreach($Acustomer as $key => $value)
        {
            $text_user    = preg_replace("/\\\$customer\['".$key."'\]/i", $value, $text_user);
        }





        if(time_nanosleep(0, 5000))
        {
            $Cpage->send_mail(SHOP_MAIL, $text_user, $subject, $Anewsletter['mail'], NO_BCC_MAIL);
        }
        $i++;
    }

    $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Status</div></div><div class='item_content'>
	<div class='content_wrapper'>Es wurden $i Newsletter versendet.</div>
	</div></div><div class='clear_float'></div>\n";

    return $content;
}