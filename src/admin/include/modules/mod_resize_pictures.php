<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Afile = array();
foreach($Cpage->Awp['shop_pictures'] as $key => $value)
{
    if(is_dir($value))
    {
        if($handle = opendir($value))
        {
            $Afile[$key] = array();
            // einlesen der Verzeichnisses
            while(($file = readdir($handle)) !== FALSE)
            {
                if(filetype($value.$file) == "file")
                {
                    array_push($Afile[$key], $value.$file);
                }
            }
            closedir($handle);
        }
    }
}

$i = 0;
foreach($Cpage->Awp['shop_pictures'] as $picture_size_type => $dummy)
    foreach($Afile[$picture_size_type] as $file)
    {

        $file_parts = pathinfo($file);
        $filetype   = $file_parts['extension'];

        if(($filetype == "jpeg") || ($filetype == "jpg"))
        {
            $image = imagecreatefromjpeg($file);
        }
        elseif($filetype == "gif")
        {
            $image = imagecreatefromgif($file);
        }
        elseif($filetype == "png")
        {
            $image = imagecreatefrompng($file);
        }
        else continue;
        $image_b = imagesx($image);
        $image_h = imagesy($image);

        if($image_b > $image_h)
        {
            $ratio = round($image_b/$image_h);

            if($picture_size_type == "big")
            {
                $resize_to = MAX_PICTURE_SIZE_BIG_WIDTH;
                $new_b     = MAX_PICTURE_SIZE_BIG_HEIGHT;
            }
            elseif($picture_size_type == "medium")
            {
                $resize_to = MAX_PICTURE_SIZE_MEDIUM_WIDTH;
                $new_b     = MAX_PICTURE_SIZE_MEDIUM_HEIGHT;
            }
            elseif($picture_size_type == "small")
            {
                $resize_to = MAX_PICTURE_SIZE_SMALL_WIDTH;
                $new_b     = MAX_PICTURE_SIZE_SMALL_HEIGHT;
            }
            elseif($picture_size_type == "tiny")
            {
                $resize_to = MAX_PICTURE_SIZE_TINY_WIDTH;
                $new_b     = MAX_PICTURE_SIZE_TINY_HEIGHT;
            }
            else continue;

            if($image_b == $resize_to)
            {
                continue;
            }
            $new_h = round($resize_to/$ratio);
        }
        else
        {
            $ratio = round($image_h/$image_b);

            if($picture_size_type == "big")
            {
                $resize_to = MAX_PICTURE_SIZE_BIG_HEIGHT;
                $new_h     = MAX_PICTURE_SIZE_BIG_WIDTH;
            }
            elseif($picture_size_type == "medium")
            {
                $resize_to = MAX_PICTURE_SIZE_MEDIUM_HEIGHT;
                $new_h     = MAX_PICTURE_SIZE_MEDIUM_WIDTH;
            }
            elseif($picture_size_type == "small")
            {
                $resize_to = MAX_PICTURE_SIZE_SMALL_HEIGHT;
                $new_h     = MAX_PICTURE_SIZE_SMALL_WIDTH;
            }
            elseif($picture_size_type == "tiny")
            {
                $resize_to = MAX_PICTURE_SIZE_TINY_HEIGHT;
                $new_h     = MAX_PICTURE_SIZE_TINY_WIDTH;
            }
            else continue;

            if($image_h == $resize_to)
            {
                continue;
            }
            $new_b = round($resize_to/$ratio);
        }

        if(($filetype == "jpeg") || ($filetype == "jpg") || ($filetype == "png"))
        {
            $image_new = imagecreatetruecolor($new_b, $new_h);
        }
        elseif($filetype == "gif")
        {
            $image_new = imagecreate($new_b, $new_h);
        }

        imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_b, $new_h, $image_b, $image_h);

        if(($filetype == "jpeg") || ($filetype == "jpg"))
        {
            imagejpeg($image_new, $file, 100);
        }

        if($filetype == "gif")
        {
            imagegif($image_new, $file);
        }

        if($filetype == "png")
        {
            imagepng($image_new, $file);
        }

        imagedestroy($image);
        imagedestroy($image_new);
        $i++;
    }

$parameter = $Cpage->get_parameter("parameter");
$paramter  = urldecode($parameter);
$back      = $Cpage->get_parameter("back");

$content .= "<div class='content_wrapper'>".$i." Bilder bearbeitet.<br /><br />".$Cpage->link("Zurück", $back, $parameter, "link_button")."</div>";
