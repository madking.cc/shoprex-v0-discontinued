<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "edit":
        $id = $Cpage->get_parameter("article_id");
        $script .= $Cpage->load_page("articles.php", "article=$id");
        break;
    case "page_back":
        $start  = $Cpage->get_parameter("back");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_stock($start, $search);
        break;
    case "page_forward":
        $start  = $Cpage->get_parameter("forward");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_stock($start, $search);
        break;
    default:
        $start  = $Cpage->get_parameter("start", 0);
        $search = $Cpage->get_parameter("search", "");
        $content .= show_stock($start, $search);
        break;
}

function show_stock($start, $search)
{
    global $Cpage;
    global $Cdb;
    global $script;

    $amount  = $Cpage->get_parameter("amount", 20);
    $content = "";

    $sql     = "SELECT COUNT(`".TBL_PREFIX."articles`.`id`) FROM `".TBL_PREFIX."articles` LEFT JOIN `".TBL_PREFIX."attributes` ON (`".TBL_PREFIX."articles`.`id` = `".TBL_PREFIX."attributes`.`to_article`) WHERE `".TBL_PREFIX."articles`.`clone_from` =0;";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(`".TBL_PREFIX."articles`.`id`)"];


    $direction = $Cpage->get_parameter("direction", "desc");
    $item = $Cpage->get_parameter("item", "stock_warning");

    $select_suffix =", `".TBL_PREFIX."attributes`.`to_article` AS `to_article`, `".TBL_PREFIX."attributes`.`id` AS `attribute_id`,
    `".TBL_PREFIX."attributes`.`netto` AS `attribute_netto`, `".TBL_PREFIX."attributes`.`number` AS `attribute_number`,
    `".TBL_PREFIX."attributes`.`pos` AS `attribute_pos`, `".TBL_PREFIX."attributes`.`type` AS `attribute_type`,
    `".TBL_PREFIX."attributes`.`size` AS `attribute_size`, `".TBL_PREFIX."attributes`.`warning` AS `attribute_warning`  ";

    $left_join = " LEFT JOIN `".TBL_PREFIX."attributes` ON (`".TBL_PREFIX."articles`.`id` = `to_article`) ";

    switch($item)
    {
        case "stock_warning":
            $order_by = "(`quantity` < `quantity_warning` OR `attribute_size` < `attribute_warning`)";
            break;
        case "name":
            $order_by = "name";
            break;
        case "number":
            $order_by = "number";
            break;
    }


    $sql = "SELECT `".TBL_PREFIX."articles`.* $select_suffix FROM `".TBL_PREFIX."articles` $left_join WHERE `clone_from` =0 ORDER BY $order_by $direction LIMIT $start, $amount;";

    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);


    $content .= "<div class='item_border'><div class='item_header'>Lagerbestand</div><div class='item_content'>\n";

    $content .= "<br />";

    $script .= "function back_page(form) 						{ form.do_action.value='page_back'; form.submit(); }\n";
    $script .= "function forward_page(form) 					{ form.do_action.value='page_forward'; form.submit(); }\n";
    $script .= "function edit_stock(form, article_id) 			{ form.article_id.value=article_id; form.submit(); }\n";


    $content .= $Cpage->form("stock", "stock.php", "edit").$Cpage->input_hidden("back", $start-$amount).$Cpage->input_hidden("forward", $start+$amount).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("direction", $direction).$Cpage->input_hidden("article_id").
        $Cpage->table()."<tr><td>";

    if($item == "name" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Name:", "stock.php", "item=name&direction=$direction_item&start=$start&amount=$amount")."</td>



						 <td>";

    if($item == "number" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Artikelnr.:", "stock.php", "item=number&direction=$direction_item&start=$start&amount=$amount")."</td>


						 <td>";

    $content .= "Anzahl:</td>
						 <td>";

    $content .= "Warnung unter:</td>
						 <td>";

    if($item == "stock_warning" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Warnung?:", "stock.php", "item=stock_warning&direction=$direction_item&start=$start&amount=$amount")."</td>

						 <td>";

    $content .= "Status:</td>
						 <td>";
    $content .= "ISBN:</td>
						 <td>";

    $content .= "Top?:</td>
						 <td>";

    $content .= "Netto:</td>
						 <td>";

    $content .= "Brutto:</td>
						 <td>";
    $content .= "MwSt.:</td>
						 <td>";
    $content .= "		 <td>".$Cpage->select("amount", 1, FALSE, FALSE, "this.form.do_action.value=\"\";this.form.submit();")."
						 <option value='5'";
    if($amount == 5)
    {
        $content .= " selected";
    }
    $content .= ">5 anzeigen</option>
						 <option value='20'";
    if($amount == 20)
    {
        $content .= " selected";
    }
    $content .= ">20 anzeigen</option>
						 <option value='50'";
    if($amount == 50)
    {
        $content .= " selected";
    }
    $content .= ">50 anzeigen</option>
						 <option value='100'";
    if($amount == 100)
    {
        $content .= " selected";
    }
    $content .= ">100 anzeigen</option>
						 <option value='999999'";
    if($amount == 999999)
    {
        $content .= " selected";
    }
    $content .= ">Alle anzeigen</option>
						 </select>
						 </td></tr>";

    if($result->num_rows > 0)
    {
        while($Astock = $result->fetch_assoc())
        {
            //var_dump($Astock);
            $content .= "<tr class='line_top' onmouseover=\"style.backgroundColor='yellow'\" onmouseout=\"style.backgroundColor='transparent'\">
			<td><nobr>".$Astock['name'];
			if(isset($Astock['attribute_type'])) $content .= " - ".$Astock['attribute_type'];
			$content .= "</nobr></td>
			<td><nobr>";
            if(isset($Astock['attribute_number']))
                $content .= $Astock['attribute_number'];
            else
                $content .= $Astock['number'];
            $content .= "</nobr></td>
			<td";

            if(isset($Astock['attribute_size']))
            {
                $Astock['quantity'] = $Astock['attribute_size'];
                $Astock['quantity_warning'] = $Astock['attribute_warning'];
            }

            if($Astock['quantity'] < $Astock['quantity_warning'])
            {
                $content .= " class='light_warning_td'";
            }
            $content .= "><nobr>".$Astock['quantity']."</nobr></td>
			<td";
            if($Astock['quantity'] < $Astock['quantity_warning'])
            {
                $content .= " class='light_warning_td'";
            }
            $content .= "><nobr>".$Astock['quantity_warning']."</td>
			<td";
            if($Astock['quantity'] < $Astock['quantity_warning'])
            {
                $content .= " class='light_warning_td'>Ja";
            }
            else $content .= ">Nein";



            $content .= "</td>
			<td><nobr>";
            if($Astock['status'])
            {
                $content .= "Aktiv";
            }
            else $content .= "Inaktiv";
            $content .= "</nobr></td>
			<td><nobr>".$Astock['isbn']."</nobr></td><td><nobr>".$Cpage->Aglobal['answer'][$Astock['top_offer']]."</nobr></td>";
            if(isset($Astock['attribute_netto'])) $Astock['netto'] += $Astock['attribute_netto'];
			$content .= "<td><nobr>".$Cpage->money($Astock['netto'])."</nobr></td>
			<td><nobr>".$Cpage->money($Cpage->tax_calc($Astock['netto'], "brutto", $Cpage->Aglobal['tax'][$Astock['tax']]))."</nobr></td>
			<td><nobr>".$Cpage->Aglobal['tax_percent'][$Astock['tax']]."</nobr></td>
			<td><nobr>".$Cpage->input_button("editieren", "edit_stock(this.form, \"".$Astock['id']."\");")."</nobr></td></tr>";
        }
    }
    else
    {
        $content .= "<tr class='line_top'><td colspan='13' align='center'><span class='information'>Keine Ergebnisse</span></td></tr>\n";
    }

    if($result->num_rows > 0)
    {
        $content .= "<tr><td colspan='6'>";
        if($start > 0)
        {
            $content .= $Cpage->input_button("Zurück", "back_page(this.form);");
        }
        $content .= "</td>";
        $content .= "<td colspan='3' align='center'>Seite ".(($start/$amount)+1)." von ".ceil($entries/$amount);
        $content .= "</td>";
        $content .= "<td align='right' colspan='4'>";
        if($start < ($entries-($amount)))
        {
            $content .= $Cpage->input_button("Vor", "forward_page(this.form);");
        }
        $content .= "</td></tr>";
    }
    $content .= "</table></form>";
    $content .= "</div></div>\n";
    return $content;
}





