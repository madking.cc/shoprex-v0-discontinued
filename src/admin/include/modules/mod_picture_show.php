<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$article    = $Cpage->get_parameter("article");
$category   = $Cpage->get_parameter("category", 0);
$picture_id = $Cpage->get_parameter("picture_id");

$uploaddir = UPLOAD_DIR;
if($category == 0)
{
    $sql           = "SELECT * FROM `".TBL_PREFIX."articles` WHERE `id` =$article;";
    $result        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $article_field = $result->fetch_assoc();

    $Apictures        = unserialize($article_field['picture']);
    $Apictures_text   = unserialize($article_field['picture_text']);

    $file         = $uploaddir.$Apictures[$picture_id];
    $file_big     = $uploaddir."big/".$Apictures[$picture_id];
    $file_medium  = $uploaddir."medium/".$Apictures[$picture_id];
    $file_small   = $uploaddir."small/".$Apictures[$picture_id];
    $file_tiny    = $uploaddir."tiny/".$Apictures[$picture_id];
    $picture_text = $Apictures_text[$picture_id];

    $header_text = "Bild ".($picture_id+1)." zu Artikel ".$article_field['name'];
}
else
{
    $sql            = "SELECT * FROM `".TBL_PREFIX."categories` WHERE `id` =$category;";
    $result         = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $category_field = $result->fetch_assoc();

    $picture        = $category_field['picture'];
    $picture_text   = $category_field['picture_text'];

    $file        = $uploaddir.$picture;
    $file_big    = $uploaddir."big/".$picture;
    $file_medium = $uploaddir."medium/".$picture;
    $file_small  = $uploaddir."small/".$picture;
    $file_tiny   = $uploaddir."tiny/".$picture;

    $header_text = "Bild zu Kategorie ".$category_field['name'];
}

$Cpage->load_image($file);
$image_b = $Cpage->get_image_width();
$image_h = $Cpage->get_image_height();
$Cpage->load_image($file_big);
$big_b = $Cpage->get_image_width();
$big_h = $Cpage->get_image_height();
$Cpage->load_image($file_medium);
$medium_b = $Cpage->get_image_width();
$medium_h = $Cpage->get_image_height();
$Cpage->load_image($file_small);
$small_b = $Cpage->get_image_width();
$small_h = $Cpage->get_image_height();
$Cpage->load_image($file_tiny);
$tiny_b = $Cpage->get_image_width();
$tiny_h = $Cpage->get_image_height();

$content = "<div class='item_border'><div class='item_header'>$header_text</div>
		<div class='item_content'>\n";
$content .= "<div class='content_wrapper'><br />
    <a href='#' onClick='window.close();' class='link_button'>Fenster schließen</a>
    </div>
	<p class='p_line'></p>
	<div class='content_wrapper'>
    <H2>Bild für die Suche: ".$file_tiny."</H2>
    Breite ".$tiny_b." * Höhe ".$tiny_h."<br><img src='".$file_tiny."'>
    </div>
	<p class='p_line'></p>
	<div class='content_wrapper'>
    <H2>Bild für die Artikelliste: ".$file_small."</H2>
    Breite ".$small_b." * Höhe ".$small_h."<br><img src='".$file_small."'>
    </div>
	<p class='p_line'></p>
    <div class='content_wrapper'>
	<H2>Bild für die Artikelseite: ".$file_medium."</H2>
    Breite ".$medium_b." * Höhe ".$medium_h."<br><img src='".$file_medium."'>
    </div>
	<p class='p_line'></p>
    <div class='content_wrapper'>
	<H2>Bild für das Anzeigen des Artikelbildes: ".$file_big."</H2>
    Breite ".$big_b." * Höhe ".$big_h."<br><img src='".$file_big."'>
    </div>
	<p class='p_line'></p>
    <div class='content_wrapper'>
	<H2>Originalbild: ".$file."</H2>
    Breite ".$image_b." * Höhe ".$image_h."<br><img src='".$file."'>
    </div>
	<p class='p_line'></p>
	<div class='content_wrapper'><br />       
    <a href='#' onClick='window.close();' class='link_button'>Fenster schließen</a><br /><br /></div>";
$content .= "</div></div>";
 