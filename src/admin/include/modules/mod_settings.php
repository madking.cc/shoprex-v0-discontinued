<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$cod_enable          = $Cpage->get_parameter("cod_enable", NULL);
$prepayment_enable   = $Cpage->get_parameter("prepayment_enable", NULL);
$request_enable      = $Cpage->get_parameter("request_enable", NULL);
$remote_areas_enable = $Cpage->get_parameter("remote_areas_enable", NULL);

if(isset($cod_enable))
{
    $Asettings["ENABLE_COD"] = $cod_enable;
}
else $cod_enable = ENABLE_COD;
if(isset($prepayment_enable))
{
    $Asettings["ENABLE_PREPAYMENT"] = $prepayment_enable;
}
else $prepayment_enable = ENABLE_PREPAYMENT;
if(isset($request_enable))
{
    $Asettings["ENABLE_REQUEST"] = $request_enable;
}
else $request_enable = ENABLE_REQUEST;
if(isset($remote_areas_enable))
{
    $Asettings["ENABLE_REMOTE_AREAS"] = $remote_areas_enable;
}
else $remote_areas_enable = ENABLE_REMOTE_AREAS;

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "store_settings":
        $condition       = $Cpage->get_parameter("condition", NULL, NO_ESCAPE);
        $guarantee       = $Cpage->get_parameter("guarantee", NULL, NO_ESCAPE);
        $customer_type   = $Cpage->get_parameter("customer_type", NULL, NO_ESCAPE);
        $delivery        = $Cpage->get_parameter("delivery", NULL, NO_ESCAPE);
        $cod_text        = $Cpage->get_parameter("cod_text", "");
        $prepayment_text = $Cpage->get_parameter("prepayment_text", "");
        if(!empty($cod_text) && !empty($prepayment_text))
        {
            if(!isset($delivery))
            {
                $delivery = array();
            }
            array_unshift($delivery, $prepayment_text, $cod_text);

            foreach($delivery as $key => $value)
            {
                $tmp                      = $value;
                $delivery[$key]           = array();
                $delivery[$key]['value']  = $tmp;
                $delivery[$key]['enable'] = 1;
                $delivery[$key]['code']   = $key;
            }
            $delivery[0]['enable'] = $prepayment_enable;
            $delivery[1]['enable'] = $cod_enable;
        }

        $status                           = $Cpage->get_parameter("status", NULL, NO_ESCAPE);
        $tax                              = $Cpage->get_parameter("tax", NULL, NO_ESCAPE);
        $Alanguage                        = $Cpage->get_parameter("language", NULL, NO_ESCAPE);
        $Asettings                        = $Cpage->get_parameter("settings", NULL, NO_ESCAPE);
        $Asettings["ENABLE_COD"]          = $cod_enable;
        $Asettings["ENABLE_PREPAYMENT"]   = $prepayment_enable;
        $Asettings["ENABLE_REQUEST"]      = $request_enable;
        $Asettings["ENABLE_REMOTE_AREAS"] = $remote_areas_enable;

        $language_array = array("condition" => "ARRAY_CONDITION", "customer_type" => "ARRAY_CUSTOMER_TYPE", "delivery" => "ARRAY_DELIVERY", "guarantee" => "ARRAY_GUARANTEE", "status" => "ARRAY_STATUS");
        $settings_array = array("tax" => "ARRAY_TAX");
        $sql            = "UPDATE `".TBL_PREFIX."language_".LANGUAGE."` SET `value` = ? WHERE `name` =? LIMIT 1;";
        $stmt           = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        if(!empty($Asettings['TIME_TO_PWD_RESET']))
        {
            $Asettings['TIME_TO_PWD_RESET'] *= 3600;
        }
        foreach($language_array as $key => $value)
        {
            if(isset($$key))
            {
                $$value = serialize($$key);
                $stmt->bind_param('ss', $$value, $value);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }
        if(isset($Alanguage))
        {
            foreach($Alanguage as $key => $value)
            {
                $stmt->bind_param('ss', $value, $key);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }
        $sql  = "UPDATE `".TBL_PREFIX."settings` SET `value` = ? WHERE `name` =? LIMIT 1;";
        $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
        foreach($settings_array as $key => $value)
        {
            if(isset($$key))
            {
                $$value = serialize($$key);
                $stmt->bind_param('ss', $$value, $value);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }
        if(isset($Asettings))
        {
            foreach($Asettings as $key => $value)
            {
                $stmt->bind_param('ss', $value, $key);
                $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
            }
        }

    default:
        $content .= load_menu();
        $page = $Cpage->get_parameter("page", "shop");
        $content .= "<div class='item_border'><div class='item_header'></div><div class='item_content'>\n";
        $content .= show_page($page);
        $content .= "</div></div>\n";
        break;
}

function load_menu()
{
    global $Cpage;
    $content = "";

    $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Auswahlmenü</div></div><div class='item_content'>";
    $content .= "<ul class='sub_menu'>\n";
    $content .= "<li>".$Cpage->link("Shop, Theme", "settings.php", "page=shop");
    $content .= "<li>".$Cpage->link("Artikel", "settings.php", "page=article");
    $content .= "<li>".$Cpage->link("Rechnungen, Kunden", "settings.php", "page=invoice");
    $content .= "<li>".$Cpage->link("Bilder", "settings.php", "page=picture");
    $content .= "</ul>\n";
    $content .= "</div></div>\n";
    return $content;
}

function show_page($page)
{
    global $Cpage;
    global $Cdb;
    global $prepayment_enable;
    global $cod_enable;
    global $request_enable;
    global $remote_areas_enable;
    global $script;
    $content = "";

    $is_array = array("ARRAY_CONDITION", "ARRAY_CUSTOMER_TYPE", "ARRAY_DELIVERY", "ARRAY_GUARANTEE", "ARRAY_TAX", "ARRAY_STATUS");
    $sql      = "SELECT * FROM `".TBL_PREFIX."language_".LANGUAGE."` WHERE (`type` like 'setting');";
    $result   = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Aitem = $result->fetch_assoc())
    {
        if(in_array($Aitem['name'], $is_array))
        {
            $Asettings['language'][$Aitem['name']] = unserialize($Aitem['value']);
        }
        else $Asettings['language'][$Aitem['name']] = $Aitem['value'];
    }
    $sql    = "SELECT * FROM `".TBL_PREFIX."settings` WHERE (`type` like 'setting');";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    while($Aitem = $result->fetch_assoc())
    {
        if(in_array($Aitem['name'], $is_array))
        {
            $Asettings['settings'][$Aitem['name']] = unserialize($Aitem['value']);
        }
        else $Asettings['settings'][$Aitem['name']] = $Aitem['value'];
    }

    $script .= "
		function entry_up(elsel)
		{
			if(elsel.selectedIndex!=0)
			{
				temp = elsel.options[elsel.selectedIndex-1].text;
				elsel.options[elsel.selectedIndex-1].text = elsel.options[elsel.selectedIndex].text;
				elsel.options[elsel.selectedIndex].text = temp;
				temp = elsel.selectedIndex;
				elsel.options[elsel.selectedIndex-1].selected=true;
				elsel.options[temp].selected=false;
			}
		}

		function entry_down(elsel)
		{
			if(elsel.selectedIndex!=(elsel.length-1))
			{
				temp = elsel.options[elsel.selectedIndex+1].text;
				elsel.options[elsel.selectedIndex+1].text = elsel.options[elsel.selectedIndex].text;
				elsel.options[elsel.selectedIndex].text = temp;
				temp = elsel.selectedIndex;
				elsel.options[elsel.selectedIndex+1].selected=true;
				elsel.options[temp].selected=false;
			}
		}

		function entry_delete(elsel)
		{
			elsel.options[elsel.selectedIndex] = null;
		}

		function entry_add(elsel, intex)
		{
			  NewEntry = new Option(intex.value);
			  elsel.options[elsel.length] = NewEntry;
			  intex.value = '';			
		}
	
		function select_all(thisForm)
		{
			objSelect = new Array();
			if(thisForm.elements[\"condition[]\"]) objSelect.push(thisForm.elements[\"condition[]\"]);
			if(thisForm.elements[\"guarantee[]\"]) objSelect.push(thisForm.elements[\"guarantee[]\"]);
			if(thisForm.elements[\"tax[]\"]) objSelect.push(thisForm.elements[\"tax[]\"]);
			if(thisForm.elements[\"customer_type[]\"]) objSelect.push(thisForm.elements[\"customer_type[]\"]);
			if(thisForm.elements[\"status[]\"]) objSelect.push(thisForm.elements[\"status[]\"]);
			if(thisForm.elements[\"delivery[]\"]) objSelect.push(thisForm.elements[\"delivery[]\"]);
			if(objSelect.length>0)
			{
				for(var i=0;i < objSelect.length; i++)
				{
					for(var t=0;t < objSelect[i].length; t++)	
					{
						objSelect[i].options[t].selected = true;
					}
				}
			}
			return true;
		}
	";
    $content .= "<div id='right_content'>\n";

    switch($page)
    {
        case "article":
            $content .= $Cpage->form("settings", "settings.php", "store_settings", "return select_all(document.settings);");
            $content .= $Cpage->input_hidden("page", "article");
            $content .= $Cpage->table()."<tr class='item_start'><td valign='top'>Artikel Zustände:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("condition[]", 7, MULTIPLE);
            foreach($Asettings['language']['ARRAY_CONDITION'] as $key => $value)
                $content .= "<option>".$value."</option>\n";
            $content .= "</select>\n</td><td align='left' valign='bottom'>".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"condition[]\"])")."</td><td valign='top'><span class='information'>Eine Änderung in der<br />Reihenfolge ändert auch die<br />Werte in allen gespeicherten Artikeln!</span></td></tr></table></tr>\n<tr class='line_bottom'><td></td><td>".$Cpage->input_text("condition_text").$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"condition[]\"],document.settings.condition_text)")."</td>
			</tr>\n";

            $content .= "<tr><td valign='top'>Garantiebestimmungen:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("guarantee[]", 7, MULTIPLE);
            foreach($Asettings['language']['ARRAY_GUARANTEE'] as $key => $value)
                $content .= "<option>".$value."</option>\n";
            $content .= "</select>\n</td><td align='left' valign='bottom'>".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"guarantee[]\"])")."</td><td valign='top'><span class='information'>Eine Änderung in der<br />Reihenfolge ändert auch die<br />Werte in allen gespeicherten Artikeln!</span></td></tr></table>\n</tr><tr class='line_bottom'><td></td><td>".$Cpage->input_text("guarantee_text").$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"guarantee[]\"],document.settings.guarantee_text)")."</td>
			 </tr>\n";
            $content .= "<tr><td colspan='2'><span class='information'>Die Nummer nachträglich nicht verringern! Nur, wenn die Prefixe und Suffixe ebenfalls geändert werden. Es sind keine doppelten Einträge erlaubt!</span></td></tr>\n";
            $content .= "<tr><td>Start der Artikelnummern:</td><td>".$Cpage->input_text("settings[ARTICLE_START]", $Asettings['settings']['ARTICLE_START'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Prefix der Artikelnummer:</td><td>".$Cpage->input_text("settings[ARTICLE_PREFIX]", $Asettings['settings']['ARTICLE_PREFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Suffix der Artikelnummer:</td><td>".$Cpage->input_text("settings[ARTICLE_SUFFIX]", $Asettings['settings']['ARTICLE_SUFFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";


            $content .= "<tr><td>Sollen Artikel bei der<br />Bestellung reserviert werden?:</td><td valign='bottom'>".$Cpage->select_multi("settings[RESERVE_ARTICLE]", $Cpage->Aglobal['answer'], $Asettings['settings']['RESERVE_ARTICLE'])."</td></tr>\n";
            $content .= "<tr><td>Dauer der Reservierung:</td><td>".$Cpage->input_text("settings[RESERVE_TIME]", $Asettings['settings']['RESERVE_TIME'], FORM_TEXT_SIZE_HALF)." Stunden</td></tr>\n";
            $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table>\n";
            $content .= "</form>";
            break;

        case "invoice":
            $content .= $Cpage->form("settings", "settings.php", "store_settings", "return select_all(document.settings);");
            $content .= $Cpage->input_hidden("page", "invoice");
            $content .= $Cpage->table()."<tr class='item_start line_bottom'><td>Währungssymbol:</td><td>".$Cpage->input_text("language[CURRENCY]", $Asettings['language']['CURRENCY'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";

            $content .= "<tr><td valign='top'>Mehrwertsteuer:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("tax[]", 7, MULTIPLE);
            foreach($Asettings['settings']['ARRAY_TAX'] as $key => $value)
                $content .= "<option value='$value'>$value%</option>\n";
            $content .= "</select>\n</td><td valign='bottom'>".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"tax[]\"])")."</td><td valign='top'><span class='information'>Eine Änderung in der<br />Reihenfolge ändert auch die<br />Werte in allen gespeicherten Artikeln<br />und Bestellungen, nicht aber die<br />Rechnungen!</span></td></tr></table></td></tr>\n<tr class='line_bottom'><td></td><td>".$Cpage->input_text("tax_text")."% ".$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"tax[]\"],document.settings.tax_text)")."</td></tr>\n";

            $content .= "<tr class='line_bottom'><td>Steuer der Versandkosten:</td><td>".$Cpage->select("settings[SHIPPING_TAX_KEY]", 1);
            foreach($Asettings['settings']['ARRAY_TAX'] as $key => $value)
            {
                $content .= "<option value='$key'";
                if($key == $Asettings['settings']['SHIPPING_TAX_KEY'])
                {
                    $content .= " selected";
                }
                $content .= ">$value%</option>\n";
            }
            $content .= "</select></td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Steuer der Nachnahmekosten:</td><td>".$Cpage->select("settings[COD_TAX_KEY]", 1);
            foreach($Asettings['settings']['ARRAY_TAX'] as $key => $value)
            {
                $content .= "<option value='$key'";
                if($key == $Asettings['settings']['COD_TAX_KEY'])
                {
                    $content .= " selected";
                }
                $content .= ">$value%</option>\n";
            }
            $content .= "</select></td></tr>\n";

            $content .= "<tr class='line_bottom' valign='top'><td>Der Shop soll<br />die Mehrwertsteuer ausweisen?:</td><td>".$Cpage->select_multi("settings[ENABLE_TAX]", $Cpage->Aglobal['answer'], $Asettings['settings']['ENABLE_TAX'])." <span class='information'><br />Wichtig, wenn Sie die Rechnungsbeträge nach Kleinunternehmerregelung § 19 (1) UStG nicht<br />
			                                                                                                                                                                                                                                                      mit Mehrwertsteuer ausweisen sollen.</td></tr>\n";

            $content .= "<tr><td colspan='2'><span class='information'>Die Nummer nachträglich nicht verringern! Nur, wenn die Prefixe und Suffixe ebenfalls geändert werden. Es sind keine doppelten Einträge erlaubt!</span></td></tr>\n";
            $content .= "<tr><td>Startnummer der Rechnungen:</td><td>".$Cpage->input_text("settings[INVOICE_START]", $Asettings['settings']['INVOICE_START'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Prefix der Rechnungsnummer:</td><td>".$Cpage->input_text("settings[INVOICE_PREFIX]", $Asettings['settings']['INVOICE_PREFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Suffix der Rechnungsnummer:</td><td>".$Cpage->input_text("settings[INVOICE_SUFFIX]", $Asettings['settings']['INVOICE_SUFFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Startnummer der Bestellungen:</td><td>".$Cpage->input_text("settings[ORDER_START]", $Asettings['settings']['ORDER_START'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Prefix der Bestellungsnummer:</td><td>".$Cpage->input_text("settings[ORDER_PREFIX]", $Asettings['settings']['ORDER_PREFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Suffix der Bestellungsnummer:</td><td>".$Cpage->input_text("settings[ORDER_SUFFIX]", $Asettings['settings']['ORDER_SUFFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Start der Kundennummern:</td><td>".$Cpage->input_text("settings[CUSTOMER_START]", $Asettings['settings']['CUSTOMER_START'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr><td>Prefix der Kundennummer:</td><td>".$Cpage->input_text("settings[CUSTOMER_PREFIX]", $Asettings['settings']['CUSTOMER_PREFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Suffix der Kundennummer:</td><td>".$Cpage->input_text("settings[CUSTOMER_SUFFIX]", $Asettings['settings']['CUSTOMER_SUFFIX'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";

            $content .= "<tr><td valign='top'>Kundentypen:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("customer_type[]", 7, MULTIPLE);
            foreach($Asettings['language']['ARRAY_CUSTOMER_TYPE'] as $key => $value)
                $content .= "<option>".$value."</option>\n";
            $content .= "</select>\n</td><td align='left' valign='bottom'>".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"customer_type[]\"])")."</td><td valign='top'><span class='information'>Eine Änderung in der<br />Reihenfolge ändert auch die<br />Werte in allen gespeicherten<br />Newslettern und Bestellungen!</span></td></tr></table></td></tr>\n<tr class='line_bottom'><td></td><td>".$Cpage->input_text("customer_type_text").$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"customer_type[]\"],document.settings.customer_type_text)")."</td></tr>\n";

            $content .= "<tr><td>Zeit für Passwort Reset:</td><td>".$Cpage->input_text("settings[TIME_TO_PWD_RESET]", ($Asettings['settings']['TIME_TO_PWD_RESET']/3600), FORM_TEXT_SIZE_HALF)." Stunden</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Passwort Länge:</td><td>".$Cpage->input_text("settings[PASSWORD_LENGTH]", $Asettings['settings']['PASSWORD_LENGTH'], FORM_TEXT_SIZE_HALF)."</td></tr>\n";

            $content .= "<tr class='line_bottom'><td>Sollen Artikelnummern verwendet werden?:</td><td>".$Cpage->select_multi("settings[USE_ARTICLE_NUMBER]", $Cpage->Aglobal['answer'], $Asettings['settings']['USE_ARTICLE_NUMBER'])."</td></tr>\n";

            $content .= "<tr><td valign='top'>Statusarten der Bestellung:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("status[]", 7, MULTIPLE);
            foreach($Asettings['language']['ARRAY_STATUS'] as $key => $value)
                $content .= "<option>".$value."</option>\n";
            $content .= "</select>\n</td><td align='left' valign='bottom'>".$Cpage->input_button("Hoch", "entry_up(document.settings.elements[\"status[]\"])")."<br />".$Cpage->input_button("Runter", "entry_down(document.settings.elements[\"status[]\"])")."<br />".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"status[]\"])")."</td><td valign='top'><span class='information'>Eine Änderung in der<br />Reihenfolge ändert auch die<br />Werte in allen gespeicherten<br />Bestellungen!</span></td></tr></table></td></tr>\n<tr class='line_bottom'><td></td><td>".$Cpage->input_text("status_text").$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"status[]\"],document.settings.status_text)")."</td></tr>\n";
            $content .= "<tr><td>Standard Versand-Land:</td><td>".$Cpage->select_countries("settings[DEFAULT_COUNTRY_CODE]", $Asettings['settings']['DEFAULT_COUNTRY_CODE'])."</td></tr>\n";
            $content .= "<tr><td>Wenn Versandgewicht zu hoch, Kunden<br />die Möglichkeit geben, Bestellung anzufragen?:</td><td valign='bottom'>".$Cpage->select_multi("request_enable", $Cpage->Aglobal['answer'], $request_enable)."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Wenn Lieferort in einer Remotearea, Kunden<br />die Möglichkeit geben, Bestellung anzufragen?<br />Ansonsten wird die Lieferung verweigert.:</td><td valign='bottom'>".$Cpage->select_multi("remote_areas_enable", $Cpage->Aglobal['answer'], $remote_areas_enable)."</td></tr>\n";
            $content .= "<tr><td>Vorkasse einschalten?:</td><td>".$Cpage->select_multi("prepayment_enable", $Cpage->Aglobal['answer'], $prepayment_enable)."</td></tr>\n";
            $content .= "<tr><td>Bezeichnung Vorkasse:</td><td>".$Cpage->input_text("prepayment_text", $Asettings['language']['ARRAY_DELIVERY'][0]['value'])."</td></tr>\n";

            $content .= "<tr><td>Nachnahme einschalten?:</td><td>".$Cpage->select_multi("cod_enable", $Cpage->Aglobal['answer'], $cod_enable)."</td></tr>\n";
            $content .= "<tr><td>Bezeichnung Nachnahme:</td><td>".$Cpage->input_text("cod_text", $Asettings['language']['ARRAY_DELIVERY'][1]['value'])."</td></tr>\n";

            $content .= "<tr><td valign='top'>Abholungen,<br />bzw. kostenlose Lieferungen:</td><td class='no_padding'>".$Cpage->table()."<tr><td>".$Cpage->select("delivery[]", 7, MULTIPLE);
            array_shift($Asettings['language']['ARRAY_DELIVERY']);
            array_shift($Asettings['language']['ARRAY_DELIVERY']);
            foreach($Asettings['language']['ARRAY_DELIVERY'] as $key => $value)
                $content .= "<option>".$value['value']."</option>\n";
            $content .= "</select>\n</td><td valign='bottom' align='left'>".$Cpage->input_button("Hoch", "entry_up(document.settings.elements[\"delivery[]\"])")."<br />".$Cpage->input_button("Runter", "entry_down(document.settings.elements[\"delivery[]\"])")."<br />".$Cpage->input_button("Löschen", "entry_delete(document.settings.elements[\"delivery[]\"])")."</td><td valign='top'><span class='information'>Eine leere Liste deaktiviert<br />die Möglichkeit zur Abholung!</span></td></tr></table></td></tr>\n<tr class='line_bottom'><td></td><td>".$Cpage->input_text("delivery_text").$Cpage->input_button("Hinzufügen", "entry_add(document.settings.elements[\"delivery[]\"],document.settings.delivery_text)")."</td></tr>\n";

            $content .= "<tr class='line_bottom'><td valign='top'>Zusätzlicher Text<br />in der Bestellung und Rechnung:</td>
			<td>".$Cpage->textarea("language[INVOICE_FOOTER_TEXT]", 400, 200).$Asettings['language']['INVOICE_FOOTER_TEXT']."</textarea></td></tr>
			<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table>\n";
            $content .= "</form>";

            break;
        case "picture":
            $content .= $Cpage->form("settings", "settings.php", "store_settings");
            $content .= $Cpage->input_hidden("page", "picture").$Cpage->table();
            $content .= "<tr class='item_start'><td>Große Bild Breite:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_BIG_WIDTH]", $Asettings['settings']['MAX_PICTURE_SIZE_BIG_WIDTH'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Große Bild Höhe:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_BIG_HEIGHT]", $Asettings['settings']['MAX_PICTURE_SIZE_BIG_HEIGHT'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr><td>Mittlere Bild Breite:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_MEDIUM_WIDTH]", $Asettings['settings']['MAX_PICTURE_SIZE_MEDIUM_WIDTH'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Mittlere Bild Höhe:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_MEDIUM_HEIGHT]", $Asettings['settings']['MAX_PICTURE_SIZE_MEDIUM_HEIGHT'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr><td>Kleine Bild Breite:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_SMALL_WIDTH", $Asettings['settings']['MAX_PICTURE_SIZE_SMALL_WIDTH'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Kleine Bild Höhe:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_SMALL_HEIGHT]", $Asettings['settings']['MAX_PICTURE_SIZE_SMALL_HEIGHT'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr><td>Sehr kleine Bild Breite:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_TINY_WIDTH]", $Asettings['settings']['MAX_PICTURE_SIZE_TINY_WIDTH'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Sehr kleine Bild Höhe:</td><td>".$Cpage->input_text("settings[MAX_PICTURE_SIZE_TINY_HEIGHT]", $Asettings['settings']['MAX_PICTURE_SIZE_TINY_HEIGHT'], FORM_TEXT_SIZE_HALF)." Pixel</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Alternativ Text für Bildlos:</td><td>".$Cpage->input_text("language[NO_PICTURE_TEXT]", $Asettings['language']['NO_PICTURE_TEXT'])."</td></tr>\n";

            $paramter = urlencode("page=picture");
            $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")." ".$Cpage->link("Bilder Größe neu anpassen", "resize_pictures.php", "do_action=auto_resize&back=settings.php&parameter=".$paramter, NO_TARGET, "link_button")."</td></tr></table>\n";
            $content .= "</form>";

            break;
        case "shop":
        default:
            $content .= $Cpage->form("settings", "settings.php", "store_settings");
            $content .= $Cpage->input_hidden("page", "shop").$Cpage->table();
            $content .= "<tr class='item_start'><td>Seiten Titel:</td><td>".$Cpage->input_text("language[PAGE_TITLE]", $Asettings['language']['PAGE_TITLE'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Theme:</td><td>".$Cpage->input_text("language[THEME]", $Asettings['language']['THEME'])."</td></tr>\n";

            $content .= "<tr class='line_bottom'><td>Über Bestellungen per Mail informieren?:</td><td>".$Cpage->select_multi("settings[SEND_ORDERS_HOME]", $Cpage->Aglobal['answer'], $Asettings['settings']['SEND_ORDERS_HOME'])."</td></tr>\n";
            // $content .= "<tr><td>Alle Mails per BCC zusenden?:</td><td>".$Cpage->select("settings[DO_BCC_MAIL]", $Cpage->Aglobal['answer'], $Asettings['settings']['DO_BCC_MAIL'])."</td></tr>\n";

            $content .= "<tr><td>Artikel Anzahl in Reihe:</td><td>".$Cpage->input_text("settings[ARTICLE_COLSPAN]", $Asettings['settings']['ARTICLE_COLSPAN'])."</td></tr>\n";
            $content .= "<tr class=''><td>Kategorien Anzahl in Reihe:</td><td>".$Cpage->input_text("settings[CATEGORY_COLSPAN]", $Asettings['settings']['CATEGORY_COLSPAN'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Zeige Unterkategorien in Artikelliste an?:</td><td>".$Cpage->select_multi("settings[SHOW_SUB_CATEGORIES_IN_ARTICLE_LIST]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SUB_CATEGORIES_IN_ARTICLE_LIST'])."</td></tr>\n";
            $content .= "<tr><td colspan='2'><span class='information'>(Mögliche) Startseite des Shops:</span></td></tr>\n";
            $content .= "<tr><td>Haupt Artikel Anzahl in Reihe:</td><td>".$Cpage->input_text("settings[MAIN_ARTICLE_COLSPAN]", $Asettings['settings']['MAIN_ARTICLE_COLSPAN'])."</td></tr>\n";
            $content .= "<tr><td>Anzahl Neuer Artikel in Reihe:</td><td>".$Cpage->input_text("settings[CHANGED_ITEMS_COLSPAN]", $Asettings['settings']['CHANGED_ITEMS_COLSPAN'])."</td></tr>\n";
            $content .= "<tr><td>Maximale Anzahl neuer Artikel:</td><td>".$Cpage->input_text("settings[SHOW_CHANGED_ITEMS]", $Asettings['settings']['SHOW_CHANGED_ITEMS'])."</td></tr>\n";
            $content .= "<tr><td>Anzahl gekaufter Artikel in Reihe:</td><td>".$Cpage->input_text("settings[BOUGHT_ITEMS_COLSPAN]", $Asettings['settings']['BOUGHT_ITEMS_COLSPAN'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Maximale Anzahl gekaufter Artikel:</td><td>".$Cpage->input_text("settings[SHOW_BOUGHT_ITEMS]", $Asettings['settings']['SHOW_BOUGHT_ITEMS'])."</td></tr>\n";

            $content .= "<tr class='line_bottom'><td>Maximale Anzahl an bestellbarem Artikel:</td><td>".$Cpage->input_text("settings[MAX_ORDER_ARTICLE]", $Asettings['settings']['MAX_ORDER_ARTICLE'])."</td></tr>\n";

            $content .= "<tr><td>Zeige Artikelanzahl bei Kategorien:</td><td>".$Cpage->select_multi("settings[SHOW_ARTICLE_COUNT]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_ARTICLE_COUNT'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Zeige Bilder im Kategorienmenü</td><td>".$Cpage->select_multi("settings[SHOW_PICTURE_CATEGORY_MENU]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_PICTURE_CATEGORY_MENU'])."</td></tr>\n";

            $content .= "<tr><td>Individuelle Startseite anzeigen?:</td><td>".$Cpage->select_multi("settings[SHOW_START_PAGE]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_START_PAGE'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop, wenn nur die Domain aufgerufen wird?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_INDEX]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_INDEX'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Account\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_ACCOUNT]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_ACCOUNT'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Warenkorb\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_CART]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_CART'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Newsletter\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_NEWSLETTER]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_NEWSLETTER'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Bestellung\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_ORDER]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_ORDER'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Passwort Reset\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_PASSWORD]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_PASSWORD'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Frage stellen\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_QUESTION]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_QUESTION'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Registrieren\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_REGISTER]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_REGISTER'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Suchergebnis\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_SEARCH]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_SEARCH'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Versandkosten Info\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_SHIPPING_COST]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_SHIPPING_COST'])."</td></tr>\n";
            $content .= "<tr><td>Zeige Shop auf der Seite \"Start\"?:</td><td>".$Cpage->select_multi("settings[SHOW_SHOP_START]", $Cpage->Aglobal['answer'], $Asettings['settings']['SHOW_SHOP_START'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Verberge Shop auf jeder Seite?:</td><td>".$Cpage->select_multi("settings[HIDE_SHOP]", $Cpage->Aglobal['answer'], $Asettings['settings']['HIDE_SHOP'])."</td></tr>\n";

            $content .= "<tr><td>Untertitel Teiler:</td><td>".$Cpage->input_text("language[SUBTITLE_SEPARATOR]", $Asettings['language']['SUBTITLE_SEPARATOR'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Account\":</td><td>".$Cpage->input_text("language[SUBTITLE_ACCOUNT]", $Asettings['language']['SUBTITLE_ACCOUNT'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Warenkorb\":</td><td>".$Cpage->input_text("language[SUBTITLE_CART]", $Asettings['language']['SUBTITLE_CART'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Newsletter\":</td><td>".$Cpage->input_text("language[SUBTITLE_NEWSLETTER]", $Asettings['language']['SUBTITLE_NEWSLETTER'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Bestellung\":</td><td>".$Cpage->input_text("language[SUBTITLE_ORDER]", $Asettings['language']['SUBTITLE_ORDER'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Passwort Reset\":</td><td>".$Cpage->input_text("language[SUBTITLE_PASSWORD]", $Asettings['language']['SUBTITLE_PASSWORD'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Frage stellen\":</td><td>".$Cpage->input_text("language[SUBTITLE_QUESTION]", $Asettings['language']['SUBTITLE_QUESTION'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Registrierung\":</td><td>".$Cpage->input_text("language[SUBTITLE_REGISTER]", $Asettings['language']['SUBTITLE_REGISTER'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Suchergebnis\":</td><td>".$Cpage->input_text("language[SUBTITLE_SEARCH]", $Asettings['language']['SUBTITLE_SEARCH'])."</td></tr>\n";
            $content .= "<tr><td>Untertitel \"Versandkosten Info\":</td><td>".$Cpage->input_text("language[SUBTITLE_SHIPPING_COST]", $Asettings['language']['SUBTITLE_SHIPPING_COST'])."</td></tr>\n";
            $content .= "<tr class='line_bottom'><td>Untertitel \"Start\":</td><td>".$Cpage->input_text("language[SUBTITLE_START]", $Asettings['language']['SUBTITLE_START'])."</td></tr>\n";

            $content .= "<tr><td></td><td>".$Cpage->input_submit("Speichern")."</td></tr></table>\n";
            $content .= "</form>";
            break;
    }
    $content .= "</div>";
    return $content;
}

function resize_pictures()
{
}