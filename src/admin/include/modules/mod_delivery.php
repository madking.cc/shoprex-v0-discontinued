<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "weight":
        set_delivery_type();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "weight_save":
        save_weight();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "cod_weight_save":
        save_weight_cod();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "delete_shipping_weight":
    case "delete_shipping_weight_cod":
        delete_shipping_weight();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "flatrate":
        set_delivery_type();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "flatrate_save":
        save_flatrate();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "cod_flatrate_save":
        save_flatrate_cod();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "delete_shipping_flatrate":
    case "delete_shipping_flatrate_cod":
        delete_shipping_flatrate();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "free":
        set_delivery_type();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "country_delete":
        delete_country();
        $content .= show_countries();
        $content .= show_menu();

        break;
    case "country_add":
        $content .= add_country();
        $content .= show_countries();
        $content .= show_menu();

        break;
    default:

        $content .= show_countries();
        $content .= show_menu();
        break;
}

function delete_country()
{
    global $Cdb;
    global $Cpage;

    $country_id = $Cpage->get_parameter("countries");

    $sql = "DELETE FROM `".TBL_PREFIX."countries_".LANGUAGE."` WHERE `id`=".$country_id.";";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}

function add_country()
{
    global $Cdb;
    global $Cpage;
    $content = "";

    $country_name = $Cpage->get_parameter("country_name");
    $country_code = $Cpage->get_parameter("country_code");
    $country_code = strtolower($country_code);

    if(empty($country_name) OR empty($country_code))
    {
        $content .= $Cpage->alert("Kein Feld darf leer sein.");
        return $content;
    }

    $sql    = "SELECT * FROM `".TBL_PREFIX."countries_".LANGUAGE."` WHERE `code` LIKE '$country_code';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    if($result->num_rows > 0)
    {
        $content .= $Cpage->alert("Der Code \\\"$country_code\\\" eines Landes ist bereits vergeben.");
        return $content;
    }

    $sql  = "INSERT INTO `".TBL_PREFIX."countries_".LANGUAGE."` (`code`, `country`) VALUES (?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ss', $country_code, $country_name);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);

    return $content;
}

function set_delivery_type()
{
    global $Cdb;
    global $action;

    $sql  = "UPDATE `".TBL_PREFIX."settings` SET
	`value` = ?
	WHERE `name` = 'delivery_method';";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('s', $action);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function show_countries()
{
    global $Cdb;
    global $Cpage;
    $content = "";

    $sql    = "SELECT * FROM `".TBL_PREFIX."countries_".LANGUAGE."` ORDER BY `code` ASC;";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= $Cpage->form("country_form", "delivery.php", "country_delete");
    $content .= "<div class='item_border'><div class='item_header'>Lieferorte</div><div class='item_content content_wrapper'>\n".$Cpage->table()."<tr><td>".$Cpage->select("countries", 7, NOT_MULTIPLE);
    while($countries = $result->fetch_assoc())
    {
        $content .= "<option value='".$countries['id']."'>".$countries['code']." - ".$countries['country']."</option>\n";
    }
    $content .= "</select></td><td valign='bottom'>".$Cpage->input_submit("Land löschen")."</td><td valign='top'><span class='information'></span></td></tr></table></form>";

    $content .= $Cpage->form("country_form2", "delivery.php", "country_add")."<br />Name des Landes: ".$Cpage->input_text("country_name")." Code des Landes: ".$Cpage->input_text("country_code", "", "20")." ".$Cpage->input_submit("Land hinzufügen")."<br />"."<span class='information'>Als Ländercode siehe <a href='http://www.iso.org/iso/home/standards/country_codes/iso-3166-1_decoding_table.htm' target='_blank'>Code Tabelle</a><br /></form>";
    $content .= "</div></div><div class='clear_float'></div>\n";
    return $content;
}

function show_menu()
{
    global $Cdb;
    global $Cpage;
    global $script;
    global $script_footer;

    $sql                          = "SELECT * FROM `".TBL_PREFIX."countries_".LANGUAGE."` ORDER BY `code` ASC;";
    $result                       = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Acountries                   = array();
    $Acountries['country']        = array();
    $Acountries['code']           = array();
    $Acountries_weight            = array();
    $Acountries_weight['country'] = array();
    $Acountries_weight['code']    = array();
    while($countries = $result->fetch_assoc())
    {
        array_push($Acountries['country'], $countries['country']);
        array_push($Acountries['code'], $countries['code']);
        array_push($Acountries_weight['country'], $countries['country']);
        array_push($Acountries_weight['code'], $countries['code']);
    }
    $Acountries_cod        = $Acountries;
    $Acountries_weight_cod = $Acountries_weight;

    $sql                             = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate` WHERE `type` ='normal';";
    $result                          = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Ashipping_flatrate              = array();
    $Ashipping_flatrate['countries'] = array();
    $Ashipping_flatrate['netto']     = array();
    $Ashipping_flatrate['id']        = array();
    $Atemp                           = array();
    if($result->num_rows > 0)
    {
        while($Ashipping = $result->fetch_assoc())
        {
            $Atemp = array_merge($Atemp, unserialize($Ashipping['targets']));
            array_push($Ashipping_flatrate['countries'], unserialize($Ashipping['targets']));
            array_push($Ashipping_flatrate['netto'], $Ashipping['cost_netto']);
            array_push($Ashipping_flatrate['id'], $Ashipping['id']);
        }
        foreach($Atemp as $key => $value)
        {
            foreach($Acountries['code'] as $key2 => $value2)
            {
                if($value2 == $value)
                {
                    unset($Acountries['code'][$key2]);
                    unset($Acountries['country'][$key2]);
                    break;
                }
            }
        }
    }

    $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_flatrate` WHERE `type` ='cod';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    unset($Atemp);
    $Ashipping_flatrate_cod              = array();
    $Ashipping_flatrate_cod['countries'] = array();
    $Ashipping_flatrate_cod['netto']     = array();
    $Ashipping_flatrate_cod['id']        = array();
    $Atemp                               = array();
    if($result->num_rows > 0)
    {
        while($Ashipping = $result->fetch_assoc())
        {
            $Atemp = array_merge($Atemp, unserialize($Ashipping['targets']));
            array_push($Ashipping_flatrate_cod['countries'], unserialize($Ashipping['targets']));
            array_push($Ashipping_flatrate_cod['netto'], $Ashipping['cost_netto']);
            array_push($Ashipping_flatrate_cod['id'], $Ashipping['id']);
        }
        foreach($Atemp as $key => $value)
        {
            foreach($Acountries_cod['code'] as $key2 => $value2)
            {
                if($value2 == $value)
                {
                    unset($Acountries_cod['code'][$key2]);
                    unset($Acountries_cod['country'][$key2]);
                    break;
                }
            }
        }
    }

    $sql                           = "SELECT * FROM `".TBL_PREFIX."shipping_weight` WHERE `type` ='normal';";
    $result                        = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $Ashipping_weight              = array();
    $Ashipping_weight['countries'] = array();
    $Ashipping_weight['netto_1']   = array();
    $Ashipping_weight['netto_2']   = array();
    $Ashipping_weight['netto_3']   = array();
    $Ashipping_weight['netto_4']   = array();
    $Ashipping_weight['netto_5']   = array();
    $Ashipping_weight['netto_6']   = array();
    $Ashipping_weight['netto_7']   = array();
    $Ashipping_weight['netto_8']   = array();
    $Ashipping_weight['netto_9']   = array();
    $Ashipping_weight['netto_10']   = array();
    $Ashipping_weight['weight_1']  = array();
    $Ashipping_weight['weight_2']  = array();
    $Ashipping_weight['weight_3']  = array();
    $Ashipping_weight['weight_4']  = array();
    $Ashipping_weight['weight_5']  = array();
    $Ashipping_weight['weight_6']  = array();
    $Ashipping_weight['weight_7']  = array();
    $Ashipping_weight['weight_8']  = array();
    $Ashipping_weight['weight_9']  = array();
    $Ashipping_weight['weight_10']  = array();
    $Ashipping_weight['id']        = array();
    $Atemp                         = array();
    $Atemp2                        = array();
    if($result->num_rows > 0)
    {
        while($Ashipping = $result->fetch_assoc())
        {
            $Atemp = array_merge($Atemp, unserialize($Ashipping['targets']));
            array_push($Ashipping_weight['countries'], unserialize($Ashipping['targets']));
            $Atemp2 = unserialize($Ashipping['weight']);
            array_push($Ashipping_weight['weight_1'], $Atemp2[0]);
            array_push($Ashipping_weight['weight_2'], $Atemp2[1]);
            array_push($Ashipping_weight['weight_3'], $Atemp2[2]);
            array_push($Ashipping_weight['weight_4'], $Atemp2[3]);
            array_push($Ashipping_weight['weight_5'], $Atemp2[4]);
            array_push($Ashipping_weight['weight_6'], $Atemp2[5]);
            array_push($Ashipping_weight['weight_7'], $Atemp2[6]);
            array_push($Ashipping_weight['weight_8'], $Atemp2[7]);
            array_push($Ashipping_weight['weight_9'], $Atemp2[8]);
            array_push($Ashipping_weight['weight_10'], $Atemp2[9]);
            $Atemp2 = unserialize($Ashipping['cost_netto']);
            array_push($Ashipping_weight['netto_1'], $Atemp2[0]);
            array_push($Ashipping_weight['netto_2'], $Atemp2[1]);
            array_push($Ashipping_weight['netto_3'], $Atemp2[2]);
            array_push($Ashipping_weight['netto_4'], $Atemp2[3]);
            array_push($Ashipping_weight['netto_5'], $Atemp2[4]);
            array_push($Ashipping_weight['netto_6'], $Atemp2[5]);
            array_push($Ashipping_weight['netto_7'], $Atemp2[6]);
            array_push($Ashipping_weight['netto_8'], $Atemp2[7]);
            array_push($Ashipping_weight['netto_9'], $Atemp2[8]);
            array_push($Ashipping_weight['netto_10'], $Atemp2[9]);
            array_push($Ashipping_weight['id'], $Ashipping['id']);
        }
        foreach($Atemp as $key => $value)
        {
            foreach($Acountries_weight['code'] as $key2 => $value2)
            {
                if($value2 == $value)
                {
                    unset($Acountries_weight['code'][$key2]);
                    unset($Acountries_weight['country'][$key2]);
                    break;
                }
            }
        }
    }

    $sql    = "SELECT * FROM `".TBL_PREFIX."shipping_weight` WHERE `type` ='cod';";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    unset($Atemp);
    $Ashipping_weight_cod              = array();
    $Ashipping_weight_cod['countries'] = array();
    $Ashipping_weight_cod['netto']     = array();
    $Ashipping_weight_cod['id']        = array();
    $Atemp                             = array();
    if($result->num_rows > 0)
    {
        while($Ashipping = $result->fetch_assoc())
        {
            $Atemp = array_merge($Atemp, unserialize($Ashipping['targets']));
            array_push($Ashipping_weight_cod['countries'], unserialize($Ashipping['targets']));
            array_push($Ashipping_weight_cod['netto'], $Ashipping['cost_netto']);
            array_push($Ashipping_weight_cod['id'], $Ashipping['id']);
        }
        foreach($Atemp as $key => $value)
        {
            foreach($Acountries_weight_cod['code'] as $key2 => $value2)
            {
                if($value2 == $value)
                {
                    unset($Acountries_weight_cod['code'][$key2]);
                    unset($Acountries_weight_cod['country'][$key2]);
                    break;
                }
            }
        }
    }

    $sql             = "SELECT * FROM `".TBL_PREFIX."settings` WHERE `name` ='delivery_method';";
    $result          = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $delivery_method = $result->fetch_assoc();
    $delivery_method = $delivery_method['value'];

    $content = "";
    $script .= "
				 function selectChange(formID)
				 {
				 	document.getElementById('weight').style.display = 'none';
				 	document.getElementById('flatrate').style.display = 'none';
				 	document.getElementById(formID.action.options[formID.action.selectedIndex].value).style.display = 'block';
				 }
				function entry_add(selin,selout,formID)
				{
					if(selin.selectedIndex > -1)
					{
						selout.options[selout.length] = selin.options[selin.selectedIndex];
					}
				    for (var i=0; i<selout.options.length; i++) 
				    {
				    	selout.options[i].selected = false;
				    }
				    selin.options[0].selected = true;
				}
				function entry_remove(selin,selout,formID)
				{
					if(selout.selectedIndex > -1)
					{
						selin.options[selin.length] = selout.options[selout.selectedIndex];
					}	
				    for (var i=0; i<selin.options.length; i++) 
				    {
				    	selin.options[i].selected = false;
				    }	
				    selout.options[0].selected = true;					
				}
	
		function check_submit_flatrate(formID)
		{
			if(formID.elements[\"shipping_flatrate_add[]\"].length == 0)
			{
				alert('Sie müssen ein Land angeben. Abbruch.');
				return false;
			}
			if((formID.elements[\"shipping_brutto_flatrate\"].value == '') && (formID.elements[\"shipping_netto_flatrate\"].value == ''))
			{
				alert('Geben Sie einen Preis zu den Versandkosten angeben. Abbruch.');
				return false;
			}
			if(formID.elements[\"shipping_netto_flatrate\"].value != '')
			{
				formID.elements[\"shipping_netto_flatrate\"].value = formID.elements[\"shipping_netto_flatrate\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_netto_flatrate\"].value))
				{
					alert('Es sind nur Zahlen bei den Versandkosten erlaubt. Abbruch.');
					return false;
				}
			}
			if(formID.elements[\"shipping_brutto_flatrate\"].value != '')
			{
				formID.elements[\"shipping_brutto_flatrate\"].value = formID.elements[\"shipping_brutto_flatrate\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_brutto_flatrate\"].value))
				{
					alert('Es sind nur Zahlen bei den Versandkosten erlaubt. Abbruch.');
					return false;
				}
			}			
		    for (var i=0; i<formID.elements[\"shipping_flatrate_add[]\"].options.length; i++) 
		    {
		    	formID.elements[\"shipping_flatrate_add[]\"].options[i].selected = true;
		    }
			return true;
		}

		function check_submit_cod_flatrate(formID)
		{
			if(formID.elements[\"shipping_cod_flatrate_add[]\"].length == 0)
			{
				alert('Sie müssen ein Land angeben. Abbruch.');
				return false;
			}
			if((formID.elements[\"shipping_cod_brutto_flatrate\"].value == '') && (formID.elements[\"shipping_cod_netto_flatrate\"].value == ''))
			{
				alert('Geben Sie einen Preis zu den Nachnahmekosten angeben. Abbruch.');
				return false;
			}
			if(formID.elements[\"shipping_cod_netto_flatrate\"].value != '')
			{
				formID.elements[\"shipping_cod_netto_flatrate\"].value = formID.elements[\"shipping_cod_netto_flatrate\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_cod_netto_flatrate\"].value))
				{
					alert('Es sind nur Zahlen bei den Nachnahmekosten erlaubt. Abbruch.');
					return false;
				}
			}
			if(formID.elements[\"shipping_cod_brutto_flatrate\"].value != '')
			{
				formID.elements[\"shipping_cod_brutto_flatrate\"].value = formID.elements[\"shipping_cod_brutto_flatrate\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_cod_brutto_flatrate\"].value))
				{
					alert('Es sind nur Zahlen bei den Nachnahmekosten erlaubt. Abbruch.');
					return false;
				}
			}			
		    for (var i=0; i<formID.elements[\"shipping_cod_flatrate_add[]\"].options.length; i++) 
		    {
		    	formID.elements[\"shipping_cod_flatrate_add[]\"].options[i].selected = true;
		    }
			return true;
		}
				
		function check_submit_weight(formID)
		{
			if(formID.elements[\"shipping_weight_add[]\"].length == 0)
			{
				alert('Sie müssen ein Land angeben. Abbruch.');
				return false;
			}
			for (var i=formID.elements[\"shipping_netto_weight[]\"].length-1; i>=0; i--)
			{
				if(formID.elements[\"shipping_netto_weight[]\"][i].value != '')
				{
					formID.elements[\"shipping_netto_weight[]\"][i].value = formID.elements[\"shipping_netto_weight[]\"][i].value.replace(/,/, \".\");
					if(isNaN(formID.elements[\"shipping_netto_weight[]\"][i].value))
					{
						alert('Es sind nur Zahlen bei den Versandkosten erlaubt. Abbruch.');
						return false;
					}
				}
				if(formID.elements[\"shipping_brutto_weight[]\"][i].value != '')
				{
					formID.elements[\"shipping_brutto_weight[]\"][i].value = formID.elements[\"shipping_brutto_weight[]\"][i].value.replace(/,/, \".\");
					if(isNaN(formID.elements[\"shipping_brutto_weight[]\"][i].value))
					{
						alert('Es sind nur Zahlen bei den Versandkosten erlaubt. Abbruch.');
						return false;
					}
				}
			}
		    if (formID.elements[\"shipping_weight[]\"][0].value == '')
		    {
		    	alert('Sie müssen mindestens ein Gewicht angeben. Abbruch.');
		    	return false;
		    }
			for (var i=formID.elements[\"shipping_weight[]\"].length-1; i>=0; i--)
			{
				if(formID.elements[\"shipping_weight[]\"][i].value != '')
				{
					if(isNaN(formID.elements[\"shipping_weight[]\"][i].value))
					{
						alert('Es sind nur Zahlen beim Gewicht erlaubt. Abbruch.');
						return false;
					}
					if((formID.elements[\"shipping_brutto_weight[]\"][i].value == '') && (formID.elements[\"shipping_netto_weight[]\"][i].value == ''))
					{
						alert('Geben Sie einen Preis zum Gewicht an. Abbruch.');
						return false;
					}
				}
			}

		    for (var i=formID.elements[\"shipping_weight[]\"].length-1; i>=1; i--) 
		    {
		    	if ((formID.elements[\"shipping_weight[]\"][i-1].value == '') && (formID.elements[\"shipping_weight[]\"][i].value != ''))
		    	{
		    		alert('Die Gewichte müssen hinterneinander angegeben sein. Abbruch.');
		    		return false;
		    	}
		    	if ((formID.elements[\"shipping_weight[]\"][i-1].value != '') && (formID.elements[\"shipping_weight[]\"][i].value != ''))
		    	{
		    		if (parseInt(formID.elements[\"shipping_weight[]\"][i-1].value) >= parseInt(formID.elements[\"shipping_weight[]\"][i].value))
		    		{
		    			alert('Die Gewichte müssen hinterneinander größer werden. Abbruch.');
		    			return false;
		    		}
		    	}
		    	
		    }			
		    for (var i=0; i<formID.elements[\"shipping_weight_add[]\"].options.length; i++) 
		    {
		    	formID.elements[\"shipping_weight_add[]\"].options[i].selected = true;
		    }
			return true;
		}

		function check_submit_cod_weight(formID)
		{
			if(formID.elements[\"shipping_cod_weight_add[]\"].length == 0)
			{
				alert('Sie müssen ein Land angeben. Abbruch.');
				return false;
			}
			if((formID.elements[\"shipping_cod_brutto_weight\"].value == '') && (formID.elements[\"shipping_cod_netto_weight\"].value == ''))
			{
				alert('Geben Sie einen Preis zu den Nachnahmekosten angeben. Abbruch.');
				return false;
			}
			if(formID.elements[\"shipping_cod_netto_weight\"].value != '')
			{
				formID.elements[\"shipping_cod_netto_weight\"].value = formID.elements[\"shipping_cod_netto_weight\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_cod_netto_weight\"].value))
				{
					alert('Es sind nur Zahlen bei den Nachnahmekosten erlaubt. Abbruch.');
					return false;
				}
			}
			if(formID.elements[\"shipping_cod_brutto_weight\"].value != '')
			{
				formID.elements[\"shipping_cod_brutto_weight\"].value = formID.elements[\"shipping_cod_brutto_weight\"].value.replace(/,/, \".\");
				if(isNaN(formID.elements[\"shipping_cod_brutto_weight\"].value))
				{
					alert('Es sind nur Zahlen bei den Nachnahmekosten erlaubt. Abbruch.');
					return false;
				}
			}			
		    for (var i=0; i<formID.elements[\"shipping_cod_weight_add[]\"].options.length; i++) 
		    {
		    	formID.elements[\"shipping_cod_weight_add[]\"].options[i].selected = true;
		    }
			return true;
		}		
		
	";

    $content .= $Cpage->form("delivery", "delivery.php");
    $content .= "<div class='item_border'><div class='item_header'>Versandkostenart</div><div class='item_content content_wrapper'>\n".$Cpage->select("do_action", 1, NOT_MULTIPLE, NOT_READONLY, "selectChange(this.form);")."
				<option value='weight'";
    if($delivery_method == "weight")
    {
        $content .= " selected";
    }
    $content .= ">Versandkosten nach Gewicht</option>
				<option value='flatrate'";
    if($delivery_method == "flatrate")
    {
        $content .= " selected";
    }
    $content .= ">Versand mit Pauschalpreis</option>
				<option value='free'";
    if($delivery_method == "free")
    {
        $content .= " selected";
    }
    $content .= ">Keine Versandkosten</option></select>
				<br />".$Cpage->input_submit("Speichern");
    $content .= "</div></div></form>\n";

    $content .= "<div class='item_border' id='weight'><div class='item_header'>Versandkosten nach Gewicht</div><div class='item_content'>\n";
    if(sizeof($Acountries_weight['country']) > 0)
    {
        $content .= $Cpage->form("weight", "delivery.php", "weight_save", "return check_submit_weight(this);")."<div class='content_wrapper'>".$Cpage->table()."
		<tr>
		 <td colspan='2'>
		  ".$Cpage->table()."
		  <tr>
		   <td>".$Cpage->select("shipping_weight_select", 7, NOT_MULTIPLE);
        foreach($Acountries_weight['country'] as $key => $value)
        {
            $content .= "<option value='".$Acountries_weight['code'][$key]."'>".$Acountries_weight['code'][$key]." - ".$value."</option>\n";
        }
        $content .= "</select></td>
		    <td valign='center'>".$Cpage->input_button("-->", "entry_add(this.form.elements[\"shipping_weight_select\"],this.form.elements[\"shipping_weight_add[]\"],this.form)")."<br />".$Cpage->input_button("&#60;--", "entry_remove(this.form.elements[\"shipping_weight_select\"],this.form.elements[\"shipping_weight_add[]\"],this.form)")."</td>
		    <td>".$Cpage->select("shipping_weight_add[]", 7, MULTIPLE)."</select>\n</td>
		   </tr>
		   </table>
		  </td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][0].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][0].value=\"\";")."
		Gewicht von 0 </td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][1].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][1].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][2].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][2].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][3].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][3].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][4].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][4].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][5].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][5].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][6].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][6].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][7].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][7].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][8].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][8].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm</td>
		</tr>
		<tr>
		<td>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_netto_weight[]\"][9].value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_weight[]", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.elements[\"shipping_brutto_weight[]\"][9].value=\"\";")."
		</td><td>bis ".$Cpage->input_text("shipping_weight[]", "", 50)." gramm ".$Cpage->input_submit("Hinzufügen")."</td>
		</tr>";
        $content .= "</table>";
        $content .= "</div>";
        $content .= "</form>";
    }

    if(ENABLE_COD && (sizeof($Acountries_weight_cod['country']) > 0))
    {
        $content .= $Cpage->form("cod_weight", "delivery.php", "cod_weight_save", "return check_submit_cod_weight(this);")."<div class='div_line'></div><div class='content_wrapper'>".$Cpage->table()."
		<tr>
		<td>".$Cpage->select("shipping_cod_weight_select", 7, NOT_MULTIPLE);
        foreach($Acountries_weight_cod['country'] as $key => $value)
        {
            $content .= "<option value='".$Acountries_weight_cod['code'][$key]."'>".$Acountries_weight_cod['code'][$key]." - ".$value."</option>\n";
        }
        $content .= "</select></td>
		<td valign='center'>".$Cpage->input_button("-->", "entry_add(this.form.elements[\"shipping_cod_weight_select\"],this.form.elements[\"shipping_cod_weight_add[]\"],this.form)")."<br />".$Cpage->input_button("&#60;--", "entry_remove(this.form.elements[\"shipping_cod_weight_select\"],this.form.elements[\"shipping_cod_weight_add[]\"],this.form)")."</td>
		<td>".$Cpage->select("shipping_cod_weight_add[]", 7, MULTIPLE)."</select>\n</td>
		</tr>
		<tr>";
        $content .= "<tr>
		<td colspan='3'>Nachnahmekosten Brutto: ".$Cpage->input_text("shipping_cod_brutto_weight", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_cod_netto_weight.value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_cod_netto_weight", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_cod_brutto_weight.value=\"\";")." ".$Cpage->input_submit("Hinzufügen")."</td>
		</tr>";
        $content .= "</table>";
        $content .= "</div>";

        $content .= "</form>";
    }
    foreach($Ashipping_weight['countries'] as $key => $value)
    {
        $content .= "<div class='div_line'></div><div class='content_wrapper'>".$Cpage->form("delete_shipping_form_".$key, "delivery.php", "delete_shipping_weight").$Cpage->input_hidden("id", $Ashipping_weight['id'][$key]).$Cpage->table()."
				<tr>
				<td colspan='3'>Versandkosten ".($key+1).":</td>
				</tr>
				<tr>
				<td colspan='3'>".$Cpage->select("shipping_temp_".$key, 7, NOT_MULTIPLE, READONLY);
        foreach($value as $key2 => $value2)
        {
            $content .= "<option>".$value2." - ".$Cpage->get_country_name($value2)."</option>";
        }
        $content .= "</select></td>
		</tr>";

        $content .= "<tr>
			 <td>Kosten Brutto: ".$Cpage->input_text("brutto_1".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_1'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			 Kosten Netto: ".$Cpage->input_text("netto_1".$key, $Cpage->money($Ashipping_weight['netto_1'][$key]), 75, READONLY)." Gewicht von: 0 </td>
			 <td>bis: ".$Cpage->input_text("weight_1".$key, $Ashipping_weight['weight_1'][$key], 75, READONLY)." gramm</td>
			</tr>";

        if($Ashipping_weight['weight_2'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_2".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_2'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_2".$key, $Cpage->money($Ashipping_weight['netto_2'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_2".$key, $Ashipping_weight['weight_2'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_3'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_3".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_3'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_3".$key, $Cpage->money($Ashipping_weight['netto_3'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_3".$key, $Ashipping_weight['weight_3'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_4'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_4".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_4'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_4".$key, $Cpage->money($Ashipping_weight['netto_4'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_4".$key, $Ashipping_weight['weight_4'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_5'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_5".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_5'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_5".$key, $Cpage->money($Ashipping_weight['netto_5'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_5".$key, $Ashipping_weight['weight_5'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_6'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_6".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_6'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_6".$key, $Cpage->money($Ashipping_weight['netto_6'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_6".$key, $Ashipping_weight['weight_6'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_7'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_7".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_7'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_7".$key, $Cpage->money($Ashipping_weight['netto_7'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_7".$key, $Ashipping_weight['weight_7'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_8'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_8".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_8'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_8".$key, $Cpage->money($Ashipping_weight['netto_8'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_8".$key, $Ashipping_weight['weight_8'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_9'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_9".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_9'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_9".$key, $Cpage->money($Ashipping_weight['netto_9'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_9".$key, $Ashipping_weight['weight_9'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }

        if($Ashipping_weight['weight_10'][$key] != 0)
        {
            $content .= "<tr>
			<td>Kosten Brutto: ".$Cpage->input_text("brutto_10".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight['netto_10'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."
			Kosten Netto: ".$Cpage->input_text("netto_10".$key, $Cpage->money($Ashipping_weight['netto_10'][$key]), 75, READONLY)."</td>
			<td>bis: ".$Cpage->input_text("weight_10".$key, $Ashipping_weight['weight_10'][$key], 75, READONLY)." gramm</td>
			</tr>";
        }


        $content .= "<tr>
		 <td colspan='2'>".$Cpage->input_submit("Löschen")."</td>
		</tr>
		</table>
		</form></div>";
    }
    foreach($Ashipping_weight_cod['countries'] as $key => $value)
    {
        $content .= "<div class='div_line'></div><div class='content_wrapper'>".$Cpage->form("delete_shipping_cod_form_".$key, "delivery.php", "delete_shipping_weight_cod").$Cpage->input_hidden("id", $Ashipping_weight_cod['id'][$key]).$Cpage->table()."
				<tr>
				<td colspan='3'>Nachnahmekosten ".($key+1).":</td>
				</tr>
				<tr>
				<td colspan='3'>".$Cpage->select("shipping_cod_temp_".$key, 7, NOT_MULTIPLE, READONLY);
        foreach($value as $key2 => $value2)
        {
            $content .= "<option>".$value2." - ".$Cpage->get_country_name($value2)."</option>";
        }
        $content .= "</select></td>
		</tr>
		<tr>
		<td>Kosten Brutto: </td><td>".$Cpage->input_text("brutto_cod".$key, $Cpage->money($Cpage->tax_calc($Ashipping_weight_cod['netto'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."</td>
		<td rowspan='2'>".$Cpage->input_submit("Löschen")."</td>
		</tr>
		<tr>
		<td>Kosten Netto: </td><td>".$Cpage->input_text("netto_cod".$key, $Cpage->money($Ashipping_weight_cod['netto'][$key]), 75, READONLY)."</td>
		</tr>
		</table>
		</form></div>";
    }
    $content .= "</div></div>\n";

    $content .= "<div class='item_border' id='flatrate'><div class='item_header'>Versand mit Pauschalpreis</div><div class='item_content'>\n";
    if(sizeof($Acountries['country']) > 0)
    {
        $content .= $Cpage->form("flatrate", "delivery.php", "flatrate_save", "return check_submit_flatrate(this);")."<div class='content_wrapper'>".$Cpage->table()."
					<tr>
					 <td>".$Cpage->select("shipping_flatrate_select", 7, NOT_MULTIPLE);
        foreach($Acountries['country'] as $key => $value)
        {
            $content .= "<option value='".$Acountries['code'][$key]."'>".$Acountries['code'][$key]." - ".$value."</option>\n";
        }
        $content .= "</select></td>
					 <td valign='center'>".$Cpage->input_button("-->", "entry_add(this.form.elements[\"shipping_flatrate_select\"],this.form.elements[\"shipping_flatrate_add[]\"],this.form)")."<br />".$Cpage->input_button("&#60;--", "entry_remove(this.form.elements[\"shipping_flatrate_select\"],this.form.elements[\"shipping_flatrate_add[]\"],this.form)")."</td>
					 <td>".$Cpage->select("shipping_flatrate_add[]", 7, MULTIPLE)."</select>\n</td>
					</tr>
					<tr>
					 <td colspan='3'>Versandkosten Brutto: ".$Cpage->input_text("shipping_brutto_flatrate", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_netto_flatrate.value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_netto_flatrate", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_brutto_flatrate.value=\"\";")." ".$Cpage->input_submit("Hinzufügen")."</td>
					</tr>";
        $content .= "</table>";
        $content .= "</div>";
        $content .= "</form>";
    }

    if(ENABLE_COD && (sizeof($Acountries_cod['country']) > 0))
    {
        $content .= $Cpage->form("cod_flatrate", "delivery.php", "cod_flatrate_save", "return check_submit_cod_flatrate(this);")."<div class='div_line'></div><div class='content_wrapper'>".$Cpage->table()."
					<tr>
					<td>".$Cpage->select("shipping_cod_flatrate_select", 7, NOT_MULTIPLE);
        foreach($Acountries_cod['country'] as $key => $value)
        {
            $content .= "<option value='".$Acountries_cod['code'][$key]."'>".$Acountries_cod['code'][$key]." - ".$value."</option>\n";
        }
        $content .= "</select></td>
					<td valign='center'>".$Cpage->input_button("-->", "entry_add(this.form.elements[\"shipping_cod_flatrate_select\"],this.form.elements[\"shipping_cod_flatrate_add[]\"],this.form)")."<br />".$Cpage->input_button("&#60;--", "entry_remove(this.form.elements[\"shipping_cod_flatrate_select\"],this.form.elements[\"shipping_cod_flatrate_add[]\"],this.form)")."</td>
					<td>".$Cpage->select("shipping_cod_flatrate_add[]", 7, MULTIPLE)."</select>\n</td>
					</tr>
					<tr>";
        $content .= "<tr>
					<td colspan='3'>Nachnahmekosten Brutto: ".$Cpage->input_text("shipping_cod_brutto_flatrate", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_cod_netto_flatrate.value=\"\";")." oder Netto: ".$Cpage->input_text("shipping_cod_netto_flatrate", "", 75, NOT_READONLY, "", NO_MAXLENGTH, "input", SUBMIT_CHANGE."this.form.shipping_cod_brutto_flatrate.value=\"\";")." ".$Cpage->input_submit("Hinzufügen")."</td>
					</tr>";
        $content .= "</table>";
        $content .= "</div>";

        $content .= "</form>";
    }
    foreach($Ashipping_flatrate['countries'] as $key => $value)
    {
        $content .= "<div class='div_line'></div><div class='content_wrapper'>".$Cpage->form("delete_shipping_form_".$key, "delivery.php", "delete_shipping_flatrate").$Cpage->input_hidden("id", $Ashipping_flatrate['id'][$key]).$Cpage->table()."
					<tr>
					 <td colspan='3'>Versandkosten ".($key+1).":</td>
					</tr>
					<tr>
					 <td colspan='3'>".$Cpage->select("shipping_temp_".$key, 7, NOT_MULTIPLE, READONLY);
        foreach($value as $key2 => $value2)
        {
            $content .= "<option>".$value2." - ".$Cpage->get_country_name($value2)."</option>";
        }
        $content .= "</select></td>
					</tr>
					<tr>
					 <td>Kosten Brutto: </td><td>".$Cpage->input_text("brutto_1".$key, $Cpage->money($Cpage->tax_calc($Ashipping_flatrate['netto'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."</td>
					 <td rowspan='2'>".$Cpage->input_submit("Löschen")."</td>
					</tr>
					<tr>
					 <td>Kosten Netto: </td><td>".$Cpage->input_text("brutto_1".$key, $Cpage->money($Ashipping_flatrate['netto'][$key]), 75, READONLY)."</td>
					</tr>
					</table>
					</form></div>";
    }
    foreach($Ashipping_flatrate_cod['countries'] as $key => $value)
    {
        $content .= "<div class='div_line'></div><div class='content_wrapper'>".$Cpage->form("delete_shipping_cod_form_".$key, "delivery.php", "delete_shipping_flatrate_cod").$Cpage->input_hidden("id", $Ashipping_flatrate_cod['id'][$key]).$Cpage->table()."
				<tr>
				<td colspan='3'>Nachnahmekosten ".($key+1).":</td>
				</tr>
				<tr>
				<td colspan='3'>".$Cpage->select("shipping_cod_temp_".$key, 7, NOT_MULTIPLE, READONLY);
        foreach($value as $key2 => $value2)
        {
            $content .= "<option>".$value2." - ".$Cpage->get_country_name($value2)."</option>";
        }
        $content .= "</select></td>
					</tr>
					<tr>
					<td>Kosten Brutto: </td><td>".$Cpage->input_text("brutto_1".$key, $Cpage->money($Cpage->tax_calc($Ashipping_flatrate_cod['netto'][$key], "brutto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY])), 75, READONLY)."</td>
					<td rowspan='2'>".$Cpage->input_submit("Löschen")."</td>
					</tr>
					<tr>
					<td>Kosten Netto: </td><td>".$Cpage->input_text("brutto_1".$key, $Cpage->money($Ashipping_flatrate_cod['netto'][$key]), 75, READONLY)."</td>
					</tr>
					</table>
					</form></div>";
    }
    $content .= "</div></div>\n";

    $script_footer .= "
				 document.getElementById('weight').style.display = 'none';
				 document.getElementById('flatrate').style.display = 'none';
				 document.getElementById('$delivery_method').style.display = 'block';
				";

    return $content;
}

function save_flatrate()
{
    global $Cpage;
    global $Cdb;

    $netto      = $Cpage->get_parameter("shipping_netto_flatrate", "empty");
    $brutto     = $Cpage->get_parameter("shipping_brutto_flatrate", "empty");
    $Acountries = array();
    $Acountries = $Cpage->get_parameter("shipping_flatrate_add", "empty", NO_ESCAPE);

    if(((strcmp($netto, "empty")) && (strcmp($brutto, "empty"))) || ($Acountries == "empty"))
    {
        return;
    }

    if($netto == "empty")
    {
        $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]);
    }
    $countries = serialize($Acountries);

    $sql  = "INSERT INTO `".TBL_PREFIX."shipping_flatrate` (`type`, `targets`, `cost_netto`) VALUES ('normal', ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ss', $countries, $netto);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function save_flatrate_cod()
{
    global $Cpage;
    global $Cdb;

    $netto      = $Cpage->get_parameter("shipping_cod_netto_flatrate", "empty");
    $brutto     = $Cpage->get_parameter("shipping_cod_brutto_flatrate", "empty");
    $Acountries = array();
    $Acountries = $Cpage->get_parameter("shipping_cod_flatrate_add", "empty", NO_ESCAPE);

    if(((strcmp($netto, "empty")) && (strcmp($brutto, "empty"))) || ($Acountries == "empty"))
    {
        return;
    }

    if($netto == "empty")
    {
        $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]);
    }
    $countries = serialize($Acountries);

    $sql  = "INSERT INTO `".TBL_PREFIX."shipping_flatrate` (`type`, `targets`, `cost_netto`) VALUES ('cod', ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ss', $countries, $netto);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function delete_shipping_flatrate()
{
    global $Cpage;
    global $Cdb;

    $id  = $Cpage->get_parameter("id");
    $sql = "DELETE FROM `".TBL_PREFIX."shipping_flatrate` WHERE `id`=".$id.";";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}

function save_weight()
{
    global $Cpage;
    global $Cdb;

    $Anetto     = array();
    $Anetto     = $Cpage->get_parameter("shipping_netto_weight", "empty", NO_ESCAPE);
    $Abrutto    = array();
    $Abrutto    = $Cpage->get_parameter("shipping_brutto_weight", "empty", NO_ESCAPE);
    $Acountries = array();
    $Acountries = $Cpage->get_parameter("shipping_weight_add", "empty", NO_ESCAPE);
    $Aweight    = array();
    $Aweight    = $Cpage->get_parameter("shipping_weight", "empty", NO_ESCAPE);

    if((($Anetto == "empty") && ($Abrutto == "empty")) || ($Acountries == "empty") || ($Aweight == "empty"))
    {
        return;
    }

    foreach($Abrutto as $key => $value)
    {
        if((!empty($value)) || ($value != 0))
        {
            $Anetto[$key] = $Cpage->tax_calc($value, "netto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]);
        }
    }
    //foreach($Anetto as $key => $value)
    //{
    //	$Anetto[$key] = number_format((float) $value, 6, ".", "");
    //}
    $countries = serialize($Acountries);
    $netto     = serialize($Anetto);
    $weight    = serialize($Aweight);

    $sql  = "INSERT INTO `".TBL_PREFIX."shipping_weight` (`type`, `targets`, `cost_netto`, `weight`) VALUES ('normal', ?, ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('sss', $countries, $netto, $weight);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function save_weight_cod()
{
    global $Cpage;
    global $Cdb;

    $netto      = $Cpage->get_parameter("shipping_cod_netto_weight", "empty");
    $brutto     = $Cpage->get_parameter("shipping_cod_brutto_weight", "empty");
    $Acountries = array();
    $Acountries = $Cpage->get_parameter("shipping_cod_weight_add", "empty", NO_ESCAPE);

    if(((strcmp($netto, "empty")) && (strcmp($brutto, "empty"))) || ($Acountries == "empty"))
    {
        return;
    }

    if($netto == "empty")
    {
        $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][SHIPPING_TAX_KEY]);
    }
    $countries = serialize($Acountries);

    $sql  = "INSERT INTO `".TBL_PREFIX."shipping_weight` (`type`, `targets`, `cost_netto`) VALUES ('cod', ?, ?);";
    $stmt = $Cdb->db_prepare($sql, __FILE__.":".__LINE__);
    $stmt->bind_param('ss', $countries, $netto);
    $Cdb->db_execute($stmt, __FILE__.":".__LINE__);
}

function delete_shipping_weight()
{
    global $Cpage;
    global $Cdb;

    $id  = $Cpage->get_parameter("id");
    $sql = "DELETE FROM `".TBL_PREFIX."shipping_weight` WHERE `id`=".$id.";";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);
}