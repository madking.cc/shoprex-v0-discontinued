<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "page_back":
        $start  = $Cpage->get_parameter("back");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_orders($start, $search);
        break;
    case "page_forward":
        $start  = $Cpage->get_parameter("forward");
        $search = $Cpage->get_parameter("search", "");
        $content .= show_orders($start, $search);
        break;
    default:
        $start  = $Cpage->get_parameter("start", 0);
        $search = $Cpage->get_parameter("search", "");
        $content .= show_orders($start, $search);
        break;
}

function show_orders($start, $search)
{
    global $Cpage;
    global $Cdb;
    global $script;

    $amount  = $Cpage->get_parameter("amount", 20);
    $content = "";

    $item      = $Cpage->get_parameter("item", "id");
    $direction = $Cpage->get_parameter("direction", "desc");

    $status2      = $Cpage->get_parameter("status2", -1);
    $where        = "";
    $search_where = "";
    if($status2 != -1)
    {
        $where = "WHERE `status` =$status2 ";
        if(!empty($search))
        {
            $search_where = "AND `status` =$status2 ";
        }
    }

    switch($item)
    {
        case "name":
            $item      = "lastname` $direction, `company";
            $item_temp = "name";
            break;
        case "target_country":
            $item      = "del_country` $direction, `country";
            $item_temp = "target_country";
            break;
        /*
        case "invoice_time":
            if(empty($search))
            {
                if(!empty($where))
                {
                    $where .= " AND `invoice_time` IS NOT NULL ";
                }
                else $where .= " WHERE `invoice_time` IS NOT NULL ";
            }
            $item_temp = $item;
            break;
        case "invoice_id":
            if(empty($search))
            {
                if(!empty($where))
                {
                    $where .= " AND `invoice_id` IS NOT NULL ";
                }
                else $where .= " WHERE `invoice_id` IS NOT NULL ";
            }*/
        default:
            $item_temp = $item;
            break;
    }

    $sql     = "SELECT COUNT(id) FROM `".TBL_PREFIX."orders` $where;";
    $result  = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $entries = $result->fetch_assoc();
    $entries = $entries["COUNT(id)"];

    if(empty($search))
    {
        $sql = "SELECT * FROM `".TBL_PREFIX."orders` $where ORDER BY `$item` $direction LIMIT $start, $amount;";
    }
    else
    {
        $sql = "	SELECT  * FROM `".TBL_PREFIX."orders`
			WHERE  ((`customer_id` LIKE '%$search%') OR (`invoice_id` LIKE '%$search%') OR (`firstname` LIKE '%$search%') OR (`lastname` LIKE '%$search%') OR (`address1` LIKE '%$search%') OR (`address2` LIKE '%$search%') OR (`zip` LIKE '%$search%') OR (`city` LIKE '%$search%') OR (`country` LIKE '%$search%') OR (`phone` LIKE '%$search%') OR (`email` LIKE '%$search%') OR (`company` LIKE '%$search%') OR (`del_name` LIKE '%$search%') OR (`del_address1` LIKE '%$search%') OR (`del_address2` LIKE '%$search%') OR (`del_zip` LIKE '%$search%') OR (`del_city` LIKE '%$search%') OR (`del_country` LIKE '%$search%') OR (`del_phone` LIKE '%$search%') OR (`cart` LIKE '%$search%'))
			$search_where ORDER BY `$item` $direction LIMIT $start, $amount;";
    }

    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $content .= "<div class='item_border'><div class='item_header'>Auftragsverwaltung</div><div class='item_content'>\n";
    $content .= $Cpage->form("order_search", "correspondence.php", "search").$Cpage->input_hidden("amount", $amount).$Cpage->table()."<tr><td>Suche:</td><td>".$Cpage->input_text("search", $search, FORM_TEXT_SIZE, NOT_READONLY, "", NO_MAXLENGTH, "input", NO_ON_SUBMIT)."</td><td>".$Cpage->input_submit("Suchen")." ".$Cpage->input_button("Suche löschen", "delete_search(this.form);")."
		</td><td>Zeige nur Status von ";

    $Atemp = array();
    $Atemp = $Cpage->Aglobal['status'];
    array_unshift($Atemp, "Alle anzeigen");

    $content .= $Cpage->select_multi("status2", $Atemp, ($status2+1), 0, -1, NOT_READONLY, "this.form.submit();");

    $content .= "</td></tr></table></form><br />";

    $script .= " function back_page(form) 						{ form.do_action.value='page_back'; form.submit(); }";
    $script .= "function forward_page(form) 					{ form.do_action.value='page_forward'; form.submit(); }";
    $script .= "function delete_search(form) 					{ form.search.value=''; form.submit(); }";
    $script .= "function edit_order(form, order_id) 			{ form.action=\"correspondence_edit.php\"; form.id.value=order_id; form.submit(); }";

    $content .= $Cpage->form("correspondence", "correspondence.php", "default").$Cpage->input_hidden("back", $start-$amount).$Cpage->input_hidden("forward", $start+$amount).$Cpage->input_hidden("start", $start).$Cpage->input_hidden("direction", $direction).$Cpage->input_hidden("item", $item).$Cpage->input_hidden("search", $search).$Cpage->input_hidden("status2", $status2).$Cpage->input_hidden("id").

        $Cpage->table()."<tr>
						 <td>";
    if($item == "order_id" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Bestellnr.:", "correspondence.php", "item=order_id&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "order_time" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Bestellzeit:", "correspondence.php", "item=order_time&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "invoice_id" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Rechnungsnr:", "correspondence.php", "item=invoice_id&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "invoice_time" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Rechnungsdatum:", "correspondence.php", "item=invoice_time&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "status" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Status:", "correspondence.php", "item=status&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "customer_type" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Kunden Typ:", "correspondence.php", "item=customer_type&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item_temp == "name" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Name:", "correspondence.php", "item=name&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "message" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Nachricht?:", "correspondence.php", "item=message&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item == "delivery_type" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Zahlweise:", "correspondence.php", "item=delivery_type&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>";
    if($item_temp == "target_country" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Zielland:", "correspondence.php", "item=target_country&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>Brutto:</td>
						 <td>Artikel:</td>
						 <td>";
    if($item == "request" && $direction == "asc")
    {
        $direction_item = "desc";
    }
    else $direction_item = "asc";
    $content .= $Cpage->link("Aktion nötig?.:", "correspondence.php", "item=request&direction=$direction_item&start=$start&amount=$amount&status2=$status2")."</td>
						 <td>".$Cpage->select("amount", 1, FALSE, FALSE, "this.form.submit();")."
						 <option value='5'";
    if($amount == 5)
    {
        $content .= " selected";
    }
    $content .= ">5 anzeigen</option>
						 <option value='20'";
    if($amount == 20)
    {
        $content .= " selected";
    }
    $content .= ">20 anzeigen</option>
						 <option value='50'";
    if($amount == 50)
    {
        $content .= " selected";
    }
    $content .= ">50 anzeigen</option>
						 <option value='100'";
    if($amount == 100)
    {
        $content .= " selected";
    }
    $content .= ">100 anzeigen</option>
						 <option value='999999'";
    if($amount == 999999)
    {
        $content .= " selected";
    }
    $content .= ">Alle anzeigen</option>
						 </select>
						 </td></tr>";

    if($result->num_rows > 0)
    {
        while($Aorder = $result->fetch_assoc())
        {
            $ar_cart = unserialize($Aorder['cart']);

            $content .= "<tr class='line_top' onmouseover=\"style.backgroundColor='yellow'\" onmouseout=\"style.backgroundColor='transparent'\">
			<td><nobr>".$Aorder['order_id']."</nobr></td>
			<td><nobr>".$Cpage->format_time($Aorder['order_time'], "sql")."</nobr></td>
			<td><nobr>".$Aorder['invoice_id']."</nobr></td>
			<td><nobr>";
            if(!empty($Aorder['invoice_time']))
            {
                $content .= $Cpage->format_time($Aorder['invoice_time'], "sql");
            }
            $content .= "</nobr></td>
			<td class='status_".$Aorder['status']."'><nobr>".$Cpage->Aglobal['status'][$Aorder['status']]."</nobr></td>
			<td><nobr>".$Cpage->Aglobal['customer_type'][$Aorder['customer_type']]."</nobr></td>
			<td><nobr>";
            if(($Aorder['customer_type'] == 2) && !empty($Aorder['company']))
            {
                $content .= $Aorder['company'];
            }
            else
            {
                $content .= $Aorder['firstname']." ".$Aorder['lastname'];
            }
            $content .= "</nobr></td>
			<td><nobr>";
            if(empty($Aorder['message']))
            {
                $content .= "";
            }
            else $content .= "Ja";
            $content .= "</nobr></td>
			<td><nobr>".$Cpage->Aglobal['delivery'][$Aorder['delivery_type']]['value']."</nobr></td>
			<td><nobr>";
            if(empty($Aorder['del_country']))
            {
                $content .= $Cpage->get_country_name($Aorder['country']);
            }
            else $content .= $Cpage->get_country_name($Aorder['del_country']);
            $content .= "</nobr></td>
			<td><nobr>".$Cpage->money($ar_cart['cart_price_brutto']+$ar_cart['shipping_cost_brutto']+$ar_cart['cod_shipping_cost_brutto'])."</nobr></td>
			<td><nobr>".$ar_cart['entire_quantity']."</nobr></td>
			<td><nobr>";
            if($Aorder['request'] > 0)
            {
                $content .= "Ja";
            }
            $content .= "</nobr></td>
			<td><nobr>".$Cpage->input_button("editieren", "edit_order(this.form, \"".$Aorder['id']."\");")."</nobr></td></tr>";
        }
    }
    else
    {
        $content .= "<tr class='line_top'><td colspan='13' align='center'><span class='information'>Keine Ergebnisse</span></td></tr>\n";
    }

    if($result->num_rows > 0)
    {
        $content .= "<tr><td colspan='6'>";
        if($start > 0)
        {
            $content .= $Cpage->input_button("Zurück", "back_page(this.form);");
        }
        $content .= "</td>";
        $content .= "<td colspan='3' align='center'>Seite ".(($start/$amount)+1)." von ".ceil($entries/$amount);
        $content .= "</td>";
        $content .= "<td align='right' colspan='4'>";
        if($start < ($entries-($amount)))
        {
            $content .= $Cpage->input_button("Vor", "forward_page(this.form);");
        }
        $content .= "</td></tr>";
    }
    $content .= "</table></form>";
    $content .= "</div></div>\n";
    return $content;
}
