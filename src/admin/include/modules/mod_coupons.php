<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$action = $Cpage->get_parameter("do_action");
switch($action)
{
    case "update":
        update_coupon();
        $content .= show_coupons();
        break;
    case "add":
        add_coupon();
        $content .= show_coupons();
        break;
    case "delete":
        delete_coupon();
        $content .= show_coupons();
        break;

    default:
        $content .= show_coupons();
        break;
}


function show_coupons()
{
    global $Cpage;
    global $Cdb;
    global $script;
    global $script_footer;
    $content = "";

    $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Coupon hinzufügen</div></div><div class='item_content'><div class='content_wrapper'>";

    $content .= $Cpage->form("coupon_add", "coupons.php", "add", 'return check_submit(this);')."Code: ".$Cpage->input_text("code")." Gültig bis ".$Cpage->input_text("valid_till", "0", 120, NOT_READONLY, "id='valid_till'")." ".$Cpage->input_button("Reset", "reset_time(this.form.valid_till);")." Netto Wert: ".$Cpage->input_text("netto", "", 60, NOT_READONLY, "onkeyup='this.form.brutto.value=\"\";'").CURRENCY_SYMBOL." MwSt.: ".$Cpage->select_multi('tax', $Cpage->Aglobal['tax_percent'], "1")." Brutto Wert: ".$Cpage->input_text("brutto", "", 60, NOT_READONLY, "onkeyup='this.form.netto.value=\"\";'").CURRENCY_SYMBOL." Mindestbestellwert: ".$Cpage->input_text("minimum", "0", 60)."€ Brutto<br />"
        .$Cpage->input_submit("Hinzufügen")."</form>";

    $content .= "</div>
		</div></div><div class='clear_float'></div>\n";


    $sql = "SELECT * FROM `".TBL_PREFIX."coupons` ORDER BY id";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);
    $i=0;
    if($result->num_rows > 0)
    {
        $content .= "<div class='item_border'><div class='item_header'><div class='item_point_single'>Vorhandene Coupons</div></div><div class='item_content'><div class='content_wrapper'>";

        while($row = $result->fetch_assoc())
        {
            $sql = "SELECT * FROM `".TBL_PREFIX."coupons_used` WHERE `coupon_id`=".$row['id'];
            $result2 = $Cdb->db_query($sql, __FILE__.":".__LINE__);
            $used = $Cdb->affected_rows();
            $brutto = $Cpage->tax_calc($row['netto'], "brutto", $Cpage->Aglobal['tax'][$row['tax']]);
            $till = $Cpage->format_time($row['till'], "sql");
            $content .= $Cpage->form("coupon".$i, "coupons.php", "update", 'return check_submit(this);')."Code: ".$Cpage->input_text("code", $row['code'])." Gültig bis ".$Cpage->input_text("valid_till", $till, 120, NOT_READONLY, "id='valid_till".$i."'")." ".$Cpage->input_button("Reset", "reset_time(this.form.valid_till);")." Netto Wert: ".$Cpage->input_text("netto", $Cpage->money($row['netto'], NO_CURRENCY_SYMBOL), 60, NOT_READONLY, "onkeyup='this.form.brutto.value=\"\";'").CURRENCY_SYMBOL." MwSt.: ".$Cpage->select_multi('tax', $Cpage->Aglobal['tax_percent'], $row['tax'])." Brutto Wert: ".$Cpage->input_text("brutto", $Cpage->money($brutto, NO_CURRENCY_SYMBOL), 60, NOT_READONLY, "onkeyup='this.form.netto.value=\"\";'").CURRENCY_SYMBOL." Mindestbestellwert: ".$Cpage->input_text("minimum", $Cpage->money($row['minimum'], NO_CURRENCY_SYMBOL), 60)."€ Brutto "
            .$Cpage->input_hidden("check", $i).$Cpage->input_hidden("id", $row['id']).$Cpage->input_submit("Aktualisieren")."</form>".$Cpage->form("coupon_delete".$i, "coupons.php", "delete").$Cpage->input_hidden("id", $row['id']).$Cpage->input_submit("Löschen")."</form> Eingelöst: $used<br />";
            $script .= $Cpage->datepicker("valid_till".$i);
            $i++;
        }

        $content .= "</div>
		</div></div><div class='clear_float'></div>\n";

    }

    $sql = "SELECT * FROM `".TBL_PREFIX."coupons` ORDER BY id";
    $result = $Cdb->db_query($sql, __FILE__.":".__LINE__);

    $script_footer .= $Cpage->datepicker("valid_till");
    $script .= "

    code = new Array();
    ";

    $i = 0;
    if($result->num_rows > 0)
        while($row = $result->fetch_assoc())
        {
            $script .= "    code[".($i++)."] = '".$row['code']."'\n";
        }

    $script .= "
    function reset_time(elementID)
	{
	    elementID.value = 0;
	}

	function check_submit(formID)
	{
	  	if( ((formID.netto.value==null)||(formID.netto.value==\"\")) && ((formID.brutto.value==null)||(formID.brutto.value==\"\")) )
		{
			alert(\"Sie müssen einen Brutto oder Netto Preis angeben. Sie können auch 0 Euro eintragen.\");
			return false;
		}
		formID.netto.value = formID.netto.value.replace(/,/, \".\");
		formID.brutto.value = formID.brutto.value.replace(/,/, \".\");
		formID.minimum.value = formID.minimum.value.replace(/,/, \".\");
		if(formID.code.value.length == 0)
		{
			alert(\"Sie müssen einen Code angeben.\");
			formID.code.focus();
			return false;
		}

		check = 0;
		if(formID.check)
		    check = formID.check.value;

		for(i=0; i < code.length; i++)
		{
		    if((formID.code.value == code[i]) && (check != i))
		    {
		        alert(\"Dieser Code ist bereits vergeben\");
		        formID.code.focus();
		        return false;
		    }
		}

		return true;
	}
    ";

    return $content;
}

function add_coupon()
{
    global $Cpage;
    global $Cdb;

    $code = $Cpage->get_parameter("code");
    $till = $Cpage->get_parameter("valid_till");
    $netto = $Cpage->get_parameter("netto");
    $tax = $Cpage->get_parameter("tax");
    $brutto = $Cpage->get_parameter("brutto");
    $minimum = $Cpage->get_parameter("minimum");

    if(empty($netto) || !empty($brutto))
    {
        $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][$tax]);
    }

    $till = $Cpage->get_timestamp($till);
    $till = $Cpage->get_sqltime($till);

    $sql = "INSERT INTO `".TBL_PREFIX."coupons` (
		`code` ,
		`netto` ,
		`minimum` ,
		`tax` ,
		`till`
		)
		VALUES ( '$code', '$netto', '$minimum', '$tax', '$till');";
    $Cdb->db_query($sql, __FILE__.":".__LINE__);

}

function update_coupon()
{
    global $Cpage;
    global $Cdb;

    $code = $Cpage->get_parameter("code");
    $till = $Cpage->get_parameter("valid_till");
    $netto = $Cpage->get_parameter("netto");
    $tax = $Cpage->get_parameter("tax");
    $brutto = $Cpage->get_parameter("brutto");
    $id = $Cpage->get_parameter("id");
    $minimum = $Cpage->get_parameter("minimum");

    if(empty($netto) || !empty($brutto))
    {
        $netto = $Cpage->tax_calc($brutto, "netto", $Cpage->Aglobal['tax'][$tax]);
    }

    $till = $Cpage->get_timestamp($till);
    $till = $Cpage->get_sqltime($till);

    $sql  = "UPDATE `".TBL_PREFIX."coupons` SET
	`code`       = '$code' ,
	`netto`      = '$netto' ,
	`minimum`    = '$minimum',
	`tax`        = '$tax' ,
	`till`       = '$till'
	WHERE `id` = $id;";

    $Cdb->db_query($sql, __FILE__.":".__LINE__);

}


function delete_coupon()
{
    global $Cpage;
    global $Cdb;

    $id = $Cpage->get_parameter("id");

    $sql  = "DELETE FROM `".TBL_PREFIX."coupons` WHERE `id` = $id;";

    $Cdb->db_query($sql, __FILE__.":".__LINE__);

}
