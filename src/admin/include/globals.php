<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$Cpage->language     = LANGUAGE;
$Cpage->allowed_keys = array('login_error', 'subject', 'category', 'article', 'key', 'page', 'do_action', 'id', 'item', 'is_array', 'parameter', 'back', 'show_table', 'picture_id', 'direction', 'start', 'amount', 'customer_id', 'order_id', 'status2', 'uploaddir');
$Cpage->allowed_pages = FALSE;

$Cpage->Aglobal['condition']      = $Acondition;
$Cpage->Aglobal['guarantee']      = $Aguarantee;
$Cpage->Aglobal['customer_type']  = $Acustomer_type;
$Cpage->Aglobal['customer_types'] = $Acustomer_types;
$Cpage->Aglobal['answer']         = $Aanswer;
$Cpage->Aglobal['status']         = $Astatus;
$Cpage->Aglobal['delivery']       = $Adelivery;

$tmp            = $Adelivery;
$tmp2           = array();
$tmp2['value']  = NOT_USED;
$tmp2['enable'] = 1;
$tmp2['code']   = -1;
array_shift($tmp);
array_shift($tmp);
array_unshift($tmp, $tmp2);
$Cpage->Aglobal['pickup'] = $tmp;

$Cpage->Aglobal['tax'] = $Atax;
$Cpage->Apath          = $Apath;
$Cpage->Awp            = $Awp;

$host                     = $_SERVER['HTTP_HOST'];
$uri                      = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$Cpage->Aglobal['http']   = "http://".$host.$uri."/";
$Cpage->Aglobal['www']    = $host;
$Cpage->Aglobal['domain'] = "http://".$host."/";
$Cpage->Aglobal['now']    = $Cpage->format_time($Cpage->get_sqltime());
$Cpage->Aglobal['time']   = time();

$Cpage->Amail['mail_default'] = SHOP_MAIL;

$Cpage->Aglobal['tax_percent'] = array();
foreach($Atax as $value)
    array_push($Cpage->Aglobal['tax_percent'], $value."%");
