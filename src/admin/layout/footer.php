<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

?>
</div>
</div>
<script>
    /* <![CDATA[ */// Javascript Global Variables
    widthNames=new Array("marginRight", "paddingLeft", "paddingRight", "marginLeft", "borderLeftWidth", "borderRightWidth");
    boxestoFix=new Array("pagewrapper");// Javascript Global Functions
    function getStyleValue(element, cssProperty)
    {
        var value="";
        if(window.getComputedStyle)
        {
            value=window.getComputedStyle(element, null)[cssProperty];
        }
        else
        {
            if(element.currentStyle)
            {
                value=element.currentStyle[cssProperty];
            }
        }
        return value;
    }// Javascript Functions
    function setboxes()
    {
        var innerscreen_width=window.innerWidth;
        for(var i=0; i < boxestoFix.length; i++)
        {
            var fixsize=setbox(boxestoFix[i]);
            document.getElementById(boxestoFix[i]).style.width=(innerscreen_width-fixsize)+"px";
        }
    }
    function setbox(boxname)
    {
        var fixsize=0;
        for(var j=0; j < widthNames.length; j++)
        {
            fixsize+=parseInt(getStyleValue(document.getElementById(boxname), widthNames[j]));
        }
        // Fix:
        fixsize+=parseInt(getStyleValue(document.getElementById(boxname), widthNames[0]));
        return fixsize;
    }// Javascript Actions
    var setTheBoxes=true;
    if(setTheBoxes)
    {
        setboxes();
        window.onresize=setboxes;
    }
    <?php echo $script_footer; ?>
    /* ]]> */
</script>
</body>
</html>