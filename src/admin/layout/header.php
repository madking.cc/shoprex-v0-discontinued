<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="de" lang="de">
<head>
    <title><?php echo PAGE_TITLE.$subtitle; ?></title>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
    <?php if(!isset($do_not_load_style))
{
    ?>
    <link rel='stylesheet' href='layout/css/screen.css' type='text/css' media='all'/>
    <?php
    if($load_shop_default_style == TRUE)
    {
        echo "<link rel='stylesheet' href='../".THEME."screen.css' type='text/css' media='all' />";
    }
}
    ?>
    <link rel='stylesheet' href='layout/css/print.css' type='text/css' media='print'/>
    <link rel='shortcut icon' href='layout/favicon.ico' type='image/x-icon'/>
    <?php echo $css; ?>
    <?php echo $script_head; ?>
</head>
<body class='<?php echo $nav_control; ?>'>
<script>
    /* <![CDATA[ */
<?php echo $script; ?>
    /* ]]> */
</script>