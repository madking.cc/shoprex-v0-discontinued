<?php defined('SECURITY_CHECK') or die;
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

?>
    <div id="pagewrapper">
        <div id="inner_head">
            <div class="float_left" id="inner_left"><H1 class='inner_head_h1'>Shop Administration</H1></div>
            <div class="float_right" id="inner_right"><H1 class='inner_head_h1'>shoprex</H1></div>
        </div>
        <div id="inner_line"></div>
        <?php if($show_menu)
    {
        ?>
        <ul id="mainmenu">
            <li><?php echo $Cpage->link("<nobr>Aufträge</nobr>", "correspondence.php"); ?></li>
            <li><span>Produkte</span>
                <ul>
                    <li><?php echo $Cpage->link("<nobr>Artikel+Kategorien</nobr>", "articles.php"); ?></li>
                    <li><?php echo $Cpage->link("Warenbestand", "stock.php"); ?></li>
                    <li><?php echo $Cpage->link("Gutscheine", "coupons.php", NO_PARAMETER, "lastmainmenuitem"); ?></li>
                </ul>
            </li>
            <li><span>Kunden</span>
                <ul>
                    <li><?php echo $Cpage->link("<nobr>Verwaltung</nobr>", "customers.php"); ?></li>
                    <li><?php echo $Cpage->link("<nobr>Newsletter</nobr>", "newsletter.php", NO_PARAMETER, "lastmainmenuitem"); ?></li>
                </ul>
            </li>
            <li><span>Einstellungen</span>
                <ul>
                    <li><?php echo $Cpage->link("<nobr>Adressen / Daten / Zugang</nobr>", "addresses.php"); ?></li>
                    <li><?php echo $Cpage->link("<nobr>Shop-Einstellungen</nobr>", "settings.php"); ?></li>
                    <li><?php echo $Cpage->link("Versandkosten", "delivery.php"); ?></li>
                    <li><?php echo $Cpage->link("Seiten", "pages.php"); ?></li>
                    <li><?php echo $Cpage->link("Vorlagen", "templates.php"); ?></li>
                    <li><?php echo $Cpage->link("Email Anhänge", "email_attachement.php"); ?></li>
                    <li><?php echo $Cpage->link("Sprache", "language.php", NO_PARAMETER, "lastmainmenuitem"); ?></li>
                </ul>
            </li>
            <li><?php echo $Cpage->link("<nobr>Ausloggen</nobr>", "logout.php"); ?></li>
        </ul>
        <?php } ?>
        <div id="maincontent">
            <?php echo $content; ?>

