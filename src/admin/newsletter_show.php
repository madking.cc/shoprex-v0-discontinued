<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$page              = "newsletter_show";
$show_menu         = FALSE;
$do_not_load_style = TRUE;
require ("index.php");