<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

$page                    = "article_preview";
$show_menu               = FALSE;
$load_shop_default_style = TRUE;

require ("index.php");

