<?php
define('SECURITY_CHECK', "OK");

if(!isset($page))
{
    $page = "correspondence";
}

$dir_root = dirname(__FILE__)."/../";

// Initialisiere die Page:
require ($dir_root."admin/include/init.php");

// Hole den Seiteninhalt:
require($dir_root."admin/include/get_content.php");

// Gebe die Page aus:
if($NO_LAYOUT)
{
    echo $content;
}
else
{
    require ($dir_root."admin/include/do_output.php");
}

// Ende:
require ($dir_root."admin/include/terminate.php");

