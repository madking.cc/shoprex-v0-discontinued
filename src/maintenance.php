<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="de" lang="de">
<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=utf-8"/>

    <title>Wartungsarbeiten</title>
    <link rel="shortcut icon"
          href="/favicon.ico" type="image/x-icon"/>
    <meta name="language"
          content="de"/>
    <meta name="robots"
          content="index, follow"/>
    <meta http-equiv="Content-Script-Type"
          content="text/javascript"/>
    <meta http-equiv="content-style-type"
          content="text/css"/>
</head>
<body>
<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

if(!isset($error_type))
{
    $error_type = "Unbekannt";
}

?>

<script>
    /* <![CDATA[ */
    var Ratezahl;
    var Zufallszahl;
    var Versuche=0;
    Zufallszahl=Math.ceil(Math.random()*100);
    function zufall()
    {
        Ratezahl=parseInt(document.getElementById('Ratefenster').value);
        if(Ratezahl > Zufallszahl)
        {
            Versuche++;
            alert("Zu groß");
        }
        if(Ratezahl < Zufallszahl)
        {
            Versuche++;
            alert("Zu klein");
        }
        if(Ratezahl == Zufallszahl)
        {
            Versuche++;
            alert("Richtig!");
            alert("Du hast "+Versuche+" Versuche gebraucht.");
            alert("Es wird eine neue Zufallszahl generiert.");
            Versuche=0;
            Zufallszahl=Math.ceil(Math.random()*100);
        }
    }
    /* ]]> */
</script>

<center>
    <br><br>

    <H1><span style='border-bottom:1px solid black;'>Momentan wird unsere Seite gewartet</span></H1>

    <H1>Bitte besuchen Sie uns zu einem späteren Zeitpunkt wieder</H1>
    <br><br>

    <H3>Erraten Sie meine Zahl zwischen 1 und 100:</H3><br>

    <form name='zuffiForm'>
        <input type='text' name='Ratefenster' id='Ratefenster' value='' size='5'/>
        <input type='button' name='Ratebutton' id='Ratebutton' value='Raten' onclick='zufall();'/>
    </form>
    <br><br>

    <H2>Wartungsmeldung: <?php echo $error_type; ?></H2>
    <br><br>
</center>
</HTML>
</BODY>