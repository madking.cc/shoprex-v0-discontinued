<?php
/**
 * shoprex - Online Shop
 * Copyright by Andreas Rex
 *
 * This software version is freeware.
 * Any modifikation and distribution is strictly prohibited.
 *
 * Distribution and new Versions can be found on www.shoprex.de
 */

// Definiere Zugriffsschutz für die anderen Seiten
define('SECURITY_CHECK', "OK");

$dir_root = dirname(__FILE__)."/";

// Initialisiere die Page
require($dir_root."include/init.php");

// Hole den Seiteninhalt
require($dir_root."include/get_content.php");

// Gebe die Page aus
require($dir_root."include/do_output.php");

// Ende
require($dir_root."include/terminate.php");
