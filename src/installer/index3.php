<?php
require("header.php");


$db_server = $_POST["db_server"];
$db_user = $_POST["db_user"];
$db_pass = $_POST["db_pass"];
$db_table = $_POST["db_table"];
$tbl_prefix = $_POST["tbl_prefix"];
$salt = $_POST["salt"];

$admin_name = $_POST["admin_name"];
$admin_password = $_POST["admin_password"];

$admin_password = md5($admin_password);
$admin_password = substr($admin_password, 0, 3) . $salt . substr($admin_password, 4);
$admin_password = md5($admin_password);

@$id = new mysqli($db_server, $db_user, $db_pass, $db_table);

if(mysqli_connect_error())
{
?>

<form action="index2.php" method="POST">
    <table border="0">
        <tr>
            <td><H1 class='error'>Fehler</H1></td>
            <td align="right"><input type="submit" value="Zurück" /></td>
        </tr>
        <tr>
            <td>Fehlermeldung:</td>
            <td><?php echo mysqli_connect_error(); ?></td>
        </tr>
    </table>
</form>
<?php
}
else
{
?>
    <H1>Fülle Datenbank</H1>
<?php

    $id->query("SET NAMES 'utf8'");
    $id->query("SET CHARACTER SET 'utf8'");

    require("sql.php");

    foreach($sql as $value)
    {
        $id->query($value);
    }
?>
    <H2>Fertig</H2>
    <H1>Schreibe Konfigurationsdatei</H1>
<?php

$content = array();
$content[] = "<?php defined('SECURITY_CHECK') or die;     \n";
$content[] = "define('DB_SERVER', '".$db_server."');      \n";
$content[] = "define('DB_USER', '".$db_user."');          \n";
$content[] = "define('DB_PASS', '".$db_pass."');          \n";
$content[] = "define('DB_TABLE', '".$db_table."');        \n";
$content[] = "define('TBL_PREFIX', '".$tbl_prefix."');    \n";
$content[] = "define('SALT', '".$salt."'); // Only change once, otherwise no old\n";
$content[] = "// login will be possible                   \n";
$content[] = "\$dir_root = dirname(__FILE__).'/';                    \n";

$fp=fopen("../_configuration.php","w");
foreach($content as $value)
    fwrite($fp,$value);
fclose($fp);
?>
        <H2>Fertig</H2>
        <br />
        <H2>Sie müssen das Verzeichnis "installer" nun löschen.</H2>

<?php
}
require("footer.php"); ?>