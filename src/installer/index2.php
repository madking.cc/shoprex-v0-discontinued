<?php
require("header.php"); ?>

<form action="index3.php" method="POST">
    <table border="0">
        <tr>
            <td><H1>Einstellungen</H1></td>
            <td align="right"><input type="submit" value="Weiter" /></td>
        </tr>
        <tr>
            <td>Datenbankserver:</td>
            <td><input type="text" size="40" name="db_server" value="localhost"></td>
        </tr>
        <tr>
            <td>Datenbankuser</td>
            <td><input type="text" size="40" name="db_user"></td>
        </tr>
        <tr>
            <td>Datenbankpasswort:</td>
            <td><input type="text" size="40" name="db_pass"></td>
        </tr>
        <tr>
            <td>Datenbanktabelle:</td>
            <td><input type="text" size="40" name="db_table"></td>
        </tr>
        <tr>
            <td>Tabellenprefix:</td>
            <td><input type="text" size="10" value="shoprex_" name="tbl_prefix">
                <br><span class="information">Vorhandene Tabellen mit diesem Prefix werden überschrieben!</span></td>
        </tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr>
            <td>Passwortschutz:</td>
            <td><input type="text" size="40" name="salt" value="<?php echo uniqid(); ?>">
                <br><span class="information">Damit werden die Passwörter zusätzlich verschlüsselt.</span></td>
        </tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr>
            <td>Administrator Name:</td>
            <td><input type="text" size="40" name="admin_name"></td>
        </tr>
        <tr>
            <td>Administrator Passwort:</td>
            <td><input type="password" size="40" name="admin_password"></td>
        </tr>
    </table>
</form>

<?php
require("footer.php"); ?>