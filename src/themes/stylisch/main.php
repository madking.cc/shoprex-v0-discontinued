<?php defined('SECURITY_CHECK') or die; ?>


<div class="wrapper">
    <?php if(isset($time_left)) {?> <div id='shop_reset' style='color:#FFF;'>Zeit bis zum Zurücksetzen des Shops: <?php echo $time_left; ?> Minute<?php if($time_left!=1) echo "n";?></div><?php } ?>
    <div id="header">
        <div id="logo_header"><?php echo $Cpage->img(THEME."/images/logo.png", "Logo"); ?></div>
        <div id="search_header"><span class="header_text">Suche</span><br />
            <div id="search_form"><?php echo $Cpage->form("search_form", "search.php");
            echo $Cpage->input_text("search", "Suchbegriffe eingeben", "170", WRITEABLE, "onclick='clear_search(this);'", NO_MAXLENGTH, "search_field");
            echo " ".$Cpage->input_submit("suchen", "id='search_button'", NO_CLASS); ?></form></div>
        </div>
        <div id="login_header"><span class="header_text">Mein Konto</span><br />
            <div id="login_status">
                <?php echo $Cpage->img(THEME."/images/login_symbol.png", "Logo"); ?>
                <?php if(empty($customer_login_name)) { ?>
                <div id="login_status_text">Sie sind nicht<br />
                angemeldet</div> <?php } else { ?>
                <div id="login_status_text"><?php echo $customer_login_name; ?></div> <?php } ?>
            </div>
            <div class="align_right"><?php
            if(empty($customer_login_name))
                echo $Cpage->link_id("anmelden", "account.php");
            else
                echo $Cpage->link_id("zum Account", "account.php");
                ?></div>
        </div>
        <div id="cart_header2"><span class="header_text">Warenkorb</span><br />
            <div id="cart_info">
                <?php echo $Cpage->img(THEME."/images/cart_symbol.png", "Logo"); ?>
                <div id="cart_quantity"><?php echo $Ccart->get_quantity(); ?></div>
                <div id="cart_money"><?php echo $Cpage->money($Ccart->get_price()); ?></div>
            </div>
            <div class="align_right"><?php echo $Cpage->link_id("zum Warenkorb", "cart.php"); ?></div>
        </div>
    </div>
    <div id="menu_top">
        <ul id="menu_top_left">
            <li><?php echo $Cpage->link_id($Cpage->img(THEME."/images/home.jpg", "Home"), "index.php", "page=start", NO_STYLE, "nav_shop"); ?></li>
            <li><?php echo $Cpage->link_db("contact"); ?></li>
            <li><?php echo $Cpage->link_db("download"); ?></li>
        </ul>
        <ul id="menu_top_right">
            <li><?php echo $Cpage->link_db("agb"); ?></li>
            <li>-</li>
            <li><?php echo $Cpage->link_db("privacy"); ?></li>
            <li>-</li>
            <li><?php echo $Cpage->link_db("impressum"); ?></li>
        </ul>
    </div>
    <div class="clear_float"></div>
    <?php if(constant("SHOW_SHOP_".strtoupper($page)) && !HIDE_SHOP)
    {
        ?>

        <div id="menu_left">
            <div id='categories'><H1>Kategorien:</H1>
                <?php echo $categories; ?>
            </div>
            <div class="clear_float"></div>
            <?php if(!empty($top_offer))
            {
                ?>
                <div id='top_offer'>
                    <H1><?php echo TOP_OFFER; ?>:</H1>
                    <div id='top_offer_box'>
                        <?php echo $Cpage->link($top_offer['name'], "index.php", "article=".$top_offer['id']); ?>
                        <br /><br />
                        <?php echo $Cpage->link($Cpage->img_id(UPLOAD_DIR."small/".$top_offer['picture'][0], $top_offer['picture_text'][0], "top_offer_picture"), "index.php", "article=".$top_offer['id']); ?>
                        <p id='top_offer_price'>
                            Nur <?php echo $Cpage->money($Cpage->tax_calc($top_offer['netto'], "brutto", $Atax[$top_offer['tax']])) ?></p>
                    </div>
                </div>
                <div class="clear_float"></div>
            <?php } ?>
            <div id='advertising'>
                <H1><?php echo ADVERTISING; ?>:</H1>
                <div id='advertising_content'>
                    <table id="eins_table" border="0" cellspacing="0" width="160" align="center">
                        <tr>
                            <td valign="top"><a href="http://www.profiseller.de" target="_blank"><span
                                        style="font-size:26px;">1&1</span><br/>Partner</a></td>
                            <td><a href="http://P7088836.profiseller.de" target="_blank"><span style="font-size:12px;">Günstig<br/>Telefonieren<br/>und Surfen</span></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    <?php } ?>

    <div id="content<?php if(!constant("SHOW_SHOP_".strtoupper($page)) || HIDE_SHOP)
    {
        echo "_full_wide";
    } ?>"><a name='output'></a>
        <?php echo $content; ?>
    </div>
    <div class="clear_float"></div>

</div>