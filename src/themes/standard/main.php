<?php defined('SECURITY_CHECK') or die; ?>

<div class="wrapper">
    <div id="logo_top">
        <div id="logo_for_print"><?php echo $Cpage->img(THEME."/images/logo.gif", "Logo"); ?></div>
        <div id="contact">E-Mail: <a
            href="mailto:<?php echo SHOP_MAIL; ?>"><?php echo SHOP_MAIL; ?></a><br/
                          ><?php $foo = constant('SHOP_PHONE'); if(!empty($foo)) { echo PHONE; ?> : <?php echo SHOP_PHONE; } ?>
            <br/><?php echo SHOP_STREET; ?>, <?php echo SHOP_ZIP; ?> <?php echo SHOP_CITY; ?>
        </div>
        <div id="werbetext_shadow"><?php echo SHOP_HEADER; ?></div>
        <div id="werbetext"><?php echo SHOP_HEADER; ?></div>
        <div id="search"><?php echo $Cpage->form("search_form", "search.php");
        echo $Cpage->input_text("search", YOUR_SEARCHTEXT, FORM_TEXT_SIZE, WRITEABLE, "onclick='clear_search(this);'");
        echo " ".$Cpage->input_submit(SEARCH_TEXT, "", "search_button"); ?></form></div>
    </div>
    <ul id="menu_top_left">
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
        <li><?php echo $Cpage->link_id(DO_SHOPPING, "index.php", "page=start", NO_STYLE, "nav_shop"); ?></li>
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
        <li><?php echo $Cpage->link_id("Meine Daten", "account.php", NO_PARAMETER, NO_STYLE, "nav_account"); ?></li>
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
        <li><?php echo $Cpage->link_db("contact"); ?></li>
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
        <li><?php echo $Cpage->link_db("download"); ?></li>
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
    </ul>
    <ul id="menu_top_right">
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
        <li><?php echo $Cpage->link_id("Warenkorb: ".$Cpage->money($Ccart->get_price()), "cart.php", NO_PARAMETER, NO_STYLE, "nav_cart"); ?></li>
        <li><img src="<?php echo THEME; ?>/images/menu_top_line.jpg" width="1" height="41"></li>
    </ul>
    <div class="clear_float"></div>

    <?php if(constant("SHOW_SHOP_".strtoupper($page)) && !HIDE_SHOP)
{
    ?>

    <div id="menu_left"><H2>Kategorien:</H2>
        <?php echo $categories; ?>

        <?php if(!empty($top_offer))
        {
            ?>
            <H2><?php echo TOP_OFFER; ?>:</H2>
            <div id='top_offer_box'>
                <?php echo $Cpage->link("<font id='top_offer_name'>".$top_offer['name']."</font>", "index.php", "article=".$top_offer['id']); ?>
                <?php echo $Cpage->link($Cpage->img_id(UPLOAD_DIR."small/".$top_offer['picture'][0], $top_offer['picture_text'][0], "top_offer_picture"), "index.php", "article=".$top_offer['id']); ?>
                <p id='top_offer_price'>
                    Nur <?php echo $Cpage->money($Cpage->tax_calc($top_offer['netto'], "brutto", $Atax[$top_offer['tax']])) ?></p>
            </div>
            <?php } ?>
        <H2><?php echo ADVERTISING; ?>:</H2>
        <table id="eins_table" border="0" cellspacing="0" width="200">
            <tr>
                <td valign="top"><a href="http://www.profiseller.de" target="_blank"><span
                    style="font-size:26px;">1&1</span><br/>Partner</a></td>
                <td><a href="http://P7088836.profiseller.de" target="_blank"><span style="font-size:12px;">Günstig<br/>Telefonieren<br/>und Surfen</span></a>
                </td>
            </tr>
        </table>
    </div>

    <?php } ?>

    <div id="content<?php if(!constant("SHOW_SHOP_".strtoupper($page)) || HIDE_SHOP)
    {
        echo "_full_wide";
    } ?>"><a name='output'></a>
        <?php echo $content; ?>
    </div>
    <div class="clear_float"></div>
</div>
<div id="footer">
    <div class="wrapper">
        <?php echo $Cpage->link_db("agb"); ?>
        <?php echo $Cpage->link_db("privacy"); ?>
        <?php echo $Cpage->link_db("impressum"); ?>
    </div>
</div>