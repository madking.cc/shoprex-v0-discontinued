<?php defined('SECURITY_CHECK') or die; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="<?php echo LANGUAGE; ?>" lang="<?php echo LANGUAGE; ?>">
<head>
    <meta http-equiv="content-type"
          content="text/html; charset=utf-8"/>

    <title><?php echo PAGE_TITLE.$subtitle; ?></title>

    <meta name="description"
          content="<?php echo DESCRIPTION; ?>"/>
    <!-- bis 150 Zeichen -->
    <meta name="keywords"
          content="<?php echo KEYWORDS; ?>"/>
    <meta name="author"
          content="<?php echo AUTHOR; ?>"/>
    <meta name="author-mail"
          content="mailto:<?php echo AUTHOR_MAIL; ?>"/>
    <meta name="language"
          content="<?php echo LANGUAGE; ?>"/>
    <meta name="robots"
          content="index, follow"/>
    <meta http-equiv="Content-Script-Type"
          content="text/javascript"/>
    <meta http-equiv="content-style-type"
          content="text/css"/>
    <link rel="stylesheet"
          href="<?php echo THEME; ?>css/screen.css" type="text/css" media="all"/>
    <link rel="stylesheet"
          href="<?php echo THEME; ?>css/print.css" type="text/css" media="print"/>
    <link rel="shortcut icon"
          href="<?php echo THEME; ?>favicon.ico" type="image/x-icon"/>
    <?php echo $script_header; ?>
    <?php echo $css; ?>
</head>
<body id='body_<?php echo $nav_control; ?>'>
<script>
    /* <![CDATA[ */
    <?php echo $script; ?>
    /* ]]> */
</script>
  